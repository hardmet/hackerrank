package example

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

case class Person(name: String, nickName: Option[String])


class HelloSpec extends AnyFlatSpec with Matchers {
  "The Hello object" should "say hello" in {
    val people = Seq(Person("Ned", Some("d")), Person("Alex", None))
    val suspects = Map("c" -> 1, "d" -> 2)

    val suspectsContainsExpr: String => Boolean = suspects.contains
    val suspectsContainsExprUnsugar: String => Boolean = n => suspects.contains(n)
    val resultExist = people.filter { p =>
      p.nickName.exists {
        suspectsContainsExpr
      }
    }
    val resultExistUnsugar = people.filter { p =>
      p.nickName.exists {
        suspectsContainsExprUnsugar
      }
    }

    println(resultExist)
    println(resultExistUnsugar)

    val result3 = people.filter {
      _.nickName match {
        case Some(n) => suspects.contains(n)
        case None => false
      }
    }
    val result4 = people.filter(_.nickName.exists(suspects.contains))

    println(result3)
    println(result4)
  }
}
