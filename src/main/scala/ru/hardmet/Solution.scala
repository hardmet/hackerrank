package ru.hardmet

import scala.io.StdIn
import scala.collection.Searching._
import scala.collection.mutable

case class Tree(id: Int, sons: Vector[Int])

object Solution extends App {

  val prefixA = Array("|--", "|  ")
  val prefixB = Array("|--", "   ")
  val branch = "|--"
  val spaces = "   "
  val down = "|  "

  // [7,1,4,1,9,1,1,9,1,7,9]
  //  val testsCount = StdIn.readLine().toInt
  //  (1 to testsCount).foreach {
  //    _ =>
  //      val m = StdIn.readLine().toInt


  def prepareDictionaries(cs: IndexedSeq[String]): (Map[Int, Vector[Int]], Map[Int, String]) = {
    cs.map { line =>
      val Pattern = "(\\d+)\\s([\\d-]+)\\s(.*)".r
      line match {
        case Pattern(id, parent, text) => parent.toInt -> (id.toInt -> text)
      }
    }.foldLeft(Map.empty[Int, Vector[Int]] -> Map.empty[Int, String]) {
      case ((mapAcc, texts), (parent, (id, text))) =>
        (mapAcc + (parent -> mapAcc.get(parent).fold(Vector(id))(sons => id +: sons)), texts + (id -> text))
    }
  }

  //      val (comments: Map[Int, Vector[Int]], texts: Map[Int, String]) = prepareDictionaries((1 to m).map { _ => StdIn.readLine() })
  val (comments: Map[Int, Vector[Int]], texts: Map[Int, String]) = prepareDictionaries(Vector(
    "1 -1 How are you?",
    "2 1 So-so",
    "3 2 What's wrong?",
    "5 3 Visit the doctor",
    "4 3 Maybe I got sick",
    "6 -1 What's going on?"
  ))

  def printCommentsTree(prefix: String = "", isFirst: Boolean = true)(sb: mutable.StringBuilder, root: Int = -1): mutable.StringBuilder = {
    //    sb.append(prefix)
    //    if (level > 0) sb.append("--")
    //    sb.append(texts(root))
    //      .append("\n")
    //      .append("|\n")
    //      .append(prefix)

    val pr = if (isFirst) s"${texts(root)}" else s"$branch${texts(root)}"
    val sp = if (isFirst) "" else spaces

    comments.get(root).fold(sb.append(pr).append("\n"))(
      branchComments =>
        if (branchComments.length == 1) {
          printCommentsTree(prefix = s"$prefix$sp", false)(sb.append(prefix).append(pr).append("\n"), branchComments.head)
        } else {
          branchComments.sorted
            .foldLeft(sb.append(prefix).append(pr).append(s"\n"))(
              (sb, root) =>
              printCommentsTree(prefix = s"$prefix$sp", false)(sb.append("\n").append(prefix).append(sp).append(down).append("\n"), root)
            )
        }
    )
  }

  println(comments(-1).sorted.foldLeft(new mutable.StringBuilder())(printCommentsTree()).toString())
  //  }
}

/*
How are you?
|
|--So-so
   |
   |--What's wrong?
      |
      |--Maybe I got sick
      |
      |--Visit the doctor
What's going on?

 */

/*
1
6
1 -1 How are you?
2 1 So-so
3 2 What's wrong?
4 3 Maybe I got sick
5 4 Visit the doctor
6 -1 What's going on?

 */


/*
1
7
1 1 1 4 4 4 4

1
7
2 2 2 2 4 4 4 4 4 6 6 6 6 7 6 6 6 6 6 1 1


 */

/*

5
11
7 1 4 1 9 1 1 9 1 7 9
5
1 2 3 4 5
5
5 5 5 5 5
9
10 20 10 10 30 10 20 10 40
1
1000000000

/*
10
1 9 2022
21 9 2022
29 2 2022
31 2 2022
29 2 2000
29 2 2100
31 11 1999
31 12 1999
29 2 2024
29 2 2023

 */
/*
делится на 4, но при этом не делится на 100; делится на 400.
*/
//def isLeapYear (y: Int): Boolean = (y % 4 == 0 && y % 100 != 0) || y % 400 == 0
//val Array (d, m, y) = StdIn.readLine ().split (" ").map (_.toInt)
//val isValidYear = Try (Month.of (m) ).flatMap (validMonth =>
//Either.cond (
//d >= 1 && validMonth.length (isLeapYear (y) ) >= d,
//"YES",
//new Exception ("wrong day")
//).toTry
//).getOrElse ("NO")
//println (isValidYear)

def findMaxRequests(rs: Array[Int]): Int =
  if (rs.length <= 1) {
    1
  } else {
    val sameElements = rs.zipWithIndex.find(r => r._1 != rs.head).fold(1)(_._2)
    var current = sameElements + 1
    var currentMax = sameElements + 1
    var a = (rs.head, sameElements)
    var b = (rs(sameElements), 1)

    (sameElements + 1 until rs.length).foreach {
      i =>
        if (rs(i) == b._1) {
          b = b._1 -> (b._2 + 1)
          current = current + 1
        } else if (rs(i) == a._1) {
          current = current + 1
          val t = b
          b = a._1 -> 1
          a = t
        } else {
          current = b._2 + 1
          a = b
          b = rs(i) -> 1
        }
        if (current > currentMax) currentMax = current
    }
    currentMax
  }
 */