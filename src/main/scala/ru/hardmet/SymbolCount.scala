package ru.hardmet


object SymbolCount extends App {

  import java.io.{BufferedReader, InputStreamReader}

  val reader = new BufferedReader(new InputStreamReader(System.in))
  val ps = System.out
  val yesBytes = "Yes\n".getBytes
  val noBytes = "No\n".getBytes

  val array = Array.fill(('A' to 'Z').length)(0)
  val count = reader.readLine().toInt
  (1 to count).foreach{
    _ =>
    var line: String = reader.readLine()
    val indices = line.map(x => x - 'A')
    indices.foreach(i => array(i) += 1)
    if (array.count(_ == 2) == 2)
      ps.write(yesBytes)
    else
      ps.write(noBytes)
    array.indices.foreach(i => array(i) = 0)
  }
  ps.close()
  reader.close()
}
