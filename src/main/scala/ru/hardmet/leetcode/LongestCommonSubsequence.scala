package ru.hardmet.leetcode

object LongestCommonSubsequence extends App {

  import math._

  def longestCommonSubsequence(text1: String, text2: String): Int = {
    val cache = Array.ofDim[Int](text2.length + 1, text1.length + 1)
    for (i <- 1 to text2.length) {
      for (j <- 1 to text1.length) {
        if (text2(i - 1) == text1(j - 1))
          cache(i)(j) = cache(i - 1)(j - 1) + 1
        else
          cache(i)(j) = max(cache(i - 1)(j), cache(i)(j - 1))
      }
    }
    cache(text2.length)(text1.length)
  }

  println(longestCommonSubsequence("mhunuzqrkzsnidwbun", "szulspmhwpazoxijwbq"))
  println(longestCommonSubsequence("abcde", "ace"))
  println(longestCommonSubsequence("abc", "abc"))
  println(longestCommonSubsequence("abc", "def"))
}