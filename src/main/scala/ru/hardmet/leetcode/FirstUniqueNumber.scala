package ru.hardmet.leetcode


object FirstUniqueNumber extends App {

  case class Node(key: Int, var left: Option[Node] = None, var right: Option[Node] = None)
  import scala.collection.mutable
  class FirstUnique(_nums: Array[Int]) {

    val map: mutable.Map[Int, Node] = mutable.Map[Int, Node]()

    var head: Option[Node] = None
    var tail: Option[Node] = None

    _nums.foreach(add)

    def showFirstUnique(): Int = {
      head.map {
        h =>
          h.key
        }.getOrElse(-1)
    }

    def add(key: Int): Unit = {
      map.get(key).map{
        node =>
          if (node.left.isDefined || node.right.isDefined || head.contains(node))
            removeNode(node)
      }.getOrElse{
        val node = Node(key)
        addToTail(node)
        map.addOne(key, node)
      }
    }


    def addToTail(node: Node): Unit = {
      node.right = None
      node.left = tail
      tail.foreach{
        _.right = Option(node)
      }
      tail = Option(node)
      head match {
        case None => head = Option(node)
        case _ =>
      }
    }

    def removeNode(node: Node): Unit = {
      val prevNode = node.left
      val nextNode = node.right
      prevNode match {
        case Some(value) => value.right = nextNode
        case None => head = nextNode
      }
      nextNode match {
        case Some(value) => value.left = prevNode
        case None => tail = prevNode
      }
      node.left = None
      node.right = None
    }
  }


//  val firstUnique = new FirstUnique(Array(2,3, 5))
//  println(firstUnique.showFirstUnique()) // return 2
//  firstUnique.add(5)            // the queue is now [2,3,5,5]
//  println(firstUnique.showFirstUnique()) // return 2
//  firstUnique.add(2)            // the queue is now [2,3,5,5,2]
//  println(firstUnique.showFirstUnique()) // return 3
//  firstUnique.add(3)            // the queue is now [2,3,5,5,2,3]
//  println(firstUnique.showFirstUnique()) // return -1

  val firstUnique = new FirstUnique(Array(7,7,7 , 17,7,7,7, 7))
  println(firstUnique.showFirstUnique()) // return -1
  firstUnique.add(7);            // the queue is now [7,7,7,7,7,7,7]
  firstUnique.add(7);            // the queue is now [7,7,7,7,7,7,7]
  firstUnique.add(7);            // the queue is now [7,7,7,7,7,7,7]
  firstUnique.add(7);            // the queue is now [7,7,7,7,7,7,7]
  firstUnique.add(7);            // the queue is now [7,7,7,7,7,7,7]
  firstUnique.add(7);            // the queue is now [7,7,7,7,7,7,7]
  firstUnique.add(7);            // the queue is now [7,7,7,7,7,7,7]
  firstUnique.add(7);            // the queue is now [7,7,7,7,7,7,7]
  firstUnique.add(7);            // the queue is now [7,7,7,7,7,7,7]
  firstUnique.add(7);            // the queue is now [7,7,7,7,7,7,7]
  firstUnique.add(3);            // the queue is now [7,7,7,7,7,7,7,3]
  firstUnique.add(3);            // the queue is now [7,7,7,7,7,7,7,3,3]
  firstUnique.add(7);            // the queue is now [7,7,7,7,7,7,7,3,3,7]
  firstUnique.add(17);           // the queue is now [7,7,7,7,7,7,7,3,3,7,17]
  println(firstUnique.showFirstUnique()) // return 17

}
