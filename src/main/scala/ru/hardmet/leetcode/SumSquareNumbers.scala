package ru.hardmet.leetcode

object SumSquareNumbers extends App {

  @scala.annotation.tailrec
  def find(target: Int, squares: Set[Int], threshold: Int)(x: Int): Boolean =
    x <= threshold && (squares(target - x * x) || find(target, squares, threshold)(x + 1))

  def judgeSquareSum(c: Int): Boolean = {
    (0 to math.sqrt(c).toInt).exists(x => math.sqrt(c - x * x) % 1 == 0)
//    val mid = math.sqrt(c).toInt
//    val squares = (0 to mid).map(x => x * x).toSet
//    find(c, squares, mid)(0)
  }

  println(judgeSquareSum(6) == false)
  println(judgeSquareSum(5) == true)
  println(judgeSquareSum(4) == true)
  println(judgeSquareSum(3) == false)
  println(judgeSquareSum(2) == true)
  println(judgeSquareSum(1) == true)
  println(judgeSquareSum(0) == true)
  println(1.00 % 1 == 0)
  println(1.01 % 1 == 0)
}
