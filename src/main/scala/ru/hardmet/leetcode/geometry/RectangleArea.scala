package ru.hardmet.leetcode.geometry

import ru.hardmet.leetcode.structures.SegmentTree


object RectangleArea extends App {

  import scala.collection.mutable
  def rectangleArea(rectangles: Array[Array[Int]]): Int = {
    val open = 1
    val close = -1
//    val events = Array.ofDim[Array[Int]](rectangles.length * 2)
    val xvals = mutable.Set.empty[Int]
    val events: List[Array[Int]] = rectangles
      .toList
      .filter(rec => (rec(0) < rec(2)) && (rec(1) < rec(3)))
      .flatMap {
        rec =>
          xvals.addOne(rec(0))
          xvals.addOne(rec(2))
          Seq(
            Array(rec(1), open, rec(0), rec(2)),
            Array(rec(3), close, rec(0), rec(2))
          )
      }
    val sortedValues = xvals.toArray.sorted
    val valuesMap = mutable.Map.empty[Int, Int]
    sortedValues.zipWithIndex.foreach(
      v =>
        valuesMap += v
    )

    val tree = new SegmentTree(0, sortedValues.length - 1, sortedValues)
    val sorteEvents = events.sortBy(a => a(0))
    (sorteEvents.takeWhile(_ != null).foldLeft((0L, 0L, sorteEvents.head(0))) {
      case ((res, curXSum, curYSum), Array(y1, y2, x1, x2)) =>
        (res + curXSum * (y1 - curYSum), tree.update(valuesMap(x1), valuesMap(x2), y2), y1)
    }._1 % 1_000_000_007).toInt
  }

  println(rectangleArea(Array(Array(0, 0, 2, 2), Array(1, 0, 2, 3), Array(1, 0, 3, 1))))
  println(rectangleArea(Array(Array(0, 0, 1000000000, 1000000000))))
}
