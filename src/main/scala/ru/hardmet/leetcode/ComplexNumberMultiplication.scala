package ru.hardmet.leetcode

object ComplexNumberMultiplication extends App {

  case class Complex(a: Int, b: Int) {

    def isSum: Boolean = b > 0

    def multiply(that: Complex): Complex = Complex(-(b * that.b) + a * that.a, b * that.a + that.b * a)

    override def toString: String = s"$a+${b}i"
  }

  val plusPlus = "(\\d+)\\+(\\d+)i".r
  val minusPlus = "-(\\d+)\\+(\\d+)i".r
  val plusMinus = "(\\d+)\\+-(\\d+)i".r
  val minusMinus = "-(\\d+)\\+-(\\d+)i".r

  def parse(s: String): Complex = s match {
    case plusPlus(a, b) => Complex(a.toInt, b.toInt)
    case minusPlus(a, b) => Complex(-a.toInt, b.toInt)
    case plusMinus(a, b) => Complex(a.toInt, -b.toInt)
    case minusMinus(a, b) => Complex(-a.toInt, -b.toInt)
  }

  def complexNumberMultiply(num1: String, num2: String): String =
    parse(num1).multiply(parse(num2)).toString

  println(complexNumberMultiply("78+-76i", "-86+72i"))
  println(complexNumberMultiply("1+-2i", "1+-2i"))
  println(complexNumberMultiply("1+2i",  "1+2i"))
  println(complexNumberMultiply("1+-2i",  "1+2i"))
  println(complexNumberMultiply("1+2i", "1+-2i"))
  println(complexNumberMultiply("1+1i", "1+1i"))
  println(complexNumberMultiply("1+-1i", "1+-1i"))
}
