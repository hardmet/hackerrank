package ru.hardmet.leetcode

import java.util

object RotateArray extends App {

  def rotate(nums: Array[Int], k: Int): Unit =
    nums match {
      case Array() => Array.empty[Int]
      case Array(a) => Array(a)
      case _ =>
        val modK = k % nums.length
        val end = util.Arrays.copyOf(nums, nums.length - modK)
        val newArr = util.Arrays.copyOfRange(nums, nums.length - modK, nums.length) ++ end
        nums.indices.foreach{
          i =>
            nums(i) = newArr(i)
        }
    }

  Seq(
    (Array(1,2,3,4,5,6,7), 3, Array(5,6,7,1,2,3,4)),
    (Array(-1,-100,3,99), 2, Array(3,99,-1,-100))
  ).foreach{
    case (array, k, exp) =>
      rotate(array, k)

      if (!(array sameElements exp))
        println(s"Wrong test. Expected: ${exp.mkString(", ")}. actual: ${array.mkString(", ")}")

      println(array.mkString(", "))
  }
}
