package ru.hardmet.leetcode


object MakingALargeIsland extends App {

  import scala.collection.mutable.ArrayBuffer
  val dxs = List(-1, 1, 0, 0)
  val dys = List(0, 0, -1, 1)

  def isValidPoint(mat: Array[Array[Int]])(x: Int, y: Int): Boolean =
    x < mat.length && y < mat.head.length && x >= 0 && y >= 0

  def dfs(grid: Array[Array[Int]], islands: ArrayBuffer[Int], x: Int, y: Int)(count: Int, current: Int, others: Int): (Int, Int, Int) = {
    if (isValidPoint(grid)(x, y) && grid(x)(y) <= 1) {
      var newOthers = others
      if (grid(x)(y) == 0) {
        var (up, down, left, temp) = (0, 0, 0, 0)
        if (x > 0 && grid(x - 1)(y) != current) {
          temp += islands(grid(x - 1)(y))
          up = grid(x - 1)(y)
        }
        if (x < grid.length - 1 && grid(x + 1)(y) != current &&
          grid(x + 1)(y) != up) {
          temp += islands(grid(x + 1)(y))
          down = grid(x + 1)(y)
        }
        if (y > 0 && grid(x)(y - 1) != current &&
          grid(x)(y - 1) != up &&
          grid(x)(y - 1) != down) {
          temp += islands(grid(x)(y - 1))
          left = grid(x)(y - 1)
        }
        if (y < grid.length - 1 && grid(x)(y + 1) != current && grid(x)(y + 1) != up &&
          grid(x)(y + 1) != down && grid(x)(y + 1) != left) {
          temp += islands(grid(x)(y + 1))
        }
        newOthers = math.max(others, temp)
        (count, current, newOthers)
      } else {
        grid(x)(y) = current
        (0 to 3).foldLeft((count + 1, current, newOthers)){
          (acc, i) =>
            (dfs(grid, islands, x + dxs(i), y + dys(i)) _).tupled(acc)
        }
      }
    } else
      (count, current, others)
  }

  def largestIsland(grid: Array[Array[Int]]): Int = {
    val n = grid.length
    val islands = ArrayBuffer[Int](0, 0)
    var curIsland = 2
    var maxNumber = 1
    grid.indices.foreach {
      i =>
        grid(i).indices.foreach {
          j =>
            if (grid(i)(j) == 1) {
              val (nc, nci, no) = dfs(grid, islands, i, j)(0, curIsland, 0)
              curIsland = nci + 1
              islands.addOne(nc)
              maxNumber = math.max(maxNumber, nc + no + 1)
            }
        }
    }
    math.min(n * n, maxNumber)
  }
}
