package ru.hardmet.leetcode

import scala.collection.mutable


object LRUCacheSolution extends App {

  val cache = new LRUCache( 2 /* capacity */ )
  println("here")
  println(cache.get(2))
  cache.put(2, 6)
  println(cache.get(1))
  cache.put(1, 5)
  cache.put(1, 2)
  println(cache.get(1))
  println(cache.get(2))

}

case class Node(key: Int, var value: Int, var left: Option[Node] = None, var right: Option[Node] = None)

class LRUCache(_capacity: Int) {
  val map: mutable.Map[Int, Node] = mutable.Map[Int, Node]()

  var head: Option[Node] = None
  var tail: Option[Node] = None

  def addToHead(node: Node): Unit = {
    node.right = head
    node.left = None
    head.foreach{
      _.left = Option(node)
    }
    head = Option(node)
    tail match {
      case None => tail = Option(node)
      case _ =>
    }
  }

  def removeNode(node: Node): Unit = {
    val prevNode = node.left
    val nextNode = node.right
    prevNode match {
      case Some(value) => value.right = nextNode
      case None => head = nextNode
    }
    nextNode match {
      case Some(value) => value.left = prevNode
      case None => tail = prevNode
    }
  }

  def moveToHead(node: Node): Unit = {
    removeNode(node)
    addToHead(node)
  }

  def get(key: Int): Int = {
    map.get(key).map {
      vNode =>
        moveToHead(vNode)
        vNode.value
    }.getOrElse(-1)
  }

  def put(key: Int, value: Int): Unit = {
    map.get(key).map{
      node =>
        node.value = value
        moveToHead(node)
    }.getOrElse{
      if (map.size == _capacity) {
        map.remove(tail.get.key)
        removeNode(tail.get)
      }
      val node = Node(key, value)
      map.addOne(key, node)
      addToHead(node)
      value
    }
  }

}
