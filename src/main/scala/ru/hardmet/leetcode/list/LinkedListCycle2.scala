package ru.hardmet.leetcode.list

object LinkedListCycle2 extends App {

  def detectCycle(h: ListNode): ListNode =
    (for {
      head <- Option(h)
      foundNode <- detect(head.next, head)
      result = countSteps(head, foundNode)(head, foundNode)
    } yield result).orNull

  def hasCycle(h: ListNode): Boolean = Option(h).exists(head => detect(head.next, head).isDefined)

  @scala.annotation.tailrec
  def detect(fast: ListNode, slow: ListNode): Option[ListNode] = {
    if (fast == slow) Some(slow)
    else if (fast != null && fast.next != null) {
      detect(fast.next.next, slow.next)
    } else None
  }

  @scala.annotation.tailrec
  def countSteps(firstHead: ListNode, firstCycleNode: ListNode)(head: ListNode, node: ListNode, count: Int = 0, minimum: Int = 0): ListNode = {
    if (head == node && count <= minimum) head
    else if (head != firstCycleNode) countSteps(firstHead, firstCycleNode)(head.next, node, count + 1, minimum)
    else countSteps(firstHead, firstCycleNode)(firstHead, node.next, 0, count)
  }

  val node2 = ListNode(2)
  val node0 = ListNode()
  val node1 = ListNode(1)
  val node5 = ListNode(5)
  val node6 = ListNode(6)
  val node4 = ListNode(4)
  node2.next = node0
  node0.next = node4
  node4.next = node2
  println(detectCycle(ListNode(3, node2)).x)
}
