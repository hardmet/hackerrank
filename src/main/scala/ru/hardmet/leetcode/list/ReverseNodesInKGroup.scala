package ru.hardmet.leetcode.list

object ReverseNodesInKGroup extends App {

  println(reverseList(ListNode(1, ListNode(2, ListNode(3, ListNode(4, null)))), ListNode(4, null)))

  println(reverseKGroup(ListNode(1, ListNode(2, ListNode(3, ListNode(4, ListNode(5, null))))), 3))
  println(reverseKGroup(ListNode(1, ListNode(2, null)), 1))
  println(reverseKGroup(ListNode(1, ListNode(2, null)), 2))

  import scala.annotation.tailrec

  def reverseList(head: ListNode, last: ListNode): (ListNode, ListNode) = {
    var next = head.next
    var prev = head
    var nn = next.next
    head.next = last.next
    while (next != last) {
      nn = next.next
      next.next = prev
      prev = next
      next = nn
    }
    last.next = prev
    last -> head
  }

  def reverseKGroup(head: ListNode, k: Int): ListNode = {
    if (k != 1 && head.next != null)
      reverseKGroupAcc(head, k, head, null)
    else head
  }

  @tailrec
  def reverseKGroupAcc(head: ListNode, k: Int, first: ListNode, last: ListNode): ListNode = {
    var lastNode = head
    var i = 1
    while (i != k && lastNode.next != null) {
      lastNode = lastNode.next
      i += 1
    }
    if (k == i) {
      val nextKHead = lastNode.next
      val (f, l) = reverseList(head, lastNode)
      val nextFirst = if (last == null) f else {
        last.next = f
        first
      }
      if (nextKHead == null) nextFirst else reverseKGroupAcc(nextKHead, k, nextFirst, l)
    } else {
      last.next = head
      first
    }
  }
}
