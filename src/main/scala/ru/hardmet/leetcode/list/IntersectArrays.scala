package ru.hardmet.leetcode.list

object IntersectArrays extends App {

  def counts(a: Array[Int]): Array[Int] = {
    val cs = Array.fill(1001)(0)
    a.foreach(ai => cs(ai) += 1)
    cs
  }

  def intersect(nums1: Array[Int], nums2: Array[Int]): Array[Int] = {
    val counts1 = counts(nums1)
    val counts2 = counts(nums2)
    (0 to 1000).foldLeft(List.empty[Int])(
      (acc, i) =>
        if (counts1(i) > 0)
          (1 to math.min(counts1(i), counts2(i))).foldLeft(acc)((ac, _) => i +: ac)
        else
          acc
    ).toArray
  }

  intersect(Array(8, 0, 3), Array(0))
  intersect(Array(4, 9, 5), Array(9, 4, 9, 8, 4))
  intersect(Array(1, 2, 2, 1), Array(2, 2))
  intersect(Array(1, 1, 2, 1), Array(1, 1, 1))
  intersect(Array(4, 9, 5), Array(4, 9, 5, 5))
}
