package ru.hardmet.leetcode.list

object AddTwoNumbers extends App {

  def addTwoNumbers(l1: ListNode, l2: ListNode, withAdd: Int = 0, root: Option[ListNode] = None): ListNode = {
    if (l1 == null && l2 == null)
      if (withAdd > 0) ListNode(withAdd, root.orNull)
      else root.orNull
    else {
      val (lx, lNext) = Option(l1).map(l => l.x -> l.next).getOrElse(0 -> null)
      val (rx, rNext) = Option(l2).map(l => l.x -> l.next).getOrElse(0 -> null)
      val sum = lx + rx + withAdd
      ListNode(sum % 10, addTwoNumbers(lNext, rNext, sum / 10, root))
    }
  }

  println(addTwoNumbers(ListNode(2, ListNode(4, ListNode(3, null))), ListNode(5, ListNode(6, ListNode(4, null)))))
  println(addTwoNumbers(null, null))
  println(addTwoNumbers(ListNode(2, null), ListNode(3, ListNode(4, null))))
  println(addTwoNumbers(ListNode(3, ListNode(4, null)), ListNode(2, null)))
}
