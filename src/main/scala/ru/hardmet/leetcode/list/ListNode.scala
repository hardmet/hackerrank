package ru.hardmet.leetcode.list

case class ListNode(var x: Int = 0, var next: ListNode = null)
