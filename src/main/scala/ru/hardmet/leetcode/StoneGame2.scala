package ru.hardmet.leetcode

object StoneGame2 extends App {

  def stoneGame(piles: Array[Int]): Boolean = {
    if (piles == null || piles.isEmpty)
      false
    else {
      val dp = Array.fill(piles.length)(Array.fill(piles.length)(0))
      val sums = Array.fill[Int](piles.length)(0)
      sums(piles.length - 1) = piles.last
      Range.inclusive(piles.length - 2, 0, -1).foreach(
        i => sums(i) = sums(i + 1) + piles(i)
      )
      dynamicTraverse(dp, sums, piles, 0, 1) != 0
    }
  }

  def dynamicTraverse(dp: Array[Array[Int]], sums: Array[Int], piles: Array[Int], i: Int, m: Int): Int =
    if (i == piles.length)
      0
    else if (2 * m >= piles.length - i)
      sums(i)
    else if (dp(i)(m) != 0)
      dp(i)(m)
    else {
      val minValue = (1 to 2 * m).foldLeft(Int.MaxValue)(
        (minV, j) => math.min(minV, dynamicTraverse(dp, sums, piles, i + j, math.max(m, j)))
      )
      dp(i)(m) = sums(i) - minValue
      dp(i)(m)
    }

  println(stoneGame(Array(5,4,4,5)))
}
