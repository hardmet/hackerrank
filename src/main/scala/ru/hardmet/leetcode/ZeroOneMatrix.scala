package ru.hardmet.leetcode


object ZeroOneMatrix extends App {

  import scala.collection.mutable

  def isValidPoint(mat: Array[Array[Int]])(x: Int, y: Int): List[(Int, Int)] =
    if (x < mat.length && y < mat.head.length && x >= 0 && y >= 0)
      List(x -> y)
    else
      Nil

  def updateMatrix(mat: Array[Array[Int]]): Array[Array[Int]] = {
    val result = Array.fill(mat.length)(Array.fill(mat.head.length)(Int.MaxValue))
    val queue = mutable.LinkedHashSet.empty[(Int, Int)]
    mat.indices.foldLeft(queue)((q, i) =>
      mat(i).indices.foldLeft(q)(
        (q, j) =>
          if (mat(i)(j) == 0) {
            result(i)(j) = 0
            q += (i -> j)
          } else q
      )
    )
    val dxs = List(1, 0, -1, 0)
    val dys = List(0, 1, 0, -1)
    while (queue.nonEmpty) {
      val (x, y) = queue.head
      queue -= (x -> y)
      for {
        i <- 0 to 3
        dx = dxs(i)
        dy = dys(i)
        (nx, ny) <- isValidPoint(mat)(dx + x, dy + y)
      } yield {
        if (result(nx)(ny) == Int.MaxValue && !queue(nx -> ny)) {
          queue += (nx -> ny)
          result(nx)(ny) = result(x)(y) + 1
        }
      }
    }
    result
  }
/*
 [0,0,0]
 [0,1,0]
 [1,1,1]
 */
  println(updateMatrix(Array(Array(0, 0, 0), Array(0, 1, 0), Array(1, 1, 1))).map(_.mkString(", ")).mkString("\n"))
  println("")
  println(updateMatrix(Array(Array(0, 0, 0), Array(0, 1, 0), Array(0, 0, 0))).map(_.mkString(", ")).mkString("\n"))
}
