package ru.hardmet.leetcode



object BagOfTokensScore extends App {

  def bagOfTokensScore(nonOrderedTokens: Array[Int], p: Int): Int = {
    val tokens = nonOrderedTokens.sorted
    import scala.annotation.tailrec
    @tailrec
    def iteration(i: Int, j: Int, maxScore: Int, score: Int, power: Int): Int = {
      if (i <= j) {
        if (power >= tokens(i)) {
            iteration(i + 1, j, math.max(maxScore, score + 1), score + 1, power - tokens(i))
        } else if (score > 0) {
          iteration(i, j - 1, maxScore, score - 1, power + tokens(j))
        } else maxScore
      } else maxScore
    }
    iteration(0, tokens.length - 1, 0, 0, p)
  }

  println(bagOfTokensScore(Array(100, 200, 300, 400), 200) == 2)
  println(bagOfTokensScore(Array(4, 37, 54, 64, 66, 70, 71, 75, 91, 91), 20) == 2)
  println(bagOfTokensScore(Array.empty[Int], 50) == 0)
  println(bagOfTokensScore(Array(100), 50) == 0)
  println(bagOfTokensScore(Array(100, 200), 150) == 1)
}
