package ru.hardmet.leetcode.tree

object DiameterOfBinaryTree {

  def dfs(root: TreeNode, curMax: Int): (Int, Int) = {
    if (root == null) curMax -> 0
    else {
      val (dL, deepL) = dfs(root.left, curMax)
      val (dR, deepR) = dfs(root.right, dL)
      math.max(deepL + deepR, dR) -> (math.max(deepL, deepR) + 1)
    }
  }

  def diameterOfBinaryTree(root: TreeNode): Int = {
    dfs(root, 0)._1
  }
}
