package ru.hardmet.leetcode.tree

object VerifyPreorderSerializationBinaryTree extends App {

  @scala.annotation.tailrec
  def btBuild(preorder: Array[String])(pointer: Int, counter: Int): Boolean = {
    if (pointer == preorder.length)
      counter == 0
    else if (counter > 0) {
      if (preorder(pointer) == "#")
        btBuild(preorder)(pointer + 1, counter - 1)
      else
        btBuild(preorder)(pointer + 1, counter + 1)
    } else false
  }

  def isValid(preorder: String): Boolean =
    preorder.split(",").foldLeft(Option(1)) {
      case (Some(count), next) =>
        if (count - 1 < 0)
          None
        else if (next != "#")
          Some(count + 1)
        else Some(count - 1)
      case (None, _) => None
    }.contains(0)

  def isValidSerialization(preorder: String): Boolean = {
    preorder != null && btBuild(preorder.split(","))(0, 1)
  }
  /*
1
6
9 3 4 1 2 6
   */

  println(isValidSerialization("1,#,#"))
  println(isValidSerialization("9,3,4,#,#,1,#,#,2,#,6,#,#"))
  println(isValidSerialization("9,#,92,#,#"))
  println(!isValidSerialization("#,1,#"))
  println(!isValidSerialization("1,#"))
  println(!isValidSerialization("1,#,1"))
  println(!isValidSerialization("1,#,#,1"))
  println(!isValidSerialization("1,#,1,1,#"))
}
