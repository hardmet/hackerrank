package ru.hardmet.leetcode.tree

object TwoSumBST extends App {

  import scala.collection.mutable

  def findTargetMem(root: TreeNode, k: Int, visited: mutable.Set[Int] = mutable.Set.empty): Boolean = {
    if (root == null)
      false
    else {
      visited(k - root.value) || findTargetMem(root.right, k, visited += root.value) || findTargetMem(root.left, k, visited)
    }
  }

  def findTarget(node: TreeNode, k: Int): Boolean = find(node, k)(node)

  def find(root: TreeNode, k: Int)(node: TreeNode): Boolean = {
    if (node == null)
      false
    else {
      search(root, k - node.value, node) || find(root, k)(node.left) || find(root, k)(node.right)
    }
  }

  @scala.annotation.tailrec
  def search(root: TreeNode, x: Int, node: TreeNode): Boolean = {
    if (root == null)
      false
    else if (root.value > x)
      search(root.left, x, node)
    else if (root.value < x)
      search(root.right, x, node)
    else root != node || search(root.right, x, node)
  }
}
