package ru.hardmet.leetcode.tree

object ValidSequenceInBinaryTree extends App {

  def dfs(root: TreeNode, pointer: Int, arr: Array[Int]): Boolean =
    pointer < arr.length && root.value == arr(pointer) &&
      (pointer == arr.length - 1 && root.left == null && root.right == null) ||
      dfs(root.left, pointer + 1, arr) || dfs(root.right, pointer + 1, arr)

  def isValidSequence(root: TreeNode, arr: Array[Int]): Boolean = {
    dfs(root, 0, arr)
  }


  val tree1 = new TreeNode(1)
  val tree2 = new TreeNode(2)
  val tree3 = null

  tree1.left = tree2
  tree1.right = tree3
  println(isValidSequence(tree1, Array(1)))

  val tree10 = new TreeNode(-10)
  val tree20 = new TreeNode(20)
  val tree15 = new TreeNode(15)
  val tree7 = new TreeNode(7)
  val tree9 = new TreeNode(9)

  //  tree20.left = tree15
  //  tree20.right = tree7
  //  tree10.right = tree20
  //  tree10.left = tree9
  //  println(isValidSequence(tree10))
  //
  //  val tree13 = new TreeNode(-13)
  //  println(isValidSequence(tree13))
  //
  //  val tree_2 = new TreeNode(2)
  //  val tree_1 = new TreeNode(-1)
  //  tree_2.left = tree_1
  //  println(isValidSequence(tree_2))
}
