package ru.hardmet.leetcode.tree

object BinaryTreePruning extends App {

  def pruneTree(root: TreeNode): TreeNode = {
    if (dfs(root))
      root
    else
      null
  }

  def dfs(root: TreeNode): Boolean = {
    var hasOnesOrEmptyL = Option(root.left).forall(dfs)
    var hasOnesOrEmptyR = Option(root.right).forall(dfs)
    if (!hasOnesOrEmptyL) {
      root.left = null
      hasOnesOrEmptyL = true
    }
    if (!hasOnesOrEmptyR) {
      root.right = null
      hasOnesOrEmptyR = true
    }
    if (root.left == null && root.right == null) root.value == 1
    else hasOnesOrEmptyL || hasOnesOrEmptyR || root.value == 1
  }

  val root = new TreeNode(1)
  val node11 = new TreeNode(0)
  val node12 = new TreeNode(0)
  val node13 = new TreeNode(1)
  node11.left = node12
  node11.right = node13
  root.right = node11
  println(pruneTree(root))

  val node1 = new TreeNode(1)
  val node2 = new TreeNode(0)
  val node3 = new TreeNode(1)
  val node4 = new TreeNode(0)
  val node5 = new TreeNode(0)
  val node6 = new TreeNode(0)
  val node7 = new TreeNode(1)
  node2.left = node4
  node2.right = node5
  node3.left = node6
  node3.right = node7
  node1.left = node2
  node1.right = node3

  println(pruneTree(node1))
}
