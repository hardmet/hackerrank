package ru.hardmet.leetcode.tree

object PathSumTwo extends App {

  import scala.annotation.tailrec

  def pathSum(root: TreeNode, targetSum: Int): List[List[Int]] = {
    if (root == null)
      List.empty
    else
      pathSumR(List((root, targetSum, List.empty)))
  }

  def isLeaf(root: TreeNode): Boolean =
    root.right == null && root.left == null

  @tailrec
  def pathSumR(calls: List[(TreeNode, Int, List[Int])], acc: List[List[Int]] = List.empty): List[List[Int]] =
    calls match {
      case ::((root, targetSum, current), next) =>
        val (newCalls, newAcc) = if (isLeaf(root))
          next -> (if (root.value == targetSum) (root.value :: current).reverse :: acc else acc)
        else {
          val addRightCalls = if (root.right != null) {
            (root.right, targetSum - root.value, root.value :: current) :: next
          } else next
          (if (root.left != null) {
            (root.left, targetSum - root.value, root.value :: current) :: addRightCalls
          } else addRightCalls) -> acc
        }
        pathSumR(newCalls, newAcc)
      case Nil => acc
    }

  val root = new TreeNode(-2)
  val l = new TreeNode(-3)
  root.left = l
  println(pathSum(root, -5))
}
