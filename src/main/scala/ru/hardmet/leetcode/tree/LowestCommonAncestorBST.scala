package ru.hardmet.leetcode.tree

import scala.annotation.tailrec

object LowestCommonAncestorBST extends App {

  @tailrec
  def lowestCommonAncestor(root: TreeNode, p: TreeNode, q: TreeNode): TreeNode =
    if (root.value < p.value && root.value < q.value)
      lowestCommonAncestor(root.right, p, q)
    else if (root.value > p.value && root.value > q.value)
      lowestCommonAncestor(root.left, p, q)
    else root

  val zero = new TreeNode(0)
  val two = new TreeNode(2)
  val six = new TreeNode(6)
  val eight = new TreeNode(8)
  val seven = new TreeNode(7)
  val four = new TreeNode(4)
  val three = new TreeNode(3)
  val five = new TreeNode(5)
  val nine = new TreeNode(9)

  six.left = two
  six.right = eight
  two.left = zero
  two.right = four
  eight.left = seven
  eight.right = nine
  four.left = three
  four.right = five

  println(lowestCommonAncestor(six, two, four).value)

  println(lowestCommonAncestor(six, two, eight).value)
}
