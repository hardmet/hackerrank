package ru.hardmet.leetcode.tree

object BinaryTreeMaximumPathSum extends App {

  def dfs(root: TreeNode, curMax: Int): (Int, Int) = {
    if (root == null) 0 -> curMax
    else {
      val (sumL, maxL) = dfs(root.left, curMax)
      val (sumR, maxR) = dfs(root.right, curMax)
      val m = math.max(math.max(sumL, sumR) + root.value, root.value)
      (math.max(sumL, sumR) + root.value) -> math.max(math.max(math.max(math.max(maxL, maxR), m), root.value), sumL + sumR + root.value)
    }
  }

  def maxPathSum(root: TreeNode): Int = {
    dfs(root, Int.MinValue)._2
  }

  val tree1 = new TreeNode(1)
  val tree2 = new TreeNode(2)
  val tree3 = new TreeNode(3)

  tree1.left = tree2
  tree1.right = tree3
  println(maxPathSum(tree1))

  val tree10 = new TreeNode(-10)
  val tree20 = new TreeNode(20)
  val tree15 = new TreeNode(15)
  val tree7 = new TreeNode(7)
  val tree9 = new TreeNode(9)

  tree20.left = tree15
  tree20.right = tree7
  tree10.right = tree20
  tree10.left = tree9
  println(maxPathSum(tree10))

  val tree13 = new TreeNode(-13)
  println(maxPathSum(tree13))

  val tree_2 = new TreeNode(2)
  val tree_1 = new TreeNode(-1)
  tree_2.left = tree_1
  println(maxPathSum(tree_2))
}
