package ru.hardmet.leetcode.tree

class TreeNode(_value: Int) {
  var value: Int = _value
  var left: TreeNode = _
  var right: TreeNode = _
}
