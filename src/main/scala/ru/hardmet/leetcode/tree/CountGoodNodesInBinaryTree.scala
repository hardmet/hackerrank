package ru.hardmet.leetcode.tree

object CountGoodNodesInBinaryTree extends App {

  def goodNodes(root: TreeNode): Int = {
//    if (root != null) {
//      val (newMax, newCounter) = if (root.value >= curMax) root.value -> (counter + 1) else curMax -> counter
//      goodNodes(root.right, newMax, goodNodes(root.left, newMax, newCounter))
//    } else counter
    goodNodesStackSafety(List(root -> root.value), 0)
  }

  @scala.annotation.tailrec
  def goodNodesStackSafety(nextNodes: List[(TreeNode, Int)], counter: Int): Int =
    nextNodes match {
      case ::((node, _), tail) if node == null => goodNodesStackSafety(tail, counter)
      case ::((root, curMax), tail) =>
        val (newMax, newCounter) = if (root.value >= curMax)
          root.value -> (counter + 1)
        else
          curMax -> counter
        goodNodesStackSafety((root.right, newMax) :: (root.left, newMax) :: tail, newCounter)
      case Nil => counter
    }

  val one = new TreeNode(3)
  val two = new TreeNode(1)
  val three = new TreeNode(3)
  val four = new TreeNode(3)
  val five = new TreeNode(7)
  val six = new TreeNode(5)

  one.left = two
  one.right = three
  two.left = four
  three.left = five
  three.right = six
  println(goodNodes(one))
}
