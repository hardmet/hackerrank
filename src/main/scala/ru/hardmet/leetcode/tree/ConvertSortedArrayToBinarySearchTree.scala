package ru.hardmet.leetcode.tree

object ConvertSortedArrayToBinarySearchTree extends App {

  def sortedArrayToBST(nums: Array[Int]): TreeNode = {
    if (nums.length == 1) {
      new TreeNode(nums.head)
    } else if (nums.length > 1) {
      val m = nums.length / 2
      val l = sortedArrayToBST(nums.take(m))
      val r = sortedArrayToBST(nums.drop(m + 1))
      val rt = new TreeNode(
        nums(m)
      )
      rt.left = l
      rt.right = r
      rt
    } else null
  }
}
