package ru.hardmet.leetcode.tree

object MaximumProductSplitBinaryTree extends App {

  def maxProduct(root: TreeNode): Int = {
    val total = postDFS(root) {
      tree =>
        tree.value = Option(tree.left).fold(0)(_.value) + Option(tree.right).fold(0)(_.value) + tree.value
        tree.value
    }
    var maxProduct: Long = 0
    val res = postDFS(root) {
      tree =>
        maxProduct = math.max(maxProduct, (total - tree.value) * tree.value)
        maxProduct
    }
    (res % (1e9 + 7).toLong).toInt
  }

  def postDFS(root: TreeNode)(callback: TreeNode => Long): Long = {
    Option(root) match {
      case Some(tree) =>
        postDFS(tree.left)(callback)
        postDFS(tree.right)(callback)
        callback(root)
      case None => 0
    }
  }

  @scala.annotation.tailrec
  def dfs(roots: List[(TreeNode, List[TreeNode])]): Unit = {
    roots match {
      case ::((root, parents), next) =>
        Option(root) match {
          case Some(existingRoot) =>
            parents.foreach(x => x.value += existingRoot.value)
            List(
              Option(existingRoot.left).map[(TreeNode, List[TreeNode])] {
                t =>
                  t -> (root :: parents)
              },
              Option(existingRoot.right).map[(TreeNode, List[TreeNode])] {
                t =>
                  t -> (root :: parents)
              }
            ).flatten match {
              case l :: r :: Nil => dfs(l :: r :: next)
              case ::(l, Nil) => dfs(l :: next)
              case _ => dfs(next)
            }
          case None => dfs(next)
        }
      case Nil =>
    }
  }

  @scala.annotation.tailrec
  def findMaxProduct(roots: List[TreeNode], total: Long, maxProduct: Long): Long =
    roots match {
      case ::(head, next) =>
        Option(head) match {
          case Some(value) =>
            val s = (total - value.value) * value.value
            findMaxProduct(head.left :: head.right :: next, total, if (s > maxProduct) s else maxProduct)
          case None =>
            findMaxProduct(next, total, maxProduct)
        }
      case Nil => maxProduct
    }
}
