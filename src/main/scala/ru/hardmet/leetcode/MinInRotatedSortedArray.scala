package ru.hardmet.leetcode

object MinInRotatedSortedArray extends App {

  //4,5,6,7,0,1,2
  //4,5,0,1,2
  //0,1,2,4,5,6,7
  @scala.annotation.tailrec
  def search(nums: Array[Int])(l: Int, r: Int): Int = {
    if (l < r) {
      val mid = (l + r) / 2
      val prev = nums((mid + nums.length - 1) % nums.length)
      val next = nums((mid + 1) % nums.length)
      if (nums(mid) > next)
        mid + 1
      else if (prev > nums(mid))
        mid
      else if (nums(mid) > nums(0))
        search(nums)(mid + 1, r)
      else search(nums)(l, mid - 1)
    } else l
  }

  def findMin(nums: Array[Int]): Int = {
    if (nums.last > nums.head) nums.head
    else nums(search(nums)(0, nums.length - 1))
  }

  println(findMin(Array(2, 3, 4, 5, 1)))
  println(findMin(Array(1, 2, 3)))
  println(findMin(Array(3, 1, 2)))
  println(findMin(Array(2, 3, 1)))
  println(findMin(Array(1, 2)))
  println(findMin(Array(2, 1)))
  println(findMin(Array(5, 1, 2, 3, 4)))
  println(findMin(Array(4, 5, 6, 7, 0, 1, 2)))
  println(findMin(Array(4, 5, 0, 1, 2)))
  println(findMin(Array(0, 1, 2, 4, 5, 6, 7)))
  println(findMin(Array(4, 5, 1, 2)))
  println(findMin(Array(4, 0, 1, 2)))
  println(findMin(Array(3, 4, 5, 1, 2)))
  println(findMin(Array(11, 13, 15, 17)))
  println(findMin(Array(1)))
}
