package ru.hardmet.leetcode


object SubsetsTwo extends App {

  import scala.collection.mutable

  def subsetsWithDup[A](nums: Array[A])(implicit ordering: Ordering[A]): List[List[A]] = {
    val powerSetSize = math.pow(2, nums.length).toInt
    (0 until powerSetSize).foldLeft(List(mutable.Map.empty[A, Int]))(
      (powerSet, counter) =>
        nums.indices.foldLeft(mutable.Map.empty[A, Int])(
          (map, j) =>
            if ((counter & (1 << j)) > 0)
              map.addOne(nums(j) -> (map.getOrElse(nums(j), 0) + 1))
            else
              map
        ) +: powerSet
    ).distinct.map(
      map => map.foldLeft(List.empty[A]) {
        case (ls, (key, count)) => (1 to count).foldLeft(ls)((ls, _) => key +: ls)
      })
  }

  println(subsetsWithDup(Array(1, 2, 2, 1)))
  println(subsetsWithDup(Array(1, 2, 2)))
  println(subsetsWithDup(Array(0)))
}
