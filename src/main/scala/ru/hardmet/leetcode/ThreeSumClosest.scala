package ru.hardmet.leetcode


object ThreeSumClosest extends App {

  import java.util

  def threeSumClosest(nums: Array[Int], target: Int): Int = {
    var res = nums(0) + nums(1) + nums(nums.length - 1)
    util.Arrays.sort(nums)
    (0 until nums.length - 2).foreach {
      i =>
        var a = i + 1
        var b = nums.length - 1
        while (a < b) {
          val current = nums(i) + nums(a) + nums(b)
          if (current > target)
            b = b - 1
          else
            a = a + 1

          if (math.abs(current - target) < math.abs(res - target))
            res = current
        }
    }
    res
  }

  println(threeSumClosest(Array(-1, 2, 1, -4), 2))
}
