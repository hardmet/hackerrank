package ru.hardmet.leetcode.narytree

case class Node(var _value: Int) {
  val value: Int = _value
  var children: List[Node] = List()
}

object NAryTreeLevelOrderTraversal extends App {

  def levelOrder(root: Node): List[List[Int]] = {
    if (root == null) Nil
    else
    lOrder(List(root), Nil, Nil, Nil)
  }

  import scala.annotation.tailrec

  @tailrec
  def lOrder(roots: List[Node], nextRoots: List[List[Node]], current: List[Int], acc: List[List[Int]]): List[List[Int]] = {
    roots match {
      case ::(root, next) => lOrder(next, root.children :: nextRoots, root.value :: current, acc)
      case Nil if nextRoots.isEmpty && current.isEmpty => acc.reverse
      case Nil if nextRoots.isEmpty => (current.reverse :: acc).reverse
      case Nil => lOrder(nextRoots.reverse.flatten, Nil, Nil, current.reverse :: acc)
    }
  }

  val one = Node(1)
  val two = Node(2)
  val three = Node(3)
  val four = Node(4)
  val five = Node(5)
  val six = Node(6)
  three.children = List(five, six)
  one.children = List(three, two, four)

  println(levelOrder(one))

  val node1 = Node(1)
  val node2 = Node(2)
  val node3 = Node(3)
  val node4 = Node(4)
  val node5 = Node(5)
  val node6 = Node(6)
  val node7 = Node(7)
  val node8 = Node(8)
  val node9 = Node(9)
  val node10 = Node(10)
  val node11 = Node(11)
  val node12 = Node(12)
  val node13 = Node(13)
  val node14 = Node(14)

  node11.children = List(node14)
  node7.children = List(node11)
  node8.children = List(node12)
  node9.children = List(node13)
  node3.children = List(node6, node7)
  node4.children = List(node8)
  node5.children = List(node9, node10)
  node1.children = List(node2, node3, node4, node5)
  println(levelOrder(node1))
}
