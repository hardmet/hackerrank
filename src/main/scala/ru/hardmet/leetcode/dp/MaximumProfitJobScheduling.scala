package ru.hardmet.leetcode.dp

object MaximumProfitJobScheduling extends App {

  @scala.annotation.tailrec
  private def getLastNonConflictingJob(jobs: IndexedSeq[(Int, Int, Int)], n: Int, from: Int, to: Int): Option[Int] = {
    if (from <= to) {
      val mid = (from + to) / 2
      if (jobs(mid)._2 <= jobs(n)._1)
        if (jobs(mid + 1)._2 <= jobs(n)._1)
          getLastNonConflictingJob(jobs, n, mid + 1, to)
        else
          Some(mid)
      else
        getLastNonConflictingJob(jobs, n, from, mid - 1)
    } else None
  }

  def jobScheduling(startTime: Array[Int], endTime: Array[Int], profit: Array[Int]): Int = {
    val sortedByEndJobs =
      startTime
        .indices
        .map(i => (startTime(i), endTime(i), profit(i)))
        .sortBy { case (_, end, _) => end }
    val maxProfit = Array.fill(sortedByEndJobs.length)(0)
    val (_, _, profitWithMinimalEnd) = sortedByEndJobs(0)
    maxProfit(0) = profitWithMinimalEnd
    (1 until sortedByEndJobs.length).foldLeft(maxProfit) {
      (maxProfit, current) =>
        val (_, _, currentJobProfit) = sortedByEndJobs(current)
        val lastAddableProfit = getLastNonConflictingJob(sortedByEndJobs, current, from = 0, to = current).fold(0)(maxProfit)
        maxProfit(current) = math.max(currentJobProfit + lastAddableProfit, maxProfit(current - 1))
        maxProfit
    }.last
  }

  println(jobScheduling(Array(1, 1, 1), Array(2, 3, 4), Array(5, 6, 4)))
  println(jobScheduling(Array(1, 2, 3, 3), Array(3, 4, 5, 6), Array(50, 10, 40, 70)))
  println(jobScheduling(Array(1, 2, 3, 4, 6), Array(3, 5, 10, 6, 9), Array(20, 20, 100, 70, 60)))
}
