package ru.hardmet.leetcode.strings

object GroupAnagrams extends App {

  def groupAnagrams(strings: Array[String]): List[List[String]] = {
    strings.map {
      str =>
        str.foldLeft(Array.ofDim[Int](('a' to 'z').length))(
          (arr, symbol) => arr.updated(symbol - 'a', arr(symbol - 'a') + 1)
        ).mkString(" ") -> str
    }
      .groupBy(_._1)
      .map(_._2.map(_._2).toList)
      .toList
  }

  println(groupAnagrams(Array("bdddddddddd", "bbbbbbbbbbc")))
  println(groupAnagrams(Array("eat", "tea", "tan", "ate", "nat", "bat")))
  println(groupAnagrams(Array("")))
  println(groupAnagrams(Array("a")))
}
