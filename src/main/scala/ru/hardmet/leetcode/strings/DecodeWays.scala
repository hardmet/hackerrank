package ru.hardmet.leetcode.strings

object DecodeWays extends App {

  def isDoubleDigit(prevS: Char, n: Char): Boolean = prevS != '0' && ((prevS == '1') || (prevS == '2' && n <= '6'))

  def numDecodings(s: String): Int = {
    if (s.isEmpty || s.head == '0') 0 else {
      s.tail.foldLeft((1, 1, s.head)) {
        case ((prevCount, count, prevS), n) if isDoubleDigit(prevS, n) => (count, (if (n == '0') 0 else count) + prevCount, n)
        case ((_, count, _), n) if n != '0' => (count, count, n)
        case _ => (0, 0, '0')
      }._2
    }
  }

  def slowDecoding(s: String): Int = {
    val codesSet = ('A' to 'Z').indices.map(_ + 1).map(_.toString).toSet

    @scala.annotation.tailrec
    def ways(calls: List[List[Char]], count: Int): Int = {
      calls match {
        case ::(call, restCalls) =>
          call match {
            case '0' :: _ => ways(restCalls, count)
            case first :: second :: tail =>
              //              println(tail.length)
              if (codesSet(s"$first$second"))
                ways(tail :: (second :: tail) :: restCalls, count)
              else
                ways((second :: tail) :: restCalls, count)
            case _ => ways(restCalls, count + 1)
          }
        case Nil => count
      }
    }

    ways(List(s.toList), 0)
  }

  println(numDecodings("10") == 1)
  println(numDecodings("100") == 0)
  println(numDecodings("101") == 1)
  println(numDecodings("1010") == 1)
  println(numDecodings("111") == 3)
  println(numDecodings("111111111111111111111111111111111111111111111"))
  println(numDecodings("12"))
  println(numDecodings("226"))
  println(numDecodings("12") == 2)
  println(numDecodings("226") == 3)
  println(numDecodings("0") == 0)
  println(numDecodings("06") == 0)
  /*
   1
  12
 212
        1212
    12         2
    12        121
  ``   2   21     1
       1    1     12
      ``   ``   ``  1
                   ``
   */
}
