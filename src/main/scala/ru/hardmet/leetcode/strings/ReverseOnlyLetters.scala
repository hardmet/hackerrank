package ru.hardmet.leetcode.strings

object ReverseOnlyLetters extends App {
  def reverseOnlyLetters(s: String): String = {
    val chars = s.toCharArray
    @scala.annotation.tailrec
    def reverse(chs: Array[Char], l: Int, r: Int): String =
      if (l < r) {
        val isLetterL = chs(l).isLetter
        val isLetterR = chs(r).isLetter
        if (isLetterL && isLetterR) {
          val tmp = chs(r)
          chs(r) = chs(l)
          chs(l) = tmp
          reverse(chs, l + 1, r - 1)
        } else if (!isLetterL && !isLetterR)
          reverse(chs, l + 1, r - 1)
        else if (isLetterL && !isLetterR)
          reverse(chs, l, r - 1)
        else
          reverse(chs, l + 1, r)
      } else chs.mkString("")

    reverse(chars, 0, s.length - 1)
  }

  println(reverseOnlyLetters("ab-cd"))
  println(reverseOnlyLetters("a-bC-dEf-ghIj"))
  println(reverseOnlyLetters("Test1ng-Leet=code-Q!"))
  println(reverseOnlyLetters(""))
  println(reverseOnlyLetters("!"))
  println(reverseOnlyLetters("s!q"))
  println(reverseOnlyLetters("w"))
}
