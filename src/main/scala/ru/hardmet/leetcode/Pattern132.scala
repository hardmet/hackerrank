package ru.hardmet.leetcode


object Pattern132 extends App {

  import scala.collection.mutable.ArrayBuffer
  def find132pattern(nums: Array[Int]): Boolean = {
    if (nums.length < 3) false
    else {
      var prevMax = Int.MinValue
      val arrayBuffer = ArrayBuffer.empty[Int]
      (nums.length - 1 to(0, -1)).exists{
        i =>
        if (nums(i) < prevMax) true
        else {
          while (arrayBuffer.nonEmpty && arrayBuffer.last < nums(i)) {
            prevMax = arrayBuffer.last
            arrayBuffer.remove(arrayBuffer.length - 1)
          }
          arrayBuffer.append(nums(i))
          false
        }
      }
    }
  }

  println(find132pattern(Array(-2, 1, 2, -2, 1, 2)))
  println(!find132pattern(Array(1, 0, 1, -4, -3)))
  println(find132pattern(Array(3, 5, 0, 3, 4)))
  println(!find132pattern(Array(1, 2, 3, 4)))
  println(find132pattern(Array(3, 1, 4, 2)))
  println(find132pattern(Array(-1, 3, 2, 0)))
}
