package ru.hardmet.leetcode



object MinimumWindowSubstring extends App {

  def minWindow(s: String, t: String): String = {
    if (s.length < t.length)
      ""
    else
      findSubstring(s, t)
  }

  @scala.annotation.tailrec
  def findNewWindow(s: String, strArray: Array[Int], patternArray: Array[Int], start: Int): Int =
    if (strArray(s(start)) > patternArray(s(start)) || patternArray(s(start)) == 0) {
      strArray(s(start)) -= 1
      findNewWindow(s, strArray, patternArray, start + 1)
    } else start

  def findSubstring(s: String, t: String): String = {
    val patternArray = Array.ofDim[Int](256)
    val strArray = Array.ofDim[Int](256)
    t.foreach(c => patternArray(c) += 1)
    val (_, startIndex, minLength, _) = s.zipWithIndex.foldLeft((0, -1, Int.MaxValue, 0)) {
      case ((prevStart, startIndex, minLength, prevCount), (symbol, i)) =>
        strArray(symbol) += 1
        val count = if (strArray(symbol) <= patternArray(symbol)) prevCount + 1 else prevCount

        if (count == t.length) {
          val start = findNewWindow(s, strArray, patternArray, prevStart)
          val lenWindow = i - start + 1
          if (lenWindow < minLength) {
            (start, start, lenWindow, count)
          } else (start, startIndex, minLength, count)
        } else (prevStart, startIndex, minLength, count)
    }
    if (startIndex == -1)
      ""
    else
      s.substring(startIndex, startIndex + minLength)
  }

  println(minWindow(s = "ADOBECODEBANC", t = "ABC"))
  println(minWindow(s = "a", t = "a"))
  println(minWindow(s = "a", t = "aa"))
}
