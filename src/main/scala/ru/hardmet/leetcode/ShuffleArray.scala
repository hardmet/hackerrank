package ru.hardmet.leetcode

object ShuffleArray extends App {

  val _nums: Array[Int] = Array(1, 2, 3, 4, 5)
  val source: Array[Int] = Array.copyOf(_nums, _nums.length)
  var lastPermutation: Array[Int] = _nums
  var permutationsIt: Iterator[Array[Int]] = _nums.permutations
  permutationsIt.next()

  /** Resets the array to its original configuration and return it. */
  def reset(): Array[Int] = {
    System.arraycopy(source, 0, _nums, 0, source.length)
    _nums
  }

  /** Returns a random shuffling of the array. */
  def shuffle(): Array[Int] = {
    if (!permutationsIt.hasNext) {
      permutationsIt = lastPermutation.permutations
      permutationsIt.next()
    }
    lastPermutation = permutationsIt.next()
    System.arraycopy(lastPermutation, 0, _nums, 0, _nums.length)
    _nums
  }
}
