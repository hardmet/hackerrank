package ru.hardmet.leetcode.maxtrix

object SetMatrixZeroes extends App {

  def matrixTraverse(m: Array[Array[Int]], function: Array[Array[Int]] => Int => Int => Unit): Unit =
    m.indices.foreach(i => m(i).indices.foreach(function(m)(i)))


  def setZeroes(matrix: Array[Array[Int]]): Unit = {
    val containsZeroX = matrix(0).contains(0)
    val containsZeroY = matrix.indices.map(i => matrix(i)(0)).contains(0)
    (1 until matrix(0).length).foreach(j =>
      (1 until matrix.length).foreach(i =>
        if (matrix(i)(j) == 0 && (matrix(i)(0) != 0 || matrix(0)(j) != 0)) {
          matrix(i)(0) = 0
          matrix(0)(j) = 0
        })
    )
    (1 until matrix(0).length).foreach(j => if (matrix(0)(j) == 0) (1 until matrix.length).foreach(i => matrix(i)(j) = 0))
    (1 until matrix.length).foreach(i => if (matrix(i)(0) == 0) (1 until matrix(0).length).foreach(j => matrix(i)(j) = 0))
    if (containsZeroX) {
      matrix(0).indices.foreach(matrix(0)(_) = 0)
    }
    if (containsZeroY) {
      matrix.indices.foreach(matrix(_)(0) = 0)
    }
  }

  //       0  0     0
  //    [ [1, 2, 3, 4],
  //0    [ 5, 0, 7, 8],
  //     [ 0,10,11,12],
  //0    [13,14,15, 0]]
  val c = Array(Array(1, 2, 3, 4), Array(5, 0, 7, 8), Array(0, 10, 11, 12), Array(13, 14, 15, 0))
  setZeroes(c)
  println(c.map(_.mkString("[", " ,", "]")).mkString("\n"))
  println()

  val a = Array(Array(1, 1, 2, 2), Array(3, 0, 5, 0), Array(1, 3, 1, 5))
  setZeroes(a)
  println(a.map(_.mkString("[", " ,", "]")).mkString("\n"))

  println()
  val b = Array(Array(0, 1, 2, 0), Array(3, 4, 5, 2), Array(1, 3, 1, 5))
  setZeroes(b)
  println(b.map(_.mkString("[", " ,", "]")).mkString("\n"))

  println()
  val d = Array(Array(1, 1, 2, 1), Array(0, 4, 5, 2), Array(1, 3, 1, 5))
  setZeroes(d)
  println(d.map(_.mkString("[", " ,", "]")).mkString("\n"))
}
