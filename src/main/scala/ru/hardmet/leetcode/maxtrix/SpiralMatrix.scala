package ru.hardmet.leetcode.maxtrix

object SpiralMatrix extends App {

  def range(start: Int, times: Int, r: Boolean): Seq[Int] =
    if (r)
      Seq.iterate(start, times)(_ - 1)
    else
      Seq.iterate(start, times)(_ + 1)

  def nextHead(h: Boolean, r: Boolean)(y: Int, x: Int): (Int, Int) = {
    if (h && r)
      (y - 1) -> x
    else if (h && !r)
      (y + 1) -> x
    else if (!h && r)
      y -> (x + 1)
    else
      y -> (x - 1)
  }

  def spiralOrder(matrix: Array[Array[Int]]): List[Int] = {
    val ys = LazyList.from(matrix.length - 1, -1).takeWhile(_ > 0)
    val xs = LazyList.from(matrix.head.length, -1).takeWhile(_ > 0)

    @scala.annotation.tailrec
    def spiral(y: Int, x: Int, xs: LazyList[Int], ys: LazyList[Int], h: Boolean, r: Boolean, acc: List[(Int, Int)]): List[(Int, Int)] = {
      val (newAcc, nxs, nys) = if (h)
        (range(x, xs.head, r).foldLeft(acc)((ac, i) => (y -> i) +: ac), xs.tail, ys)
      else
        (range(y, ys.head, r).foldLeft(acc)((ac, j) => (j -> x) +: ac), xs, ys.tail)
      if (newAcc.length < matrix.length * matrix.head.length) {
        val (ny, nx) = (nextHead(h, r) _).tupled(newAcc.head)
        spiral(ny, nx, nxs, nys, !h, (h && r) || (!h && !r), newAcc)
      } else
        newAcc
    }
    spiral(0, 0, xs, ys, h = true, r = false, List.empty).reverse.map{ case (x, y) => matrix(x)(y) }
  }

  println(spiralOrder(
    Array(
      Array(7),
      Array(9),
      Array(6)
    )
  ))

  println(spiralOrder(Array(Array(7, 9, 6))))

  println(spiralOrder(
    Array(
      Array(1,2,3,4),
      Array(5,6,7,8),
      Array(9,10,11,12)
    )
  ))

  println(spiralOrder(
    Array(
      Array(1,2,3),
      Array(4,5,6),
      Array(7,8,9)
    )
  ))
}
