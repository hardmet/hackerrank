package ru.hardmet.leetcode.maxtrix

object ValidSudoku extends App {

  def isValidRow(array: Array[Char]): Boolean = isValidRow(array.iterator)

  def isValidRow(row: IterableOnce[Char]): Boolean =
    row.iterator.foldLeft(Array.ofDim[Byte](9) -> true)(
      (countsAndResult, a) =>
        if (countsAndResult._2 && a != '.') {
          val i = a - '1'
          if (countsAndResult._1(i) > 0)
            countsAndResult._1 -> false
          else {
            countsAndResult._1(i) = 1
            countsAndResult
          }
        } else countsAndResult
    )._2

  def isValidColumn(matrix: Array[Array[Char]])(colNum: Int): Boolean =
    isValidRow(matrix.map(row => row(colNum)))

  def isValidSquare(board: Array[Array[Char]])(xs: Range.Inclusive, ys: Range.Inclusive): Boolean =
    isValidRow(
      for {
        x <- xs
        y <- ys
      } yield board(x)(y)
    )

  val squares: Seq[(Range.Inclusive, Range.Inclusive)] = {
    val ranges = Seq(0 to 2, 3 to 5, 6 to 8)
    for {
      row <- ranges
      col <- ranges
    } yield row -> col
  }

  def isValidSudoku(board: Array[Array[Char]]): Boolean = {
    squares.forall((isValidSquare(board) _).tupled) &&
      board.head.indices.forall(isValidColumn(board)) &&
      board.forall(isValidRow)
  }

  println(
    isValidSudoku(
      Array(
        Array('5', '3', '.', '.', '7', '.', '.', '.', '.'),
        Array('6', '.', '.', '1', '9', '5', '.', '.', '.'),
        Array('.', '9', '8', '.', '.', '.', '.', '6', '.'),
        Array('8', '.', '.', '.', '6', '.', '.', '.', '3'),
        Array('4', '.', '.', '8', '.', '3', '.', '.', '1'),
        Array('7', '.', '.', '.', '2', '.', '.', '.', '6'),
        Array('.', '6', '.', '.', '.', '.', '2', '8', '.'),
        Array('.', '.', '.', '4', '1', '9', '.', '.', '5'),
        Array('.', '.', '.', '.', '8', '.', '.', '7', '9')
      )
    )
  )
}
