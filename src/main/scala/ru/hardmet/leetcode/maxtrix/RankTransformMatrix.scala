package ru.hardmet.leetcode.maxtrix

import ru.hardmet.leetcode.structures.DisjointSetUnion

object RankTransformMatrix extends App {

  import scala.annotation.tailrec

  def group(pss: List[(Int, Int)]): Iterable[List[(Int, Int)]] = {
    val map = pss.flatMap(xy => List(xy._1, xy._2 + 500)).zipWithIndex.toMap
    val dsu = DisjointSetUnion(pss.length * 2)

    @tailrec
    def g(ps: List[(Int, Int)], acc: DisjointSetUnion[Int]): Iterable[List[(Int, Int)]] =
      ps match {
        case ::((i, j), next) =>
          acc.unionSets(map(i), map(j + 500))
          g(next, acc)
        case Nil =>
          pss.map {
            case (x, y) =>
              (x, y) -> acc.findSet(map(x))
          }.groupBy(_._2).map(_._2.map(_._1))
      }

    g(pss, dsu)
  }

  def workInGroup(list: List[(Int, Int)], colsMin: Array[Option[Int]], rowsMin: Array[Option[Int]], result: Array[Array[Int]]): Unit =
    group(list).foreach {
      gr =>
        val nextVal = gr.flatMap {
          case (x, y) => rowsMin(x).map(r => colsMin(y).fold(r)(c => math.max(c, r))).orElse(colsMin(y)).toList
        }.maxOption.fold(1)(_ + 1)
        gr.foldLeft(nextVal) {
          case (value, (x, y)) =>
            rowsMin(x) = Some(value)
            colsMin(y) = Some(value)
            result(x)(y) = value
            value
        }
    }

  def matrixRankTransform(matrix: Array[Array[Int]]): Array[Array[Int]] = {
    val result = Array.fill(matrix.length)(Array.fill(matrix.head.length)(0))
    val rowsMin = Array.fill(matrix.length)(Option.empty[Int])
    val colsMin = Array.fill(matrix.head.length)(Option.empty[Int])
    val cellsIs = for {
      i <- matrix.indices
      j <- matrix(i).indices
    } yield i -> j
    val sorted = cellsIs.sortBy { case (x, y) => matrix(x)(y) }
    val a = sorted.tail.foldLeft(List(sorted.head)) {
      case (list@ ::((x, y), _), (i, j)) if matrix(i)(j) == matrix(x)(y) => (i -> j) :: list
      case (list, (i, j)) =>
        workInGroup(list.reverse, colsMin, rowsMin, result)
        List(i -> j)
    }
    workInGroup(a.reverse, colsMin, rowsMin, result)
    result
  }

  //  println(group(List(4 -> 4, 8 -> 0, 8 -> 4)))
  //  println(group(List(4 -> 4, 8 -> 0, 8 -> 4, 2 -> 2, 2 -> 3)))

  println(
    matrixRankTransform(
      Array(
        Array(-24, -9, -14, -15, 44, 31, -46, 5, 20, -5, 34),
        Array(9, -40, -49, -50, 17, 40, 35, 30, -39, 36, -49),
        Array(-18, -43, -40, -5, -30, 9, -28, -41, -6, -47, 12),
        Array(11, 42, -23, 20, 35, 34, -39, -16, 27, 34, -15),
        Array(32, 27, -30, 29, -48, 15, -50, -47, -28, -21, 38),
        Array(45, 48, -1, -18, 9, -4, -13, 10, 9, 8, -41),
        Array(-42, -35, 20, -17, 10, 5, 36, 47, 6, 1, 8),
        Array(3, -50, -23, 16, 31, 2, -39, 36, -25, -30, 37),
        Array(-48, -41, 18, -31, -48, -1, -42, -3, -8, -29, -2),
        Array(17, 0, 31, -30, -43, -20, -37, -6, -43, 8, 19),
        Array(42, 25, 32, 27, -2, 45, 12, -9, 34, 17, 32)
      )
    ).map(_.mkString("|", ", ", "|")).mkString("\n")
  )
  println()
  println(
    matrixRankTransform(Array(Array(20)))
      .map(_.mkString("|", ", ", "|")).mkString("\n")
  )
  println("")
  println(
    matrixRankTransform(
      Array(Array(-37, -50, -3, 44), Array(-37, 46, 13, -32), Array(47, -42, -3, -40), Array(-17, -22, -39, 24))
    )
      .map(_.mkString("|", ", ", "|")).mkString("\n")
  )
  println("")
  println(matrixRankTransform(Array(Array(7, 7), Array(7, 7)))
    .map(_.mkString("|", ", ", "|")).mkString("\n"))
  println("")
  println(matrixRankTransform(Array(Array(20, -21, 14), Array(-19, 4, 19), Array(22, -47, 24), Array(-19, 4, 19)))
    .map(_.mkString("|", ", ", "|")).mkString("\n"))
  println("")

  /*
   -37, -50,  -3,  44
   -37,  46,  13, -32
    47, -42,  -3, -40
   -17, -22, -39,  24

[[-24, -9,-14,-15,44,31,-46, 5, 20,-5, 34],
 [  9,-40,-49,-50,17,40, 35,30,-39,36,-49],
 [-18,-43,-40,-5,-30,9,-28,-41,-6,-47,12],
 [11,42,-23,20,35,34,-39,-16,27,34,-15],
 [32,27,-30,29,-48,15,-50,-47,-28,-21,38],
 [45,48,-1,-18,9,-4,-13,10,9,8,-41],
 [-42,-35,20,-17,10,5,36,47,6,1,8],
 [3,-50,-23,16,31,2,-39,36,-25,-30,37],
 [-48,-41,18,-31,-48,-1,-42,-3,-8,-29,-2],
 [17,0,31,-30,-43,-20,-37,-6,-43,8,19],
 [42,25,32,27,-2,45,12,-9,34,17,32]]

   */
  println("")
  println(matrixRankTransform(Array(Array(7, 3, 6), Array(1, 4, 5), Array(9, 8, 2)))
    .map(_.mkString("|", ", ", "|")).mkString("\n"))
}
