package ru.hardmet.leetcode.maxtrix


object RangeAddition2 extends App {

  def maxCount(mm: Int, n: Int, ops: Array[Array[Int]]): Int = {
    val r = ops.map(a => a(0)).minOption.getOrElse(mm)
    val c = ops.map(a => a(1)).minOption.getOrElse(n)
    r * c
  }

  println(maxCount(3, 3, Array(Array(2, 2), Array(3, 3))))
  println(maxCount(3, 3,
    Array(
      Array(2, 2), Array(3, 3), Array(3, 3), Array(3, 3),
      Array(2, 2), Array(3, 3), Array(3, 3), Array(3, 3),
      Array(2, 2), Array(3, 3), Array(3, 3), Array(3, 3)
    )
  ))
  println(maxCount(3, 3, Array.empty[Array[Int]]))
}
