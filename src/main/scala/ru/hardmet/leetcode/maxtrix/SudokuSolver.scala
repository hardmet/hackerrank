package ru.hardmet.leetcode.maxtrix

import scala.annotation.tailrec

object SudokuSolver extends App {

  def isValidInsert(board: Array[Array[Char]], row: Int, col: Int, toInsert: Char, i: Int): Boolean =
    isValidRowsAndColumns(board, row, col, toInsert, i) && isValidSquare(board, row, col, toInsert, i)

  def isValidRowsAndColumns(board: Array[Array[Char]], row: Int, col: Int, toInsert: Char, i: Int): Boolean =
    board(i)(col) != toInsert && board(row)(i) != toInsert

  def isValidSquare(board: Array[Array[Char]], row: Int, col: Int, toInsert: Char, i: Int): Boolean =
    board(3 * (row / 3) + i / 3)(3 * (col / 3) + i % 3) != toInsert

  @tailrec
  def checkValid(board: Array[Array[Char]], row: Int, col: Int, toInsert: Char, res: Boolean = true)(i: Int): Boolean = {
    if (res && i < 9) {
      checkValid(board, row, col, toInsert, isValidInsert(board, row, col, toInsert, i))(i + 1)
    } else res
  }

  def solveSudoku(board: Array[Array[Char]]): Boolean = {
    board.indices.forall {
      i =>
        board(i).indices.forall {
          j =>
            if (board(i)(j) == '.') {
              ('1' to '9').exists {
                k =>
                  if (checkValid(board, i, j, k)(0)) {
                    board(i)(j) = k
                    if (solveSudoku(board)) true else {
                      board(i)(j) = '.'
                      false
                    }
                  } else false
              }
            } else true
        }
    }
  }

  val x = Array(
    Array('5', '3', '.', '.', '7', '.', '.', '.', '.'),
    Array('6', '.', '.', '1', '9', '5', '.', '.', '.'),
    Array('.', '9', '8', '.', '.', '.', '.', '6', '.'),
    Array('8', '.', '.', '.', '6', '.', '.', '.', '3'),
    Array('4', '.', '.', '8', '.', '3', '.', '.', '1'),
    Array('7', '.', '.', '.', '2', '.', '.', '.', '6'),
    Array('.', '6', '.', '.', '.', '.', '2', '8', '.'),
    Array('.', '.', '.', '4', '1', '9', '.', '.', '5'),
    Array('.', '.', '.', '.', '8', '.', '.', '7', '9')
  )
  println(solveSudoku(x))

}
