package ru.hardmet.leetcode


object StoneGame4 extends App {

  def winnerSquareGame(number: Int): Boolean = {
    val dp = Array.fill[Option[Boolean]](100001)(Option.empty[Boolean])
    def winner(n: Int): Boolean = {
      if (n == 0) false
      else if (dp(n).isDefined) dp(n).get
      else {
        val res = (1 to math.sqrt(n).toInt).foldLeft(false){
          case (true, _) => true
          case (_, i) => !winner(n - i * i)
        }
        dp(n) = Some(res)
        res
      }
    }
    winner(number)
  }


  println(!winnerSquareGame(15))
  println(!winnerSquareGame(17))
  println(!winnerSquareGame(7))
  println(winnerSquareGame(1))
  println(!winnerSquareGame(2))
  println(winnerSquareGame(4))
}
