package ru.hardmet.leetcode



object TwoSum extends App {

  import scala.annotation.tailrec
  @tailrec
  def find(nums: Array[(Int, Int)], target: Int, a: Int, b: Int): Option[(Int, Int)] = {
    if (a < b) {
      val current = nums(a)._1 + nums(b)._1
      if (current > target)
        find(nums, target, a, b - 1)
      else if (current < target)
        find(nums, target, a + 1, b)
      else
        Some(nums(a)._2 -> nums(b)._2)
    } else None
  }
  def twoSum(nums: Array[Int], target: Int): Array[Int] = {
    val sortedArr = nums.zipWithIndex.sortBy(_._1)

    (0 until nums.length - 2).foldLeft(Option.empty[(Int, Int)]) {
      case (None, i) => find(sortedArr, target, i, nums.length - 1)
      case (otherwise, _) => otherwise
    } match {
      case Some((x, y)) => Array(x, y)
      case None if nums.head + nums.last == target => Array(0, 1)
      case None => throw new Exception("bad")
    }
  }

  import scala.collection.mutable
  def twoSum2(nums: Array[Int], target: Int): Array[Int] = {
    val map = mutable.Map.empty[Int, Int]
    nums.indices.foldLeft(Array.empty[Int]){
      case (result, i) if result.isEmpty =>
        map.get(target - nums(i)).fold{
          map.put(nums(i), i)
          result
        }(Array(_, i))
      case (r, _) => r
    }
  }

  println(twoSum(Array(3, 2, 4), 6).mkString(", "))
  println(twoSum(Array(3, 3), 6).mkString(", "))
}
