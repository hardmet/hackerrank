package ru.hardmet.leetcode


object JumpGameSolution extends App {

  import scala.collection.mutable
  @scala.annotation.tailrec
  def existsIn(nums: Array[Int], jump: Int, p: Int, cache: mutable.Set[Int]): (Boolean, mutable.Set[Int]) =
    if (jump > 0) {
      val (res, newCache) = canJump(nums, jump + p, cache.addOne(p))
      if (res)
        res -> newCache
      else
        existsIn(nums, jump - 1, p, newCache)
    } else
      false -> cache.addOne(p)

  def canJump(nums: Array[Int], p: Int, cache: mutable.Set[Int]): (Boolean, mutable.Set[Int]) =
    if (!cache.contains(p))
      p match {
        case _ if p < nums.length - 1 =>
          existsIn(nums, nums(p), p, cache)
        case _ => true -> cache.addOne(p)
      } else
      false -> cache

  @scala.annotation.tailrec
  def canJump(nums: Array[Int], p: Int = 0, maxI: Int = 0): Boolean = {
    if (p < nums.length && p <= maxI  &&  maxI < nums.length - 1)
      canJump(nums, p + 1, math.max(p + nums(p), maxI))
    else maxI >= nums.length - 1
  }


  println(!canJump(Array(3, 2, 1, 0, 4)))
  println(canJump(Array(2, 3, 1, 1, 4)))
}
