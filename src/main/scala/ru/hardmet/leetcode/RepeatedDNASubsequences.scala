package ru.hardmet.leetcode

object RepeatedDNASubsequences extends App {

  def findRepeatedDnaSequences(s: String): List[String] = {
    val sList = s.toList
    val seed = sList.take(10).toVector
    sList.drop(10).foldLeft((Set(seed), seed) -> Set.empty[String]) {
      case (((seen, prev), result), nextChar) =>
        val subSeq = prev.tail :+ nextChar
        if (seen.contains(subSeq))
          (seen, subSeq) -> (result + subSeq.mkString(""))
        else
          (seen + subSeq, subSeq) -> result
    } match {
      case ((_, _), result) => result.toList
    }
  }

  println(findRepeatedDnaSequences("AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"))
  println(findRepeatedDnaSequences("AAAAAAAAAAAAA"))

}
