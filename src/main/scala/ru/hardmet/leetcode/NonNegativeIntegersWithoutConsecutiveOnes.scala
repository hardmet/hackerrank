package ru.hardmet.leetcode

object NonNegativeIntegersWithoutConsecutiveOnes extends App {

  def findIntegers(n: Int): Int = {
    if (n < 2)
      n + 1
    else {
      val sb = new StringBuilder(Integer.toBinaryString(n)).reverse
      val k = sb.length

      val f = Array.ofDim[Int](k)
      f(0) = 1
      f(1) = 2
      (2 until k).foreach(
        i =>
          f(i) = f(i - 1) + f(i - 2)
      )
      var rst = 0
      (k - 1).to(0, -1).foreach(
        i =>
          if (sb.charAt(i) == '1') {
            rst += f(i)
            if (i < k - 1 && sb.charAt(i + 1) == '1')
              return rst
          }
      )
      rst + 1
    }
  }

  println(findIntegers(5))
  println(findIntegers(1000000000))
}
