package ru.hardmet.leetcode

object PushDominoes extends App {

  def pushDominoes(dominoes: String): String = {
    val arrayD = dominoes.toArray
    var lastR = -1
    var lastL = -1
    arrayD.indices.foreach(
      i =>
        if (lastR == -1) {
          arrayD(i) match {
            case 'L' =>
              if (lastL == -1)
                lastL += 1
              (lastL until i).foreach(j => arrayD(j) = 'L')
            case 'R' =>
              lastR = i
            case _ =>
          }
        } else {
          arrayD(i) match {
            case 'L' =>
              if (lastL > lastR) (lastL until i).foreach(j => arrayD(j) = 'L')
              else
              if (lastR != i - 1 && lastL != i - 1) {
                var j = (i - lastR - 1) / 2
                val m = (i - lastR) / 2
                while (j > 0) {
                  arrayD(i - j) = 'L'
                  j -= 1
                }
                if (m != (i - lastR - 1) / 2) arrayD(lastR + m) = '.'
              }
              lastL = i
            case 'R' =>
              lastR = i
            case '.' =>
              if (lastL < lastR)
                arrayD(i) = 'R'
          }
        }
    )
    arrayD.mkString("")
  }

  println(pushDominoes("L.....RR.RL.....L.R."))
  println(pushDominoes("RR.L"))
  println(pushDominoes("LL.RR.LLRRLL.."))
  println(pushDominoes("LRRRLLLRRR.LLLLL."))
}
