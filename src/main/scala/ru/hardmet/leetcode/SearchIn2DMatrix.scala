package ru.hardmet.leetcode

object SearchIn2DMatrix extends App {

  import scala.collection.Searching._

  def searchMatrix(matrix: Array[Array[Int]], target: Int): Boolean = {
    implicit val ordInt: Ordering[Int] = Ordering.Int
    matrix match {
      case Array() | Array(Array()) => false
      case _ =>
        matrix.map(x => x.head).search[Int](target) match {
          case Found(_) => true
          case InsertionPoint(insertionPoint) if insertionPoint > 0 =>
            matrix(insertionPoint - 1).search[Int](target) match {
              case Found(_) => true
              case InsertionPoint(_) => false
            }
          case _ => false
        }
    }
  }

  println(searchMatrix(Array(Array(1, 3, 5, 7), Array(10, 11, 16, 20), Array(23, 30, 34, 50)), 2))
  println(searchMatrix(Array(Array(1, 3, 5, 7), Array(10, 11, 16, 20), Array(23, 30, 34, 50)), 3))
  println(searchMatrix(Array(Array(1, 3, 5, 7), Array(10, 11, 16, 20), Array(23, 30, 34, 50)), 13))
  println(searchMatrix(Array.empty[Array[Int]], 0))
  println(searchMatrix(Array(Array.empty[Int]), 0))
  println(searchMatrix(Array(Array(1, 3, 5, 7), Array(10, 11, 16, 20), Array(23, 30, 34, 50)), 54))

}
