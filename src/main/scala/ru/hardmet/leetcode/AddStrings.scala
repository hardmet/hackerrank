package ru.hardmet.leetcode


object AddStrings extends App {

import scala.annotation.tailrec
  @tailrec
  def addOne(a: List[Char], acc: List[Char]): List[Char] =
    a match {
      case ::(head, next) => addOne(next, head :: acc)
      case Nil => acc
    }

  @tailrec
  def add(a: List[Char], b: List[Char], addition: Int, acc: List[Char]): List[Char] = {
    a match {
      case ::(aHead, nextA) =>
        b match {
          case ::(bHead, nextB) =>
            val nextD = (aHead + bHead - '0') + addition
            if (nextD > '9')
              add(nextA, nextB, 1, (nextD - ':' + '0').toChar :: acc)
            else
              add(nextA, nextB, 0, nextD.toChar :: acc)
          case Nil if addition > 0 =>
            add(a, List('1'), 0, acc)
          case Nil => addOne(a, acc)
        }
      case Nil if addition > 0 => add(b, List('1'), 0, acc)
      case Nil => addOne(b, acc)
    }
  }

  def addStrings(num1: String, num2: String): String = {
    add(num1.toList.reverse, num2.toList.reverse, 0, List.empty).mkString("")
  }

  println(addStrings("456", "77"))
  println(addStrings("11", "123"))
}
