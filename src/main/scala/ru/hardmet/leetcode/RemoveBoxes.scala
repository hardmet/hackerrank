package ru.hardmet.leetcode

object RemoveBoxes extends App {

  def removeBoxes(boxes: Array[Int]): Int = {
    val dp = Array.fill(boxes.length)(Array.fill(boxes.length)(Array.fill(boxes.length)(0)))
    dfs(boxes, 0, boxes.length - 1, 0)(dp)
  }

  def dfs(boxes: Array[Int], i: Int, j: Int, k: Int)(dp: Array[Array[Array[Int]]]): Int = {
    if (i > j) 0 else if (dp(i)(j)(k) > 0) {
      dp(i)(j)(k)
    } else {
      dp(i)(j)(k) = dfs(boxes, i, j - 1, 0)(dp) + (k + 1) * (k + 1)
      (i until j).foreach(p =>
        if (boxes(p) == boxes(j)) {
          dp(i)(j)(k) = math.max(dp(i)(j)(k), dfs(boxes, i, p, k + 1)(dp) + dfs(boxes, p + 1, j - 1, 0)(dp))
        }
      )
      dp(i)(j)(k)
    }
  }
}
