package ru.hardmet.leetcode

object PartitionArrayIntoDisjointIntervals extends App {

  def partitionDisjoint(nums: Array[Int]): Int = {
    val rNums = nums.reverse
    val rightMin = rNums.tail.foldLeft(List(rNums.head, 1))(
      (mins, n) => if (mins.head > n) n :: mins else mins.head :: mins
    )
    val leftMax = nums.foldLeft(List(Int.MinValue))(
      (maxs, n) => if (maxs.head < n) n :: maxs else maxs.head :: maxs
    ).reverse
      .tail
    leftMax.indices.takeWhile(i => leftMax(i) > rightMin(i + 1)).length + 1
  }

  println(partitionDisjoint(Array(1, 1)))
  println(partitionDisjoint(Array(5, 0, 3, 8, 6)))
  println(partitionDisjoint(Array(1, 1, 1, 0, 6, 12)))
}
