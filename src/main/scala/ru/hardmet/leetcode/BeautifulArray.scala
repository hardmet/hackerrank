package ru.hardmet.leetcode

object BeautifulArray extends App {
  def beautifulArray(n: Int): Array[Int] = {
    var result = List(1)
    var l = 1
    while (l < n) {
      result = result.map(i => i * 2 - 1) ++ result.map(i => i * 2)
      l = result.length
    }
    result.filter(_ <= n).toArray
  }

  println(beautifulArray(10).mkString(", "))
}
