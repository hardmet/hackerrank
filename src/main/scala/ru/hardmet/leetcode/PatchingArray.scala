package ru.hardmet.leetcode

object PatchingArray {

  @scala.annotation.tailrec
  def findMinPatches(nums: Array[Int], n: Int)(miss: Long, count: Int, i: Int): Int =
    if (miss <= n) {
      if (i < nums.length && nums(i) <= miss) {
        findMinPatches(nums, n)(miss + nums(i), count, i + 1)
      } else
        findMinPatches(nums, n)(miss << 1, count + 1, i)
    } else count

  def minPatches(nums: Array[Int], n: Int): Int = findMinPatches(nums, n)(1, 0, 0)
}
