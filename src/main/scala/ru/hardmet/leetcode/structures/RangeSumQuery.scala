package ru.hardmet.leetcode.structures

class NumArray(_nums: Array[Int]) {
  val sums: Vector[Int] = _nums.zipWithIndex.foldLeft(Vector.fill(_nums.length + 1)(0)){
    case (sums, (n, i)) => sums.updated(i + 1, sums(i) + n)
  }

  def sumRange(left: Int, right: Int): Int = {
    sums(right + 1) - sums(left)
  }
}

object RangeSumQuery extends App {

  val numArray = new NumArray(Array(-2, 0, 3, -5, 2, -1))

  println(numArray.sumRange(0, 2))
  println(numArray.sumRange(2, 5))
  println(numArray.sumRange(0, 5))
  // 0 1 2 3 4 5
// 0 0 1 3 6 10 15
  //     3
  //     (2 + 3 + 4 + 5) = 14
  //     15 - 1 = 14
}
