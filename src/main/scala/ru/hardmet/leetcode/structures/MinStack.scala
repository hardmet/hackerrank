package ru.hardmet.leetcode.structures

case class MinStack(var value: Int = 0, var minimum: Int = Int.MaxValue, var prev: MinStack = null) {

  /** initialize your data structure here. */

  def push(value: Int): Unit = {
    prev = this.copy()
    this.value = value
    minimum = math.min(minimum, value)
  }

  def pop(): Unit = {
    value = prev.value
    minimum = prev.minimum
    prev = prev.prev
  }

  def top(): Int = value

  def getMin: Int = minimum

}


