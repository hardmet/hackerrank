package ru.hardmet.leetcode.structures

class DisjointSetUnion[T](keyMapping: Map[T, Int]) {

  val parent: Array[Int] = Array.iterate(0, keyMapping.size){_ + 1}
  val rank: Array[Int] = Array.fill(keyMapping.size)(0)
  val sizes: Array[Int] = Array.fill(keyMapping.size)(1)

  def makeSet(kx: T): DisjointSetUnion[T] = {
    val x = keyMapping(kx)
    parent(x) = x
    rank(x) = 0
    sizes(x) = 1
    this
  }

  private def addNodeToRoot(a: Int, b: Int): Unit = {
    sizes(a) = sizes(a) + sizes(b)
    parent(b) = a
    if (rank(a) == rank(b))
      rank(a) += 1
  }

  def unionSets(ka: T, kb: T): DisjointSetUnion[T] = {
    val a = keyMapping(ka)
    val b = keyMapping(kb)
    val rootA = findSet(a)
    val rootB = findSet(b)
    if (rootA != rootB) {
      if (rank(rootA) < rank(rootB))
        addNodeToRoot(rootB, rootA)
      else
        addNodeToRoot(rootA, rootB)
    }
    this
  }

  @scala.annotation.tailrec
  private final def findSet(v: Int, calls: List[Int] = List.empty[Int]): Int = {
    if (v == parent(v)) {
      calls.foreach{
        i =>
          if (parent(i) != v)
            parent(i) = v
      }
      v
    } else {
      findSet(parent(v), v :: calls)
    }
  }

  final def findSet(kv: T): Int = {
    findSet(keyMapping(kv))
  }
}

object DisjointSetUnion {
  def apply(x: Int): DisjointSetUnion[Int] = new DisjointSetUnion[Int]((0 to x).map(x => x -> x).toMap)

  def apply[K](mapping: Map[K, Int]): DisjointSetUnion[K] = new DisjointSetUnion[K](mapping)
}
