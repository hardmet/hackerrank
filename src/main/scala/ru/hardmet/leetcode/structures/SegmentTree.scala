package ru.hardmet.leetcode.structures

class SegmentTree(start: Int, end: Int, values: Array[Int],
                  var left: SegmentTree = null,
                  var right: SegmentTree = null) {
  var count: Long = 0L
  var total: Long = 0L

  def getRangeMid: Int = start + (end - start) / 2

  def getLeft: SegmentTree = Option(left).getOrElse {
    left = new SegmentTree(start, getRangeMid, values)
    left
  }

  def getRight: SegmentTree = Option(right).getOrElse {
    right = new SegmentTree(getRangeMid, end, values)
    right
  }

  def update(i: Int, j: Int, value: Int): Long = {
    if (i >= j)
      0
    else {
      if (start == i && end == j) {
        count += value
      } else {
        getLeft.update(i, math.min(getRangeMid, j), value)
        getRight.update(math.max(getRangeMid, i), j, value)
      }
      total = if (count > 0) values(end) - values(start) else getLeft.total + getRight.total
      total
    }
  }
}
