package ru.hardmet.leetcode


object FlipStringMonotoneIncreasing extends App {

  import scala.annotation.tailrec

  @tailrec
  def minFlipsMonoIncr(s: String, flipCount: Int = 0, onesCount: Int = 0): Int = {
    if (s.isEmpty)
      flipCount
    else {
      if (s.head == '0')
        if (onesCount == 0)
          minFlipsMonoIncr(s.tail, flipCount, onesCount)
        else
          minFlipsMonoIncr(s.tail, if (onesCount < flipCount + 1) onesCount else flipCount + 1, onesCount)
      else
        minFlipsMonoIncr(s.tail, if (onesCount + 1 < flipCount) onesCount + 1 else flipCount, onesCount + 1)
    }
  }

  println(minFlipsMonoIncr("00011000"))
  println(minFlipsMonoIncr("010110"))
}
