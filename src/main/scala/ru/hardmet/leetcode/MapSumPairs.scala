package ru.hardmet.leetcode

import scala.annotation.tailrec

case class MultiNode(var value: Int = 0) {
  lazy val nodes: Array[Option[MultiNode]] = Array.fill(('a' to 'z').length)(Option.empty[MultiNode])
}

class MapSum() {

  lazy val root: MultiNode = MultiNode()

  /** Initialize your data structure here. */
  @tailrec
  private def insertSeq(node: MultiNode, ss: List[Char], value: Int): Int = {
    ss match {
      case ::(head, next) => node.nodes(head - 'a') match {
        case Some(node) => insertSeq(node, next, value)
        case None =>
          val newNode = MultiNode()
          node.nodes(head - 'a') = Some(newNode)
          insertSeq(newNode, next, value)
      }
      case Nil =>
        node.value = value
        value
    }
  }

  def insert(key: String, value: Int): Unit = {
    insertSeq(root, key.toList, value)
  }

  @tailrec
  private def findNode(node: MultiNode, prefix: List[Char]): Option[MultiNode] = {
    prefix match {
      case ::(head, next) =>
        node.nodes(head - 'a') match {
          case Some(nextNode) => findNode(nextNode, next)
          case None => None
        }
      case Nil => Some(node)
    }
  }

  private def dfs(node: MultiNode): Int = {
    if (node.nodes.isEmpty)
      node.value
    else
      node.value + node.nodes.map(maybeNode => maybeNode.map(dfs).getOrElse(0)).sum
  }

  def sum(prefix: String): Int = {
    findNode(root, prefix.toList).fold(0)(dfs)
  }

}

object MapSumPairs extends App {

  val mapSum = new MapSum()
  mapSum.insert("apple", 3)
  println(mapSum.sum("ap"))           // return 3 (apple = 3)
  mapSum.insert("app", 2)
  println(mapSum.sum("ap"))
}
