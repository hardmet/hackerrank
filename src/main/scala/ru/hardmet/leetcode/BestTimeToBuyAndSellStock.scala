package ru.hardmet.leetcode

object BestTimeToBuyAndSellStock extends App {

  def maxProfit(k: Int, prices: Array[Int]): Int = {
    val dp = Array.fill(k * 2 + 1)(0)
    dp(0) = -prices(0)
    (1 until 2 * k).foreach{
      i =>
      if (i % 2 == 0) dp(i) = Int.MinValue
      else dp(i) = 0
    }
    prices.indices.foreach{
      i =>
        (0 to 2 * k).foreach{
          j =>
            if (j == 0)
              dp(j) = math.max(dp(j), -prices(i))
            else if (j % 2 == 0)
              dp(j) = math.max(dp(j), dp(j - 1) - prices(i))
            else
              dp(j) = math.max(dp(j), dp(j - 1) + prices(i))
        }
    }
    dp(2 * k - 1)
  }

  println(maxProfit(2, Array(3,2,6,5,0,3)))
  println(maxProfit(2, Array(2,4,1)))
}
