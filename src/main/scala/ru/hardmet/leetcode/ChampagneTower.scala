package ru.hardmet.leetcode

object ChampagneTower extends App {

  def champagneTower(poured: Int, query_row: Int, query_glass: Int): Double = {
    val tower = Array.fill(query_row + 1)(Array.fill(query_glass + 2)(0.0))
    tower(0)(0) = poured

    (0 until query_row).foreach{
      row =>
        val maxCol = math.min(row, query_glass)
          (0 to maxCol).foreach{
          col =>
            if (tower(row)(col) > 1) {
              val spill = tower(row)(col) - 1
              tower(row + 1)(col) += spill / 2
              tower(row + 1)(col + 1)  += spill / 2
            }
        }
    }
    if (tower(query_row)(query_glass) > 1) 1.0 else tower(query_row)(query_glass)
  }

  println(champagneTower(1, 1, 1))
  println(champagneTower(2, 1, 1))
  println(champagneTower(100000009, 33, 17))
}
