package ru.hardmet.leetcode

object MaxSquare extends App {

  def maximalSquare(matrix: Array[Array[Char]]): Int =
    matrix match {
      case Array() => 0
      case _ =>
        val dp = Array.fill(matrix.length + 1)(Array.fill(matrix.head.length + 1)(0))
        var m = 0
        for {
          i <- 1 to matrix.length
          j <- 1 to matrix.head.length
        } {
          if (matrix(i - 1)(j - 1) == '1')
            dp(i)(j) = math.min(math.min(dp(i)(j - 1), dp(i - 1)(j)), dp(i - 1)(j - 1)) + 1
          m = math.max(dp(i)(j), m)
        }
        m * m
    }

  println(
    maximalSquare(
      Array(
        Array('0', '1', '1', '0', '1'),
        Array('1', '1', '0', '1', '0'),
        Array('0', '1', '1', '1', '0'),
        Array('1', '1', '1', '1', '0'),
        Array('1', '1', '1', '1', '1'),
        Array('0', '0', '0', '0', '0')
      )
    )
  )

  println(
    maximalSquare(
      Array(
        Array('1', '0', '1', '0', '0'),
        Array('1', '0', '1', '1', '1'),
        Array('1', '1', '1', '1', '1'),
        Array('1', '0', '0', '1', '0')
      )
    )
  )

  println(
    maximalSquare(
      Array.empty[Array[Char]]
    )
  )
}
