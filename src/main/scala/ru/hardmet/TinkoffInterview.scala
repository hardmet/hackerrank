package ru.hardmet

import scala.concurrent.{ExecutionContext, Future}

object TinkoffInterview extends App {

  /**
   * Transformation Chain
   * Дан набор возможных трансформаций: type Transformation[T] = T => Option[T]
   * Написать функцию преобразования последовательности трансформаций в возможную трансформацию.
   * Новая трансформация это результат работы всей цепочки трансформаций, которые не вернули None.
   * Если все вернули None, то общий результат None.
   */

}

object TransformationChain extends App {

  //  type Transformation[T] = T => Option[T]
  //
  //  def transformationChain[T](chain: Seq[Transformation[T]]): Transformation[T] = {
  //    @tailrec
  //    def transformationRec(chain: Seq[Transformation[T]], prevWorkedInput: T, resultPrevTr: Option[T]): Option[T] = {
  //      if (chain.isEmpty) resultPrevTr
  //      else
  //        chain.head(resultPrevTr.getOrElse(prevWorkedInput)) match {
  //          case Some(successResult) => transformationRec(chain.tail, successResult, Some(successResult))
  //          case None => transformationRec(chain.tail, prevWorkedInput, resultPrevTr)
  //        }
  //    }
  //    (x: T) =>
  //    if (chain.isEmpty)
  //      None
  //    else
  //      transformationRec(chain.tail, x, chain.head(x))
  //  }


  //  val t1: Transformation[Int] = t => Some(t + t)
  //  val t2: Transformation[Int] = _ => None
  //  val t3: Transformation[Int] = t => if(t > 2)Some(t * t) else None
  //
  //  val tc = transformationChain(Seq(t1,t2,t3))
  //
  //  println(tc(2))
  //  println(transformationChain(Seq(t1,t2, t2,t3))(2))
  //  println(transformationChain(Seq(t2,t2, t1,t3))(2))
  //  println(transformationChain(Seq(t2,t2,t2))(2))
  //  println(transformationChain(Seq(t1,t2,t2))(2))

  type Transformation[T] = T => Future[T]

  /**
   * AsyncTransformation Chain
   * Дан набор возможных трансформаций: type Transformation[T] = T => Future[T]
   * Написать функцию преобразования последовательности трансформаций в возможную трансформацию.
   * Новая трансформация это результат работы всей цепочки трансформаций, которые не вернули Future.failed.
   * Если все вернули Future.failed, то общий результат Future.failed.
   */
  def transformationChain[T](chain: Seq[Transformation[T]])(implicit ec: ExecutionContext): Transformation[T] = {
    def transformationRec(chain: Seq[Transformation[T]], prevWorkedInput: T, resultPrevTr: Future[T]): Future[T] = {
      if (chain.isEmpty) resultPrevTr
      else
        resultPrevTr.recoverWith {
          case _: Throwable => Future(prevWorkedInput)
        }.flatMap {
          res =>
            transformationRec(chain.tail, res, chain.head(res))
        }
    }

    (x: T) =>
      if (chain.isEmpty)
        Future.failed(new IllegalArgumentException("transformationChain ob empty chain"))
      else
        transformationRec(chain.tail, x, chain.head(x))
  }

  //  val t1: Transformation[Int] = t => Future.successful(t + t)
  //  val t2: Transformation[Int] = _ => Future.failed(new NoSuchElementException)
  //  val t3: Transformation[Int] = t =>
  //    if (t > 2) Future.successful(t * t)
  //    else Future.failed(new NoSuchElementException)
  //
  //  implicit val ec: ExecutionContext = ExecutionContext.Implicits.global
  //  val tc = transformationChain(Seq(t1, t2, t2, t3))
  //  val tc2 = transformationChain(Seq(t2, t2, t2))
  //  val tc3 = transformationChain(Seq(t2, t3, t1))
  //
  //  println(Await.result(tc(2), 5.seconds))  // 16
  //  println(Try(Await.result(tc2(2), 5.seconds))) // throw NoSuchElementException
  //  println(Await.result(tc3(2), 5.seconds)) // 4

  /**
   * Реализовать расчет состояния поля игры "Жизнь" Конвея.
   * Игра "Жизнь" относится к клеточным автоматам.
   * Классический клеточный автомат - это бесконечное двумерное поле клеток.
   * На каждом шаге каждая клетка меняет свое состояние в зависимости от состояния соседей.
   * В игре "Жизнь" клетка может иметь два состояния - жива или мертва.
   * Правила игры "Жизнь" такие:
   * - живая клетка с менее чем двумя живыми соседями умирает от одиночества
   * - живая клетка с двумя или тремя живыми соседями продолжает жить
   * - живая клетка с более чем тремя живыми соседями умирает от перенаселения
   * - мертвая клетка с тремя живыми соседями становится живоё
   * Бонусом реализовать абстрактный клеточный автомат.
   */
  class Life(val field: Vector[Vector[Boolean]]) {

    def findNeighbors(x: Int, y: Int, maxX: Int, maxY: Int): Seq[(Int, Int)] =
      for {
        dX <- Seq(-1, 0, 1)
        dY <- Seq(-1, 0, 1)
        nx = dX + x
        ny = dY + y
        if nx < maxX && nx >= 0 && ny < maxY && ny >= 0 && (dX == 0 || dY == 0)
      } yield nx -> ny

    def findLiveNeighbors(x: Int, y: Int, maxX: Int, maxY: Int): Int = {
      findNeighbors(x, y, maxX, maxY).count {
        case (x, y) =>
          field(x)(y)
      }
    }

    def cellLifeCycle(x: Int, y: Int, maxX: Int, maxY: Int, field: Vector[Vector[Boolean]]): Boolean = {
      if (field(x)(y))
        Set(2, 3).contains(findLiveNeighbors(x, y, maxX, maxY))
      else
        findLiveNeighbors(x, y, maxX, maxY) == 3 // todo 3 or 3 and more
    }


    def nextDay: Life = {
      new Life(
        field.zipWithIndex.foldLeft(field) {
          case (newWorld, (oldRow, i)) =>
            newWorld.updated(
              i,
              oldRow.indices.foldLeft(oldRow) {
                case (newRow, j) =>
                  newRow.updated(
                    j,
                    cellLifeCycle(i, j, field.length, field(i).length, field)
                  )
              }
            )
        }
      )
    }

    def printField(): Unit = {
      println(
        field.map(_.map {
          case true => 1
          case false => 0
        }.mkString(", ")).mkString("\n")
      )
      println("=====================================")
    }
  }

  object Life {
    def fromMap(map: Map[(Int, Int), Boolean]): Life = {
      val maxX = map.keys.map(_._1).max + 1
      val maxY = map.keys.map(_._2).max + 1
      val field = Vector.fill(maxX)(Vector.fill(maxY)(false))
      new Life(
        map.foldLeft(field) {
          case (f, ((x, y), isAlive)) => f.updated(x, f(x).updated(y, isAlive))
        }
      )
    }
  }

  val init = Map(
    (0, 0) -> false,
    (0, 1) -> true,
    (0, 2) -> false,
    (0, 3) -> false,
    (1, 0) -> false,
    (1, 1) -> false,
    (1, 2) -> true,
    (1, 3) -> false,
    (2, 0) -> false,
    (2, 1) -> true,
    (2, 2) -> false,
    (2, 3) -> false,
    (3, 0) -> true,
    (3, 1) -> false,
    (3, 2) -> true,
    (3, 3) -> false
  )
  val life = Life.fromMap(init)
  life.printField()
  life
    .nextDay
    .printField()

  life
    .nextDay
    .nextDay
    .printField()

  life
    .nextDay
    .nextDay
    .nextDay
    .printField()

  //  println(findNeighbors(0, 1, 2, 2))

}
