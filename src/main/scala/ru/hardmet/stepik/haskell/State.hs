{-# LANGUAGE RebindableSyntax #-}
module State where

import Prelude
import Control.Monad (ap, liftM, replicateM, replicateM_)
import Reader hiding (Monad, (>>=), return, (=<<))
import Writer hiding (Monad, (>>=), return, (>>), (=<<))

--class Monad m where
--  return :: a -> m a
--  (>>=) :: m a ->  (a -> m b) -> m b
--  (=<<) :: (a -> m b) -> m a -> m b
--  (=<<) = flip (>>=)
--  (>>) :: m a -> m b -> m b
--  (>>) _ mb = mb

instance Functor (State s) where
  fmap = liftM

instance Applicative (State s) where
  pure  = return
  (<*>) = ap

newtype State s a = State { runState :: s -> (a, s) }

instance Monad (State s) where
  return a = State $ \st -> (a, st)
  (>>=) ma k = State $ \st -> let
                                (v1, st1) = runState ma st
                              in runState (k v1) st1
  (>>) ma mb = ma >>= \_ -> mb

execState :: State s a -> s -> s
execState m = snd . runState m

evalState :: State s a -> s -> a
evalState m = fst . runState m

get :: State s s
get = State $ \st -> (st, st)

put :: s -> State s ()
put st = State $ \_ -> ((), st)

tick :: State Int Int
tick = do
  n <- get
  put (n + 1)
  return n

tick' :: State Integer Integer
tick' = do
    n <- get
    put (n + 1)
    return $ n

modify :: (s -> s) -> State s ()
modify f = do
  s <- get
  put (f s)

readerToState :: Reader r a -> State r a
readerToState m = State $ \st -> (runReader m st, st)

writerToState :: Monoid w => Writer w a -> State w a
writerToState m = State $ \st -> let
                                   (value, context) = runWriter m
                                 in (value, mappend st context)

succ' :: Int -> Int
succ' n = execState tick n

plus :: Int -> Int -> Int
plus n = execState . sequence $ replicate n tick

plus' :: Int -> Int -> Int
plus' n x = execState (replicateM n tick) x

fib :: Int -> Integer
fib n = fst $ execStateN n fibStep (0, 1)

fibStep :: State (Integer, Integer) ()
fibStep = modify $ \(a,b) -> (b, a + b)

execStateN :: Int -> State s a -> s -> s
execStateN n m = execState $ replicateM_ n m

data Tree a = Leaf a | Fork (Tree a) a (Tree a) deriving Show

numberTree :: Tree () -> Tree Integer
numberTree = flip evalState 1 . kl where
  kl :: Tree () -> State Integer (Tree Integer)
  kl (Leaf ()) = tick' >>= return . Leaf
  kl (Fork left () right) = do
    l <- kl left
    t <- tick'
    r <- kl right
    return $ Fork l t r

--runState ((State $ \x -> (Leaf (), x + 1)) >>= fu) 0
