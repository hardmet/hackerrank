package ru.hardmet.stepik.haskell

object Part5 extends App {

  case class Log[T](msgs: List[String], value: T)

  def toLogger[A, B](f: A => B, msg: String): A => Log[B] = (a: A) => Log[B](List(msg), f(a))

  def execLoggers[A, B, C](f: A => Log[B], g: B => Log[C], x: A): Log[C] = {
    def withEffect[T, R](f: T => Log[R], effect: Log[T]): Log[R] =
      effect match {
        case Log(ms, a) => f apply a match {
          case Log(fms, fa) => Log(ms ++ fms, fa)
        }
      }

    withEffect(g, withEffect(f, Log[A](Nil, x)))
  }

  def tests(): Unit = {
    val f = toLogger((a: Int) => a + 1, "add 1")
    val g = toLogger((a: Int) => a * 2, "multiply 2")
    println(execLoggers(f, g, 0))
    println(execLoggers(f, g, 11))
    println(execLoggers(f, g, 2))
    println(execLoggers(g, f, 0))
  }

}
