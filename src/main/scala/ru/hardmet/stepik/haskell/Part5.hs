module Part5 where
import Prelude hiding (Functor, fmap, Monad, (>>=), return, (=<<))
import Control.Monad (ap, liftM)
import Data.Functor.Identity
import Data.Char

class Functor f where
  fmap :: (a -> b) -> f a -> f b

instance Functor [] where
  fmap = map

instance Functor Maybe where
  fmap _ Nothing  = Nothing
  fmap f (Just a) = Just $ f a

class Monad m where
  return :: a -> m a
  (>>=) :: m a ->  (a -> m b) -> m b
  (=<<) :: (a -> m b) -> m a -> m b

data Point3D a = Point3D a a a deriving Show

--class Functor f where
--  fmap :: (a -> b) -> f a -> f b

instance Functor Point3D where
    fmap f (Point3D x y z) = Point3D (f x) (f y) (f z)

data GeomPrimitive a = Point (Point3D a) | LineSegment (Point3D a) (Point3D a) deriving Show

instance Functor GeomPrimitive where
    fmap f (Point x) = Point $ fmap f x
    fmap f (LineSegment p1 p2) = LineSegment (fmap f p1) (fmap f p2)

data Tree a = Leaf (Maybe a) | Branch (Tree a) (Maybe a) (Tree a) deriving Show

instance Functor Tree where
    fmap f (Leaf v) = Leaf $ fmap f v
    fmap f (Branch l v r) = Branch (fmap f l) (fmap f v) (fmap f r)


data Log a = Log [String] a deriving Show

toLogger :: (a -> b) -> String -> (a -> Log b)
toLogger f m = \a -> Log [m] $ f a

execLoggers :: a -> (a -> Log b) -> (b -> Log c) -> Log c
execLoggers x f g = let 
  Log ma fx = f x
  Log mb gx = g fx
  in Log (ma ++ mb) gx

bindLog :: Log a -> (a -> Log b) -> Log b
bindLog (Log m x) f = let
  Log mf fx = f x
  in Log (m ++ mf) fx

returnLog :: a -> Log a
returnLog x = Log ["some msg"] x

instance Functor Log where
  fmap f (Log m v) = Log m $ f v


instance Monad Log where
    return = returnLog
--   (>>=):: m a -> (a -> m b) -> m b
    (>>=) = bindLog
--    (>>) :: m a -> m b -> m b
--    (>>) :: m a -> m b -> m b
--    fail :: String -> m a
--    fail s = error s
--    (=<<) :: Monad m => (a -> m b) -> m a -> m b
    (=<<) = flip (>>=)
--    (<=<) :: Monad m => (b -> m c) -> (a -> m b) -> (a -> m c)
--    f <=< g = \x -> g x >>= f

execLoggersList :: a -> [a -> Log a] -> Log a
execLoggersList x binds = foldl (\ini b -> ini >>= b) (returnLog x) binds

(&) :: a -> (a -> b) -> b
(&) = flip ($)

data SomeType a = Monad a deriving Show
infixl 1 >>=
instance Monad SomeType where
  return a = (Monad a)
  (>>=)  = let liftF (Monad x) fx = fx x in liftF
  (=<<) = flip (>>=)

k :: (a -> b) -> (a -> Log b)
k g = \x -> return (g x)

instance Functor SomeType where
  fmap g fa = fa >>= return . g

instance Monad Identity where
  return x = Identity x
  Identity x >>= k = k x
  (=<<) = flip (>>=)

{-
first Monad law
return a >>= k === k a
second Monad law
m >>= return === m
third Monad law
(m >>= k) >>= k' ===  m >>= (k >>= k')
-}

instance Monad Maybe where
    return = Just
    (>>=) Nothing _ = Nothing
    (>>=) (Just x) k = k x
    (=<<) = flip (>>=)

data Token = Number Int | Plus | Minus | LeftBrace | RightBrace
    deriving (Eq, Show)

asToken :: String -> Maybe Token
asToken "+" = Just(Plus)
asToken "-" = Just(Minus)
asToken "(" = Just(LeftBrace)
asToken ")" = Just(RightBrace)
asToken str | all isDigit str = Just $ Number $ read str
asToken _ = Nothing

tokenize :: String -> Maybe [Token]
tokenize x = foldr (\word maybeTokens -> do
  token <- asToken word
  tokens <- maybeTokens
  return $ token : tokens) (return []) $ words x

instance Monad [] where
  return = flip (:) []
  (>>=) [] k = []
  (>>=) l k = concat $ map k l
  (=<<) = flip (>>=)

data Board = Some
nextPositions :: Board -> [Board]
nextPositions b = undefined

nextPositionsN :: Board -> Int -> (Board -> Bool) -> [Board]
nextPositionsN b n pred | n < 0 = []
nextPositionsN b 0 pred = if pred b then [b] else []
nextPositionsN b n pred = return b >>= nextPositions >>= (\p -> nextPositionsN p (n - 1) pred)

pythagoreanTriple :: Int -> [(Int, Int, Int)]
pythagoreanTriple x | x < 0 = []
pythagoreanTriple x = do 
  c <- [1..x]
  b <- [1..c-1]
  a <- [1..b-1]
  if a^2 + b^2 == c^2 then [(a, b, c)] else [] 
  