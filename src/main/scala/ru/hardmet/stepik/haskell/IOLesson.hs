module IOLesson where
import Prelude
import System.Directory (getDirectoryContents, removeFile)
import Data.List as L

main' :: IO ()
main' = do
  putStr "Substring: "
  predicate <- getLine
  if predicate == [] then putStrLn "Canceled"
  else
    do
    files <- getDirectoryContents "."
    filtered <- return $ filter (L.isInfixOf predicate) files
    mapM_ (\file -> removeFile file >> putStrLn ("Removing file: " ++ file)) filtered