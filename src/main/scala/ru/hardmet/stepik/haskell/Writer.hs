{-# LANGUAGE RebindableSyntax #-}
module Writer where

import Prelude hiding (Monad, (>>=), return, (=<<), (>>))

class Monad m where
  return :: a -> m a
  (>>=) :: m a ->  (a -> m b) -> m b
  (=<<) :: (a -> m b) -> m a -> m b
  (=<<) = flip (>>=)
  (>>) :: m a -> m b -> m b

data Writer w a = Writer { runWriter :: (a, w) } deriving Show

writer :: (a, w) -> Writer w a
writer = Writer

execWriter :: Writer w a -> w
execWriter = snd . runWriter

evalWriter :: Writer w a -> a
evalWriter = fst . runWriter

tell :: Monoid w => w -> Writer w ()
tell w = writer ((), w)

instance (Monoid w) => Monad (Writer w) where
  return x = Writer $ (x, mempty)
  m >>= k  = let
             (b, w1) = runWriter m
             Writer (a, w2) = k b
             in Writer (a, (w1 `mappend` w2))
  ma >> mb = let
                (_, w1) = runWriter ma
                (vb, w2) = runWriter mb
               in Writer(vb, (w1 `mappend` w2))

instance (Semigroup a, Monoid a) => Monoid (Sum a) where
 mempty = Sum (mempty)
 mappend (Sum v1) (Sum v2) = Sum (v1 <> v2)

instance Semigroup a => Semigroup (Sum a) where
  (Sum v1) <> (Sum v2) = Sum (v1 <> v2)

instance Semigroup Integer where
  a1 <> a2 = a1 + a2
  
instance Monoid Integer where
  mempty = 0
  mappend v1 v2 = v1 <> v2


data Sum a = Sum { getSum :: a } deriving Show
type Shopping = Writer (Sum Integer, [String]) ()

purchase :: String -> Integer -> Shopping
purchase msg = tell . flip (,) [msg] . Sum

total :: Shopping -> Integer
total = getSum . fst . execWriter

items :: Shopping -> [String]
items = snd . execWriter

shopping1 :: Shopping
shopping1 = do
  purchase "Jeans"   19200
  purchase "Water"     180
  purchase "Lettuce"   328