module Reader where

import Prelude hiding (Monad, (>>=), return, (=<<))

class Monad m where
  return :: a -> m a
  (>>=) :: m a ->  (a -> m b) -> m b
  (=<<) :: (a -> m b) -> m a -> m b
  (=<<) = flip (>>=)

--runReader :: Reader r a -> r -> a
data Reader r a = Reader { runReader :: (r -> a) }

instance Monad (Reader r) where
  return x = Reader $ \_ -> x
--  (>>=) :: Reader r -> (r -> Reader t) -> Reader t
  m >>= k  = Reader $ \r -> runReader (k (runReader m r)) r

local' :: (r -> r') -> Reader r' a -> Reader r a
local' f (Reader g) = Reader (g . f)

asks :: (r -> a) -> Reader r a
asks = Reader

type User = String
type Password = String
type UsersTable = [(User, Password)]
pwds = [("Bill", "123"), ("Ann","qwerty"),("John","2sRq8P")]

usersWithBadPasswords :: Reader UsersTable [User]
usersWithBadPasswords = Reader $ map fst . filter ((==) "123456" . snd)