import ru.hardmet.stepik.haskell.Part5._

val f = toLogger((a: Int) => a + 1, "add 1")
val g = toLogger((a: Int) => a * 2, "multiply 2")
println(execLoggers(f, g, 0))
println(execLoggers(f, g, 11))
println(execLoggers(f, g, 2))
println(execLoggers(g, f, 0))