package ru.hardmet

package object math {

  @scala.annotation.tailrec
  def gcdBig(x: BigInt, y: BigInt): BigInt = if (x == 0) y else gcdBig(y % x, x)

  @scala.annotation.tailrec
  def gcd(x: Long, y: Long): Long = if (x == 0) y else gcd(y % x, x)
}
