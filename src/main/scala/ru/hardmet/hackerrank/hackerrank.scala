package ru.hardmet

package object hackerrank {

  trait Input[T] {

    def input: T
  }

  trait Output[T] {

    def output(result: T): Unit
  }

  trait Process[I, O] {

    def process(inputVal: I): O

  }

  trait RunnableTask[I, O] extends App with Process[I, O] with Input[I] with Output[O] {

    def run(): Unit = {
      output(process(input))
    }

    run()
  }

  trait FunctionalTask[I, O] extends RunnableTask[I, O] {

    def testCases: List[I]

    def f(a: I): O

    def expectedOutput: List[O]

    override def run(): Unit = {
      testCases.zip(expectedOutput).forall {
        testCaseExpected =>
          f(testCaseExpected._1).equals(testCaseExpected._2)
      }
    }
  }

  trait ListTasks[I, O] extends RunnableTask[List[I], List[O]] {

    import scala.io.StdIn

    def size: Int = StdIn.readInt()

    def processOnce(inputVal: I): O

    override def process(inputVal: List[I]): List[O] = inputVal.map(processOnce)

    def prepareForPrint(result: O): String

    override def output(result: List[O]): Unit = result.foreach(x => println(prepareForPrint(x)))

    def inputOnce: I

    override def input: List[I] = (1 to size).map(_ => inputOnce).toList
  }

}
