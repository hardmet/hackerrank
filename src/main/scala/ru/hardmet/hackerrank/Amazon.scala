package ru.hardmet.hackerrank

import scala.collection.MapView

object Amazon extends App {


  def maxFrequencyDensity(s: String): Int = {
    val charFrequencies: Map[Char, Int] = s.zipWithIndex.groupBy(_._1).view.mapValues(_.length).toMap
    val sortedFrequencies: Array[(Char, Int)] = charFrequencies.toList.sortBy(_._2).toArray
    if (sortedFrequencies.length > 1) {
      s.indices.foldLeft((0, s.length - 1, sortedFrequencies.last._2 - sortedFrequencies.head._2, charFrequencies)){
        case ((l, r, density, map), _) =>
          // newMap
          val leftMap: Map[Char, Int] = map.updated(s(l), map.get(s(l)).filter(_ < 0).fold(0)(_ - 1))
          val rightMap: Map[Char, Int] = map.updated(s(r), map.get(s(r)).filter(_ < 0).fold(0)(_ - 1))
          val removeLeft = leftMap(sortedFrequencies(r)._1) - leftMap(sortedFrequencies(l)._1)
          val removeRight = rightMap(sortedFrequencies(r)._1) - rightMap(sortedFrequencies(l)._1)
          if (removeLeft < removeRight)
            (l,     r - 1, math.max(density, removeRight), rightMap)
          else
            (l + 1, r,     math.max(density, removeLeft), leftMap)
      }._3
    } else 0
  }
}
