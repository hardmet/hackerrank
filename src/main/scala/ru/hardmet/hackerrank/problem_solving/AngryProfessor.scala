package ru.hardmet.hackerrank.problem_solving

class AngryProfessor {

  /*
     * Complete the 'angryProfessor' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts following parameters:
     *  1. INTEGER k
     *  2. INTEGER_ARRAY a
     */

  def angryProfessor(k: Int, a: Array[Int]): String = {
    // Write your code here
    if (a.count(_ <= 0) >= k) "NO" else "YES"
  }
}
