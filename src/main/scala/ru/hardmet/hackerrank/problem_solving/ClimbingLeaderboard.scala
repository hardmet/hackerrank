package ru.hardmet.hackerrank.problem_solving

import scala.util.Try

object ClimbingLeaderboard extends App {

  def climbingLeaderboard(ranked: Array[Int], player: Array[Int]): Array[Int] = {
    // Write your code here
    val newRanked = Array.copyOf(ranked, ranked.length + 1)
    val ranks = Array.ofDim[Int](ranked.length + 1)
    var i = 0
    ranks(0) = 1
    i += 1
    val (r, _) = ranked.tail.foldLeft(1 -> ranked.head){
      case ((ranksAcc, prevRank), next) =>
        if (next < prevRank) {
          ranks(i) = ranksAcc + 1
          i+=1
          ranksAcc + 1 -> next
        } else {
          ranks(i) = ranksAcc
          i+=1
          ranksAcc -> next
        }
    }
    ranks(ranks.length - 1) = r + 1
    player.map{
      score =>
        val point = newRanked.search(score)(Ordering[Int].reverse).insertionPoint
        newRanked.update(point, score)
        ranks(point)
    }
  }

  val r = Array(100, 100, 50, 40, 40, 20, 10)
//  val r = Array(100, 90, 90, 80, 75, 60)
  val players = Array(5, 25, 50, 120)
//  val players = Array(50, 65, 77, 90, 102)
  println(climbingLeaderboard(r, players).mkString(", "))
}
