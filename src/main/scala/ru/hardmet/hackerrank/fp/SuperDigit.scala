package ru.hardmet.hackerrank.fp

import ru.hardmet.hackerrank.RunnableTask

object SuperDigit extends RunnableTask[(BigInt, BigInt), BigInt] {

  import scala.io.StdIn

  @scala.annotation.tailrec
  def superDigit(n: BigInt, k: BigInt): BigInt = {
    if (n < 10) {
      n
    } else {
      superDigit(n.toString().map(_.asDigit).sum * k, 1)
    }
  }

  override def process(inputVal: (BigInt, BigInt)) = (superDigit _).tupled.apply(inputVal)

  override def output(result: BigInt): Unit = println(result.toString)

  override def input = StdIn.readLine().split(" ").map(BigInt(_)) match {
    case Array(x: BigInt, y: BigInt, _*) => (x, y)
  }
}
