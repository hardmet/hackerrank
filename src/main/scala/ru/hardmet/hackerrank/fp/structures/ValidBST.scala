package ru.hardmet.hackerrank.fp.structures


case class Fork(value: Int, left: Option[Fork] = None, right: Option[Fork] = None, parent: Option[Fork] = None)

object ValidBST extends App {

  def pre(l: List[Int], low: Int = -1, high: Int = Int.MaxValue): List[Int] = {
    l match {
      case Nil => Nil
      case x :: ls => if (low < x && x < high) pre(pre(ls, low, x), x, high) else l
    }
  }

  @scala.annotation.tailrec
  def bstBuild(preorder: Array[Int])(pointer: Int, predicates: List[Int => Boolean]): Int = {
    if (pointer == preorder.length)
      pointer
    else
      predicates match {
        case ::(predicate, ps) if predicate(preorder(pointer)) =>
          bstBuild(preorder)(pointer + 1, ((x: Int) => predicate(x) && x <= preorder(pointer)) :: ((x: Int) => predicate(x) && x > preorder(pointer)) :: ps)
        case ::(_, ps) => bstBuild(preorder)(pointer, ps)
        case _ => pointer
      }
  }

  def validate(nodes: Array[Int]): String = {
    if (bstBuild(nodes)(0, List(_ => true)) == nodes.length)
      "YES"
    else "NO"
  }

  import scala.io.StdIn

  (1 to StdIn.readLine().toInt).foreach {
    _ =>
      val _ = StdIn.readLine().toInt
      val input = StdIn.readLine().split(" ").map(_.toInt)
      println(validate(input))
  }

  //  def validate(nodes: List[Int], root: Fork, p: Option[Int => Boolean] = None): Option[Boolean] = {
  //    nodes match {
  //      case Nil => Some(true)
  //      case n :: _ if p.isDefined && !p.get(n) => None
  //      case n :: ns =>
  //        if (n > root.value)
  //          root.right.map(
  //            r =>
  //              validate(nodes, r, Some(k => k > root.value))
  //          ).getOrElse(
  //            validate(ns,
  //              Fork(root.left, root.value, Some(Fork(None, n, None, Some(root)))), Some(k => k > root.value))
  //          )
  //        else
  //          root.left.map(
  //            l =>
  //              validate(nodes, l)
  //          ).getOrElse(
  //            validate(ns,
  //              Fork(Some(Fork(None, n, None, Some(root))), root.value, root.right)))
  //
  //    }
  //  }

  //  import scala.io.StdIn
  //
  //  (1 to StdIn.readLine().toInt).foreach {
  //    _ =>
  //      val _ = StdIn.readLine().toInt
  //      val input = StdIn.readLine().split(" ").map(_.toInt).toList
  //      println(validate(input.tail, Fork(value = input.head)).map(_ => "YES").getOrElse("NO"))
  //  }
}
