package ru.hardmet.hackerrank.fp.structures

import ru.hardmet.leetcode.structures.DisjointSetUnion

import scala.io.StdIn

object GraphComponents {

  def main(args: Array[String]) {
    val (n, m) = StdIn.readLine().split(" ").map(_.toInt) match {
      case Array(p, q) => p -> q
    }
    val dsu = (1 to m).foldLeft(DisjointSetUnion(n)){
      case (d, _) =>
        StdIn.readLine().split(" ").map(_.toInt) match {
          case Array(p, q) => d.unionSets(p - 1, q - 1)
        }
    }
    val count = dsu.parent.map(p => dsu.findSet(p)).toSet.size
    println(count)
  }
}
