package ru.hardmet.hackerrank.fp.structures


object ListsAndGCD extends App {

  import scala.io.StdIn

  def toMap(a: List[Int]): Map[Int, Int] = a.grouped(2).map(xs => xs.head -> xs(1)).toMap

  def combine(a: Map[Int, Int], b: Map[Int, Int]): Map[Int, Int] = (for {
    key <- if (a.size > b.size) b.keys else a.keys
    aVal <- a.get(key)
    bVal <- b.get(key)
  } yield key -> math.min(aVal, bVal)).toMap

  def gcd(a: List[Int], b: List[Int]): List[Int] = combine(toMap(a), toMap(b)).toList.sortBy(_._1).flatMap(kv => List(kv._1, kv._2))

  def gcd(list: List[List[Int]]): List[Int] = list match {
    case a :: Nil => a
    case x :: xs => gcd(x, gcd(xs))
  }

  val lines: List[List[Int]] = (1 to StdIn.readLine().toInt)
    .map(_ =>
      StdIn.readLine().split(" ")
        .map(_.toInt)
        .toList).toList

  println(gcd(lines).mkString(" "))

}
