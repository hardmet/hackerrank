package ru.hardmet.hackerrank.fp.structures

import scala.io.StdIn

object MatrixRotation extends App {

  val (m: Int, r: Int) = StdIn.readLine().split(" ").map(_.toInt).toList match {
    case mArg :: _ :: rArg :: Nil => (mArg, rArg)
  }

  val rows = (1 to m).map(_ => StdIn.readLine().split(" ").map(_.toInt).toList).toList

  preparePrint(
    rotate(
      parse(rows)
      , r)
  )
    .foreach {
      r => println(r.mkString(" "))
    }

  def parse(input: List[List[Int]]): List[List[List[Int]]] = {
    @scala.annotation.tailrec
    def parseMatrix(input: List[List[Int]], ring: List[List[Int]], matrix: List[List[Int]], ringsAcc: List[List[List[Int]]]): List[List[List[Int]]] = {
      input match {
        case last :: Nil if matrix.nonEmpty => parseMatrix(matrix.init.reverse, List(matrix.last), List.empty[List[Int]], (last :: ring) :: ringsAcc)
        case last :: Nil => (last :: ring) :: ringsAcc
        case row :: rows => parseMatrix(rows, List(row.head, row.last) :: ring, if (row.size > 2) row.tail.init :: matrix else matrix, ringsAcc)
      }
    }

    parseMatrix(input.tail, List(input.head), List.empty, Nil)
  }

  def fromLinear(list: List[Int], sideLength: Int, acc: List[List[Int]], reverseFlag: Int): List[List[Int]] = {
    val start = list.take(sideLength)
    val rest = list.drop(sideLength)
    if (rest.size != sideLength) {
      val c = (list.size - 2 * sideLength) / 2
      val right = rest.take(c).reverse
      val end = rest.slice(c, c + sideLength).reverse
      val left = rest.drop(c).slice(sideLength, sideLength + c)
      val temp = start :: (left.zip(right).map {
        case (l, r) => List(l, r)
      }.foldLeft(List.empty[List[Int]]) {
        case (acc, ls) =>
          ls :: acc
      } ++ List(end))
      temp
    } else List(start, rest.reverse)
  }

  def rotate(rings: List[List[List[Int]]], r: Int): List[List[List[Int]]] = {
    def toLinear(ring: List[List[Int]]): List[Int] = {
      val (left, right) = ring.tail.init.foldLeft(List.empty[Int] -> List.empty[Int]) {
        case ((right, left), r :: l :: Nil) => (r :: right) -> (l :: left)
      }
      ring.head ++ right.reverse ++ ring.last.reverse ++ left
    }

    rings.map {
      ring =>
        val n = ring.head.size
        val linearRing = toLinear(ring.reverse)
        val (s, end) = linearRing.splitAt(if (r >= linearRing.length) r % (linearRing.length) else r)
        fromLinear(end ++ s, n, Nil, 0)
    }
  }

  def addMid(ring: List[List[Int]], acc: Vector[Vector[Int]]): Vector[Vector[Int]] = {
    ring.zipWithIndex.foldLeft(acc) {
      case (ac, (r, i)) =>
        ac.updated(i, r.head +: ac(i) :+ r.last)
    }
  }

  def addRing(ring: List[List[Int]], acc: Vector[Vector[Int]]): Vector[Vector[Int]] = {
    ring.head.toVector +: addMid(ring.tail.init, acc) :+ ring.last.toVector
  }

  def preparePrint(rings: List[List[List[Int]]]): Vector[Vector[Int]] = {
    @scala.annotation.tailrec
    def prepare(rings: List[List[List[Int]]], acc: Vector[Vector[Int]]): Vector[Vector[Int]] = {
      rings match {
        case Nil => acc
        case ring :: rs => prepare(rs, addRing(ring, acc))
      }
    }

    prepare(rings.tail, rings.head.map(_.toVector).toVector)
  }
}
