package ru.hardmet.hackerrank.fp.structures

import ru.hardmet.leetcode.structures.DisjointSetUnion

import scala.io.StdIn

object PrisonTransport {

  def main(args: Array[String]) {
    val n = StdIn.readLine().toInt
    val m = StdIn.readLine().toInt
    val dsu = (1 to m).foldLeft(DisjointSetUnion(n)){
      case (d, _) =>
        StdIn.readLine().split(" ").map(_.toInt) match {
        case Array(p, q) => d.unionSets(p - 1, q - 1)
      }
    }
    val s = dsu.parent.map(p => dsu.findSet(p)).distinct.foldLeft(0){
      (sum, p) =>
        sum + math.sqrt(dsu.sizes(p)).ceil.toInt
    }
    println(s)
  }
}
