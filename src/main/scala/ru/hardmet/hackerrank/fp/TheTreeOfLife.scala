package ru.hardmet.hackerrank.fp

import ru.hardmet.hackerrank.RunnableTask

import scala.io.StdIn

object TheTreeOfLife extends RunnableTask[(List[Char], Seq[(Int, List[Char])], Seq[Boolean]), String] {

  def createRule(num: Int): Seq[Boolean] = {
    val line = Integer.toString(num, 2).reverse
    Vector.fill(16)(false).zipWithIndex
      .map(
        stateInd =>
          if (stateInd._2 >= line.length) false
          else line(stateInd._2) == '1')
  }

  def parseBool(b: Boolean): Char = if (b) '1' else '0'

  def toStringTuple(v0: Boolean, v1: Boolean, v2: Boolean, v3: Boolean): String =
    (parseBool(v0) :: parseBool(v1) :: parseBool(v2) :: parseBool(v3) :: Nil).mkString

  def executeRule(rule: Seq[Boolean], v0: Boolean, v1: Boolean, v2: Boolean, v3: Boolean): Boolean = {
    rule(Integer.parseInt(toStringTuple(v0, v1, v2, v3), 2))
  }

  class Tree(val value: Boolean)

  case class Node(parent: Boolean, left: Tree, override val value: Boolean, right: Tree) extends Tree(value: Boolean)

  case class Leaf(parent: Boolean, override val value: Boolean) extends Tree(value: Boolean)

  def parseDotOrX(char: Char): Boolean = char == 'X'

  def boolToSymbol(b: Boolean): Char = if (b) 'X' else '.'

  def cutBraces(tree: List[Char]): List[Char] = tree.tail.init

  @scala.annotation.tailrec
  def parseSubTree(parent: Boolean, strTree: List[Char], braces: Int, acc: List[Char]): Tree = {
    strTree match {
      case ')' :: v :: r :: Nil if braces == 1 => val value = parseDotOrX(v)
        Node(parent, parseTree((')' :: acc).reverse, value), value, Leaf(value, parseDotOrX(r)))
      case ')' :: v :: '(' :: r if braces == 1 => val value = parseDotOrX(v)
        Node(parent, parseTree((')' :: acc).reverse, value), value, parseTree('(' :: r, value))
      case ')' :: Nil if braces == 1 => parseTree((')' :: acc).reverse, parent)
      case '(' :: tail => parseSubTree(parent, tail, braces + 1, '(' :: acc)
      case ')' :: tail => parseSubTree(parent, tail, braces - 1, ')' :: acc)
      case head :: tail => parseSubTree(parent, tail, braces, head :: acc)
    }
  }

  def parseTree(treeStr: List[Char], parent: Boolean): Tree = {
    if (treeStr.length < 3) Leaf(parent, parseDotOrX(treeStr.head))
    else {
      if (treeStr.last != ')' && treeStr.head != '(')
        println("here")
      val treeStrTrimmed = cutBraces(treeStr)
      treeStrTrimmed match {
        case l :: v :: r :: Nil => val value = parseDotOrX(v)
          Node(parent, Leaf(value, parseDotOrX(l)), value, Leaf(value, parseDotOrX(r)))
        case '(' :: tail => parseSubTree(parent, tail, 1, '(' :: Nil)
        case l :: v :: '(' :: r => val value = parseDotOrX(v)
          Node(parent, Leaf(value, parseDotOrX(l)), value, parseSubTree(value, r, 1, '(' :: Nil))
      }
    }
  }

  @scala.annotation.tailrec
  def goOn(tree: Tree, path: List[Char]): Char = {
    path match {
      case Nil => boolToSymbol(tree.value)
      case '>' :: rest => goOn(tree.asInstanceOf[Node].right, rest)
      case '<' :: rest => goOn(tree.asInstanceOf[Node].left, rest)
    }
  }

  def lifeI(tree: Tree, parent: Boolean)(implicit rule: Seq[Boolean]): Tree = {
    tree match {
      case Node(p, l, v, r) => val par = executeRule(rule, p, l.value, v, r.value)
        Node(parent, lifeI(l, par), par, lifeI(r, par))
      case Leaf(p, v) => Leaf(parent, executeRule(rule, p, v1 = false, v2 = v, v3 = false))
    }
  }

  @scala.annotation.tailrec
  def life(tree: Tree, i: Int, acc: List[Tree] = Nil)(implicit rule: Seq[Boolean]): List[Tree] = {
    i match {
      case 0 => acc.reverse
      case n => val li = lifeI(tree, parent = false)
        life(li, n - 1, li :: acc)
    }
  }

  override def input: (List[Char], Seq[(Int, List[Char])], Seq[Boolean]) = {
    val rulN = StdIn.readInt()
    val rule: Seq[Boolean] = createRule(rulN)
    (StdIn.readLine().filter(_ != ' ').toList,
      (0 until StdIn.readLine().toInt).map { _ =>
        val q = StdIn.readLine().split(" ")
        q(0).toInt -> q(1).toList
      },
      rule)
  }

  override def output(result: String): Unit = print(result)

  override def process(input: (List[Char], Seq[(Int, List[Char])], Seq[Boolean])): String = {
    input match {
      case (list: List[Char], qs: Seq[(Int, List[Char])], rule: Seq[Boolean]) =>
        val tree = parseTree(list, parent = false)
        val is = qs.foldLeft(List.empty[Int]) { (acc, next) =>
          (next._1 + acc.headOption.getOrElse(0)) :: acc
        }.reverse
        val i = is.max
        implicit val r: Seq[Boolean] = rule
        val trees = life(tree, i, List(tree))
        is.zip(qs).map {
          i =>
            goOn(trees(i._1), cutBraces(i._2._2))
        }.mkString("\n")
    }
  }

  def printTree(tree: Tree): Unit = {
    def printSubTree(st: Tree): String = {
      st match {
        case Leaf(_, v) => boolToSymbol(v).toString
        case Node(_, l, v, r) => "(" + printSubTree(l) + " " + boolToSymbol(v).toString + " " + printSubTree(r) + ")"
      }
    }

    println(printSubTree(tree))
  }

  def test(): Unit = {
    val rule = createRule(7710)
    println(rule)
    println(Integer.parseInt("1011", 2))
    println(executeRule(rule, v0 = true, v1 = true, v2 = true, v3 = true))
    val t0 = parseTree("((. X (. . .)) . (X . (. X X)))".filter(_ != ' ').toList, parent = false)
    println(t0)
    println(parseTree("(.X.)".filter(_ != ' ').toList, parent = false))
    println(Node(parent = false, Leaf(true, false), value = true, Leaf(parent = true, value = false)) == parseTree("(.X.)".filter(_ != ' ').toList, false))
    val t1 = parseTree("((.X.).X)".filter(_ != ' ').toList, false)
    println(t1)
    println(t1 == Node(parent = false, left = Node(false, Leaf(true, false), true, Leaf(parent = true, value = false)), value = false, right = Leaf(false, true)))
    val t2 = parseTree("(.X(.X.))".filter(_ != ' ').toList, false)
    println(t2 == Node(false, Leaf(true, false), true, Node(true, Leaf(true, false), true, Leaf(true, false))))
    val t3 = parseTree("((.X.).(.X.))".filter(_ != ' ').toList, false)
    println(t3 == Node(false, Node(false, Leaf(true, false), true, Leaf(true, false)), false, Node(false, Leaf(true, false), true, Leaf(true, false))))
    val t4 = parseTree("(((.X.)X.).X)".filter(_ != ' ').toList, false)
    println(t4 == Node(false, Node(false, Node(true, Leaf(true, false), true, Leaf(true, false)), true, Leaf(true, false)), false, Leaf(false, true)))

    println(parseTree("((X . (. X .)) X (. X (X . X)))".filter(_ != ' ').toList, false))
    println(parseTree("((. X (X . X)) . (X X (. X .)))".filter(_ != ' ').toList, false))
    println(parseTree("((X . (. X .)) X (X . (X X X)))".filter(_ != ' ').toList, false))
    println(parseTree("((. X (X . X)) . (. X (X . X)))".filter(_ != ' ').toList, false))
    //    [<><<<><>>]
    println(goOn(t0, cutBraces("[><]".toList)))
    println(goOn(t0, cutBraces("[<>]".toList)))
  }

  def testTree(implicit rule: Seq[Boolean]): Unit = {
    println("testTree")
    val tr = parseTree("((. X (. . .)) . (X . (. X X)))".filter(_ != ' ').toList, false)
    life(tr, 4, tr :: Nil).foreach(printTree)
    printTree(lifeI(tr, false))
  }

  def testRule(implicit rule: Seq[Boolean]): Unit = {
    println(Seq(
      Seq('X', 'X', 'X', 'X') -> 'X',
      Seq('X', 'X', 'X', '.') -> '.',
      Seq('X', 'X', '.', 'X') -> 'X',
      Seq('X', 'X', '.', '.') -> '.',
      Seq('X', '.', 'X', 'X') -> '.',
      Seq('X', '.', 'X', '.') -> 'X',
      Seq('X', '.', '.', 'X') -> '.',
      Seq('X', '.', '.', '.') -> 'X',
      Seq('.', 'X', 'X', 'X') -> '.',
      Seq('.', 'X', 'X', '.') -> 'X',
      Seq('.', 'X', '.', 'X') -> 'X',
      Seq('.', 'X', '.', '.') -> 'X',
      Seq('.', '.', 'X', 'X') -> '.',
      Seq('.', '.', 'X', '.') -> '.',
      Seq('.', '.', '.', 'X') -> 'X',
      Seq('.', '.', '.', '.') -> '.'
    ).forall {
      t =>
        val t4 = t._1.map(parseDotOrX)
        boolToSymbol(executeRule(rule, t4.head, t4(1), t4(2), t4(3))) == t._2
    })
  }
}
