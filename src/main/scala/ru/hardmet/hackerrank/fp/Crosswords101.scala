package ru.hardmet.hackerrank.fp

import ru.hardmet.hackerrank.RunnableTask

object Crosswords101 extends RunnableTask[(List[List[Char]], Crossword), List[Crossword]] {

  import scala.io.StdIn._

  override def input = {
    val crossword = new Crossword((0 until 10).map(_ => readLine().toCharArray.toList).toList)
    val words: List[List[Char]] = readLine().split(";").map(word => word.toCharArray.toList).toList
    (words, crossword)
  }

  override def process(args: (List[List[Char]], Crossword)): List[Crossword] =
    inputWords(args._1, args._2)

  override def output(args: List[Crossword]): Unit = args.foreach(_.print())

  def insertWord(word: List[Char], vector: List[Char]): Option[List[Char]] = word match {
    case Nil => Some(vector)
    case headWord :: tailWord => vector match {
      case Nil => None
      case headVector :: tailVector if headVector == '-' || headWord == headVector =>
        insertWord(tailWord, tailVector).flatMap(x => Some(headWord :: x))
      case _ => None
    }
  }

  def insertWord(word: List[Char], vector: List[Char], acc: List[Char]): List[List[Char]] = {
    vector match {
      case Nil => Nil
      case head :: tail if head == '-' || head == word.head && (acc.isEmpty || acc.last == '+') =>
        val restDecisions = insertWord(word, tail, acc ++ List(head))
        insertWord(word, vector)
          .flatMap(inserted => Some(acc ++ inserted)).flatMap(x => Some(x :: restDecisions)).getOrElse(restDecisions)
      case head :: tail => insertWord(word, tail, acc ++ List(head))
    }
  }

  def findMatchedVectors(word: List[Char], vectors: List[List[Char]], i: Int): List[(Int, List[Char])] = vectors match {
    case Nil => Nil
    case vector :: tail =>
      insertWord(word, vector, Nil)
        .map(vector => i -> vector) ++
        findMatchedVectors(word, tail, i + 1)
  }

  def inputWord(word: List[Char], crossword: Crossword): List[Crossword] =
    findMatchedVectors(word, crossword.getVerticals, 0)
      .map {
        pair =>
          crossword.replaceVertical(pair._1, pair._2)
      } ++
      findMatchedVectors(word, crossword.getHorizontals, 0)
        .map {
          pair =>
            crossword.replaceHorizontal(pair._1, pair._2)
        }

  def inputWords(words: List[List[Char]], crossword: Crossword): List[Crossword] = {
    words match {
      case Nil => crossword :: Nil
      case word :: tailWords =>
        inputWord(word, crossword)
          .flatMap(
            cross =>
              inputWords(tailWords, cross))
    }
  }
}

class Crossword(val field: List[List[Char]]) {
  def apply(list: List[List[Char]]): Crossword = new Crossword(list)

  def getVerticals: List[List[Char]] = (0 until 10).map(i => field.map(_ (i))).toList

  def getHorizontals: List[List[Char]] = field

  def replaceVertical(field: List[List[Char]], i: Int, vertical: List[Char]): Crossword =
    apply(
      vertical.zip(field)
        .map(
          newValVertical =>
            newValVertical._2.updated(i, newValVertical._1)
        ))

  def replaceVertical(i: Int, vertical: List[Char]): Crossword = replaceVertical(field, i, vertical)

  def replaceHorizontal(i: Int, vertical: List[Char]): Crossword = apply(field.updated(i, vertical))

  def print(): Unit = getHorizontals.foreach(horizontal => println(horizontal.mkString("")))
}
