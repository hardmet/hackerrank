package ru.hardmet.hackerrank.fp

import ru.hardmet.hackerrank.RunnableTask

import scala.io.StdIn

class Tree(val value: Int)

case class Fork(left: Option[Tree], override val value: Int, right: Option[Tree]) extends Tree(value: Int)

case class Leaf(override val value: Int) extends Tree(value: Int)

object SwapNodes extends RunnableTask[List[(Int, Int)], List[String]] {

  def parseTree(key: Int, lrs: List[(Int, Int)]): (Tree, List[(Int, Int)]) = {
    lrs match {
      case (-1, -1) :: restNodes => Leaf(key) -> restNodes
      case (-1, r) :: restNodes =>
        val (parsedR, nodes) = parseTree(r, restNodes)
        Fork(None, key, Some(parsedR)) -> nodes
      case (l, -1) :: restNodes =>
        val (parsedL, nodes) = parseTree(l, restNodes)
        Fork(Some(parsedL), key, None) -> nodes
      case (l, r) :: restNodes =>
        val (parsedL, ns) = parseTree(l, restNodes)
        val (parsedR, nodes) = parseTree(r, ns)
        Fork(Some(parsedL), key, Some(parsedR)) -> nodes
    }
  }


  @scala.annotation.tailrec
  def parseTree(nodes: Vector[Option[Tree]], elements: List[(Int, Int)]): Tree = {
    def parseNode(value: Int, someNode: Option[Tree] = None): Option[Tree] =
      if (value == -1) None else
        Some(someNode match {
          case None => Leaf(value)
          case Some(Leaf(_)) => Leaf(value)
          case Some(Fork(left, _, right)) => Fork(left, value, right)
        })

    elements match {
      case Nil => parseNode(1, nodes.head).get
      case e :: es => parseTree(
        e match {
          case (-1, -1) => nodes :+ None
          case (-1, r) => nodes.drop(1) :+ Some(Fork(None, -1, parseNode(r, nodes.head)))
          case (l, -1) => nodes.drop(1) :+ Some(Fork(parseNode(l, nodes.head), -1, None))
          case (l, r) => nodes.drop(2) :+ Some(Fork(parseNode(l, nodes.drop(1).head), -1, parseNode(r, nodes.head)))
        }, es)
    }
  }

  def swap(tree: Tree, h: Int): Tree = {
    def iteration(l: Option[Tree], r: Option[Tree], key: Int, i: Int): Fork =
      Fork(l.map(swapI(_, i + 1)), key, r.map(swapI(_, i + 1)))

    def swapI(tree: Tree, i: Int): Tree = {
      tree match {
        case Fork(l, key, r) if i % h == 0 => iteration(r, l, key, i)
        case Fork(l, key, r) => iteration(l, r, key, i)
        case leaf => leaf
      }
    }

    swapI(tree, 1)
  }

  def printInfixOrder(tree: Tree): String = {
    def infix(acc: List[Int], tree: Tree): List[Int] = {
      tree match {
        case Leaf(key) => key :: acc
        case Fork(l, key, r) =>
          r.foldLeft(
            key :: l.foldLeft(acc)(infix)
          )(infix)
      }
    }

    infix(List.empty[Int], tree).reverse.mkString(" ")
  }


  override def process(inputVal: List[(Int, Int)]): List[String] =
    (1 to StdIn.readInt()).foldLeft(parseTree(Vector.empty[Option[Tree]], inputVal), List.empty[String]) {
      case ((tr, result), _) =>
        val k = StdIn.readInt()
        val swapped = swap(tr, k)
        swapped -> (printInfixOrder(swapped) :: result)
    }._2.reverse

  override def output(result: List[String]): Unit = result.foreach(println)

  def input: List[(Int, Int)] = (1 to StdIn.readInt()).map {
    _ =>
      val nodes = StdIn.readLine().split(" ").map(_.toInt)
      nodes(0) -> nodes(1)
  }.toList.reverse

}