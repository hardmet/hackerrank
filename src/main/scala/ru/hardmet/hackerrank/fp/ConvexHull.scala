package ru.hardmet.hackerrank.fp

import ru.hardmet.hackerrank.RunnableTask
import ru.hardmet.hackerrank.fp.ConvexHull._

import scala.io.StdIn._
import scala.math._

case class Point(x: Int, y: Int)

object Point {
  def apply(xyArr: Array[String]): Point = new Point(xyArr(0).toInt, xyArr(1).toInt)
}

class ConvexHull extends RunnableTask[List[Point], Double] {
  override def input: List[Point] = {
    (0 until readLine().trim.toInt)
      .map(_ => Point(readLine().split(" "))).toList
  }

  override def process(points: List[Point]): Double = {
    val (max: Point, min: Point) = points.tail
      .foldLeft(points.head -> points.head) {
        case (acc: (Point, Point), nextPoint: Point) =>
          maxX(acc._1, nextPoint) -> minX(acc._2, nextPoint)
      }
    val convexHull: List[Point] = (quickHull(points, Line(min, max), leftSide = false) ++
      quickHull(points, Line(max, min), leftSide = false)).distinct
    calculatePerimeter(convexHull, convexHull.head, 0.0)
  }

  override def output(result: Double): Unit = println(result)
}

object ConvexHull {

  case class Line(start: Point, end: Point)


  def maxX(a: Point, b: Point): Point = if (a.x > b.x) a else b

  def minX(a: Point, b: Point): Point = if (a.x < b.x) a else b

  def distance(line: Line, point: Point): Int = (point.y - line.start.y) * (line.end.x - line.start.x) -
    (line.end.y - line.start.y) * (point.x - line.start.x)

  def isLeftSide(line: Line, point: Point): Boolean = if (distance(line, point) > 0) true else false

  def isLeftSide(distance: Int): Boolean = if (distance > 0) true else false

  def square(a: Point, b: Point, c: Point): Double = abs((a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y)) / 2.0)

  def isInsideTriangle(line: Line, top: Point, point: Point): Boolean =
    square(line.start, line.end, top) == square(line.start, line.end, point) + square(line.start, top, point) + square(line.end, top, point)

  def quickHull(points: List[Point], line: Line, leftSide: Boolean): List[Point] = {
    val filteredPoints = points.map(
      point =>
        point -> distance(line, point))
      .filter(
        pointDistance =>
          if (leftSide) isLeftSide(pointDistance._2)
          else !isLeftSide(pointDistance._2))
    if (filteredPoints.isEmpty) Nil
    else {
      val maxPoint = filteredPoints.maxBy(point => abs(point._2))
      val pointsOutsideTriangle = filteredPoints
        .map(
          point => point._1)
        .filter(
          point =>
            !isInsideTriangle(line, maxPoint._1, point))
      line.start ::
        quickHull(pointsOutsideTriangle, Line(line.start, maxPoint._1), leftSide = false) ++
          List(maxPoint._1) ++
          quickHull(pointsOutsideTriangle, Line(maxPoint._1, line.end), leftSide = false) ++
          List(line.end)
    }
  }

  def pointsDistance(a: Point, b: Point): Double = sqrt(pow(b.x - a.x, 2) + pow(b.y - a.y, 2))

  @scala.annotation.tailrec
  def calculatePerimeter(points: List[Point], headPoint: Point, acc: Double): Double = points match {
    case last :: Nil => acc + pointsDistance(last, headPoint)
    case a :: b :: tail => calculatePerimeter(b :: tail, headPoint, acc + pointsDistance(a, b))
  }
}
