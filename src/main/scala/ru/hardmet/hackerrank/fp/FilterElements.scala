package ru.hardmet.hackerrank.fp

import ru.hardmet.hackerrank.ListTasks

object FilterElements extends ListTasks[(List[Int], Int), List[Int]] {

  import scala.io._

  def constructAnswer(list: List[Int]): String = {
    list.map(_.toString).reduceLeftOption((acc, elem) => s"$acc $elem").getOrElse("-1")
  }

  def findKTimesInList(numbers: List[Int], k: Int): List[Int] = numbers
    .zipWithIndex
    .map(n => (n._1, n._2))
    .groupBy(n => n._1)
    .map(
      tuple2 =>
        (tuple2._1, tuple2._2.length, tuple2._2.head._2))
    .filter(tuple3 => tuple3._2 >= k)
    .map(tuple3 => tuple3._1 -> tuple3._3)
    .toList
    .sortBy(_._2)
    .map(_._1)

  override def inputOnce = {
    val k = StdIn.readLine().split(" ").toList(1).toInt
    val list = StdIn.readLine().split(" ").map(_.toInt).toList
    list -> k
  }

  override def prepareForPrint(result: List[Int]): String = constructAnswer(result)

  override def processOnce(inputVal: (List[Int], Int)) = (findKTimesInList _).tupled(inputVal)
}
