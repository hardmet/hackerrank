package ru.hardmet.hackerrank.fp

import ru.hardmet.hackerrank.RunnableTask
import ru.hardmet.hackerrank.fp.ConvexHull._

import scala.io.StdIn

object ConcavePolygon extends RunnableTask[List[Point], Boolean] {

  def input: List[Point] = {
    (0 until StdIn.readInt())
      .map(_ =>
        StdIn.readLine().split(" ")
      )
      .map(point => Point.apply(point)).toList
  }

  @scala.annotation.tailrec
  def isOnEdge(points: List[Point], convexHull: List[Point], h: Point): Boolean = {
    convexHull match {
      case h :: t :: Nil => points.forall(p => distance(Line(h, t), p) == 0)
      case h :: th :: tail => isOnEdge(points.filter(distance(Line(h, th), _) != 0), th :: tail, h)
    }
  }

  def process(points: List[Point]): Boolean = {
    val (max: Point, min: Point) = points.tail
      .foldLeft(points.head -> points.head) {
        case (acc: (Point, Point), nextPoint: Point) =>
          maxX(acc._1, nextPoint) -> minX(acc._2, nextPoint)
      }
    val convexHull: List[Point] = (quickHull(points, Line(min, max), leftSide = false) ++
      quickHull(points, Line(max, min), leftSide = false)).distinct

    !isOnEdge(points.filter(!convexHull.toSet.contains(_)), convexHull, convexHull.head)
  }

  def output(isConcave: Boolean): Unit = println(if (isConcave) "YES" else "NO")

}
