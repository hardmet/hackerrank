package ru.hardmet.hackerrank.fp

import ru.hardmet.hackerrank.ListTasks

object SequenceFullOfColors extends ListTasks[List[Char], Boolean] {

  import scala.io._
  import scala.math._

  def isGoodChar(char: Char, rgyb: (Int, Int, Int, Int)): (Int, Int, Int, Int) = char match {
    case 'R' => (rgyb._1 + 1, rgyb._2, rgyb._3, rgyb._4)
    case 'G' => (rgyb._1, rgyb._2 + 1, rgyb._3, rgyb._4)
    case 'Y' => (rgyb._1, rgyb._2, rgyb._3 + 1, rgyb._4)
    case 'B' => (rgyb._1, rgyb._2, rgyb._3, rgyb._4 + 1)
  }

  def isGoodChar(rgyb: (Int, Int, Int, Int)): Option[(Int, Int, Int, Int)] =
    if (abs(rgyb._1 - rgyb._2) < 2 && abs(rgyb._3 - rgyb._4) < 2)
      Some(rgyb)
    else None

  def isGoodString(rgyb: (Int, Int, Int, Int)): Boolean = rgyb._1 == rgyb._2 && rgyb._3 == rgyb._4

  def isFullColor(symbols: List[Char], rgyb: (Int, Int, Int, Int)): Boolean = {
    symbols match {
      case Nil => isGoodString(rgyb)
      case char :: restSymbols => isGoodChar(isGoodChar(char, rgyb)).exists(isFullColor(restSymbols, _))
    }
  }

  override def processOnce(inputVal: List[Char]) = isFullColor(inputVal, (0, 0, 0, 0))

  override def prepareForPrint(result: Boolean): String = result.toString.capitalize

  override def inputOnce = StdIn.readLine().toList
}
