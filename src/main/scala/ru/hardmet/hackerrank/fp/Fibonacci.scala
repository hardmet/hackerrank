package ru.hardmet.hackerrank.fp

import ru.hardmet.math.gcd

import scala.io.StdIn

object Fibonacci extends App {

  def fibonacci(a: (Long, Long, Long, Long), n: Long, m: Long): (Long, Long, Long, Long) = {
    if (n == 0) {
      a
    } else if (n % 2 == 1) {
      if (n == 1) {
        (0, 1, 1, 1)
      } else {
        multiply(fibonacci(a, n - 1, m), a, m)
      }
    } else {
      val b = fibonacci(a, n / 2, m)
      multiply(b, b, m)
    }
  }

  def multiply(a: (Long, Long, Long, Long), b: (Long, Long, Long, Long), m: Long): (Long, Long, Long, Long) = {
    val c1 = ((a._1 * b._1) % m + (a._2 * b._3)) % m
    val c2 = ((a._1 * b._2) % m + (a._2 * b._4)) % m
    val c3 = ((a._3 * b._1) % m + (a._4 * b._3)) % m
    val c4 = ((a._3 * b._2) % m + (a._4 * b._4)) % m
    (c1, c2, c3, c4)
  }

  @scala.annotation.tailrec
  def fib(i: Int, n: Long, f1: Long, f2: Long): Long = {
    i match {
      case x if x == n => (f1 + f2) % 10
      case _ => fib(i + 1, n, (f1 + f2) % 10, f1 % 10)
    }
  }

  //  @scala.annotation.tailrec
  //  def gcd(x: Long, y: Long, n: Long, m: Long): Long = if (x == 0) y else gcd(fibonacci((0, 1, 1, 1), m, n)._4, x, m, n)

  def minSquares(m: Long, n: Long): Long = {
    val squareSide = gcd(m, n)
    // Number of squares.
    (m * n) / (squareSide * squareSide)
  }

  val arg = StdIn.readLine().split(" ")
  val n = arg(0).toLong
  val m = arg(1).toLong
  //  val g = gcd(n, m)
  println(
    minSquares(n, m)
  )


  //  println(n match {
  //    case 0 => 0
  //    case 1 => 1
  //    case x => fib(2, x, 1, 0)
  //  })
  //  println(fibonacci((0, 1, 1, 1), g - 1, 10)._4)
  //  println(gcd(n, m))
}
