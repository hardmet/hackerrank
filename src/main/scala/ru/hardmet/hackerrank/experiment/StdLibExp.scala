package ru.hardmet.hackerrank.experiment

import java.util.concurrent.{Executors, ScheduledExecutorService, TimeUnit, TimeoutException}
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContextExecutor, Future, Promise}

object StdLibExp extends App {

val scheduller: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
implicit val ec: ExecutionContextExecutor = scala.concurrent.ExecutionContext.fromExecutorService(scheduller)

val promise = Promise[Int]()
scheduller.schedule(
  new Runnable {
    override def run(): Unit = {
      promise.failure(new TimeoutException)
      scheduller.shutdown()
    }
  },
  500,
  TimeUnit.MILLISECONDS
)
  val INF_FUTURE = Future.never
  //  val INF_FUTURE = Future {
  //    while (true) {
  //      1
  //    }
  //  }

  Await.result(Future.firstCompletedOf(Seq(INF_FUTURE, promise.future)).map {
    x =>
      println(x)
  }, 10.seconds)
}


