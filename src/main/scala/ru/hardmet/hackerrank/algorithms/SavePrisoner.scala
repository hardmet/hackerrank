package ru.hardmet.hackerrank.algorithms;

object SavePrisoner extends App {
  
  def saveThePrisoner(prisoners: Int, treats: Int, shift: Int): Int = 
    (treats - 1 + shift - 1) % prisoners + 1

  println(saveThePrisoner(3, 3, 1))
  println(saveThePrisoner(4, 6, 1))
  println(saveThePrisoner(4, 6, 2))
  println(saveThePrisoner(5, 2, 1))
  println(saveThePrisoner(5, 2, 2))
}
