package ru.hardmet.hackerrank.algorithms;

import ru.hardmet.hackerrank.fp.Point;

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args){
//        taskLast();
//        meanQuads();
        findSquare();
    }

    public static void findSquare() {
        try (BufferedReader sc = new BufferedReader(new InputStreamReader(System.in))) {
            Integer[] dimensions = Arrays.stream(sc.readLine().split(" "))
                    .map(Integer::parseInt)
                    .toArray(Integer[]::new);
            int n = Integer.parseInt(sc.readLine());
            int a = dimensions[2];
            int b = dimensions[3];
            int[][] board = new int[dimensions[0]][dimensions[1]];
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[i].length; j++) {
                    board[i][j] = 1;
                }
            }
            for (int i = 0; i < n; i++) {
                String [] note = sc.readLine().split(" ");
                int x1 = Integer.parseInt(note[0]);
                int y1 = Integer.parseInt(note[1]);
                int x2 = Integer.parseInt(note[2]);
                int y2 = Integer.parseInt(note[3]);
                for (int j = x1; j <= x2; j++) {
                    for (int k = y1; k <= y2; k++) {
                        board[j][k] = 0;
                    }
                }
            }
            Point[][] dp = new Point[board.length + 1][board.length + 1];
            Point maxP = new Point(0, 0);
            int fX1 = 0, fY1 = 0, fx2 = 0, fy2 = 0;
            for (int i = 1; i < dp.length; i++) {
                for (int j = 1; j < dp.length; j++) {
                    if (board[i - 1][j - 1] == 1) {
                        int x = Math.min(dp[i - 1][j].x(), dp[i - 1][j - 1].x());
                        int y= Math.min(dp[i][j - 1].y(), dp[i - 1][j - 1].y());
                        dp[i][j] = new Point(x + 1, y + 1);
                    }
                    if (dp[i][j].x() > maxP.x() && dp[i][j].y() > maxP.y()) {
                        maxP = dp[i][j];
                        fx2 = i;
                        fy2 = j;
                        fx2 = i - maxP.x();
                        fy2 = j - maxP.y();
                    }
                }
            }
            System.out.printf("%x1 %x2 %x3 %x4", fX1, fY1, fx2, fy2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static double sqrt(double d) {
        double sqrt = Double.longBitsToDouble( ( ( Double.doubleToLongBits( d )-(1l<<52) )>>1 ) + ( 1l<<61 ) );
        double better = (sqrt + d/sqrt)/2.0;
        return  (better + d/better)/2.0;
    }

    public static void meanQuads() throws IOException {
        BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(sc.readLine());
        double[] partSums = new double[n + 1];
        partSums[0] = 0;
        String[] values = sc.readLine().split(" ");
        int q = Integer.parseInt(sc.readLine());
        String[][] vs = new String[q][];
        for (int i = 0; i < q; i++) {
            vs[i] = sc.readLine().split(" ");
        }
        sc.close();
        Map<Double, Double> quads = new HashMap<>();
        for (int i = 0; i < n; i++) {
            double x = Double.parseDouble(values[i]);
            if (!quads.containsKey(x)) {
                quads.put(x, x * x);
            }
            partSums[i + 1] = partSums[i] + quads.get(x);
        }
        PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));
        Map<Point, Double> cache = new HashMap<>();
        for (int i = 0; i < q; i++) {
            int l = Integer.parseInt(vs[i][0]);
            int r =  Integer.parseInt(vs[i][1]);
            Point p = new Point(l, r);
            if (!cache.containsKey(p)) {
                cache.put(p, Math.sqrt((partSums[r + 1] - partSums[l]) / (r - l + 1)));
            }
            pw.printf("%.6f\n", cache.get(p));
        }
        pw.flush();
        pw.close();
    }

    public static void taskLast() {
        try (BufferedReader sc = new BufferedReader(new InputStreamReader(System.in))) {
            int n = Integer.parseInt(sc.readLine());
            List<Long> arrList = new ArrayList<>();
            String[] values = sc.readLine().split(" ");
            Set<Long> set = new HashSet<>();
            for (int i = 0; i < n; i++) {
                long next = Long.parseLong(values[i]);
                if (set.contains(next)) {
                    arrList.add(next);
                } else {
                    set.add(next);
                }
            }
            System.out.println(arrList.size());
            arrList.forEach(x -> System.out.print(x + " "));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
