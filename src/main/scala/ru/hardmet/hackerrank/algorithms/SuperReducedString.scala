package ru.hardmet.hackerrank.algorithms

import scala.io.StdIn

object SuperReducedString {

  def superReducedString(s: String): String = {
    @scala.annotation.tailrec
    def reduceWithAcc(s: List[Char], acc: List[Char]): List[Char] = {
      s match {
        case Nil => acc
        case c1 :: c2 :: tail if c1 == c2 => acc match {
          case Nil => reduceWithAcc(tail, Nil)
          case accH :: accTail => reduceWithAcc(accH :: tail, accTail)
        }
        case h :: tail => reduceWithAcc(tail, h :: acc)
      }
    }
    reduceWithAcc(s.toList, Nil).reverse match {
      case Nil => "Empty String"
      case h :: t => (h :: t).mkString("")
    }
  }

  def main(args: Array[String]) {
    val s = StdIn.readLine
    val result = superReducedString(s)
    println(result)
  }
}
