@scala.annotation.tailrec
def gcdBig(x: BigInt, y: BigInt): BigInt = if (x == 0) y else gcdBig(y % x, x)

def memoizedGCD(a: BigInt, b: BigInt, cache: Map[(BigInt, BigInt), BigInt]): (BigInt, Map[(BigInt, BigInt), BigInt]) = {
  val gcdRes = cache.get((a, b)).orElse(cache.get((b, a))).getOrElse(gcdBig(a, b))
  gcdRes -> (cache + ((a, b) -> gcdRes))
}


val numbers = (1 to 2019).map(x => BigInt(2).pow(x).+(BigInt(1)))
println(numbers.length)

(1 to 321).foldLeft(List.empty[String]) {
  case (n, _) =>
    s"${n.headOption.getOrElse("")}9" :: n
}.map(BigInt(_)).sum.toString().toList.map{
  c => c.toString.toInt
}.sum
322
7 + 8 + 9 + 318
'1'.toString.toInt
