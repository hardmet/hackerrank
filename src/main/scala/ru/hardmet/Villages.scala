package ru.hardmet

object Villages extends App {

  import java.io.{BufferedReader, InputStreamReader}

  val reader = new BufferedReader(new InputStreamReader(System.in))
  val ps = System.out
  /*
  20 30
  5 10 15
   */
  val kn = reader.readLine()
  val splitted = kn.split(" ")
  val k: Long = splitted(0).toLong
  val n: Int = splitted(1).toInt

  val ai = reader.readLine().split(" ").map(_.toLong)
  val lastToFirst = ai(0) + k - ai(n - 1)
  val prev = ai(n - 1) - ai(n - 2)
  val res: Long = if (lastToFirst < prev) {
    k - (ai(n - 1) - ai(n - 2))
  } else ai(n - 1) - ai(0)
  ps.write(res.toString.getBytes)
  ps.close()
  reader.close()
}
