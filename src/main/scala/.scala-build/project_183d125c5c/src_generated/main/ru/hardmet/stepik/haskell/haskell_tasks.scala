
package ru.hardmet.stepik.haskell



object haskell_tasks {
/*<script>*/import ru.hardmet.stepik.haskell.Part5._

val f = toLogger((a: Int) => a + 1, "add 1")
val g = toLogger((a: Int) => a * 2, "multiply 2")
println(execLoggers(f, g, 0))
println(execLoggers(f, g, 11))
println(execLoggers(f, g, 2))
println(execLoggers(g, f, 0))/*</script>*/ /*<generated>*/
def args = haskell_tasks_sc.args$
  /*</generated>*/
}

object haskell_tasks_sc {
  private var args$opt0 = Option.empty[Array[String]]
  def args$set(args: Array[String]): Unit = {
    args$opt0 = Some(args)
  }
  def args$opt: Option[Array[String]] = args$opt0
  def args$: Array[String] = args$opt.getOrElse {
    sys.error("No arguments passed to this script")
  }
  def main(args: Array[String]): Unit = {
    args$set(args)
    haskell_tasks.hashCode() // hasCode to clear scalac warning about pure expression in statement position
  }
}

