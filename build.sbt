import Dependencies._

ThisBuild / scalaVersion     := "2.13.12"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "ru.hardmet"
ThisBuild / organizationName := "hardmet"

lazy val root = (project in file("."))
  .settings(
    name := "hackerrank",
    libraryDependencies += scalaTest % Test,
    libraryDependencies += "org.scalamock" %% "scalamock" % "5.1.0" % Test
  )
