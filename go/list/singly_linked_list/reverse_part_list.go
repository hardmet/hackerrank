package singlylinkedlist

func reverseBetween(head *ListNode, left int, right int) *ListNode {
	if head == nil || left == right {
		return head
	}

	prefix := &ListNode{Val: 0}
	prefix.Next = head
	prev := prefix
	for i := 0; i < left-1; i++ {
		prev = prev.Next
	}
	part := prev.Next
	for i := 0; i < right-left; i++ {
		nextNode := part.Next
		part.Next = nextNode.Next
		nextNode.Next = prev.Next
		prev.Next = nextNode
	}
	return prefix.Next
}