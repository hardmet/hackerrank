package singlylinkedlist

func splitListToParts(head *ListNode, k int) []*ListNode {
	length := 0
	for cur := head; cur != nil; cur = cur.Next {
		length++
	}
	batchSize := length / k
	addNodes := length % k

	res := make([]*ListNode, k)
	length = 0
	var cur, prev *ListNode = head, nil
	for i := 0; cur != nil && i < k; i, addNodes = i+1, addNodes-1 {
		res[i] = cur
		nextBatchSize := batchSize
		if addNodes > 0 {
			nextBatchSize++
		}
		for j := 0; j < nextBatchSize; j++ {
			prev = cur
			cur = cur.Next
		}
		if prev != nil {
			prev.Next = nil
		}
	}
	return res
}
