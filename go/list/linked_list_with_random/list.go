package linkedlistwithrandom

type Node struct {
	Val    int
	Next   *Node
	Random *Node
}

func (rl *Node) Copy() *Node {
	if rl == nil {
		return nil
	}
	head := &Node{Val: rl.Val, Random: rl.Random}
	var current *Node = head
	nodes := map[*Node]*Node{
		rl: head,
	}
	for rl = rl.Next; rl != nil; rl = rl.Next {
		newNode := &Node{Val: rl.Val, Random: rl.Random}
		nodes[rl] = newNode
		current.Next, current = newNode, newNode
	}
	for current = head; current != nil; current = current.Next {
		if current.Random != nil {
			current.Random = nodes[current.Random]
		}
	}
	return head
}
