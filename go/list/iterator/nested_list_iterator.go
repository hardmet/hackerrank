package iterator

type NestedInteger struct{}

func (this NestedInteger) IsInteger() bool {
	return false
}

func (this NestedInteger) GetInteger() int {
	return 0
}

func (n *NestedInteger) SetInteger(value int) {}

func (this *NestedInteger) Add(elem NestedInteger) {}

func (this NestedInteger) GetList() []*NestedInteger {
	return []*NestedInteger{}
}

type NestedIterator struct {
	stack []*NestedInteger
}

func Constructor(nestedList []*NestedInteger) *NestedIterator {
	nestedIterator := NestedIterator{stack: []*NestedInteger{}}
	nestedIterator.prepareStack(nestedList)
	return &nestedIterator
}

func (this *NestedIterator) Next() int {
	if !this.HasNext() {
		return -1
	}
	value := this.stack[len(this.stack)-1]
	this.stack = this.stack[:len(this.stack)-1]
	return value.GetInteger()
}

func (this *NestedIterator) HasNext() bool {
	for len(this.stack) > 0 && this.peekStackIsList() {
		value := this.stack[len(this.stack)-1]
		this.stack = this.stack[:len(this.stack)-1]
		this.prepareStack(value.GetList())
	}
	return len(this.stack) != 0
}

func (ni *NestedIterator) peekStackIsList() bool {
	return !ni.stack[len(ni.stack)-1].IsInteger()
}

func (ni *NestedIterator) prepareStack(nestedList []*NestedInteger) {
	for i := len(nestedList) - 1; i >= 0; i-- {
		ni.stack = append(ni.stack, nestedList[i])
	}
}
