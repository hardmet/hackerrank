package list

import (
	"container/heap"

	"github.com/hardmet/hackerrank/go/int_heap"
)

type KthLargest struct {
	k int
	h *int_heap.MinIntHeap
}

func Constructor(k int, nums []int) KthLargest {
	pq := int_heap.MinIntHeap(nums)
	heap.Init(&pq)
	for pq.Len() > k {
		heap.Pop(&pq)
	}
	return KthLargest{
		k: k,
		h: &pq,
	}
}

func (l *KthLargest) Add(val int) int {
	heap.Push(l.h, val)
	if l.h.Len() > l.k {
		heap.Pop(l.h)
	}
	return (*(*l).h)[0]
}

func KthLargestTest() {
	l := Constructor(1, []int{})
	values := []int{-3, -2, -4, 0, 4}
	for _, value := range values {
		println(l.Add(value))
	}
}
