package list

import "math"

func MaxList(a []int) int {
	if len(a) == 0 {
		return -1
	}
	m := math.MinInt
	for _, ai := range a {
		if ai > m {
			m = ai
		}
	}
	return m
}
