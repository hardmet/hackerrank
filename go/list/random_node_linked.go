package list

import "math/rand"

type RandLinkedList struct {
	Val  int
	Next *RandLinkedList
}

type Solution struct {
	head *RandLinkedList
	size int
}

func ConstructorRandLL(head *RandLinkedList) Solution {
	node, length := head, 0
	for node != nil {
		node = node.Next
		length++
	}
	return Solution{head: head, size: length}
}

func (s *Solution) GetRandom() int {
	n := rand.Intn(s.size)
	h := s.head
	for n > 0 {
		h = h.Next
		n--
	}
	return h.Val
}
