package list

type DoubleIntLinkedList struct {
	value int
	next  *DoubleIntLinkedList
	prev  *DoubleIntLinkedList
}
