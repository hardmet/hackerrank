package priority_queue

type SliceToInt []int

func (s SliceToInt) ToInt() int {
	return s[0]
}
