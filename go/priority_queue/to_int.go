package priority_queue

type ToIntInterface interface {
	ToInt() int
}

type SecondaryToIntInterface interface {
	ToIntInterface
	SecondaryToInt() int
}

type ToInt64Interface interface {
	ToInt64() int64
}

type BaseSecondary struct {
	ToIntInterface
}

func (bs *BaseSecondary) SecondaryToInt() int {
	return bs.ToInt()
}
