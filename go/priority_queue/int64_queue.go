package priority_queue

type PriorityQueueInt64 []ToInt64Interface

func (pq *PriorityQueueInt64) Len() int { return len(*pq) }

func (pq *PriorityQueueInt64) Less(i, j int) bool {
	return (*pq)[i].ToInt64()-(*pq)[j].ToInt64() > 0
}

func (pq *PriorityQueueInt64) Swap(i, j int) {
	(*pq)[i], (*pq)[j] = (*pq)[j], (*pq)[i]
}

func (pq *PriorityQueueInt64) Push(x interface{}) {
	*pq = append(*pq, x.(ToInt64Interface))
}

func (pq *PriorityQueueInt64) Pop() interface{} {
	p := (*pq)[len(*pq)-1]
	(*pq)[len(*pq)-1] = nil // avoid memory leak
	*pq = (*pq)[:len(*pq)-1]
	return p
}

func (pq *PriorityQueueInt64) Peek() interface{} {
	return (*pq)[0]
}
