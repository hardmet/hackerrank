package priority_queue

type PriorityQueueToInt []ToIntInterface

func (pq *PriorityQueueToInt) Len() int { return len(*pq) }

func (pq *PriorityQueueToInt) Less(i, j int) bool {
	return (*pq)[i].ToInt()-(*pq)[j].ToInt() > 0
}

func (pq *PriorityQueueToInt) Swap(i, j int) {
	(*pq)[i], (*pq)[j] = (*pq)[j], (*pq)[i]
}

func (pq *PriorityQueueToInt) Push(x interface{}) {
	*pq = append(*pq, x.(ToIntInterface))
}

func (pq *PriorityQueueToInt) Pop() interface{} {
	p := (*pq)[len(*pq)-1]
	(*pq)[len(*pq)-1] = nil // avoid memory leak
	*pq = (*pq)[:len(*pq)-1]
	return p
}

func (pq *PriorityQueueToInt) Peek() interface{} {
	return (*pq)[0]
}
