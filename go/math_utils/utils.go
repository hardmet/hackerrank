package math_utils

func Min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func Max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func Min64(x, y int64) int64 {
	if x < y {
		return x
	}
	return y
}

func Max64(x, y int64) int64 {
	if x > y {
		return x
	}
	return y
}

func Abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func Abs64(a int64) int64 {
	if a < 0 {
		return -a
	}
	return a
}

func AbsDiff(a, b int) int {
	if a > b {
		return a - b
	}
	return b - a
}
