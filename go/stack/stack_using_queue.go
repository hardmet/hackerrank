package stack

type Queue []int

func (q *Queue) Pop() interface{} {
	p := (*q)[0]
	*q = (*q)[1:]
	return p
}

func (q *Queue) Peek() interface{} {
	return (*q)[0]
}

func (q *Queue) Push(x interface{}) {
	*q = append(*q, x.(int))
}

func (q *Queue) Len() int { return len(*q) }

type MyStack struct {
	q1 Queue
	q2 Queue
}

func ConstructorStackQueue() MyStack {
	return MyStack{
		q1: Queue{},
		q2: Queue{},
	}
}

func (ms *MyStack) Push(x int) {
	ms.q1.Push(x)
}

func (ms *MyStack) Pop() int {
	for ms.q1.Len() > 1 {
		ms.q2.Push(ms.q1.Pop())
	}
	value := ms.q1.Pop()
	ms.q1, ms.q2 = ms.q2, ms.q1
	return value.(int)
}

func (ms *MyStack) Top() int {
	for ms.q1.Len() > 1 {
		ms.q2.Push(ms.q1.Pop())
	}
	value := ms.q1.Pop()
	ms.q2.Push(value)
	ms.q1, ms.q2 = ms.q2, ms.q1
	return value.(int)
}

func (ms *MyStack) Empty() bool {
	return ms.q1.Len() == 0 && ms.q2.Len() == 0
}
