package set

type IntSet struct {
	m map[int]struct{}
}

func NewIntSet() *IntSet {
	s := &IntSet{}
	s.m = make(map[int]struct{})
	return s
}

func (s *IntSet) Add(value int) {
	s.m[value] = struct{}{}
}

func (s *IntSet) Remove(value int) {
	delete(s.m, value)
}

func (s *IntSet) Contains(value int) bool {
	_, c := s.m[value]
	return c
}
