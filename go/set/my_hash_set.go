package set

import "math"

type MyHashSet struct {
	values [][]int
	size   int
	cap    int
}

func Constructor() MyHashSet {
	return MyHashSet{[][]int{}, 0, 16}
}

func (s *MyHashSet) Add(key int) {
	if !s.Contains(key) {
		i := hash(key, s.cap)
		s.values[i] = append(s.values[i], key)
	}
}

func (s *MyHashSet) Remove(key int) {
	i := hash(key, s.cap)
	for j, vi := range s.values[i] {
		if vi == key {
			s.values[i] = append(s.values[i][:j], s.values[i][j+1:]...)
			break
		}
	}
}

func (s *MyHashSet) Contains(key int) bool {
	i := hash(key, s.cap)
	for _, vi := range s.values[i] {
		if vi == key {
			return true
		}
	}
	return false
}

func hash(ai, cap int) int {
	var h float64 = 0
	var i float64 = 0
	var ai64 = float64(ai)
	for ; i < 4; i++ {
		h += ai64 * math.Pow(263.0, i)
	}
	return int(h) % 1_000_000_007 % cap
}
