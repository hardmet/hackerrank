package custom

type SmallestInfiniteSet struct {
	values   [1000]bool
	smallest int
}

func Constructor() SmallestInfiniteSet {
	s := SmallestInfiniteSet{}
	for i := 0; i < len(s.values); i++ {
		s.values[i] = true
	}
	s.smallest = 0
	return s
}

func (this *SmallestInfiniteSet) PopSmallest() int {
	res := this.smallest
	this.values[this.smallest] = false
	for i := this.smallest + 1; i < len(this.values); i++ {
		if this.values[i] {
			this.smallest = i
			break
		}
	}
	return res
}

func (this *SmallestInfiniteSet) AddBack(num int) {
	this.values[num-1] = true
}
