package tree_set

import (
	"container/heap"

	"github.com/hardmet/hackerrank/go/int_heap"
)

type IntTreeSet struct {
	values int_heap.MinIntHeap
	keys   map[int]struct{}
}

func (h *IntTreeSet) Len() int           { return len(h.keys) }
func (h *IntTreeSet) Less(i, j int) bool { return (*h).values[i] < (*h).values[j] }
func (h *IntTreeSet) Swap(i, j int)      { h.values[i], h.values[j] = h.values[j], h.values[i] }

func (h *IntTreeSet) Push(x interface{}) {
	if _, contains := h.keys[x.(int)]; !contains {
		heap.Push(&h.values, x)
		h.keys[x.(int)] = struct{}{}
	}
}

func (h *IntTreeSet) Pop() interface{} {
	p := heap.Pop(&h.values)
	delete(h.keys, p.(int))
	return p
}
