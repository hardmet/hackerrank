package main

import (
	// "github.com/hardmet/hackerrank/go/exercises"
	"github.com/hardmet/hackerrank/go/homework/lesson6"
	// "github.com/hardmet/hackerrank/go/homework/tests"
)

func main() {
	// tests.FirstLessonTest()
	// tests.SumChannelsTest()
	// tests.WorkGroupTest()
	// tests.CondTest()
	// tests.SerializationTest()
	// tests.ReadAndReverseLinesTest()
	// tests.ReadAndValidateJsonTest()
	// tests.ReadAndReverseFileLinesTest()
	// tests.TestUpdateMap()
	// tests.RaiseAndRecoverTest()
	lesson6.TestShutdownWorkerPool()
	// lesson7.LoadConfig()

	// exercises.RescueBoatsTests()
	//stack := []int{0, 1, 2}
	//stack = stack[0 : len(stack)-2]
	//fmt.Println(stack)
	//stones := [][]int{{0, 0}, {0, 1}, {1, 0}, {1, 2}, {2, 1}, {2, 2}}
	//fmt.Println(exercises.RemoveStones(stones))
	//
	//stones = [][]int{{0, 0}, {0, 2}, {1, 1}, {2, 0}, {2, 2}}
	//fmt.Println(exercises.RemoveStones(stones))
	//
	//stones = [][]int{{0, 0}}
	//fmt.Println(exercises.RemoveStones(stones))
	//fmt.Println(string(bs))
}
