package graph

type Node struct {
	Val       int
	Neighbors []*Node
}

func DeepCopy(source *Node) *Node {
	if source == nil {
		return nil
	}
	m := map[*Node]*Node{}

	var dfs func(node *Node)
	dfs = func(node *Node) {
		if node != nil {
			if _, ok := m[node]; !ok {
				newNode := &Node{Val: node.Val}
				m[node] = newNode

				for _, neighbor := range node.Neighbors {
					dfs(neighbor)
					newNode.Neighbors = append(newNode.Neighbors, m[neighbor])
				}
			}
		}
	}

	dfs(source)
	return m[source]
}

func toSlice(source *Node) [][]int {
	graph := make([][]int, 101)
	stack := []*Node{source}
	visited := map[int]struct{}{}
	for len(stack) > 0 {
		head := stack[0]
		stack = stack[1:]
		if _, contains := visited[head.Val]; !contains {
			visited[head.Val] = struct{}{}
			stack = append(stack, head.Neighbors...)
		loop1:
			for _, neighbor := range head.Neighbors {
				for i := 0; i < len(graph[head.Val]); i++ {
					if graph[head.Val][i] == neighbor.Val {
						continue loop1
					}
				}
				graph[head.Val] = append(graph[head.Val], neighbor.Val)
			}
		}
	}
	return graph
}

func fromSlice(graph [][]int) *Node {
	mp := map[int]*Node{}
	for i := len(graph) - 1; i >= 0; i-- {
		if len(graph[i]) > 0 {
			mp[i] = &Node{Val: i, Neighbors: []*Node{}}
		}
	}
	for i, node := range mp {
		for _, j := range graph[i] {
			node.Neighbors = append(node.Neighbors, mp[j])
		}
	}
	return mp[1]
}

func TestClone() {
	node1 := Node{1, nil}
	node2 := Node{2, nil}
	node3 := Node{3, nil}
	node4 := Node{4, nil}
	node1.Neighbors = []*Node{&node2, &node4}
	node2.Neighbors = []*Node{&node1, &node3}
	node3.Neighbors = []*Node{&node2, &node4}
	node4.Neighbors = []*Node{&node1, &node3}
	h2 := DeepCopy(&node1)
	println(h2)
}
