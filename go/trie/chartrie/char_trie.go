package chartrie

type CharTrie struct {
	value    string
	children [26]*CharTrie
}

func Constructor() CharTrie {
	return CharTrie{}
}

func (this *CharTrie) Insert(word string) {
	node := this
	for _, letter := range word {
		i := letter - 'a'
		child := node.children[i]
		if child == nil {
			child = &CharTrie{}
			node.children[i] = child
		}
		node = child
	}
	node.value = word
}

func (this *CharTrie) Search(word string) bool {
	found := this.find(word)
	return found != nil && found.value == word
}

func (this *CharTrie) StartsWith(prefix string) bool {
	return this.find(prefix) != nil
}

func (this *CharTrie) find(prefix string) *CharTrie {
	node := this
	for _, letter := range prefix {
		i := letter - 'a'
		child := node.children[i]
		if child == nil {
			return nil
		}
		node = child
	}
	return node
}
