package trie

import "strings"

type Trie struct {
	word        string
	height      int
	left, right *Trie
}

func ConstructorTrie() Trie {
	return Trie{}
}

func (this *Trie) GetL() *Trie {
	return this.left
}

func (this *Trie) GetVal() string {
	return this.word
}

func (this *Trie) GetR() *Trie {
	return this.right
}

func (this *Trie) getHeight() int {
	if this == nil {
		return 0
	}
	return this.height
}

func (this *Trie) Insert(word string) {
	if len(this.word) == 0 {
		*this = Trie{word: word, height: 1}
	} else {
		if word != this.word {
			if word < this.word {
				if this.left == nil {
					this.left = &Trie{word: word, height: 1}
				} else {
					this.left.Insert(word)
				}
			} else if word > this.word {
				if this.right == nil {
					this.right = &Trie{word: word, height: 1}
				} else {
					this.right.Insert(word)
				}
			}
			this.height = 1 + max(this.left.getHeight(), this.right.getHeight())
			balance := this.left.getHeight() - this.right.getHeight()
			if balance > 1 && word < this.left.word {
				rightRotate(this)
			} else if balance > 1 && word > this.left.word {
				leftRotate(this.left)
				rightRotate(this)
			} else if balance < -1 && word > this.right.word {
				leftRotate(this)
			} else if balance < -1 && word < this.right.word {
				rightRotate(this.right)
				leftRotate(this)
			}
		}
	}
}

func (this *Trie) Search(word string) bool {
	if this == nil {
		return false
	}
	if word < this.word {
		return this.left.Search(word)
	}
	if word > this.word {
		return this.right.Search(word)
	}
	return true
}

func (this *Trie) StartsWith(prefix string) bool {
	if this == nil {
		return false
	}
	if strings.HasPrefix(this.word, prefix) {
		return true
	}
	if prefix < this.word {
		return this.left.StartsWith(prefix)
	} else {
		return this.right.StartsWith(prefix)
	}
}

func leftRotate(node *Trie) {
	root := *node
	root.right = root.right.left
	*node = *node.right
	node.left = &root
	node.left.height = 1 + max(node.left.left.getHeight(), node.left.right.getHeight())
	node.height = 1 + max(node.left.getHeight(), node.right.getHeight())
}

func rightRotate(node *Trie) {
	root := *node
	root.left = root.left.right
	*node = *node.left
	node.right = &root
	node.right.height = 1 + max(node.right.left.getHeight(), node.right.right.getHeight())
	node.height = 1 + max(node.left.getHeight(), node.right.getHeight())
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}
