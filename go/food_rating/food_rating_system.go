package foodrating

import (
	"container/heap"
)

type FoodRatings struct {
	foodRatingMap  map[string]int
	foodCuisineMap map[string]string
	cuisinesMap    map[string]*MaxHeap
}

func Constructor(foods []string, cuisines []string, ratings []int) FoodRatings {
	foodRatingMap := make(map[string]int)
	foodCuisineMap := make(map[string]string)
	cuisineHeapMap := make(map[string]*MaxHeap)
	for i := 0; i < len(foods); i++ {
		foodRatingMap[foods[i]] = ratings[i]
		foodCuisineMap[foods[i]] = cuisines[i]
		if _, contains := cuisineHeapMap[cuisines[i]]; !contains {
			cuisineHeapMap[cuisines[i]] = &MaxHeap{}
		}
		heap.Push(cuisineHeapMap[cuisines[i]], Food{foods[i], ratings[i]})
	}
	return FoodRatings{
		foodRatingMap:  foodRatingMap,
		foodCuisineMap: foodCuisineMap,
		cuisinesMap:    cuisineHeapMap,
	}
}

func (fr *FoodRatings) ChangeRating(food string, newRating int) {
	fr.foodRatingMap[food] = newRating
	heap.Push(fr.cuisinesMap[fr.foodCuisineMap[food]], Food{food, newRating})
}

func (fr *FoodRatings) HighestRated(cuisine string) string {
	highRated := (*fr.cuisinesMap[cuisine])[0]
	for highRated.Rating != fr.foodRatingMap[highRated.Name] {
		heap.Pop(fr.cuisinesMap[cuisine])
		highRated = (*fr.cuisinesMap[cuisine])[0]
	}
	return highRated.Name
}

type Food struct {
	Name   string
	Rating int
}

type MaxHeap []Food

func (h MaxHeap) Len() int { return len(h) }

func (h MaxHeap) Less(i, j int) bool {
	if h[i].Rating == h[j].Rating {
		return h[i].Name < h[j].Name
	}
	return h[i].Rating > h[j].Rating
}

func (h MaxHeap) Swap(i, j int) { h[i], h[j] = h[j], h[i] }

func (h *MaxHeap) Push(x interface{}) {
	*h = append(*h, x.(Food))
}

func (h *MaxHeap) Pop() interface{} {
	x := (*h)[len(*h)-1]
	*h = (*h)[:len(*h)-1]
	return x
}
