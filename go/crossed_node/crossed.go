package crossed_node

type Node struct {
	Val         bool
	IsLeaf      bool
	TopLeft     *Node
	TopRight    *Node
	BottomLeft  *Node
	BottomRight *Node
}

func Construct(grid [][]int) *Node {
	return nodeFabric(grid, 0, 0, len(grid), len(grid[0]))
}

func nodeFabric(grid [][]int, rowStart, colStart, rowEnd, colEnd int) *Node {
	for i := rowStart; i < rowEnd; i++ {
		for j := colStart; j < colEnd; j++ {
			if grid[i][j] != grid[rowStart][colStart] {
				return &Node{
					TopLeft:     nodeFabric(grid, rowStart, colStart, (rowStart+rowEnd)/2, (colStart+colEnd)/2),
					TopRight:    nodeFabric(grid, rowStart, (colStart+colEnd)/2, (rowStart+rowEnd)/2, colEnd),
					BottomLeft:  nodeFabric(grid, (rowStart+rowEnd)/2, colStart, rowEnd, (colStart+colEnd)/2),
					BottomRight: nodeFabric(grid, (rowStart+rowEnd)/2, (colStart+colEnd)/2, rowEnd, colEnd),
				}
			}
		}
	}
	return &Node{IsLeaf: true, Val: grid[rowStart][colStart] == 1}
}
