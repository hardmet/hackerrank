package int_heap

type MaxIntHeap struct {
	IntHeap
}

func (h *MaxIntHeap) Less(i, j int) bool {
	return h.IntHeap[i] >= h.IntHeap[j]
}
