package int_heap

import (
	"github.com/hardmet/hackerrank/go/tuples"
)

type MinIntPairHeap []tuples.Pair

func (h MinIntPairHeap) Len() int           { return len(h) }
func (h MinIntPairHeap) Less(i, j int) bool { return h[i].Fst < h[j].Fst }
func (h MinIntPairHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *MinIntPairHeap) Push(x interface{}) {
	*h = append(*h, x.(tuples.Pair))
}

func (h *MinIntPairHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}
