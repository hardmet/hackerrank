package int_heap

type MinIntHeap IntHeap

func (h *MinIntHeap) Less(i, j int) bool { return (*h)[i] < (*h)[j] }
func (h *MinIntHeap) Len() int           { return (*IntHeap)(h).Len() }
func (h *MinIntHeap) Swap(i, j int)      { (*IntHeap)(h).Swap(i, j) }
func (h *MinIntHeap) Peek() int          { return (*IntHeap)(h).Peek() }

func (h *MinIntHeap) Push(x interface{}) {
	(*IntHeap)(h).Push(x)
}

func (h *MinIntHeap) Pop() interface{} {
	return (*IntHeap)(h).Pop()
}
