package sort

func QuickSort(arr []int) {
	if len(arr) > 1 {
		l, r, pivot := 0, len(arr)-1, len(arr)/2
		l = partitionQS(&arr, l, r, pivot)
		QuickSort(arr[:l])
		QuickSort(arr[l+1:])
	}
}

func partitionQS(arr *[]int, l, r, p int) int {
	(*arr)[p], (*arr)[r] = (*arr)[r], (*arr)[p]
	for i := range *arr {
		if (*arr)[i] < (*arr)[r] {
			(*arr)[i], (*arr)[l] = (*arr)[l], (*arr)[i]
			l++
		}
	}
	(*arr)[l], (*arr)[r] = (*arr)[r], (*arr)[l]
	return l
}
