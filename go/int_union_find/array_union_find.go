package int_union_find

type ArrayUnionFind struct {
	parent, rank []int
}

func ArrayUFConstructor(size int) ArrayUnionFind {
	parent := make([]int, size)
	for i := range parent {
		parent[i] = i
	}
	return ArrayUnionFind{parent: parent, rank: make([]int, size)}
}

func (uf *ArrayUnionFind) Find(x int) int {
	if uf.parent[x] != x {
		uf.parent[x] = uf.Find(uf.parent[x])
	}
	return uf.parent[x]
}

func (uf *ArrayUnionFind) Union(x, y int) bool {
	xset, yset := uf.Find(x), uf.Find(y)
	if xset != yset {
		if uf.rank[xset] < uf.rank[yset] {
			uf.parent[xset] = yset
		} else if uf.rank[xset] > uf.rank[yset] {
			uf.parent[yset] = xset
		} else {
			uf.parent[yset] = xset
			uf.rank[xset]++
		}
		return true
	}
	return false
}
