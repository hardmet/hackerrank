package int_union_find

type MapUnionFind struct {
	parent map[int]int
}

func MapUFConstructor() MapUnionFind {
	return MapUnionFind{parent: map[int]int{}}
}

func (uf *MapUnionFind) Find(x int) int {
	if x != uf.parent[x] {
		uf.parent[x] = uf.Find(uf.parent[x])
	}
	return uf.parent[x]
}

func (uf *MapUnionFind) Union(x, y int) {
	if _, contains := uf.parent[x]; !contains {
		uf.parent[x] = x
	}
	if _, contains := uf.parent[y]; !contains {
		uf.parent[y] = y
	}
	rootX := uf.Find(x)
	rootY := uf.Find(y)
	uf.parent[rootX] = rootY
}

func (uf *MapUnionFind) GetParent() map[int]int {
	return uf.parent
}
