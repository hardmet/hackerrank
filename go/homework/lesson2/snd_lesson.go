package lesson2

import (
	"errors"
	"fmt"
	"math"
	"strings"
	"unicode"
	"unicode/utf8"
)

func add(a int32) int32 {
	return a + 1
}

const R float64 = 6371

func Distance(lat1, lon1 float32, lat2, lon2 float32) float32 {
	lat1Rad, lat2Rad, lon1Rad, lon2Rad := toRadians(lat1), toRadians(lat2), toRadians(lon1), toRadians(lon2)
	dLat := lat2Rad - lat1Rad
	dLon := lon2Rad - lon1Rad

	ans := math.Pow(math.Sin(dLat/2), 2) + math.Cos(lat1Rad)*math.Cos(lat2Rad)*math.Pow(math.Sin(dLon/2), 2)

	return float32(R * float64(2) * math.Asin(math.Sqrt(ans)))
}

func toRadians(degree float32) float64 {
	var oneDeg float64 = math.Pi / 180
	return oneDeg * float64(degree)
}

func distanceCompare(lat1, lon1 float32, lat2, lon2 float32) bool {
	return Distance(lat1, lon1, lat2, lon2) <= 0.000000001
}

func Factorial(n int64) int64 {
	var i int64
	var p int64 = 1
	for i = 1; i <= n; i++ {
		p *= i
		if p == 0 {
			fmt.Println("stop")
		}
	}
	return p
}

func factorialRec(n int64) int64 {
	if n == 1 || n == 0 {
		return 1
	} else {
		r := n * factorialRec(n-1)
		return r
	}
}

func reverseSlice(slice []int) []int {
	for i := 0; i < len(slice)/2; i++ {
		slice[i], slice[len(slice)-i-1] = slice[len(slice)-i-1], slice[i]
	}
	return slice
}

func ReverseString(s string) (string, error) {
	if !utf8.ValidString(s) {
		return s, errors.New("input is not valid UTF-8")
	}
	r := []rune(s)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r), nil
}

func removeDuplicates(slice []int) []int {
	uniqElements := make(map[int]struct{})
	uniqSlice := make([]int, 0)
	for _, n := range slice {
		if _, contains := uniqElements[n]; !contains {
			uniqSlice = append(uniqSlice, n)
			uniqElements[n] = struct{}{}
		}
	}
	return uniqSlice
}

func Abs32(x float32) float32 {
	return math.Float32frombits(math.Float32bits(x) &^ (1 << 31))
}

func frequentWord(str string) string {
	words := strings.Split(str, " ")
	frequencies := make(map[string]int)
	for _, word := range words {
		frequencies[word]++
	}
	maxCount := 0
	var maxWord *string
	for word, count := range frequencies {
		if count > maxCount {
			maxCount = count
			maxWord = &word
		}
	}
	return *maxWord
}

func mapKeyIntersect(m1 map[int]struct{}, m2 map[int]struct{}) []int {
	intersection := []int{}
	for n := range m1 {
		if _, contains := m2[n]; contains {
			intersection = append(intersection, n)
		}
	}
	return intersection
}

func toFrequencyMap(s []string) map[string]int {
	frequencies := make(map[string]int)
	for _, str := range s {
		frequencies[str]++
	}
	return frequencies
}

func reverse(s string) string {
	sRunes := []rune(s)
	for i := 0; i < len(sRunes)/2; i++ {
		sRunes[i], sRunes[len(sRunes)-i-1] = sRunes[len(sRunes)-i-1], sRunes[i]
	}
	return string(sRunes)
}

func removeSpaces(s string) string {
	newRunes := []rune{}
	for _, r := range s {
		if r != ' ' {
			newRunes = append(newRunes, r)
		}
	}
	return string(newRunes)
}

func frequentRune(str string) rune {
	frequencies := make(map[rune]int)
	for _, r := range str {
		frequencies[r]++
	}
	maxRune, maxFrequency := ' ', 0
	for r, f := range frequencies {
		if maxFrequency < f {
			maxRune = r
			maxFrequency = f
		}
	}
	return maxRune
}

func stringLengthWithoutSpaces(str string) int {
	length := 0
	for _, r := range str {
		if !unicode.IsSpace(r) {
			length++
		}
	}
	return length
}

func Swap(a *int, b *int) {
	*a, *b = *b, *a
}
