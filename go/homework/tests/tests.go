package tests

import (
	"encoding/json"
	"fmt"
	"math"
	"os"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/hardmet/hackerrank/go/homework/lesson2"
	"github.com/hardmet/hackerrank/go/homework/lesson3"
	"github.com/hardmet/hackerrank/go/homework/lesson4"
)

func FirstLessonTest() {
	fmt.Println(math.MaxInt32)
	var lt1, ln1, lt2, ln2 float32 = 53.32055555555556, -1.7297222222222221, 53.31861111111111, -1.6997222222222223
	fmt.Println(lesson2.Distance(lt1, ln1, lt2, ln2))
	// fmt.Println(factorialRec(9586958))
	x, y := 10, 29
	p1 := &x
	p2 := &y
	lesson2.Swap(p1, p2)
	fmt.Println(x)
	fmt.Println(y)
}

func SumChannelsTest() {
	var chs []chan int64

	for i := 1; i <= 100; i++ {
		var ch = make(chan int64, 1000)
		var num int64
		for num = 1; num <= 1000; num++ {
			ch <- num
		}
		chs = append(chs, ch)
		close(ch)
	}

	result := lesson3.SumChannels(chs)
	fmt.Println(result)
}

func WorkGroupTest() {
	const totalTasks = 10
	const totalWorkers = 5
	tasks := make(chan string, totalTasks)
	out := make(chan string, totalTasks)
	wg := sync.WaitGroup{}
	for i := 1; i <= totalWorkers; i++ {
		wg.Add(1)
		worker := lesson3.NewWorker(tasks, &wg, out)
		go worker.Run()
	}

	words := []string{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"}
	go func() {
		for i := range totalTasks {
			tasks <- words[i]
		}
		close(tasks)
	}()
	wg.Wait()
	for range totalTasks {
		fmt.Println(<-out)
	}
	close(out)
}

func TestUpdateMap() {
	text := "The toolbar buttons offers several controls. The Use buttons the first two buttons for undoing and redoing The edits. Adjust the minimum word buttons length and minimum number of buttons repeats using the sliders The third and fourth buttons The buttons on smaller screens). Toggle the fifth control to switch The between case-sensitive and insensitive modes. The word and character The counts are buttons displayed on the right side."
	words := strings.Fields(text)

	expected := map[string]int{}
	for _, word := range words {
		expected[word]++
	}
	for word, count := range expected {
		if count == 1 {
			delete(expected, word)
		}
	}
	wg := sync.WaitGroup{}

	m1 := map[string]int{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for _, word := range words[:len(words)/2] {
			// go lesson3.UpdateMap(word, &m1)
			func() { m1[word]++ }()
		}
	}()
	wg.Add(1)
	go func() {
		defer wg.Done()
		for _, word := range words[len(words)/2:] {
			func() { m1[word]++ }()
			// go lesson3.UpdateMap(word, m1[word]+1, &m1)
		}
	}()

	m2 := map[string]int{}
	mt := sync.Mutex{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for _, word := range words[:len(words)/2] {
			lesson3.IncrementAndUpdateMap(word, &m2, &mt)
		}
	}()
	wg.Add(1)
	go func() {
		defer wg.Done()
		for _, word := range words[len(words)/2:] {
			lesson3.IncrementAndUpdateMap(word, &m2, &mt)
		}
	}()
	wg.Wait()

	fmt.Println("expected:")
	fmt.Println(expected)

	for word, count := range m1 {
		if count == 1 {
			delete(m1, word)
		}
	}
	fmt.Println("common map:")
	fmt.Println(m1)
	for word, count := range m2 {
		if count == 1 {
			delete(m2, word)
		}
	}
	fmt.Println("sync map:")
	fmt.Println(m2)
	// m1 = map[string]int{}
	// for _, word := range words {
	// 	if count, contains := syncMap.Load(word); contains && count.(int) > 1 {
	// 		m1[word] = count.(int)
	// 	}
	// }
}

func TestChannelsOnStructuresWithPointers() {
	// slice := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	slice := make([]int, 10, 11)
	for i := range 10 {
		slice[i] = i
	}
	ch := make(chan []int)
	// go func() { ch <- slice }()
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func(ss chan []int) {
		defer wg.Done()
		for s := range ss {
			_ = append(s, 1)
		}
	}(ch)
	fmt.Println(slice)
	ch <- slice
	close(ch)
	wg.Wait()
	// fmt.Println(slice)
}

func CondTest() {
	const workerPoolCount int = 5
	const dataChannelPool int = 10
	dataCh := make(chan int)

	go func() {
		for n := range dataChannelPool {
			// time.Sleep(200 * time.Millisecond)
			dataCh <- n
		}
		close(dataCh)
	}()
	liveCh := make(chan bool)
	go func() {
		for i := range 100000 {
			liveCh <- i == 75000
		}
		close(liveCh)
	}()
	cond := sync.NewCond(&sync.Mutex{})
	go livenessProbe(cond, liveCh)
	wg := sync.WaitGroup{}
	output := make(chan int)
	for i := 1; i <= workerPoolCount; i++ {
		wg.Add(1)
		go runWorker(dataCh, output, &wg, cond, i)
	}
	go func() {
		for sq := range output {
			fmt.Println(sq)
		}
		// wg2.Done()
	}()
	wg.Wait()
	close(output)
	// wg2 := sync.WaitGroup{}
	// wg2.Add(1)
	// wg2.Wait()
	time.Sleep(3 * time.Second)
}

func livenessProbe(c *sync.Cond, isAliveCh <-chan bool) {
	time.Sleep(1 * time.Second)
	c.L.Lock()
	for isAlive := range isAliveCh {
		// fmt.Println("check is alive")
		if isAlive {
			fmt.Println("broadcast")
			c.Broadcast()
		}
	}
	c.L.Unlock()
}

func runWorker(in <-chan int, out chan<- int, wg *sync.WaitGroup, c *sync.Cond, num int) {
	defer wg.Done()
	// fmt.Printf("Start worker %d\n", num)
	c.L.Lock()
	c.Wait()
	c.L.Unlock()
	// fmt.Printf("Number %d awaken\n", num)
	fmt.Println("finish waiting")
	for value := range in {
		// if num%2 == 1 {
		// time.Sleep(1250 * time.Millisecond)
		// fmt.Printf("wake up: %d\n", num)
		// }
		out <- value * value
		// fmt.Printf("write to out %d\n", num)
		fmt.Println("finish writing result to output")
	}
}

func SerializationTest() {
	e1 := lesson4.Example{A: 42, B: "Value"}
	e2 := lesson4.Example{A: 32}
	e3 := lesson4.Example{A: 22, B: ""}
	var e4 lesson4.Example
	// var e4 *lesson4.Example
	var data []byte
	var err error
	for _, e := range []lesson4.Example{e1, e2, e3, e4} {
		data, err = json.Marshal(e)
		printSerialized(data, err)
		o, err := lesson4.JsonToStruct(data)
		if err != nil {
			fmt.Println("Unmarshal error:", err)
		} else {
			fmt.Println("unmarshaled value:", *o)
		}
	}
	d1 := "{\"a\":42,\"B\":\"Value\"}"
	d2 := "{\"a\":32}"
	d3 := "{\"a\":22,\"B\":\"\"}"
	for _, d := range []string{d1, d2, d3} {
		o, err := lesson4.JsonToStruct([]byte(d))
		if err != nil {
			fmt.Println("Unmarshal error:", err)
		} else {
			fmt.Println("unmarshaled value:")
			fmt.Printf("%#v\n", *o)
		}
	}
}

func printSerialized(data []byte, err error) {
	if err != nil {
		fmt.Println("Marshal error:", err)
	}
	fmt.Println(string(data))
}

func ReadAndReverseLinesTest() {
	r := strings.NewReader("abcde\nefgh\nijklmn\no\n")
	res, err := lesson4.ReverseReader(r)
	if err != nil {
		fmt.Println("some goes wrong", err)
	} else {
		fmt.Println(res)
	}
}

func ReadAndReverseFileLinesTest() {
	// p := "/home/elly/space/hackerrank/go/homework/tests/serialization_jsons/deserializing_test6.json"
	e := "/home/elly/space/hackerrank/go/homework/tests/serialization_jsons/empty.json"
	lines, err := lesson4.ReadFileLinesReversed(e)
	if err != nil {
		fmt.Println("error while read and reverse file lines")
	}
	for _, line := range lines {
		fmt.Println(line)
	}
}

func ReadAndValidateJsonTest() {
	dir := "/home/elly/space/hackerrank/go/homework/tests/serialization_jsons"
	f, err := os.Open(dir)
	defer func() {
		err = f.Close()
		if err != nil {
			fmt.Println("Error during closing file", err)
		}
	}()
	if err != nil {
		fmt.Println("Error during reading directory with jsons", err)
		return
	}
	var ds []string
	ds, err = f.Readdirnames(-1)
	sort.Strings(ds)
	if err != nil {
		fmt.Println("Error during reading files in directory", err)
		return
	}
	for _, f := range ds {
		var o *lesson4.Example
		o, err = lesson4.ReadJsonToStruct(dir + "/" + f)
		if err != nil {
			fmt.Printf("parsing file %s error:", f)
			fmt.Println(err)
		} else {
			fmt.Printf("unmarshaled %s:\n", f)
			fmt.Printf("%#v\n", *o)
		}
	}
}

func RaiseAndRecoverTest() {
	// defer func() {
	// lesson4.HandlePanic()
	// lesson4.RaisePanicImplicitly("bar_error")
	// }()
	defer lesson4.HandlePanic()
	// lesson4.RaisePanicImplicitly("empty")
	lesson4.RaisePanicImplicitly("bar_error")
}
