CREATE TABLE IF NOT EXISTS notices (id serial, category int, user_id bigint);

INSERT INTO notices(category, user_id) VALUES (1, 41);
INSERT INTO notices(category, user_id) VALUES (0, 42);
INSERT INTO notices(category, user_id) VALUES (2, 41);
INSERT INTO notices(category, user_id) VALUES (2, 41);
INSERT INTO notices(category, user_id) VALUES (1, 40);
INSERT INTO notices(category, user_id) VALUES (2, 42);
INSERT INTO notices(category, user_id) VALUES (2, 42);
INSERT INTO notices(category, user_id) VALUES (0, 42);
INSERT INTO notices(category, user_id) VALUES (0, 42);
INSERT INTO notices(category, user_id) VALUES (0, 41);
