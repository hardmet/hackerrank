package lesson5

import (
	"errors"
	"fmt"
)

//go:generate mockgen -source $GOFILE -destination ./payment_gateway_mock.go -package ${GOPACKAGE}

type PaymentGateway interface {
	SendMoneyAndGetCurrentBalance(username string, amount int) (int, error)
}

type Logger interface {
	Error(message string)
	Info(message string)
}

var ErrConnection = errors.New("connection error")

func TransferMoney(e PaymentGateway, l Logger, username string, amount int) (int, error) {
	balance, err := e.SendMoneyAndGetCurrentBalance(username, amount)
	if err != nil {
		l.Error("send money error")
		return 0, fmt.Errorf("send money: %w", err)
	}
	l.Info("send money ok")
	return balance, nil
}
