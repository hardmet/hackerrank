package lesson5_test

import (
	"testing"

	"github.com/hardmet/hackerrank/go/homework/lesson5"
	"github.com/stretchr/testify/require"
	"go.uber.org/mock/gomock"
)

func TestFindRealEstateNoticesForUser(t *testing.T) {
	testCases := map[string]struct {
		userId             int64
		notices            []lesson5.Notice
		initUserService    func(us *lesson5.MockUserService)
		initNoticesService func(ns *lesson5.MockNoticeService)
		err                error
	}{
		"use user service and notice service but filter all notices": {
			userId:  42,
			notices: []lesson5.Notice{},
			initUserService: func(us *lesson5.MockUserService) {
				us.EXPECT().GetUser(lesson5.UserId(42)).Return(&lesson5.User{Id: 42}, nil)
			},
			initNoticesService: func(ns *lesson5.MockNoticeService) {
				ns.EXPECT().GetNoticesByUserId(lesson5.UserId(42)).Return([]lesson5.Notice{
					{Id: 2,
						Category: lesson5.Automobile,
						UserId:   42,
					},
					{Id: 19,
						Category: lesson5.HouseholdStuff,
						UserId:   42,
					},
					{Id: 31,
						Category: lesson5.Automobile,
						UserId:   42,
					},
				}, nil)
			},
		},
		"use user service and notice service and filter some notices": {
			userId: 42,
			notices: []lesson5.Notice{
				{Id: 1,
					Category: lesson5.RealEstate,
					UserId:   42,
				},
				{Id: 4,
					Category: lesson5.RealEstate,
					UserId:   42,
				},
				{Id: 11,
					Category: lesson5.RealEstate,
					UserId:   42,
				},
			},
			initUserService: func(us *lesson5.MockUserService) {
				us.EXPECT().GetUser(lesson5.UserId(42)).Return(&lesson5.User{Id: 42}, nil)
			},
			initNoticesService: func(ns *lesson5.MockNoticeService) {
				ns.EXPECT().GetNoticesByUserId(lesson5.UserId(42)).Return([]lesson5.Notice{
					{Id: 1,
						Category: lesson5.RealEstate,
						UserId:   42,
					},
					{Id: 2,
						Category: lesson5.Automobile,
						UserId:   42,
					},
					{Id: 4,
						Category: lesson5.RealEstate,
						UserId:   42,
					},
					{Id: 6,
						Category: lesson5.HouseholdStuff,
						UserId:   42,
					},
					{Id: 11,
						Category: lesson5.RealEstate,
						UserId:   42,
					},
					{Id: 19,
						Category: lesson5.Automobile,
						UserId:   42,
					},
				}, nil)
			},
		},
		"user not found error": {
			userId: 42,
			initUserService: func(us *lesson5.MockUserService) {
				us.EXPECT().GetUser(lesson5.UserId(42)).Return(nil, lesson5.ErrUserNotFound)
			},
			err: lesson5.ErrUserNotFound,
		},
		"error on getting notices for user": {
			userId:  42,
			notices: []lesson5.Notice{},
			initUserService: func(us *lesson5.MockUserService) {
				us.EXPECT().GetUser(lesson5.UserId(42)).Return(&lesson5.User{Id: 42}, nil)
			},
			initNoticesService: func(ns *lesson5.MockNoticeService) {
				ns.EXPECT().GetNoticesByUserId(lesson5.UserId(42)).Return(nil, lesson5.ErrConntection)
			},
			err: lesson5.ErrConntection,
		},
	}
	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			usMock := lesson5.NewMockUserService(ctrl)
			if tc.initUserService != nil {
				tc.initUserService(usMock)
			}
			nsMock := lesson5.NewMockNoticeService(ctrl)
			if tc.initNoticesService != nil {
				tc.initNoticesService(nsMock)
			}
			notices, err := lesson5.FindRealEstateNoticesForUser(usMock, nsMock, lesson5.UserId(tc.userId))
			if err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.NoError(t, err)
			}
			require.Equal(t, notices, tc.notices)
		})
	}
}
