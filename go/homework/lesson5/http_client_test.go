package lesson5_test

import (
	"testing"

	"github.com/hardmet/hackerrank/go/homework/lesson5"
	"github.com/stretchr/testify/require"
	"go.uber.org/mock/gomock"
)

func TestGetAccountsByUserIdL(t *testing.T) {
	testCases := map[string]struct {
		userId         int64
		accounts       []lesson5.Account
		initHttpClient func(m *lesson5.MockHttpClient)
		err            string
	}{
		"success": {
			userId: 42,
			accounts: []lesson5.Account{
				{
					Amount:   1000,
					Currency: "RUB",
				},
				{
					Amount:   35,
					Currency: "USD",
				},
			},
			initHttpClient: func(m *lesson5.MockHttpClient) {
				acs := `
				[
					{
						"amount":   1000,
						"currency": "RUB"
					},
					{
						"amount":   35,
						"currency": "USD"
					}
				]`
				m.EXPECT().SendRequest("https://127.0.0.1/accounts/42").Return([]byte(acs), nil)
			},
		},
		"error": {
			userId:   42,
			accounts: nil,
			initHttpClient: func(m *lesson5.MockHttpClient) {
				acs := `
				[
					{
						"amou":   1000,
						"currency": "RUB"
					},
					{
						"amount":   35,
						"currency": "USD"
					}
				]`
				m.EXPECT().SendRequest("https://127.0.0.1/accounts/42").Return([]byte(acs), nil)
			},
			err: "Invalid account structure: Key: 'Account.Amount' Error:Field validation for 'Amount' failed on the 'required' tag",
		},
	}
	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			extDep := lesson5.NewMockHttpClient(ctrl)
			if tc.initHttpClient != nil {
				tc.initHttpClient(extDep)
			}
			actualAccounts, err := lesson5.GetAccountsByUserId(extDep, lesson5.UserId(tc.userId))
			if err != nil {
				require.ErrorContains(t, err, tc.err)
			} else {
				require.NoError(t, err)
			}
			require.Equal(t, actualAccounts, tc.accounts)
		})
	}
}
