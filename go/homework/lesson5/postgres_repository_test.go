package lesson5_test

import (
	"context"
	"fmt"
	"path/filepath"
	"testing"
	"time"

	"github.com/hardmet/hackerrank/go/homework/lesson5"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/modules/postgres"
	"github.com/testcontainers/testcontainers-go/wait"
)

func TestPostgresRepository(t *testing.T) {
	ctx := context.Background()

	pgContainer, err := postgres.RunContainer(ctx,
		testcontainers.WithImage("postgres:15.3-alpine"),
		postgres.WithInitScripts(filepath.Join("testdata", "init-db.sql")),
		postgres.WithDatabase("test-db"),
		postgres.WithUsername("postgres"),
		postgres.WithPassword("postgres"),
		testcontainers.WithWaitStrategy(
			wait.ForLog("database system is ready to accept connections").
				WithOccurrence(2).WithStartupTimeout(5*time.Second)),
	)
	if err != nil {
		t.Fatal(err)
	}

	t.Cleanup(func() {
		if err := pgContainer.Terminate(ctx); err != nil {
			t.Fatalf("failed to terminate pgContainer: %s", err)
		}
	})

	connStr, err := pgContainer.ConnectionString(ctx, "sslmode=disable")
	assert.NoError(t, err)

	noticesRepo, err := lesson5.NewPosgtresqlRepository(ctx, connStr)
	defer func() {
		err := noticesRepo.Close()
		if err != nil {
			fmt.Printf("error closing postgresql connection, err: %s\n", err)
		}
	}()
	assert.NoError(t, err)

	notice, err := noticesRepo.CreateNotice(ctx, lesson5.Notice{
		Category: lesson5.RealEstate,
		UserId:   lesson5.UserId(42),
	})
	assert.NoError(t, err)
	assert.NotNil(t, notice)

	notices, err := noticesRepo.GetNoticesByUserId(ctx, lesson5.UserId(42))
	assert.NoError(t, err)
	assert.NotEmpty(t, notices)
	expectedNotices := []lesson5.Notice{
		{Id: 2, Category: lesson5.Automobile, UserId: 42},
		{Id: 6, Category: lesson5.HouseholdStuff, UserId: 42},
		{Id: 7, Category: lesson5.HouseholdStuff, UserId: 42},
		{Id: 8, Category: lesson5.Automobile, UserId: 42},
		{Id: 9, Category: lesson5.Automobile, UserId: 42},
		{Id: 11, Category: lesson5.RealEstate, UserId: 42},
	}
	require.Equal(t, expectedNotices, notices)
}
