package lesson5_test

import (
	"strings"
	"sync"
	"testing"
	"time"
	"unicode/utf8"

	"github.com/hardmet/hackerrank/go/homework/lesson2"
	"github.com/hardmet/hackerrank/go/homework/lesson3"
	"github.com/hardmet/hackerrank/go/homework/lesson5"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/mock/gomock"
)

func TestAdd(t *testing.T) {
	got := lesson5.Add(1, 1)
	expected := 2
	if got != expected {
		t.Fail()
	}
}

func TestAddTableDriven(t *testing.T) {
	testCases := map[string]struct {
		a      int
		b      int
		result int
	}{
		"sum equal": {
			a:      10,
			b:      10,
			result: 20,
		},
		"sum with zero": {
			a:      0,
			b:      15,
			result: 15,
		},
	}
	for _, tc := range testCases {
		got := lesson5.Add(tc.a, tc.b)
		if got != tc.result {
			t.Errorf("Expected %d but got %d", tc.result, got)
		}
	}
}

func TestFactorialTable(t *testing.T) {
	testCases := map[string]struct {
		n      int64
		result int64
	}{
		"factorial zero": {
			n:      0,
			result: 1,
		},
		"factorial some number": {
			n:      12,
			result: 479001600,
		},
		"factorial very big number (overflow)": {
			n:      66,
			result: 0,
		},
	}
	for _, tc := range testCases {
		got := lesson2.Factorial(tc.n)
		if got != tc.result {
			t.Errorf("Expected %d but got %d", tc.result, got)
		}
	}
}

func FuzzTestErrorAdd(f *testing.F) {
	f.Fuzz(func(t *testing.T, a, b int) {
		res := lesson5.AddWithError(a, b)
		if res != a+b {
			t.Errorf("a=%d, b=%d, result=%d", a, b, res)
		}
	})
}

type Runes []rune

func (r Runes) Len() int {
	return len(r)
}

func (r Runes) Less(i, j int) bool {
	return r[i] < r[j]
}

func (r Runes) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

// go test -fuzz=FuzzTestReverseString github.com/hardmet/hackerrank/go/homework/lesson2

func FuzzTestReverseString(f *testing.F) {
	f.Fuzz(func(t *testing.T, orig string) {
		rev, err1 := lesson2.ReverseString(orig)
		if err1 != nil {
			return
		}
		doubleRev, err2 := lesson2.ReverseString(rev)
		if err2 != nil {
			return
		}
		if orig != doubleRev {
			t.Errorf("Before: %q, after: %q", orig, doubleRev)
		}
		if utf8.ValidString(orig) && !utf8.ValidString(rev) {
			t.Errorf("Reverse produced invalid UTF-8 string %q", rev)
		}
	})
}

func TestService(t *testing.T) {
	testCases := map[string]struct {
		username           string
		amount             int
		balance            int
		initPaymentGateway func(m *lesson5.MockPaymentGateway)
		initLogger         func(m *lesson5.MockLogger)
		err                error
	}{
		"success": {
			username: "Jack",
			amount:   1,
			balance:  10,
			initPaymentGateway: func(m *lesson5.MockPaymentGateway) {
				m.EXPECT().SendMoneyAndGetCurrentBalance("Jack", 1).Return(10, nil)
			},
			initLogger: func(m *lesson5.MockLogger) {
				m.EXPECT().Info("send money ok")
			},
		},
		"error": {
			username: "John",
			amount:   10,
			balance:  0,
			initPaymentGateway: func(m *lesson5.MockPaymentGateway) {
				m.EXPECT().SendMoneyAndGetCurrentBalance("John", 10).Return(0, lesson5.ErrConnection)
			},
			initLogger: func(m *lesson5.MockLogger) {
				m.EXPECT().Error("send money error")
			},
			err: lesson5.ErrConnection,
		},
	}
	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			extDep := lesson5.NewMockPaymentGateway(ctrl)
			if tc.initPaymentGateway != nil {
				tc.initPaymentGateway(extDep)
			}
			logger := lesson5.NewMockLogger(ctrl)
			if tc.initLogger != nil {
				tc.initLogger(logger)
			}
			balance, err := lesson5.TransferMoney(extDep, logger, tc.username, tc.amount)
			if err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.NoError(t, err)
			}
			require.Equal(t, balance, tc.balance)
		})
	}
}

func TestFindErrorsWithIO(t *testing.T) {
	t.Run("Test find errors", func(t *testing.T) {
		result, err := lesson5.FindErrorsIn("sample.txt")
		if err != nil {
			t.Errorf("FindErrorsIn() error = %v", err)
		}

		require.Equal(t, []string{"some error", "another error"}, result)
	})
}

func TestFindErrorsWithoutIO(t *testing.T) {
	t.Run("Test find errors", func(t *testing.T) {
		reader := strings.NewReader("one\ntwo\nsome error\nthree")
		result, err := lesson5.FindErrorsInWithoutIO(reader)
		if err != nil {
			t.Errorf("FindErrorsInWithoutIO() error = %v", err)
		}

		require.Equal(t, []string{"some error"}, result)
	})
}

func TestTableLog(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name  string
		value int
	}{
		{name: "test 1", value: 1},
		{name: "test 2", value: 2},
		{name: "test 3", value: 3},
		{name: "test 4", value: 4},
	}
	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			// t.Parallel()
			t.Log(tc.value)
		})
	}
}

func TestCountIntsParallel(t *testing.T) {
	wg := sync.WaitGroup{}
	workers := 1000
	iterations := 3
	expected := 3000 // workers * iterations
	c := lesson3.CountIntsParallel(workers, iterations, &wg)
	wg.Wait()
	if *c != expected {
		t.Errorf("Expected %d but got %d", expected, *c)
	}
}

func TestCountIntsParallelUsingEventually(t *testing.T) {
	count := 0
	workers := 1000
	iterations := 3
	expected := 3000 // workers * iterations
	// startTime := time.Now().UnixMilli()
	lesson3.CountIntsParallelUnsafe(workers, iterations, &count)
	require.EventuallyWithT(t, func(c *assert.CollectT) {
		// fmt.Printf("time: %d, count: %d\n", time.Now().UnixMilli()-startTime, count)
		assert.Equal(c, expected, count, "expected count (%d) to be equal %d", count, expected)
	}, 2150*time.Millisecond, 100*time.Millisecond)
}
