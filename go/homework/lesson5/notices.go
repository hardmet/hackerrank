package lesson5

import (
	"errors"
	"fmt"
)

type NoticeId int

type Category int64

const (
	Automobile Category = iota
	RealEstate
	HouseholdStuff
)

type Notice struct {
	Id       NoticeId
	Category Category
	UserId   UserId
	// text     string
}

type User struct {
	Id UserId
}

//go:generate mockgen -source $GOFILE -destination ./notices_mock.go -package ${GOPACKAGE}

var ErrConntection error = errors.New("couldn't connect to notices storage")

type NoticeService interface {
	GetNoticesByUserId(id UserId) ([]Notice, error)
}

var ErrUserNotFound error = errors.New("user could not found")

type UserService interface {
	GetUser(id UserId) (*User, error)
}

func FindRealEstateNoticesForUser(us UserService, ns NoticeService, id UserId) (notices []Notice, err error) {
	var user *User
	user, err = us.GetUser(id)
	if err != nil {
		if errors.Is(err, ErrUserNotFound) {
			return nil, ErrUserNotFound
		}
	}
	notices, err = ns.GetNoticesByUserId(user.Id)
	if err != nil {
		fmt.Printf("error while getting notices by user id %d\n, err: %s", user.Id, err.Error())
		return []Notice{}, err
	}
	realEstateNotices := []Notice{}
	for _, notice := range notices {
		if notice.Category == RealEstate {
			realEstateNotices = append(realEstateNotices, notice)
		}
	}
	return realEstateNotices, nil
}
