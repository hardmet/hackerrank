package lesson5

import (
	"encoding/json"
	"fmt"

	"github.com/go-playground/validator/v10"
)

type UserId int64

type Account struct {
	Amount   int64  `json:"amount" validate:"required"`
	Currency string `json:"currency" validate:"required"`
}

//go:generate mockgen -source $GOFILE -destination ./http_client_mock.go -package ${GOPACKAGE}

type HttpClient interface {
	SendRequest(uri string) ([]byte, error)
}

func GetAccountsByUserId(client HttpClient, id UserId) (acs []Account, err error) {
	uri := fmt.Sprintf("https://127.0.0.1/accounts/%d", id)
	var data []byte
	data, err = client.SendRequest(uri)
	if err != nil {
		err = fmt.Errorf("error sending request to %s: %w", uri, err)
		return nil, err
	}
	err = json.Unmarshal(data, &acs)
	if err != nil {
		err = fmt.Errorf("error decoding response: %w", err)
		return nil, err
	}
	validate := validator.New()
	for _, ac := range acs {
		err = validate.Struct(ac)
		if err != nil {
			err = fmt.Errorf("invalid account structure: %w", err)
			return nil, err
		}
	}
	return acs, nil
}
