package lesson5

import (
	"context"
	"strings"
	"testing"

	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/modules/redis"
)

func TestSetValue(t *testing.T) {
	ctx := context.Background()

	redisContainer, err := redis.RunContainer(ctx, testcontainers.WithImage("docker.io/redis:7"))
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := redisContainer.Terminate(ctx); err != nil {
			panic(err)
		}
	}()
	dsn, err := redisContainer.ConnectionString(ctx)
	if err != nil {
		panic(err)
	}
	dsn = strings.Replace(dsn, "redis://", "", 1)
	client := GetClient(dsn)
	result := SetValue(context.Background(), client, "my test value")
	if result != "my test value" {
		t.Fail()
	}
}
