package lesson5

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type Request struct {
	Value string
}

type Result struct {
	Message string
}

func sendRequest(request Request, url string) (*Result, error) {
	b, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}
	body := bytes.NewBuffer(b)
	req, err := http.NewRequest("POST", url, body)
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	b, err = io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	result := Result{}
	err = json.Unmarshal(b, &result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func GetURL(url string) (string, error) {
	resURL := ""
	res, err := http.Get(url)
	if err != nil {
		return resURL, fmt.Errorf("problem fetching from %s, %v", url, err)
	}
	if res.StatusCode != http.StatusOK {
		return resURL, fmt.Errorf("did not get 200 from %s, got %d", url, res.StatusCode)
	}
	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return resURL, err
	}
	resURL = string(body)
	return resURL, nil
}

type BadStatusError struct {
	URL    string
	Status int
}

func (b BadStatusError) Error() string {
	return fmt.Sprintf("did not get 200 from %s, got %d", b.URL, b.Status)
}

func GetURLWithErrorType(url string) (string, error) {
	resURL := ""
	res, err := http.Get(url)
	if err != nil {
		return resURL, fmt.Errorf("problem fetching from %s, %v", url, err)
	}
	if res.StatusCode != http.StatusOK {
		return resURL, BadStatusError{URL: url, Status: res.StatusCode}
	}

	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return resURL, err
	}
	resURL = string(body)
	return resURL, nil
}
