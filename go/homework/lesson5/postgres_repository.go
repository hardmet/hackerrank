package lesson5

import (
	"context"
	"fmt"
	"os"

	"github.com/jackc/pgx/v5"
)

type Repository interface {
	GetNoticesByUser(ctx context.Context, id UserId, ns *[]Notice)
	// SaveNotices(ctx context.Context, notices []Notice) (int, error) // todo
}

type PosgtresqlRepository struct {
	conn *pgx.Conn
	Repository
}

func NewPosgtresqlRepository(ctx context.Context, connStr string) (*PosgtresqlRepository, error) {
	conn, err := pgx.Connect(ctx, connStr)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		return nil, err
	}
	return &PosgtresqlRepository{
		conn: conn,
	}, nil
}

func (pr *PosgtresqlRepository) Close() error {
	return pr.conn.Close(context.Background())
}

func (pr *PosgtresqlRepository) CreateNotice(ctx context.Context, notice Notice) (Notice, error) {
	var id *int
	err := pr.conn.QueryRow(ctx,
		"INSERT INTO notices (category, user_id) VALUES ($1, $2) RETURNING id", int(notice.Category), int64(notice.UserId)).Scan(&id)
	notice.Id = NoticeId(*id)
	return notice, err
}

func (pr *PosgtresqlRepository) GetNoticesByUserId(ctx context.Context, userId UserId) ([]Notice, error) {
	query := "SELECT id, category, user_id FROM notices WHERE user_id = $1"
	rows, err := pr.conn.Query(ctx, query, int64(userId))
	defer func() {
		rows.Close()
	}()
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Not found notices: %v\n", err)
		return []Notice{}, err
	}
	notices := make([]Notice, 0)
	for rows.Next() {
		notice := Notice{}
		var id, category *int
		var userId *int64
		err := rows.Scan(&id, &category, &userId)
		if err != nil {
			return nil, fmt.Errorf("unable to scan row: %w", err)
		}
		notice.Id = NoticeId(*id)
		notice.Category = Category(*category)
		notice.UserId = UserId(*userId)
		notices = append(notices, notice)
	}
	return notices, nil
}
