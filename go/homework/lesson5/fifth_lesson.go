package lesson5

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func Add(a, b int) int {
	return a + b
}

func AddWithError(a, b int) int {
	// if b == 10 {
	// 	return 0
	// }
	return a + b
}

func FindErrorsIn(file string) (es []string, err error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = f.Close()
		if err != nil {
			fmt.Printf("error closing file %s\n", err)
		}
	}()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		text := scanner.Text()
		if strings.Contains(text, "error") {
			es = append(es, text)
		}
	}
	return es, nil
}

func finErrorsInWithIO(file string) ([]string, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = f.Close()
		if err != nil {
			fmt.Printf("error closing file %s\n", err)
		}
	}()
	return FindErrorsInWithoutIO(f)
}

func FindErrorsInWithoutIO(r io.Reader) (es []string, err error) {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		text := scanner.Text()
		if strings.Contains(text, "error") {
			es = append(es, text)
		}
	}
	// rns := []rune{}
	// buffer := make([]byte, 1024)
	// for _, err = r.Read(buffer); err != io.EOF; _, err = r.Read(buffer) {
	// 	for _, rn := range string(buffer) {
	// 		if rn == '\n' {
	// 			line := string(rns)
	// 			if strings.Contains(line, "error") {
	// 				es = append(es, line)
	// 			}
	// 			rns = []rune{}
	// 		} else {
	// 			rns = append(rns, rn)
	// 		}
	// 	}
	// }
	return es, nil
}
