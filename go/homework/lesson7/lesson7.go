package lesson7

import (
	"fmt"

	"github.com/fsnotify/fsnotify"
	"github.com/go-playground/validator/v10"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	// "log/slog"
)

type DBConfig struct {
	Host     string `mapstructure:"host" validate:"url"`
	Port     int    `mapstructure:"port" validate:"gte=0,lte=100000"`
	User     string `mapstructure:"username" validate:"gte=3,lte=50"`
	Password string `mapstructure:"password" validate:"gte=8,lte=20"`
}

type HTTPConfig struct {
	Host            string `mapstructure:"host"`
	Timeout         int    `mapstructure:"timeout"`
	TestModeEnabled bool   `mapstructure:"test_mode"`
}

type Config struct {
	DBConfig   DBConfig   `mapstructure:"db"`
	HTTPConfig HTTPConfig `mapstructure:"http"`
}

func ReadEnvFile() {
	viper.SetConfigFile("../homework/lesson7/.env")
	viper.ReadInConfig()
	fmt.Printf("Port value: %s\n", viper.Get("PORT"))
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Printf("Config file changed: %s\n", e.Name)
	})
	command := &cobra.Command{
		Run: func(c *cobra.Command, args []string) {
			fmt.Println(viper.GetString("Check"))
		},
	}
	viper.SetDefault("Check", true)
	command.Execute()
}

func LoadConfig(path string) (config *Config, err error) {
	config, err = ReadConfig(path)
	if err != nil {
		return nil, err
	}
	err = ValidateConfig(config)
	if err != nil {
		return nil, err
	}
	return config, nil
}

func ReadConfig(path string) (config *Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("conf")
	viper.SetConfigType("json")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return nil, err
	}
	err = viper.Unmarshal(&config)
	if err != nil {
		return nil, err
	}
	return config, nil
}

func ValidateConfig(config *Config) (err error) {
	var validate = validator.New()
	validate.RegisterStructValidation(ConfigStructLevelValidation, DBConfig{})
	err = validate.Struct(config)
	if err != nil {
		return err
	}
	return nil
}

func ConfigStructLevelValidation(sl validator.StructLevel) {
	dbConfig := sl.Current().Interface().(DBConfig)

	if len(dbConfig.User) == 0 {
		sl.ReportError(dbConfig.User, "User", "User", "user", "")
	}
	if len(dbConfig.Password) == 0 {
		sl.ReportError(dbConfig.Password, "Password", "Password", "password", "")
	}
}
