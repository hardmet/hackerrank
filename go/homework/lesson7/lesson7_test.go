package lesson7_test

import (
	"fmt"
	"testing"

	"github.com/hardmet/hackerrank/go/homework/lesson7"
)

func TestLoadConfig(t *testing.T) {
	actual, err := lesson7.LoadConfig(".")
	var expected lesson7.Config = lesson7.Config{DBConfig: lesson7.DBConfig{Host: "postgres://localhost", Port: 5432, User: "test_user", Password: "test_password"}, HTTPConfig: lesson7.HTTPConfig{Host: "127.0.0.1", Timeout: 5, TestModeEnabled: true}}
	if err != nil {
		t.Errorf("TestLoadConfig() error = %v", err)
	} else if *actual != expected {
		fmt.Printf("Expected:\n%#v\n but got:\n%#v\n", expected, actual)
		t.Errorf("Actual config doesn't equal to expected")
	}
}
