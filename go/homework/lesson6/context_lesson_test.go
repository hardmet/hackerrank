package lesson6_test

import (
	"context"
	"sync"
	"testing"
	"time"
)

func TestGracefulShutdown(t *testing.T) {
	const totalTasks = 10
	const totalWorkers = 5
	// tasks := make(chan string, totalTasks)
	// out := make(chan string, totalTasks)
	ctx, cancel := context.WithCancel(context.Background())
	wg := sync.WaitGroup{}
	for i := 1; i <= totalWorkers; i++ {
		wg.Add(1)
		go processTask(ctx)
	}
	cancel()
	wg.Wait()
}

func processTask(ctx context.Context) {
	for {
		time.Sleep(10 * time.Millisecond)
	}
}
