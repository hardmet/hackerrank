package lesson6

import (
	"context"
	"fmt"
	"io"
	"net"
	"net/http"
	"sync"
	"sync/atomic"
	"time"
)

type oneParam string
type otherParam string

func BaseContext() {
	ctx := context.Background()
	ctx = context.WithValue(ctx, "param", 10)
	fmt.Println(ctx.Value("param")) // 10
	ctx = context.WithValue(ctx, "param", 20)
	fmt.Println(ctx.Value("param")) // 20
}

func ContextWithDifferentKeyTypes() {
	ctx := context.Background()
	var param1 oneParam = "param"
	ctx = context.WithValue(ctx, param1, 10)
	var param2 otherParam = "param"
	ctx = context.WithValue(ctx, param2, 20)
	fmt.Println(ctx.Value(param1)) // 10
	fmt.Println(ctx.Value(param2)) // 20
}

func ContextWithCancellation() {
	wg := sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	wg.Add(1)
	t1 := time.Now()
	go func() {
		defer wg.Done()
		for {
			select {
			case <-ctx.Done():
				fmt.Println("Got cancel signal")
				// time.Sleep(time.Second * 3)
				fmt.Printf("cancelled after: %s\n", time.Since(t1))
				return
			default:
				time.Sleep(3 * time.Second)
				// time.Sleep(10 * time.Millisecond)
			}
		}
	}()
	time.Sleep(5 * time.Second)
	cancel()
	wg.Wait()
	fmt.Printf("ended after %s\n", time.Since(t1))
}

func ContextCancelPropagation() {
	ctx, cancel := context.WithCancel(context.Background())
	go cancellableMethod(ctx)
	cancel()
	networkRequest()

	time.Sleep(time.Second * 5)
}

func cancellableMethod(ctx context.Context) {
	ctx = context.WithValue(ctx, "param", "value")
	cancellableMethod2(ctx)
}

func cancellableMethod2(ctx context.Context) {
	select {
	case <-ctx.Done():
		fmt.Println("cancelled inner context too")
	}
}

func networkRequest() {
	const timeout = 1 * time.Second
	client := http.Client{
		Transport: &http.Transport{
			DialContext: (&net.Dialer{
				Timeout: timeout,
			}).DialContext,
		},
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "https://example.com", nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	rsp, err := client.Do(req)
	if rsp != nil {
		defer rsp.Body.Close()
	}
	if e, ok := err.(net.Error); ok && e.Timeout() {
		fmt.Printf("Do request timeout: %s\n", err)
		return
	} else if err != nil {
		fmt.Printf("Cannot do request: %s\n", err)
	}
	body, err := io.ReadAll(rsp.Body)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("body: %s\n", body)
}

func LivenessProbeServices(hosts []string) bool {
	wg := sync.WaitGroup{}
	const RequestTimeout = 1 * time.Second
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	const timeout = 1 * time.Second
	client := http.Client{
		Transport: &http.Transport{
			DialContext: (&net.Dialer{
				Timeout: timeout,
			}).DialContext,
		},
	}
	var aliveCount atomic.Int32
	for i, host := range hosts {
		wg.Add(1)
		go func(index int, h string) {
			defer wg.Done()
			st, _ := makeRequestWithContext(ctx, RequestTimeout, h, &client)
			if st == 200 {
				aliveCount.Add(1)
			}
		}(i, host)
	}
	wg.Wait()
	isQuorum := int(aliveCount.Load()) > len(hosts)/2
	return isQuorum
}

func makeRequestWithContext(ctx context.Context, timeout time.Duration, host string, client *http.Client) (int, error) {
	var cancel context.CancelFunc
	ctx, cancel = context.WithTimeout(ctx, timeout)
	defer cancel()
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, host, nil)
	if err != nil {
		fmt.Println(err)
		return -1, err
	}
	rsp, err := client.Do(req)
	if rsp != nil {
		defer rsp.Body.Close()
	}
	if e, ok := err.(net.Error); ok && e.Timeout() {
		fmt.Printf("Do request timeout: %s\n", err)
		return 504, err
	} else if err != nil {
		fmt.Printf("Cannot do request: %s\n", err)
	}
	body, err := io.ReadAll(rsp.Body)
	if err != nil {
		fmt.Println(err)
		return -1, err
	}
	fmt.Printf("body: %s\n", body)
	return rsp.StatusCode, nil
}

func TestShutdownWorkerPool() {
	const totalTasks = 18
	const totalWorkers = 3
	tasks := make(chan int, totalTasks)
	t1 := time.Now()
	tasks <- -1
	go func() {
		for n := range totalTasks {
			fmt.Printf("written task %d\n", n)
			tasks <- n
			time.Sleep(100 * time.Millisecond)
		}
		close(tasks)
	}()
	wg := sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	for i := range totalWorkers {
		wg.Add(1)
		go processTask(ctx, &wg, tasks, i, t1)
	}
	time.Sleep(1501 * time.Millisecond)
	cancel()
	fmt.Printf("Cancel command after %dms!\n", time.Since(t1).Milliseconds())
	wg.Wait()
	fmt.Printf("Finished worker pool\n")
}

func processTask(ctx context.Context, wg *sync.WaitGroup, tasks <-chan int, n int, t0 time.Time) {
	defer wg.Done()
	for {
		select {
		case <-ctx.Done():
			fmt.Printf("worker %d canceled\n", n)
			return
		default:
			select {
			case <-ctx.Done():
				fmt.Printf("worker %d canceled\n", n)
				return
			case t, isOpen := <-tasks:
				if isOpen {
					fmt.Printf("worker %d started task %d after %dms\n", n, t, time.Since(t0).Milliseconds())
					time.Sleep(500 * time.Millisecond)
					fmt.Printf("worker %d finished task %d after %dms\n", n, t, time.Since(t0).Milliseconds())
				} else {
					return
				}
			default:
				break
			}
		}
	}
}
