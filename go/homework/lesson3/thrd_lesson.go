package lesson3

import (
	"crypto/md5"
	"encoding/hex"
	"strings"
	"sync"
	"time"
	"unicode"
)

func MapSum(numChans []chan int64, results chan<- int64) {
	var wg sync.WaitGroup
	defer close(results)
	defer wg.Wait()
	for _, ch := range numChans {
		wg.Add(1)
		go func(nums <-chan int64, sum int64) {
			defer wg.Done()
			for num := range nums {
				sum += num
			}
			results <- sum
		}(ch, 0)
	}
}

func SumChannels(inputs []chan int64) int64 {
	results := make(chan int64, len(inputs))
	MapSum(inputs, results)

	var sum int64 = 0
	for num := range results {
		sum += num
	}
	return sum
}

func step1(in <-chan string, out chan<- string) {
	go func() {
		defer close(out)
		for str := range in {
			words := strings.Fields(str)
			out <- strings.Join(words, " ")
		}
	}()
}

func step2(in <-chan string, out chan<- string) {
	go func() {
		defer close(out)
		for str := range in {
			for _, sentence := range strings.Split(str, ".") {
				out <- strings.TrimSpace(sentence)
			}
		}
	}()
}

func step3(in <-chan string) <-chan string {
	out := make(chan string)
	go func() {
		defer close(out)
		for sentence := range in {
			if sentence == "" {
				continue
			}
			runesSentence := []rune(sentence)
			fst := []rune{unicode.ToUpper(runesSentence[0])}
			out <- string(append(fst, runesSentence[1:]...))
		}
	}()
	return out
}

// Worker represents a worker that can process tasks.
type Worker struct {
	// Channel to receive tasks.
	tasks <-chan string
	// WaitGroup to signal when the worker is done.
	wg *sync.WaitGroup
	// Channel to write results
	out chan<- string
}

// NewWorker creates a new worker.
func NewWorker(tasks <-chan string, wg *sync.WaitGroup, out chan string) *Worker {
	return &Worker{
		tasks: tasks,
		wg:    wg,
		out:   out,
	}
}

// Run starts the worker.
func (w *Worker) Run() {
	defer w.wg.Done()
	for t := range w.tasks {
		w.out <- calcMD5(t)
	}
}

func calcMD5(s string) string {
	hash := md5.Sum([]byte(s))
	return hex.EncodeToString(hash[:])
}

type Counter struct {
	m sync.Mutex
	v int
}

func (c *Counter) Increment() {
	c.m.Lock()
	c.v++
	c.m.Unlock()
}

func (c *Counter) Add(n int) {
	c.m.Lock()
	c.v += n
	c.m.Unlock()
}

func (c *Counter) Substract(n int) {
	c.m.Lock()
	c.v -= n
	c.m.Unlock()
}

type semaphore chan struct{}

func NewSemaphore(n int) semaphore {
	return make(semaphore, n)
}

func (s semaphore) Acquire(n int) {
	// ваш код
	for i := 1; i <= n; i++ {
		s <- struct{}{}
	}
}

func (s semaphore) Release(n int) {
	// ваш код
	for i := 1; i <= n; i++ {
		<-s
	}
}

func UpdateMap(k string, v int, m *map[string]int) {
	(*m)[k] = v
}

func UpdateMapSafe(k string, v int, m *map[string]int, mt *sync.Mutex) {
	mt.Lock()
	(*m)[k] = v
	mt.Unlock()
}

func IncrementAndUpdateMap(k string, m *map[string]int, mt *sync.Mutex) {
	mt.Lock()
	(*m)[k] = (*m)[k] + 1
	mt.Unlock()
}

func UpdateSyncMap(k string, v int, m *sync.Map) {
	m.Store(k, v)
}

func CountIntsParallel(workers int, iterations int, wg *sync.WaitGroup) *int {
	counter := 0
	m := sync.Mutex{}
	for i := 1; i <= workers; i++ {
		wg.Add(1)
		go func(c *int) {
			defer wg.Done()
			for range iterations {
				m.Lock()
				*c = *c + 1
				m.Unlock()
				time.Sleep(1 * time.Second)
			}
		}(&counter)
	}
	return &counter
}

func CountIntsParallelUnsafe(workers int, iterations int, c *int) {
	m := sync.Mutex{}
	for i := 1; i <= workers; i++ {
		go func(c *int) {
			for range iterations {
				m.Lock()
				*c = *c + 1
				m.Unlock()
				time.Sleep(1 * time.Second)
			}
		}(c)
	}
}
