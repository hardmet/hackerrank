package lesson4

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"runtime/debug"

	"github.com/go-playground/validator/v10"
)

type MyReader struct {
	data []byte
	pos  int
}

func NewReaderFromBuffer(buffer []byte) *MyReader {
	return &MyReader{data: buffer, pos: 0}
}

func (r *MyReader) Read(p []byte) (n int, err error) {
	if p == nil {
		return 0, io.EOF
	}
	if r.pos >= len(r.data) {
		return 0, io.EOF
	}
	n = copy(p, r.data[r.pos:])
	r.pos += n
	return n, nil
}

type MyWriter struct {
	data []byte
	pos  int
}

func NewWriterToBuffer(buffer []byte) *MyWriter {
	return &MyWriter{data: buffer, pos: 0}
}

func (w *MyWriter) Write(p []byte) (n int, err error) {
	if p == nil {
		return 0, io.EOF
	}
	if w.pos >= len(w.data) {
		return 0, io.EOF
	}
	n = copy(w.data[w.pos:], p)
	w.pos += n
	return n, nil
}

type Example struct {
	A int    `json:"a" validate:"required"`
	B string `json:"B,omitempty"`
}

func (e *Example) bar() {
	fmt.Printf("Bar %s!!!", e.B)
}

func ReadJsonToStruct(p string) (res *Example, err error) {
	var bs []byte
	bs, err = os.ReadFile(p)
	if err != nil {
		fmt.Println("Error during reading file", err)
		return nil, err
	}
	return JsonToStruct(bs)
}

func JsonToStruct(s []byte) (*Example, error) {
	var example Example
	err := json.Unmarshal(s, &example)
	if err != nil {
		fmt.Println("Error during unmarshaling json", err)
		return nil, err
	}
	validate := validator.New()
	err = validate.Struct(example)
	if err != nil {
		fmt.Println("Invalid json structure", err)
		return nil, err
	}
	return &example, nil
}

func ReadFileLinesReversed(f string) (lines []string, err error) {
	var of *os.File
	of, err = os.Open(f)
	if err != nil {
		fmt.Println("Error during opening file", err)
	}
	defer func() {
		err = of.Close()
		if err != nil {
			fmt.Println("Error during closing file", err)
		}
	}()
	return ReverseReader(bufio.NewReader(of))
}

func ReverseReader(r io.Reader) (result []string, err error) {
	buf := make([]byte, 1024)
	line := []byte{}
	for _, err = r.Read(buf); err != io.EOF; _, err = r.Read(buf) {
		for _, b := range buf {
			if b == '\n' {
				lineStr := string(line)
				defer func() {
					result = append(result, lineStr)
				}()
				line = []byte{}
			} else {
				line = append(line, b)
			}
		}
		buf = make([]byte, 4)
	}
	if err == io.EOF {
		return result, nil
	}
	return result, err
}

func CopyFile(dst, src string) (count int, err error) {
	var srcF, dstF *os.File
	srcF, err = os.Open(src)
	if err != nil {
		fmt.Println("Error during opening file", src)
	}
	defer func() {
		if err = srcF.Close(); err != nil {
			fmt.Println("Error during closing file", src)
		}
	}()
	dstF, err = os.Create(dst)
	if err != nil {
		fmt.Println("Error during crearing file", dst)
	}
	defer func() {
		if err = dstF.Close(); err != nil {
			fmt.Println("Error during closing file", dst)
		}
	}()
	r := bufio.NewReader(srcF)
	w := bufio.NewWriter(dstF)
	buf := make([]byte, 1024)
	var n int
	for {
		n, err = r.Read(buf)
		if err != nil && err != io.EOF {
			fmt.Println("Error during reading to buffer", err)
		}
		if n == 0 {
			break
		}
		if _, err = w.Write(buf[:n]); err != nil {
			fmt.Println("Error during writing buffer to destination", err)
		}
		count += n
	}
	if err = w.Flush(); err != nil {
		fmt.Println("Error during flushing writer", err)
	}
	return count, err
}

var BarError = errors.New("bar error")

func RaisePanicImplicitly(a string) {
	if a == "bar_error" {
		// panic(BarError)
	}
	var e *Example = &Example{A: 42, B: "Val"}
	// e = nil
	e.bar()
}

func HandlePanic() {
	err, ok := recover().(error)
	if ok {
		debug.PrintStack()
		switch {
		case errors.Is(err, BarError):
			fmt.Printf("Recovered from BarError %s\n", err)
			// default:
			// 	panic(err)
		case err != nil:
			fmt.Printf("Recovered from error %s\n", err)
		}
	} else if err != nil {
		fmt.Printf("Recover intercept panic %s\n", err)
	}
}
