package exercises

func maximalNetworkRank(n int, roads [][]int) int {
	sums := make([]int, n)
	graph := make([][]int, n)
	for i := 0; i < n; i++ {
		graph[i] = make([]int, n)
	}

	for _, road := range roads {
		a := road[0]
		b := road[1]
		if a < b {
			graph[a][b]++
		} else {
			graph[b][a]++
		}
		sums[a]++
		sums[b]++
	}

	max := 0
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			rank := sums[i] + sums[j] - graph[i][j]
			if rank > max {
				max = rank
			}
		}
	}
	return max
}
