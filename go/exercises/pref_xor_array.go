package exercises

func findArray(pref []int) []int {
	n := len(pref)
	for i := n - 1; i > 0; i-- {
		pref[i] = pref[i] ^ pref[i-1]
	}
	return pref
}
