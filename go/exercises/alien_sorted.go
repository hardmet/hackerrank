package exercises

func isAlienSorted(words []string, order string) bool {
	alphabet := [26]int{}
	for i, char := range order {
		alphabet[char-'a'] = i
	}
	for i := 0; i < len(words)-1; i++ {
		for j := range words[i] {
			if j >= len(words[i+1]) {
				return false
			}
			if words[i][j] != words[i+1][j] {
				currentChar := words[i][j] - 'a'
				nextWord := words[i+1][j] - 'a'
				if alphabet[currentChar] > alphabet[nextWord] {
					return false
				} else {
					break
				}
			}
		}
	}
	return true
}
