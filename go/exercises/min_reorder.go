package exercises

import (
	"github.com/hardmet/hackerrank/go/tuples"
)

func minReorder(n int, connections [][]int) int {
	res := 0
	roads := make(map[tuples.Pair]struct{})
	graph := map[int][]int{}
	for _, connection := range connections {
		roads[tuples.Pair{Fst: connection[0], Snd: connection[1]}] = struct{}{}
		if list, ok := graph[connection[0]]; ok {
			graph[connection[0]] = append(list, connection[1])
		} else {
			graph[connection[0]] = []int{connection[1]}
		}
		if list, ok := graph[connection[1]]; ok {
			graph[connection[1]] = append(list, connection[0])
		} else {
			graph[connection[1]] = []int{connection[0]}
		}
	}

	var dfs func(u int, parent int)
	dfs = func(u int, parent int) {
		if _, ok := roads[tuples.Pair{Fst: parent, Snd: u}]; parent != -1 && ok {
			res++
		}
		for _, vs := range graph[u] {
			if vs == parent {
				continue
			}
			dfs(vs, u)
		}
	}
	dfs(0, -1)
	return res
}
