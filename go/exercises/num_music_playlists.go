package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func numMusicPlaylists(n int, goal int, k int) int {
	const MOD = 1e9 + 7
	dp := make([][]int, n+1)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, goal+1)
		for j := 0; j < len(dp[i]); j++ {
			dp[i][j] = -1
		}
	}
	var dpRec func(n, goal, k int) int
	dpRec = func(n, goal, k int) int {
		if n == 0 && goal == 0 {
			return 1
		}
		if n == 0 || goal == 0 {
			return 0
		}
		if dp[n][goal] != -1 {
			return dp[n][goal]
		}
		pick := dpRec(n, goal-1, k) * n
		notpick := dpRec(n, goal-1, k) * math_utils.Max(n-k, 0)
		dp[n][goal] = (pick + notpick) % MOD
		return dp[n][goal]
	}
	return dpRec(n, goal, k)
}
