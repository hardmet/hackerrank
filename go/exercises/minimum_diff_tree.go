package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"math"
)

func minDiffInBST(root *TreeNode) int {
	if root == nil {
		return 0
	}
	curMinDiff, n := math.MaxInt, 0
	nodesStack := []*TreeNode{root}
	var farNode *TreeNode
	for len(nodesStack) > 0 {
		n = len(nodesStack)
		for _, node := range nodesStack {
			if node.Left != nil {
				farNode = node.Left
				for farNode.Right != nil {
					farNode = farNode.Right
				}
				curMinDiff = math_utils.Min(node.Val-farNode.Val, curMinDiff)
				nodesStack = append(nodesStack, node.Left)
			}
			if node.Right != nil {
				farNode = node.Right
				for farNode.Left != nil {
					farNode = farNode.Left
				}
				curMinDiff = math_utils.Min(farNode.Val-node.Val, curMinDiff)
				nodesStack = append(nodesStack, node.Right)
			}
		}
		nodesStack = nodesStack[n:]
	}
	return curMinDiff
}

/*
_, minDiff := minDiffInOrder(root, root.Val-math.MaxInt, math.MaxInt)
*/
func minDiffInOrder(root *TreeNode, prevVal, curMinDiff int) (int, int) {
	if root == nil {
		return prevVal, curMinDiff
	}
	prevVal, curMinDiff = minDiffInOrder(root.Left, prevVal, curMinDiff)
	curMinDiff = math_utils.Min(curMinDiff, root.Val-prevVal)
	return minDiffInOrder(root.Right, root.Val, curMinDiff)
}
