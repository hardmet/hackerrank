package exercises

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	s1, s2 := []int{}, []int{}
	for ; l1 != nil; l1 = l1.Next {
		s1 = append(s1, l1.Val)
	}
	for ; l2 != nil; l2 = l2.Next {
		s2 = append(s2, l2.Val)
	}
	var res *ListNode
	var addOne, n1, n2 int
	for ; len(s1) > 0 && len(s2) > 0; s1, s2 = s1[:len(s1)-1], s2[:len(s2)-1] {
		n1, n2 = s1[len(s1)-1], s2[len(s2)-1]
		next := n1 + n2 + addOne
		if next > 9 {
			res = &ListNode{Val: next % 10, Next: res}
			addOne = 1
		} else {
			res = &ListNode{Val: next, Next: res}
			addOne = 0
		}
	}
	res, addOne = addFromOneStack(res, s1, addOne)
	res, addOne = addFromOneStack(res, s2, addOne)
	if addOne == 1 {
		res = &ListNode{Val: 1, Next: res}
	}
	return res
}

func addFromOneStack(res *ListNode, s []int, addOne int) (*ListNode, int) {
	for ; len(s) > 0; s = s[:len(s)-1] {
		next := s[len(s)-1] + addOne
		if next > 9 {
			res = &ListNode{Val: next % 10, Next: res}
			addOne = 1
		} else {
			res = &ListNode{Val: next, Next: res}
			addOne = 0
		}
	}
	return res, addOne
}
