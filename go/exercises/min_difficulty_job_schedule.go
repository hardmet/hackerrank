package exercises

import (
	"math"

	"github.com/hardmet/hackerrank/go/math_utils"
)

func minDifficulty(jobDifficulty []int, d int) int {
	if len(jobDifficulty) < d {
		return -1
	} else if len(jobDifficulty) == d {
		totalDifficulty := 0
		for _, difficulty := range jobDifficulty {
			totalDifficulty += difficulty
		}
		return totalDifficulty
	}
	dp := make([]int, len(jobDifficulty))
	for i := 0; i < len(dp); i++ {
		dp[i] = math.MaxInt
	}
	dp[0] = jobDifficulty[0]
	for i := 1; i < len(dp); i++ {
		dp[i] = math_utils.Max(dp[i-1], jobDifficulty[i])
	}
	dpPrev := make([]int, len(jobDifficulty))
	for i := 1; i < d; i++ {
		dp, dpPrev = dpPrev, dp
		for j := i; j < len(jobDifficulty); j++ {
			lasdDayDifficulty := jobDifficulty[j]
			tmpMin := lasdDayDifficulty + dpPrev[j-1]
			for k := j - 1; k > i-1; k-- {
				lasdDayDifficulty = math_utils.Max(lasdDayDifficulty, jobDifficulty[k])
				tmpMin = math_utils.Min(tmpMin, lasdDayDifficulty+dpPrev[k-1])
			}
			dp[j] = tmpMin
		}
	}
	return dp[len(dp)-1]
}
