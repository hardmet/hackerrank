package exercises

func numSpecial(mat [][]int) int {
	rowsOnes := make([]int, len(mat))
	colOnes := make([]int, len(mat[0]))
	for row := 0; row < len(mat); row++ {
		for col := 0; col < len(mat[0]); col++ {
			if mat[row][col] == 1 {
				rowsOnes[row]++
				colOnes[col]++
			}
		}
	}
	specialPlaces := 0
	for row := 0; row < len(mat); row++ {
		for col := 0; col < len(mat[row]); col++ {
			if mat[row][col] == 1 {
				if rowsOnes[row] == 1 && colOnes[col] == 1 {
					specialPlaces++
				}
			}
		}
	}
	return specialPlaces
}
