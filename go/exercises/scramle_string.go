package exercises

func isScramble(s1 string, s2 string) bool {
	mp := map[string]bool{}
	return isScrambleMemo(s1, s2, &mp)
}

func isScrambleMemo(s1 string, s2 string, mp *map[string]bool) bool {
	n := len(s1)
	if s1 == s2 {
		return true
	}

	a, b, c := [26]int{}, [26]int{}, [26]int{}
	key := s1 + s2
	if value, contains := (*mp)[key]; contains {
		return value
	}

	for i := 1; i <= n-1; i++ {
		j := n - i
		a[s1[i-1]-'a']++
		b[s2[i-1]-'a']++
		c[s2[j]-'a']++

		if isSameArray(a, b) && isScrambleMemo(s1[0:i], s2[0:i], mp) && isScrambleMemo(s1[i:], s2[i:], mp) {
			(*mp)[key] = true
			return true
		}

		if isSameArray(a, c) && isScrambleMemo(s1[0:i], s2[j:], mp) && isScrambleMemo(s1[i:], s2[0:j], mp) {
			(*mp)[key] = true
			return true
		}
	}
	(*mp)[key] = false
	return false

}

func isSameArray(a, b [26]int) bool {
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
