package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func lenLongestFibSubseq(arr []int) int {
	n := len(arr)

	dp := make([][]int, n)
	for i, _ := range dp {
		dp[i] = make([]int, n)
	}
	maxLen := 0
	for i := 2; i < n; i++ {
		start := 0
		end := i - 1
		for start < end {
			pairSum := arr[start] + arr[end]
			if pairSum > arr[i] {
				end--
			} else if pairSum < arr[i] {
				start++
			} else {
				dp[end][i] = dp[start][end] + 1
				maxLen = math_utils.Max(dp[end][i], maxLen)
				end--
				start++
			}
		}
	}
	if maxLen == 0 {
		return 0
	}
	return maxLen + 2
}
