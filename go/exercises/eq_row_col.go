package exercises

func equalPairs1(grid [][]int) int {
	var k, count int
	for _, row := range grid {
	loop1:
		for j := 0; j < len(row); j++ {
			k = 0
			for i := range grid {
				if grid[i][j] != row[k] {
					continue loop1
				}
				k++
			}
			count++
		}
	}
	return count
}

func equalPairs(grid [][]int) int {
	var count int
	n := len(grid)
	cols := make([][]int, n)
	for j := 0; j < n; j++ {
		cols[j] = make([]int, n)
		for i := 0; i < n; i++ {
			cols[j][i] = grid[i][j]
		}
	}
	for _, row := range grid {
		for _, col := range cols {
			if testEq(row, col, n) {
				count++
			}
		}
	}
	return count
}

func testEq(a, b []int, n int) bool {
	for i := 0; i < n; i++ {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
