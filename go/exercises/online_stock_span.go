package exercises

import "math"

type StockSpanner struct {
	prices []Stock
}

type Stock struct {
	price int
	day   int
}

func Constructor() StockSpanner {
	return StockSpanner{prices: []Stock{{price: math.MaxInt64, day: 0}}}
}

func (this *StockSpanner) Next(price int) int {
	curDay := this.prices[len(this.prices)-1].day + 1
	for price >= this.prices[len(this.prices)-1].price {
		this.prices = this.prices[:len(this.prices)-1]
	}
	span := curDay - this.prices[len(this.prices)-1].day
	this.prices = append(this.prices, Stock{price: price, day: curDay})
	return span
}
