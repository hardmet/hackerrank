package exercises

import (
	"math"
	"sort"
)

func findLongestChain(pairs [][]int) int {
	sort.Slice(pairs, func(i, j int) bool {
		if pairs[i][0] == pairs[j][0] {
			return pairs[i][1] < pairs[j][1]
		}
		return pairs[i][0] < pairs[j][0]
	})
	lastEnd := math.MinInt
	longest := 0
	for _, pair := range pairs {
		if lastEnd == math.MinInt || lastEnd < pair[0] {
			lastEnd = pair[1]
			longest++
		} else if lastEnd > pair[1] {
			lastEnd = pair[1]
		}
	}
	return longest
}
