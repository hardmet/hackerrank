package exercises

func integerBreak(n int) int {
	if n == 2 {
		return 1
	}
	if n == 3 {
		return 2
	}
	maxProduct := make([]int, n+1)
	maxProduct[1] = 1
	maxProduct[2] = 2
	maxProduct[3] = 3
	for num := 4; num <= n; num++ {
		curMax := 0
		for mult := 1; mult <= num/2; mult++ {
			product := maxProduct[mult] * maxProduct[num-mult]
			if product > curMax {
				curMax = product
			}
		}
		maxProduct[num] = curMax
	}
	return maxProduct[n]
}
