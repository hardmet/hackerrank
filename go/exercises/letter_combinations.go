package exercises

func letterCombinations(digits string) []string {
	if len(digits) == 0 {
		return []string{}
	}
	phoneMap := []string{"abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"}
	output := []string{}
	var backtrack func(current, nextDigists string)
	backtrack = func(current, nextDigists string) {
		if len(nextDigists) == 0 {
			output = append(output, current)
		} else {
			letters := phoneMap[nextDigists[0]-'2']
			for _, letter := range letters {
				backtrack(current+string(letter), nextDigists[1:])
			}
		}
	}
	backtrack("", digits)
	return output
}
