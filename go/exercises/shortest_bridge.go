package exercises

func shortestBridge(field [][]int) int {
	m, n := len(field), len(field[0])
	var visited = make([][]bool, m)
	for i := 0; i < len(visited); i++ {
		visited[i] = make([]bool, n)
	}
	dirs := [][]int{{1, 0}, {-1, 0}, {0, 1}, {0, -1}}
	var q [][]int
	// 1. dfs to find an island, mark it in `visited`
loop1:
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if field[i][j] == 1 {
				shortestBridgeDFS(field, &visited, &q, i, j, dirs)
				break loop1
			}
		}
	}
	// 2. bfs to expand this island
	step := 0
	for len(q) > 0 {
		size := len(q)
		for size > 0 {
			size--
			cur := q[0]
			q = q[1:]
			for _, dir := range dirs {
				i := cur[0] + dir[0]
				j := cur[1] + dir[1]
				if i >= 0 && j >= 0 && i < m && j < n && !visited[i][j] {
					if field[i][j] == 1 {
						return step
					}
					q = append(q, []int{i, j})
					visited[i][j] = true
				}
			}
		}
		step++
	}
	return -1
}

func shortestBridgeDFS(field [][]int, visited *[][]bool, q *[][]int, i, j int, dirs [][]int) {
	if i < 0 || j < 0 || i >= len(field) || j >= len(field[0]) || (*visited)[i][j] || field[i][j] == 0 {
		return
	}
	(*visited)[i][j] = true
	*q = append(*q, []int{i, j})
	for _, dir := range dirs {
		shortestBridgeDFS(field, visited, q, i+dir[0], j+dir[1], dirs)
	}
}
