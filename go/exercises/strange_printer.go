package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func strangePrinter(s string) int {
	dp := make([][]int, len(s))
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, len(s))
	}
	for i := len(dp) - 1; i >= 0; i-- {
		dp[i][i] = 1
		for j := i + 1; j < len(s); j++ {
			dp[i][j] = dp[i][j-1] + 1
			for k := i; k < j; k++ {
				if s[k] == s[j] {
					t := 0
					if k+1 <= j-1 {
						t = dp[k+1][j-1]
					}
					dp[i][j] = math_utils.Min(dp[i][j], dp[i][k]+t)
				}
			}
		}
	}
	return dp[0][len(s)-1]
}
