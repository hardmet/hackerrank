package exercises

import (
	"container/heap"
	"sort"

	"github.com/hardmet/hackerrank/go/int_heap"
)

func fullBloomFlowers(flowers [][]int, people []int) []int {
	sortedPeople := make([]int, len(people))
	copy(sortedPeople, people)
	sort.Ints(sortedPeople)
	sort.Slice(len(flowers), func(i, j int) bool {
		return flowers[i][0] < flowers[j][0]
	})
	dict := map[int]int{}
	q := int_heap.MinIntHeap{}
	i := 0
	for _, person := range sortedPeople {
		for i < len(flowers) && flowers[i][0] <= person {
			heap.Push(&q, flowers[i][1])
			i++
		}
		for q.Len() > 0 && q.Peek() < person {
			heap.Pop(&q)
		}
		dict[person] = q.Len()
	}
	ans := make([]int, len(people))
	for j := 0; j < len(people); j++ {
		ans[j] = dict[people[j]]
	}
	return ans
}
