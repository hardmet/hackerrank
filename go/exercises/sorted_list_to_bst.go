package exercises

func sortedListToBST(head *ListNode) *TreeNode {
	nodes := make([]int, 0)
	for head != nil {
		nodes = append(nodes, head.Val)
		head = head.Next
	}
	return constructBST(nodes)
}

func constructBST(nodes []int) *TreeNode {
	n := len(nodes)
	if n == 0 {
		return nil
	}
	if n == 1 {
		return &TreeNode{Val: nodes[0]}
	}
	mid := len(nodes) / 2
	tree := TreeNode{Val: nodes[mid]}
	tree.Left = constructBST(nodes[0:mid])
	tree.Right = constructBST(nodes[mid+1:])
	return &tree
}
