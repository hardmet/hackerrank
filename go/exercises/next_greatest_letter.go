package exercises

func nextGreatestLetter(a []byte, x byte) byte {
	n := len(a)

	//hi starts at 'n' rather than the usual 'n - 1'.
	//It is because the terminal condition is 'lo < hi' and if hi starts from 'n - 1',
	//we can never consider value at index 'n - 1'
	lo := 0
	hi := n
	var mid int

	//Terminal condition is 'lo < hi', to avoid infinite loop when target is smaller than the first element
	for lo < hi {
		mid = lo + (hi-lo)/2
		if a[mid] > x {
			hi = mid
		} else {
			lo = mid + 1
		}
	}

	//Because lo can end up pointing to index 'n', in which case we return the first element
	return a[lo%n]
}
