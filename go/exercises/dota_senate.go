package exercises

func predictPartyVictory(senate string) string {
	n := len(senate)
	qD, qR := make([]int, 0, n), make([]int, 0, n)
	for i, senator := range senate {
		if senator == 'D' {
			qD = append(qD, i)
		} else {
			qR = append(qR, i)
		}
	}

	for len(qR) > 0 && len(qD) > 0 {
		rIndex := qR[0]
		dIndex := qD[0]
		qR, qD = qR[1:], qD[1:]
		if rIndex < dIndex {
			qR = append(qR, rIndex+n)
		} else {
			qD = append(qD, dIndex+n)
		}
	}
	if len(qD) > 0 {
		return "Dire"
	} else {
		return "Radiant"
	}
}
