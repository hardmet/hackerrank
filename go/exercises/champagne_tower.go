package exercises

func champagneTower(poured int, queryRow int, queryGlass int) float64 {
	a := [102][102]float64{}
	a[0][0] = float64(poured)
	for r := 0; r <= queryRow; r++ {
		for c := 0; c <= r; c++ {
			q := (a[r][c] - 1.0) / 2.0
			if q > 0 {
				a[r+1][c] += q
				a[r+1][c+1] += q
			}
		}
	}
	if a[queryRow][queryGlass] > 1 {
		return 1
	}
	return a[queryRow][queryGlass]
}
