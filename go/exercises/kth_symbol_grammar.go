package exercises

import "math"

func kthGrammar(n int, k int) int {
	if n == 1 {
		return 0
	}
	symbol := 1
	var row float64
	for row = float64(n); row > 1; row-- {
		elements := int(math.Pow(2, row-1))
		if k > elements/2 {
			symbol = 1 - symbol
			k -= elements / 2
		}
	}
	if symbol != 0 {
		return 0
	}
	return 1
}
