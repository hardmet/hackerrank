package exercises

func paintWalls(cost []int, time []int) int {
	dp := make([]int, len(cost)+1)
	prevDP := make([]int, len(cost)+1)
	for i := 0; i < len(prevDP); i++ {
		prevDP[i] = 1e9
	}
	prevDP[0] = 0
	for i := len(cost) - 1; i >= 0; i-- {
		dp = make([]int, len(cost)+1)
		for remain := 1; remain <= len(cost); remain++ {
			index := 0
			if remain-1-time[i] > 0 {
				index = remain - 1 - time[i]
			}
			paint := cost[i] + prevDP[index]
			dontPaint := prevDP[remain]
			if paint < dontPaint {
				dp[remain] = paint
			} else {
				dp[remain] = dontPaint
			}
		}
		prevDP = dp
	}
	return dp[len(cost)]
}
