package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"math"
)

func maxPathSum(root *TreeNode) int {
	x, y := maxPathDfs(root, math.MinInt)
	return math_utils.Max(x, y)
}

func maxPathDfs(root *TreeNode, curMax int) (int, int) {
	if root == nil {
		return 0, curMax
	}
	sumL, maxL := maxPathDfs(root.Left, curMax)
	sumR, maxR := maxPathDfs(root.Right, curMax)
	m := math_utils.Max(math_utils.Max(sumL, sumR)+root.Val, root.Val)
	return m, math_utils.Max(math_utils.Max(math_utils.Max(math_utils.Max(maxL, maxR), m), root.Val), sumR+sumL+root.Val)
}
