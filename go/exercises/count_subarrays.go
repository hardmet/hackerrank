package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func countSubarrays(nums []int, minK, maxK int) int64 {
	endValidWindow, lastMin, lastMax := -1, -1, -1
	var count int64
	for i := 0; i < len(nums); i++ {
		if nums[i] >= minK && nums[i] <= maxK {
			if nums[i] == minK {
				lastMin = i
			}
			if nums[i] == maxK {
				lastMax = i
			}
			// check if we have valid windows (we have minK and maxK elements)
			if lastMin >= 0 && lastMax >= 0 {
				startValidWindow := math_utils.Min(lastMin, lastMax) // find last valid element of window
				// here we have endValidWindow - startValidWindow valid subarrays
				count += int64(endValidWindow - startValidWindow)
			}
		} else {
			endValidWindow = i
			lastMin = -1
			lastMax = -1
		}
	}
	return count
}
