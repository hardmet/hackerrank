package exercises

func findMode(root *TreeNode) []int {
	values := []int{}
	findModeDFS(root, &values)
	maxStreak, streak, num := 0, 0, 0
	modes := []int{}
	for _, value := range values {
		if value == num {
			streak++
		} else {
			streak = 1
			num = value
		}
		if streak > maxStreak {
			modes = []int{}
			maxStreak = streak
		}
		if streak == maxStreak {
			modes = append(modes, value)
		}
	}
	return modes
	// result := make([]int, len(modes))
	// for i := 0; i < len(modes); i++ {
	// 	result[i] = modes[i]
	// }
	// return result
}

func findModeDFS(node *TreeNode, values *[]int) {
	if node != nil {
		findModeDFS(node.Left, values)
		*values = append(*values, node.Val)
		findModeDFS(node.Right, values)
	}
}
