package exercises

import "fmt"

func exist(board [][]byte, word string) bool {
	m, n := len(board), len(board[0])
	if len(word) > m*n {
		return false
	}
	symbols := map[byte]int{}
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			symbols[board[i][j]] += 1
		}
	}
	wordS := map[byte]int{}
	for _, b := range []byte(word) {
		wordS[b] += 1
	}
	for k, v := range wordS {
		count, contains := symbols[k]
		if !contains || count < v {
			return false
		}
	}
	bs := []byte(word)
	if wordS[word[0]] > wordS[word[len(word)-1]] {
		reverseLR(&bs, 0, len(word)-1)
	}
	var dfs func(i, j, s int) bool
	dfs = func(i, j, s int) bool {
		if s == len(word) {
			return true
		}
		if 0 <= i && i < m && 0 <= j && j < n && board[i][j] == bs[s] {
			board[i][j] = '#'
			moves := [][]int{{i, j + 1}, {i, j - 1}, {i + 1, j}, {i - 1, j}}
			dp := false
			for _, ij := range moves {
				dp = dp || dfs(ij[0], ij[1], s+1)
			}
			board[i][j] = bs[s]
			return dp
		}
		return false
	}
	dp := false
	for i := range board {
		for j := range board[i] {
			dp = dp || dfs(i, j, 0)
		}
	}
	return dp
}

func reverseLR(bs *[]byte, l, r int) {
	for i := (r-l+1)/2 - 1; i >= 0; i-- {
		t := (*bs)[l+i]
		(*bs)[l+i] = (*bs)[r-i]
		(*bs)[r-i] = t
	}
}

func WordSearchTests() {
	fmt.Println(exist([][]byte{
		{'C', 'A', 'A'},
		{'A', 'A', 'A'},
		{'B', 'C', 'D'}}, "AAB"))
	fmt.Println(exist([][]byte{
		{'a', 'a'},
	}, "aaa"))
	board := [][]byte{
		{'A', 'B', 'C', 'E'},
		{'S', 'F', 'C', 'S'},
		{'A', 'D', 'E', 'E'},
	}
	fmt.Println(exist(board, "ABCB"))
	fmt.Println(exist(board, "SEE"))
	fmt.Println(exist(board, "ABCCED"))
}
