package exercises

var vowels = [...]byte{'a', 'e', 'o', 'u', 'i', 'A', 'E', 'O', 'U', 'I'}

func sortVowels(s string) string {
	count := make([]int, 1000)
	for _, char := range []byte(s) {
		if isVowel(char) {
			count[char-'A']++
		}
	}
	sortedVowel := []byte("AEIOUaeiou")
	result := []byte{}
	for i, j := 0, 0; i < len(s); i++ {
		if !isVowel(s[i]) {
			result = append(result, s[i])
		} else {
			for count[sortedVowel[j]-'A'] == 0 {
				j++
			}
			result = append(result, sortedVowel[j])
			count[sortedVowel[j]-'A']--
		}
	}
	return string(result)
}

func isVowel(c byte) bool {
	for _, vowel := range vowels {
		if vowel == c {
			return true
		}
	}
	return false
}
