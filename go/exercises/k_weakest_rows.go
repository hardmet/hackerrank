package exercises

import (
	"sort"

	"github.com/hardmet/hackerrank/go/tuples"
)

func kWeakestRows(mat [][]int, k int) []int {
	rowStrength := make([]tuples.Pair, len(mat))
	for i, row := range mat {
		sum := 0
		for _, value := range row {
			sum += value
		}
		rowStrength[i] = tuples.Pair{sum, i}
	}

	sort.Slice(rowStrength, func(i, j int) bool {
		if rowStrength[i].Fst == rowStrength[j].Fst {
			return rowStrength[i].Snd < rowStrength[j].Snd
		}
		return rowStrength[i].Fst < rowStrength[j].Fst
	})
	result := make([]int, k)
	for i := 0; i < k; i++ {
		result[i] = rowStrength[i].Snd
	}
	return result
}
