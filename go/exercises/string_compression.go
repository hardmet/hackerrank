package exercises

import "strconv"

func compress(chars []byte) int {
	var i, j, n, count int
	var ci byte
	n = len(chars)
	if n == 1 {
		return 1
	}
	for i < n {
		count = 1
		ci = chars[i]
		for i+1 < n && chars[i] == chars[i+1] {
			count++
			i++
		}
		if count == 1 {
			chars[j] = ci
			j++
		} else if count > 1 {
			chars[j] = ci
			j++
			counterString := strconv.Itoa(count)
			for r := range counterString {
				chars[j] = counterString[r]
				j++
			}
		}
		i++
	}
	return j
}
