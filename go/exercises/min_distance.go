package exercises

func minDistance(word1, word2 string) int {
	dp := make([][]int, len(word1)+1)
	dp[0] = make([]int, len(word2)+1)
	var curMin int
	for i := 1; i <= len(word1); i++ {
		dp[i] = make([]int, len(word2)+1)
		dp[i][0] = i
		for j := 1; j <= len(word2); j++ {
			dp[0][j] = j
			if word1[i-1] == word2[j-1] {
				dp[i][j] = dp[i-1][j-1]
			} else {
				curMin = dp[i-1][j-1]
				if dp[i-1][j] < curMin {
					curMin = dp[i-1][j]
				}
				if dp[i][j-1] < curMin {
					curMin = dp[i][j-1]
				}
				dp[i][j] = 1 + curMin
			}
		}
	}
	return dp[len(word1)][len(word2)]
}
