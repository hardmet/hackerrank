package exercises

import (
	"sort"

	"github.com/hardmet/hackerrank/go/math_utils"
)

func maxValueEvents(events [][]int, k int) int {
	sort.Slice(events, func(i, j int) bool {
		if events[i][0] == events[j][0] {
			return events[i][1] < events[j][1]
		}
		return events[i][0] < events[j][0]
	})
	dp := make([][]int, (k + 1))
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, len(events))
		for j := 0; j < len(dp[i]); j++ {
			dp[i][j] = -1
		}
	}
	var dfs func(index, count int) int
	dfs = func(index, count int) int {
		if count == 0 || index == len(events) {
			return 0
		}
		if dp[count][index] != -1 {
			return dp[count][index]
		}
		nextIndex := findNextEvent(events, events[index][1])
		dp[count][index] = math_utils.Max(dfs(index+1, count), events[index][2]+dfs(nextIndex, count-1))
		return dp[count][index]
	}
	return dfs(0, k)
}

func findNextEvent(events [][]int, k int) int {
	l, r, mid := 0, len(events), 0
	for l < r {
		mid = (l + r) / 2
		if events[mid][0] <= k {
			l = mid + 1
		} else {
			r = mid
		}
	}
	return l
}
