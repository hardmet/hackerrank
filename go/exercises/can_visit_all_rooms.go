package exercises

func canVisitAllRooms(rooms [][]int) bool {
	stack := []int{0}
	visited := make([]bool, len(rooms))
	visited[0] = true

	j := 0
	for j < len(stack) {
		key := stack[j]
		j++
		for _, k := range rooms[key] {
			if !visited[k] {
				stack = append(stack, k)
				visited[k] = true
			}
		}
	}
	for i := 0; i < len(rooms); i++ {
		if !visited[i] {
			return false
		}
	}
	return true
}
