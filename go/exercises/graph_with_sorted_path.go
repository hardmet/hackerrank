package exercises

import (
	"container/heap"
	"math"

	"github.com/hardmet/hackerrank/go/priority_queue"
	"github.com/hardmet/hackerrank/go/tuples"
)

type Graph struct {
	adjList [][]tuples.Pair
}

func GraphConstructor(n int, edges [][]int) Graph {
	list := [][]tuples.Pair{}
	for i := 0; i < n; i++ {
		list = append(list, []tuples.Pair{})
	}
	for _, edge := range edges {
		list[edge[0]] = append(list[edge[0]], tuples.Pair{Fst: edge[1], Snd: edge[2]})
	}
	return Graph{adjList: list}
}

func (g *Graph) AddEdge(edge []int) {
	g.adjList[edge[0]] = append(g.adjList[edge[0]], tuples.Pair{Fst: edge[1], Snd: edge[2]})
}

func (g *Graph) ShortestPath(node1 int, node2 int) int {
	n := len(g.adjList)
	q := priority_queue.PriorityQueueToInt{}
	costForNode := make([]int, n)
	for i := 0; i < len(costForNode); i++ {
		costForNode[i] = math.MaxInt
	}
	costForNode[node1] = 0
	var sliceToInt priority_queue.SliceToInt = []int{0, node1}
	heap.Push(&q, sliceToInt)
	for q.Len() > 0 {
		current := heap.Pop(&q).([]int)
		cost := current[0]
		node := current[1]
		if cost > costForNode[node] {
			continue
		}
		if node == node2 {
			return cost
		}
		for _, neighbor := range g.adjList[node] {
			neighborNode := neighbor.Fst
			neighborCost := neighbor.Snd
			newCost := cost + neighborCost
			if newCost < costForNode[neighborNode] {
				costForNode[neighborNode] = newCost
				sliceToInt = []int{newCost, neighborNode}
				heap.Push(&q, sliceToInt)
			}
		}
	}
	return -1
}
