package exercises

func topKFrequent(nums []int, k int) []int {
	frequencies := map[int]int{}
	for _, num := range nums {
		frequencies[num]++
	}
	increasingInvertedFrequenciesList := make([][]int, len(nums))
	for num, frequency := range frequencies {
		increasingInvertedFrequenciesList[frequency-1] = append(increasingInvertedFrequenciesList[frequency-1], num)
	}
	res := make([]int, 0)
	for i := len(increasingInvertedFrequenciesList) - 1; i >= 0 && k > 0; i-- {
		for j := 0; j < len(increasingInvertedFrequenciesList[i]) && k > 0; j++ {
			res = append(res, increasingInvertedFrequenciesList[i][j])
			k--
		}
	}
	return res
}
