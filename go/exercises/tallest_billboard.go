package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func tallestBillboard(rods []int) int {
	var sum int
	for _, rod := range rods {
		sum += rod
	}
	// Use maxPossible to prune when one rod's length goes over the
	// maxPossible
	maxPossible := sum / 2
	memo := make([][]int, len(rods))
	for i := 0; i < len(memo); i++ {
		memo[i] = make([]int, maxPossible+1)
		for j := 0; j < len(memo[i]); j++ {
			memo[i][j] = -2 // unsolved path; -1 stands for not possible
		}
	}
	res := tbbDFS(rods, 0, 0, 0, maxPossible, memo)
	return res
}

func tbbDFS(nums []int, pos int, s1, s2 int, maxPossible int, memo [][]int) int {
	if s1 > maxPossible || s2 > maxPossible {
		return -1
	}
	if pos == len(nums) {
		if s1 == s2 {
			return s1
		}
		return -1
	}
	// We use the delta to memoize because if we memoize both support sizes
	// we'll get MLE. When we use delta, we have to subtract the min or max of the rod
	// size. When we return from the memoized result, we'll add the same for the new
	// combination.
	delta := math_utils.Max(s1, s2) - math_utils.Min(s1, s2)
	if memo[pos][delta] != -2 {
		if memo[pos][delta] == -1 {
			return -1
		}
		return memo[pos][delta] + math_utils.Min(s1, s2)
	}
	// We'll go over the three possiblilities and pick the maximum
	res := math_utils.Max(
		tbbDFS(nums, pos+1, s1, s2, maxPossible, memo), // Skipping it
		math_utils.Max(
			tbbDFS(nums, pos+1, s1+nums[pos], s2, maxPossible, memo), // Add it to first support
			tbbDFS(nums, pos+1, s1, s2+nums[pos], maxPossible, memo), // Add it to second support
		),
	)
	memo[pos][delta] = math_utils.Max(-1, res-math_utils.Min(s1, s2))
	if memo[pos][delta] == -1 {
		return memo[pos][delta]
	}
	return memo[pos][delta] + math_utils.Min(s1, s2)
}
