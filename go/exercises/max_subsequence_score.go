package exercises

import (
	"container/heap"
	"sort"

	"github.com/hardmet/hackerrank/go/int_heap"
)

func maxSubsequenceScore(speed []int, efficiency []int, k int) int64 {
	n := len(speed)
	ess := make([][]int, n)
	for i := 0; i < len(ess); i++ {
		ess[i] = []int{efficiency[i], speed[i]}
	}
	sort.Slice(ess, func(i, j int) bool {
		return ess[i][0] > ess[j][0]
	})
	var pq heap.Interface
	pq = &int_heap.MaxIntHeap{}
	var res int64 = 0
	sumS := 0
	for _, es := range ess {
		heap.Push(pq, es[1])
		sumS += es[1]
		if pq.Len() > k {
			sumS -= heap.Pop(pq).(int)
		}
		if pq.Len() == k {
			var tmp = int64(sumS) * int64(es[0])
			if res < tmp {
				res = tmp
			}
		}
	}
	return res
}
