package exercises

func removeStars(s string) string {
	var stack []byte
	for _, symbol := range s {
		if symbol == '*' {
			stack = stack[:len(stack)-1]
		} else {
			stack = append(stack, byte(symbol))
		}
	}
	return string(stack)
}
