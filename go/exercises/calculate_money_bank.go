package exercises

func totalMoney(n int) int {
	weeks := n / 7
	money := 0
	for i := 0; i < weeks; i++ {
		money += ((i+1)+7+i)*3 + 4 + i
	}
	for i := 0; i < n%7; i++ {
		money += weeks + i + 1
	}
	return money
}
