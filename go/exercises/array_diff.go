package exercises

func findDifference(nums1 []int, nums2 []int) [][]int {
	m1 := toMap(nums1)
	m2 := toMap(nums2)
	res := [][]int{{}, {}}
	for num := range m1 {
		if _, contains := m2[num]; !contains {
			res[0] = append(res[0], num)
		}
	}
	for num := range m2 {
		if _, contains := m1[num]; !contains {
			res[1] = append(res[1], num)
		}
	}
	return res
}

func toMap(a []int) map[int]struct{} {
	m := map[int]struct{}{}
	for _, ai := range a {
		m[ai] = struct{}{}
	}
	return m
}
