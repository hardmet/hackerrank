package exercises

func largestAltitude(gain []int) int {
	var max, cur = 0, 0
	for _, increase := range gain {
		cur += increase
		if cur > max {
			max = cur
		}
	}
	return max
}
