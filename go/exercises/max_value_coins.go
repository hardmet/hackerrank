package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func maxValueOfCoins(piles [][]int, k int) int {
	dp := make([][]int, len(piles)+1)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, k+1)
	}
	return countCoins(0, k, piles, dp)
}

func countCoins(i, k int, piles, dp [][]int) int {
	if dp[i][k] > 0 {
		return dp[i][k]
	}
	if i == len(piles) || k == 0 {
		return 0
	}
	res, cur := countCoins(i+1, k, piles, dp), 0
	for j := 0; j < len(piles[i]) && j < k; j++ {
		cur += piles[i][j]
		res = math_utils.Max(res, countCoins(i+1, k-j-1, piles, dp)+cur)
	}
	dp[i][k] = res
	return res
}
