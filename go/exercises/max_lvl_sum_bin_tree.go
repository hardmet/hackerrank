package exercises

func maxLevelSum(root *TreeNode) int {
	var dfs func(node *TreeNode, levels *[]int, level int)
	dfs = func(node *TreeNode, levels *[]int, level int) {
		if node != nil {
			if len(*levels) == level {
				*levels = append(*levels, node.Val)
			} else {
				(*levels)[level] = (*levels)[level] + node.Val
			}
			dfs(node.Left, levels, level+1)
			dfs(node.Right, levels, level+1)
		}
	}
	var levels []int
	dfs(root, &levels, 0)
	maxLevel := 0
	for i := 0; i < len(levels); i++ {
		if levels[maxLevel] < levels[i] {
			maxLevel = i
		}
	}
	return maxLevel + 1
}
