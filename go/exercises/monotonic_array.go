package exercises

func isMonotonic(nums []int) bool {
	if len(nums) < 2 {
		return true
	}
	increasing := 0
	if nums[0] > nums[len(nums)-1] {
		increasing = -1
	} else if nums[0] < nums[len(nums)-1] {
		increasing = 1
	}
	if increasing == 0 {
		for i := 1; i < len(nums); i++ {
			if nums[i] != nums[i-1] {
				return false
			}
		}
		return true
	}
	for i := 1; i < len(nums); i++ {
		if increasing > 0 && nums[i] < nums[i-1] {
			return false
		} else if increasing < 0 && nums[i] > nums[i-1] {
			return false
		}
	}
	return true
}
