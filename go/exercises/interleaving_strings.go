package exercises

func isInterleave(s1 string, s2 string, s3 string) bool {
	c1 := []byte(s1)
	c2 := []byte(s2)
	c3 := []byte(s3)
	m, n := len(s1), len(s2)
	if m+n != len(s3) {
		return false
	}
	var dfs func(c1, c2, c3 []byte, i, j, k int, invalid [][]bool) bool
	dfs = func(c1, c2, c3 []byte, i, j, k int, invalid [][]bool) bool {
		if invalid[i][j] {
			return false
		}
		if k == len(c3) {
			return true
		}
		valid := i < len(c1) && c1[i] == c3[k] && dfs(c1, c2, c3, i+1, j, k+1, invalid) || j < len(c2) && c2[j] == c3[k] && dfs(c1, c2, c3, i, j+1, k+1, invalid)
		if !valid {
			invalid[i][j] = true
		}
		return valid
	}
	invalid := make([][]bool, m+1)
	for i := 0; i < len(invalid); i++ {
		invalid[i] = make([]bool, n+1)
	}
	return dfs(c1, c2, c3, 0, 0, 0, invalid)
}
