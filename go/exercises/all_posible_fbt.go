package exercises

func allPossibleFBT(k int) []*TreeNode {
	cached := map[int][]*TreeNode{}
	var calcPossibleFBTRec func(n int) []*TreeNode
	calcPossibleFBTRec = func(n int) []*TreeNode {
		if n%2 == 0 {
			return []*TreeNode{}
		}
		if value, contains := cached[n]; contains {
			return value
		}
		value := []*TreeNode{}
		if n == 1 {
			value = append(value, &TreeNode{})
		} else {
			for i := 1; i < n; i += 2 {
				left := calcPossibleFBTRec(i)
				right := calcPossibleFBTRec(n - i - 1)
				for _, l := range left {
					for _, r := range right {
						value = append(value, &TreeNode{Val: 0, Left: l, Right: r})
					}
				}
			}
		}
		cached[n] = value
		return value
	}
	calcPossibleFBTRec(k)
	return cached[k]
}
