package exercises

import "math"

func getHappyString(nLength int, k int) string {

	var dfs func(prefix []rune, n int, k float64) []rune
	dfs = func(prefix []rune, n int, k float64) []rune {
		if n == 0 {
			return prefix
		}
		for char := 'a'; char <= 'c'; char++ {
			if len(prefix) > 0 && char == prefix[len(prefix)-1] {
				continue
			}
			cnt := math.Pow(2, float64(nLength-len(prefix)-1))
			if cnt >= k {
				return dfs(append(prefix, char), n-1, k)
			} else {
				k -= cnt
			}
		}
		return []rune{}
	}
	return string(dfs([]rune{}, nLength, float64(k)))
}
