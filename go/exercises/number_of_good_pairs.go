package exercises

func numIdenticalPairs(nums []int) int {
	counts := [100]int{}
	for _, n := range nums {
		counts[n-1]++
	}
	res := 0
	for _, count := range counts {
		res += (count * (count - 1)) / 2
	}
	return res
}
