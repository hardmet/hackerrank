package exercises

func swapNodes(head *ListNode, k int) *ListNode {
	var a, b = head, head
	node := head

	for c := 1; c < k; c++ {
		a = a.Next
		node = node.Next
	}
	for ; node.Next != nil; node = node.Next {
		b = b.Next
	}
	b.Val, a.Val = a.Val, b.Val
	return head
}
