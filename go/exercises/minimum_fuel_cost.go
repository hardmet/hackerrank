package exercises

import "math"

func minimumFuelCost(roads [][]int, seats int) int64 {
	var n int = len(roads) + 1
	var adj map[int][]int
	degree := make([]int, n)
	for _, road := range roads {
		addAdj(&adj, road[0], road[1])
		addAdj(&adj, road[1], road[0])
		degree[road[0]]++
		degree[road[1]]++
	}
	return fuelCostBFS(n, adj, degree, seats)
}

func fuelCostBFS(n int, adj map[int][]int, degree []int, seats int) int64 {
	q := []int{}
	for i := 1; i < n; i++ {
		if degree[i] == 1 {
			q = append(q, i)
		}
	}
	representatives := make([]int, n)
	for i := 0; i < len(representatives); i++ {
		representatives[i] = 1
	}
	var fuel int64
	for len(q) > 0 {
		node := q[0]
		q = q[1:]
		fuel += int64(math.Ceil(float64(representatives[node]) / float64(seats)))
		for _, neighbor := range adj[node] {
			degree[neighbor]--
			representatives[neighbor] += representatives[node]
			if degree[neighbor] == 1 && neighbor != 0 {
				q = append(q, neighbor)
			}
		}
	}
	return fuel
}

func addAdj(m *map[int][]int, k, v int) {
	if value, contains := (*m)[k]; contains {
		value = append(value, v)
	} else {
		(*m)[k] = []int{v}
	}
}
