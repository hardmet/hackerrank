package exercises

import "sort"

func reductionOperations(nums []int) int {
	sort.Ints(nums)
	operationsCount := 0
	up := 0
	for i := 1; i < len(nums); i++ {
		if nums[i] != nums[i-1] {
			up++
		}
		operationsCount += up
	}
	return operationsCount
}
