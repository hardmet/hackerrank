package exercises

func maxAbsoluteSum(nums []int) int {
	var minPrefixSum, maxPrefixSum, prefixSum int
	for _, num := range nums {
		prefixSum += num
		if prefixSum < minPrefixSum {
			minPrefixSum = prefixSum
		}
		if prefixSum > maxPrefixSum {
			maxPrefixSum = prefixSum
		}
	}
	return maxPrefixSum - minPrefixSum
}
