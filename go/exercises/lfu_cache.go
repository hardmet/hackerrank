package exercises

import (
	"container/list"
)

type node struct {
	key, val, frequency int
}

type LFUCache struct {
	nodes             map[int]*list.Element
	lists             map[int]*list.List
	cap, minFrequency int
}

func ConstructorLFU(capacity int) LFUCache {
	return LFUCache{
		nodes:        make(map[int]*list.Element),
		lists:        make(map[int]*list.List),
		cap:          capacity,
		minFrequency: 0,
	}
}

func (c *LFUCache) Get(key int) int {
	if _, ok := c.nodes[key]; !ok {
		return -1
	}
	currNode := c.nodes[key].Value.(*node)
	c.lists[currNode.frequency].Remove(c.nodes[key])
	currNode.frequency++
	newList := c.lists[currNode.frequency]
	if _, ok := c.lists[currNode.frequency]; !ok {
		newList = list.New()
	}
	c.lists[currNode.frequency] = newList
	newNode := newList.PushFront(currNode)
	c.nodes[key] = newNode
	if currNode.frequency-1 == c.minFrequency && c.lists[currNode.frequency-1].Len() == 0 {
		c.minFrequency++
	}
	return currNode.val
}

func (c *LFUCache) Put(key int, value int) {
	if c.cap > 0 {
		if freqAndVal, contains := c.nodes[key]; contains {
			curNode := freqAndVal.Value.(*node)
			curNode.val = value
			c.Get(key)
			return
		}
		if c.cap == len(c.nodes) {
			keys := c.lists[c.minFrequency]
			keyToDelete := keys.Back()
			delete(c.nodes, keyToDelete.Value.(*node).key)
			keys.Remove(keyToDelete)
		}
		c.minFrequency = 1
		currNode := &node{
			key:       key,
			val:       value,
			frequency: 1,
		}
		if _, ok := c.lists[1]; !ok {
			c.lists[1] = list.New()
		}
		newNode := c.lists[1].PushFront(currNode)
		c.nodes[key] = newNode
	}
}
