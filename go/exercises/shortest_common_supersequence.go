package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func shortestCommonSupersequence(s1 string, s2 string) string {
	str1 := []rune(s1)
	str2 := []rune(s2)
	str1Length := len(str1)
	str2Length := len(str2)

	dp := make([][]int, str1Length+1)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, str2Length+1)
	}

	// Initialize the base cases
	// When str2 is empty, the supersequence is str1 itself (length = row index)
	for row := 0; row <= str1Length; row++ {
		dp[row][0] = row
	}
	// When str1 is empty, the supersequence is str2 itself (length = col index)
	for col := 0; col <= str2Length; col++ {
		dp[0][col] = col
	}

	// Fill the DP table
	for row := 1; row <= str1Length; row++ {
		for col := 1; col <= str2Length; col++ {
			if str1[row-1] == str2[col-1] {
				// If characters match, inherit the length from the diagonal +1
				dp[row][col] = dp[row-1][col-1] + 1
			} else {
				// If characters do not match, take the minimum length option +1
				dp[row][col] = math_utils.Min(dp[row-1][col], dp[row][col-1]) + 1
			}
		}
	}

	supersequence := []rune{}
	row := str1Length
	col := str2Length

	for row > 0 && col > 0 {
		if str1[row-1] == str2[col-1] {
			// If characters match, take it from diagonal
			supersequence = append(supersequence, str1[row-1])
			row--
			col--
		} else if dp[row-1][col] < dp[row][col-1] {
			// If str1’s character is part of the supersequence, move up
			supersequence = append(supersequence, str1[row-1])
			row--
		} else {
			// If str2’s character is part of the supersequence, move left
			supersequence = append(supersequence, str2[col-1])
			col--
		}
	}

	// Append any remaining characters
	// If there are leftover characters in str1
	for row > 0 {
		supersequence = append(supersequence, str1[row-1])
		row--
	}
	// If there are leftover characters in str2
	for col > 0 {
		supersequence = append(supersequence, str2[col-1])
		col--
	}

	// Reverse the built sequence to get the correct order
	ReverseRunes(&supersequence)
	return string(supersequence)
}

func ReverseRunes(bs *[]rune) {
	ReverseRunesLR(bs, 0, len(*bs)-1)
}

func ReverseRunesLR(bs *[]rune, l, r int) {
	for i := (r-l+1)/2 - 1; i >= 0; i-- {
		t := (*bs)[l+i]
		(*bs)[l+i] = (*bs)[r-i]
		(*bs)[r-i] = t
	}
}
