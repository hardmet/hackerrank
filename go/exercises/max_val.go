package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func maxValue(n int, index int, maxSum int) int {
	maxSum -= n
	left, right := 0, maxSum
	var mid int
	for left < right {
		mid = (left + right + 1) / 2
		if calc(n, index, mid) <= int64(maxSum) {
			left = mid
		} else {
			right = mid - 1
		}
	}
	return left + 1
}

func calc(n, index, a int) int64 {
	b := math_utils.Max(a-index, 0)
	res := (int64(a) + int64(b)) * int64(a-b+1) / 2
	b = math_utils.Max(a-((n-1)-index), 0)
	res += (int64(a) + int64(b)) * int64(a-b+1) / 2
	return res - int64(a)
}
