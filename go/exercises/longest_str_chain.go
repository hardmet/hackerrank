package exercises

import "sort"

func longestStrChain(words []string) int {
	sort.Slice(words, func(i, j int) bool {
		return len(words[i]) < len(words[j])
	})
	counts := map[string]int{}
	ans := 0
	for _, word := range words {
		longest := 0
		for i := 0; i < len(word); i++ {
			sub := append([]byte(word)[:i], word[i+1:]...)
			value := counts[string(sub)] + 1
			if value > longest {
				longest = value
			}
		}
		counts[word] = longest
		if longest > ans {
			ans = longest
		}
	}
	return ans
}
