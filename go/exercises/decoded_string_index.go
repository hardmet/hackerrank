package exercises

import "unicode"

func decodeAtIndex(s string, k int) string {
	length := 0
	i := 0
	for length < k {
		if unicode.IsDigit(rune(s[i])) {
			length *= int(s[i] - '0')
		} else {
			length++
		}
		i++
	}
	for j := i - 1; j >= 0; j-- {
		if unicode.IsDigit(rune(s[j])) {
			length = length / int(s[j]-'0')
			k %= length
		} else {
			if k == 0 || k == length {
				return string(s[j])
			}
			length--
		}
	}
	return ""
}
