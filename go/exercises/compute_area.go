package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func ComputeArea(ax1 int, ay1 int, ax2 int, ay2 int, bx1 int, by1 int, bx2 int, by2 int) int {
	area := 0
	for i := ax1 + 1; i <= ax2; i++ {
		for j := ay1 + 1; j <= ay2; j++ {
			area++
		}
	}
	for i := bx1 + 1; i <= bx2; i++ {
		for j := by1 + 1; j <= by2; j++ {
			if !(ax1 < i && i <= ax2 && ay1 < j && j <= ay2) {
				area++
			}
		}
	}
	return area
}

func computeArea(ax1 int, ay1 int, ax2 int, ay2 int, bx1 int, by1 int, bx2 int, by2 int) int {
	area := func(x1, y1, x2, y2 int) int {
		return (x2 - x1) * (y2 - y1)
	}
	max := func(x, y int) int {
		if x > y {
			return x
		}
		return y
	}
	min := func(x, y int) int {
		if x < y {
			return x
		}
		return y
	}
	xOverlap := math_utils.Max(min(ax2, bx2)-max(ax1, bx1), 0)
	yOverlap := math_utils.Max(min(ay2, by2)-max(ay1, by1), 0)
	return area(ax1, ay1, ax2, ay2) + area(bx1, by1, bx2, by2) - xOverlap*yOverlap
}
