package exercises

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func oddEvenList(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	evenHead := head.Next
	cur := head.Next.Next
	odd := head
	even := head.Next
	i := 2
	for cur != nil {
		if i%2 == 1 {
			even.Next = cur
			even = even.Next
		} else {
			odd.Next = cur
			odd = odd.Next
		}
		cur = cur.Next
		i++
	}
	even.Next = nil
	odd.Next = evenHead
	return head
}

func TestOddEvenList() {
	list := ListNode{Val: 1, Next: &ListNode{Val: 2, Next: &ListNode{Val: 3, Next: &ListNode{Val: 4, Next: &ListNode{Val: 5, Next: nil}}}}}
	head := oddEvenList(&list)
	fmt.Println(head)
}
