package exercises

func getSumAbsoluteDifferences(nums []int) []int {
	prefixSum := make([]int, len(nums))
	suffixSum := make([]int, len(nums))
	prefixSum[0] = nums[0]
	suffixSum[len(suffixSum)-1] = nums[len(nums)-1]
	for i := 1; i < len(nums); i++ {
		prefixSum[i] = prefixSum[i-1] + nums[i]
		suffixSum[len(nums)-1-i] = suffixSum[len(nums)-i] + nums[len(nums)-1-i]
	}
	sumDiffs := make([]int, len(nums))
	for i := 1; i < len(nums)-1; i++ {
		sumDiffs[i] = nums[i]*i - prefixSum[i-1] + suffixSum[i+1] - nums[i]*(len(nums)-i-1)
	}
	sumDiffs[0] = suffixSum[1] - nums[0]*(len(nums)-1)
	sumDiffs[len(suffixSum)-1] = nums[len(nums)-1]*(len(nums)-1) - prefixSum[len(nums)-2]
	return sumDiffs
}
