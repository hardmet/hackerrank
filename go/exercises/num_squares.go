package exercises

import "math"

func numSquaresMath(n int) int {
	i := 1
	for i*i <= n {
		if i*i == n {
			return 1
		}
		i++
	}
	checkTwo := func(c int) bool {
		c = divide(c, 2)
		c = divide(c, 5)
		c = divide(c, 9)
		if c%3 == 0 {
			return false
		}
		if c == 0 || c == 1 || c == 13 || c == 17 {
			return true
		}
		i, j := 0, c
		for i <= j {
			if i*i+j*j == c {
				return true
			}
			if i*i+j*j < c {
				i++
			}
			if i*i+j*j > c {
				j -= 1
			}
		}
		return false
	}
	if checkTwo(n) {
		return 2
	}
	for n%4 == 0 {
		n = n / 4
	}
	if n%8 != 7 {
		return 3
	}
	return 4
}
func divide(n, d int) int {
	for n%d == 0 {
		n = n / d
	}
	return n
}

func numSquares(n int) int {
	cur := n
	steps := 0
	for cur != 0 {
		root := int(math.Sqrt(float64(cur)))
		cur = cur - root*root
		steps++
	}
	return steps
}

func Tests() {
	tests := [][2]int{{12, 3}, {13, 2}, {14, 3}, {15, 4}, {16, 1}}
	for _, t := range tests {
		res := numSquaresMath(t[0])
		if res != t[1] {
			print("wrong test: ")
			println(t[0])
			print("expected: ")
			println(t[1])
			print("  actual: ")
			println(res)
		}
	}
}
