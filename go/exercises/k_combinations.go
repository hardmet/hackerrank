package exercises

func combine(n, k int) [][]int {
	result := [][]int{}
	generate(1, n, k, &[]int{}, &result)
	return result
}

func generate(start, n, k int, current *[]int, result *[][]int) {
	if k == 0 {
		dst := make([]int, len(*current))
		copy(dst, *current)
		*result = append(*result, dst)
	} else {
		for i := start; i < n-k+2; i++ {
			*current = append(*current, i)
			generate(i+1, n, k-1, current, result)
			*current = (*current)[:len(*current)-1]
		}
	}
}
