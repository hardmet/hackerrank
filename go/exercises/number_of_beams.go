package exercises

func numberOfBeams(bank []string) int {
	prev, num := 0, 0
	for _, b := range bank {
		count := 0
		for i := 0; i < len(b); i++ {
			if b[i] == '1' {
				count++
			}
		}
		if count > 0 {
			num += prev * count
			prev = count
		}
	}
	return num
}
