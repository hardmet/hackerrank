package exercises

func destCity(paths [][]string) string {
	hasOutgoing := map[string]struct{}{}
	for _, path := range paths {
		hasOutgoing[path[0]] = struct{}{}
	}
	for _, path := range paths {
		candidate := path[1]
		if _, contains := hasOutgoing[candidate]; !contains {
			return candidate
		}
	}
	return ""
}
