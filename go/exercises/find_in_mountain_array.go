package exercises

type MountainArray struct {
}

func (this *MountainArray) get(index int) int { return 0 }
func (this *MountainArray) length() int       { return 0 }

func findInMountainArray(target int, mountainArr *MountainArray) int {
	length := mountainArr.length()
	low := 1
	high := length - 2
	for low != high {
		testIndex := (low + high) >> 1
		cur := mountainArr.get(testIndex)
		next := mountainArr.get(testIndex + 1)
		if cur < next {
			if cur == target {
				return testIndex
			}
			if next == target {
				return testIndex + 1
			}
			low = testIndex + 1
		} else {
			high = testIndex
		}
	}
	peakIndex := low
	low = 0
	high = peakIndex
	less := func(cur, target int) bool { return cur < target }
	more := func(cur, target int) bool { return cur > target }
	maybePeak := searchPeak(target, mountainArr, low, high, less)
	if maybePeak != -1 {
		return maybePeak
	}
	low = peakIndex + 1
	high = length - 1
	return searchPeak(target, mountainArr, low, high, more)
}

func searchPeak(target int, mountainArr *MountainArray, low, high int, predicate func(x, y int) bool) int {
	for low <= high {
		testIndex := (low + high) >> 1
		cur := mountainArr.get(testIndex)
		if cur == target {
			return testIndex
		} else if predicate(cur, target) {
			low = testIndex + 1
		} else {
			high = testIndex - 1
		}
	}
	return -1
}
