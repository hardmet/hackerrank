package exercises

import "unicode"

func recoverFromPreorder(traversal string) *TreeNode {
	stack := []*TreeNode{}

	for i := 0; i < len(traversal); {
		depth := 0
		for traversal[i] == '-' {
			depth++
			i++
		}

		value := 0
		for i < len(traversal) && unicode.IsDigit(rune(traversal[i])) {
			value = value*10 + int(traversal[i]-'0')
			i++
		}
		for len(stack) > depth {
			stack = stack[:len(stack)-1]
		}
		node := &TreeNode{Val: value}
		if len(stack) > 0 {
			if stack[len(stack)-1].Left == nil {
				stack[len(stack)-1].Left = node
			} else {
				stack[len(stack)-1].Right = node
			}
		}
		stack = append(stack, node)
	}
	for len(stack) > 1 {
		stack = stack[:len(stack)-1]
	}
	return stack[len(stack)-1]
}
