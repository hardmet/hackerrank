package exercises

import (
	"math/rand"
)

type RandomizedSet struct {
	values map[int]int
	ids    []int
}

func constructor() RandomizedSet {
	return RandomizedSet{
		values: map[int]int{},
		ids:    []int{},
	}
}

func (s *RandomizedSet) Insert(val int) bool {
	_, contains := s.values[val]
	if contains {
		return false
	}
	s.values[val] = len(s.ids)
	s.ids = append(s.ids, val)
	return true
}

func (s *RandomizedSet) Remove(val int) bool {
	_, contains := s.values[val]
	if contains {
		position := s.values[val]
		last := s.ids[len(s.ids)-1]
		s.values[last] = position
		s.ids[position] = last
		delete(s.values, val)
		s.ids = s.ids[:len(s.ids)-1]
	}
	return false
}

func (s *RandomizedSet) GetRandom() int {
	return s.ids[rand.Int()%len(s.ids)]
}
