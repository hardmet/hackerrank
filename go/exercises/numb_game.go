package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func PredictTheWinner(nums []int) bool {
	dp := make([][]int, len(nums))
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, len(nums))
		for j := 0; j < len(dp[i]); j++ {
			dp[i][j] = -1
		}
	}
	return dpRec(dp, nums, 0, len(nums)-1, 0) >= 0
}

func dpRec(dp [][]int, nums []int, i, j, k int) int {
	if i > j {
		return 0
	}
	if dp[i][j] != -1 {
		return dp[i][j]
	}
	fst, last, ans := 0, 0, 0
	if k%2 == 0 {
		fst = nums[i] + dpRec(dp, nums, i+1, j, k+1)
		last = nums[j] + dpRec(dp, nums, i, j-1, k+1)
		ans = math_utils.Max(fst, last)
	} else {
		fst = -nums[i] + dpRec(dp, nums, i+1, j, k+1)
		last = -nums[j] + dpRec(dp, nums, i, j-1, k+1)
		ans = math_utils.Min(fst, last)
	}
	dp[i][j] = ans
	return ans
}
