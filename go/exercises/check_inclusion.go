package exercises

func checkInclusion(s1, s2 string) bool {
	if len(s1) > len(s2) {
		return false
	}
	frequency := [256]int{}
	left, right := 0, 0
	count := len(s1)
	for i := range s1 {
		frequency[s1[i]-'a']++
	}
	for right < len(s2) {
		if frequency[s2[right]-'a'] >= 1 {
			count--
		}
		frequency[s2[right]-'a']--
		right++
		if count == 0 {
			return true
		}
		if right-left == len(s1) {
			if frequency[s2[left]-'a'] >= 0 {
				count++
			}
			frequency[s2[left]-'a']++
			left++
		}
	}
	return false
}
