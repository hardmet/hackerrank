package exercises

func sumSubarrayMins(a []int) int {
	var stack []int
	sums := make([]int, len(a))
	mod := 1_000_000_007

	for i, n := range a {
		for len(stack) > 0 && a[stack[len(stack)-1]] > n {
			stack = stack[:len(stack)-1]
		}
		var j int
		if len(stack) > 0 {
			j = stack[len(stack)-1]
			sums[i] = (sums[j] + (i-j)*n) % mod
		} else {
			j = -1
			sums[i] = ((i - j) * n) % mod
		}
		stack = append(stack, i)
	}
	var res int
	for _, s := range sums {
		res = (res + s) % mod
	}
	return res
}
