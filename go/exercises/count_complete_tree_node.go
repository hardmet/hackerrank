package exercises

import "math"

func countNodes(root *TreeNode) int {
	if root == nil {
		return 0
	}
	left, right := root, root
	l, r := 0.0, 0.0
	for left != nil {
		l++
		left = left.Left
	}
	for right != nil {
		r++
		right = right.Right
	}
	if l == r {
		return int(math.Pow(2.0, l)) - 1
	}
	return 1 + countNodes(root.Left) + countNodes(root.Right)
}
