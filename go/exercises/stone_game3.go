package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"math"
)

func stoneGameIII(stoneValue []int) string {
	n := len(stoneValue)
	dp := make([]int, n+1)
	for i := n - 1; i >= 0; i-- {
		dp[i] = math.MinInt
		for k, take := 0, 0; k < 3 && i+k < n; k++ {
			take += stoneValue[i+k]
			dp[i] = math_utils.Max(dp[i], take-dp[i+k+1])
		}
	}
	if dp[0] > 0 {
		return "Alice"
	}
	if dp[0] < 0 {
		return "Bob"
	}
	return "Tie"
}
