package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"sort"
)

func findMinArrowShots(points [][]int) int {
	sort.Slice(points, func(a, b int) bool {
		return points[a][0] < points[b][0]
	})
	last := points[0][1]
	ans := 1
	for _, point := range points {
		if point[0] > last {
			ans++
			last = point[1]
		}
		last = math_utils.Min(point[1], last)
	}
	return ans
}
