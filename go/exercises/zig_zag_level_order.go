package exercises

func zigzagLevelOrder(root *TreeNode) [][]int {
	res := [][]int{}
	if root == nil {
		return res
	}
	stack := []*TreeNode{root}
	var level int = 1
	for len(stack) > 0 {
		list := []int{}
		size := len(stack)
		for size != 0 {
			node := stack[0]
			stack = stack[1:]
			list = append(list, node.Val)
			if node.Left != nil {
				stack = append(stack, node.Left)
			}
			if node.Right != nil {
				stack = append(stack, node.Right)
			}
			size--
		}
		if level%2 == 0 {
			reverseIntLR(&list, 0, len(list)-1)
		}
		res = append(res, list)
		level++
	}
	return res
}

func reverseIntLR(bs *[]int, l, r int) {
	for i := (r-l+1)/2 - 1; i >= 0; i-- {
		t := (*bs)[l+i]
		(*bs)[l+i] = (*bs)[r-i]
		(*bs)[r-i] = t
	}
}
