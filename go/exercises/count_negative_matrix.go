package exercises

func countNegatives(grid [][]int) int {
	// bfs
	c := 0
	n := len(grid[0])
	currRowNegativeIndex := n - 1
	for _, row := range grid {
		for currRowNegativeIndex >= 0 && row[currRowNegativeIndex] < 0 {
			currRowNegativeIndex--
		}
		c += n - (currRowNegativeIndex + 1)
	}
	return c
}

func countNegatives2(grid [][]int) int {
	// bfs
	c := 0
	q := [][2]int{{len(grid) - 1, len(grid[0]) - 1}}
	for len(q) > 0 {
		if grid[q[0][0]][q[0][1]] < 0 {
			grid[q[0][0]][q[0][1]] = 0
			c++
			point := q[0]
			if point[1]-1 >= 0 {
				q = append(q, [2]int{point[0], point[1] - 1})
			}
			if point[0]-1 >= 0 {
				q = append(q, [2]int{point[0] - 1, point[1]})
			}
		}
		q = q[1:]
	}
	return c
}

func CountNegativesTests() {
	countNegatives(
		[][]int{
			{4, 3, 2, -1},
			{3, 2, 1, -1},
			{1, 1, -1, -2},
			{-1, -1, -2, -3},
		})
}
