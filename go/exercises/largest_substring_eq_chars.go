package exercises

func maxLengthBetweenEqualCharacters(s string) int {
	firstOccurence := map[rune]int{}
	maxLength := -1
	for current, letter := range s {
		if fst, contains := firstOccurence[letter]; contains {
			if current-fst-1 > maxLength {
				maxLength = current - fst - 1
			}
		} else {
			firstOccurence[letter] = current
		}
	}
	return maxLength
}
