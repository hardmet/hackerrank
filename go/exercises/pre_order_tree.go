package exercises

func preorderTraversal(root *TreeNode) []int {
	res := []int{}
	traverse(root, &res)
	return res
}

func traverse(root *TreeNode, result *[]int) {
	if root != nil {
		*result = append(*result, root.Val)
		traverse(root.Left, result)
		traverse(root.Right, result)
	}
}
