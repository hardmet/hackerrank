package exercises

func generatePascal(numRows int) [][]int {
	if numRows == 0 {
		return [][]int{}
	}
	rows := [][]int{{1}}
	for i := 1; i < numRows; i++ {
		newRow := []int{1}
		prevRow := rows[i-1]
		for j := 1; j < len(prevRow); j++ {
			newRow = append(newRow, prevRow[j-1]+prevRow[j])
		}
		newRow = append(newRow, 1)
		rows = append(rows, newRow)
	}
	return rows
}
