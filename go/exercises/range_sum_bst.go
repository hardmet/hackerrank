package exercises

func rangeSumBST(root *TreeNode, low, high int) int {
	stack := []*TreeNode{root}
	sum := 0
	for len(stack) > 0 {
		head := stack[0]
		stack = stack[1:]
		if head.Val >= low && head.Val <= high {
			sum += head.Val
		}
		if head.Right != nil && head.Val < high {
			stack = append(stack, head.Right)
		}
		if head.Left != nil && head.Val > low {
			stack = append(stack, head.Left)
		}
	}
	return sum
}
