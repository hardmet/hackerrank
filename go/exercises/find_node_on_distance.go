package exercises

func distanceK(root *TreeNode, target *TreeNode, k int) []int {
	graph := make(map[int][]int)
	buildAdjacencyList(root, nil, graph)

	ans := []int{}
	visited := make([]bool, 501)
	visited[target.Val] = true
	for q := [][2]int{{target.Val, 0}}; len(q) > 0; q = q[1:] {
		nodeVal, dist := q[0][0], q[0][1]

		if dist == k {
			ans = append(ans, nodeVal)
		} else {
			for _, neighbor := range graph[nodeVal] {
				if !visited[neighbor] {
					visited[neighbor] = true
					q = append(q, [2]int{neighbor, dist + 1})
				}
			}
		}
	}

	return ans
}

func buildAdjacencyList(curr *TreeNode, parent *TreeNode, graph map[int][]int) {
	if curr != nil && parent != nil {
		graph[curr.Val] = append(graph[curr.Val], parent.Val)
		graph[parent.Val] = append(graph[parent.Val], curr.Val)
	}

	if curr.Left != nil {
		buildAdjacencyList(curr.Left, curr, graph)
	}
	if curr.Right != nil {
		buildAdjacencyList(curr.Right, curr, graph)
	}
}
