package exercises

func checkStraightLine(coordinates [][]int) bool {
	if len(coordinates) == 2 {
		return true
	}
	for i := 2; i < len(coordinates); i++ {
		if !isStraightLine(coordinates[i], coordinates[0], coordinates[1]) {
			return false
		}
	}
	return true
}

func isStraightLine(p, p1, p2 []int) bool {
	return (p[1]-p1[1])*(p2[0]-p1[0]) == (p2[1]-p1[1])*(p[0]-p1[0])
}

func TestCheckStraightLine() {
	println(checkStraightLine([][]int{{2, 1}, {4, 2}, {6, 3}}))
	println(checkStraightLine([][]int{{0, 0}, {0, 1}, {0, -1}}))
	println(checkStraightLine([][]int{{1, 1}, {2, 2}, {2, 0}}))
}
