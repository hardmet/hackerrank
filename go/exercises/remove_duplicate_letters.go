package exercises

func removeDuplicateLetters(s string) string {
	lastIndex := [26]int{}
	for i := 0; i < len(s); i++ {
		lastIndex[s[i]-'a'] = i
	}
	seen := [26]bool{}
	st := []byte{}
	for i := 0; i < len(s); i++ {
		cur := s[i] - 'a'
		if seen[cur] {
			continue
		}
		for len(st) > 0 && st[len(st)-1] > cur && i < lastIndex[st[len(st)-1]] {
			seen[st[len(st)-1]] = false
			st = st[:len(st)-1]
		}
		st = append(st, cur)
		seen[cur] = true
	}
	result := []byte{}
	for len(st) > 0 {
		result = append(result, st[len(st)-1]+'a')
		st = st[:len(st)-1]
	}
	reverse(&result, 0, len(result)-1)
	return string(result)
}

func reverse(bs *[]byte, l, r int) {
	for i := (r-l+1)/2 - 1; i >= 0; i-- {
		t := (*bs)[l+i]
		(*bs)[l+i] = (*bs)[r-i]
		(*bs)[r-i] = t
	}
}
