package exercises

func countPaths(grid [][]int) int {
	n := len(grid)
	m := len(grid[0])

	dp := make([][]int, n)
	for i, row := range dp {
		row = make([]int, m)
		for j := range row {
			dp[i][j] = -1
		}
	}
	paths := 0
	for i := 0; i < n; i++ {
		for j := 0; j < m; j++ {
			paths = (paths + countPathsBFS(grid, i, j, -1, dp)) % 1_000_000_007
		}
	}

	return paths
}

func countPathsBFS(grid [][]int, i, j, prev int, dp [][]int) int {
	if i < 0 || j < 0 || i >= len(grid) || j >= len(grid[0]) || grid[i][j] <= prev {
		return 0
	}

	if dp[i][j] != -1 {
		return dp[i][j]
	}

	left := countPathsBFS(grid, i, j-1, grid[i][j], dp)
	right := countPathsBFS(grid, i, j+1, grid[i][j], dp)
	up := countPathsBFS(grid, i-1, j, grid[i][j], dp)
	down := countPathsBFS(grid, i+1, j, grid[i][j], dp)

	dp[i][j] = (1 + left + right + up + down) % 1_000_000_007
	return dp[i][j]
}
