package exercises

func countCharacters(words []string, chars string) int {
	count := 0
	charsCount := [26]int{}
	for _, char := range chars {
		charsCount[char-'a']++
	}
	for _, word := range words {
		wordCharsCount := [26]int{}
		for i := 0; i < 26; i++ {
			wordCharsCount[i] = charsCount[i]
		}
		isEnough := true
		for _, char := range word {
			wordCharsCount[char-'a']--
			if wordCharsCount[char-'a'] < 0 {
				isEnough = false
				break
			}
		}
		if isEnough {
			count += len(word)
		}
	}
	return count
}
