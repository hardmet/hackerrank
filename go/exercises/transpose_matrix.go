package exercises

func transpose(matrix [][]int) [][]int {
	r, c := len(matrix), len(matrix[0])
	res := make([][]int, c)
	for i := 0; i < r; i++ {
		for j := 0; j < c; j++ {
			if i == 0 {
				res[j] = make([]int, r)
			}
			res[j][i] = matrix[i][j]
		}
	}
	return res
}
