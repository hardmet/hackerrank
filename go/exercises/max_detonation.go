package exercises

import "math"

func maximumDetonation(bombs [][]int) int {
	var dfs func(k int, visited []bool, count int) int
	dfs = func(k int, visited []bool, count int) int {
		count++
		visited[k] = true
		for i := 0; i < len(bombs); i++ {
			if !visited[i] && isInRange(bombs[k], bombs[i]) {
				visited[i] = true
				// detonate another one bomb and looking for adjacent bombs
				count = dfs(i, visited, count)
			}
		}
		return count
	}
	ans := math.MinInt
	// count number bombs in range starting from each bomb and take max
	for i := 0; i < len(bombs); i++ {
		c := dfs(i, make([]bool, len(bombs)), 0)
		if c > ans {
			ans = c
		}
	}
	return ans
}

func isInRange(p1, p2 []int) bool {
	dx := int64(p1[0]) - int64(p2[0])
	dy := int64(p1[1]) - int64(p2[1])
	dSquare := dx*dx + dy*dy
	rSquare := int64(p1[2]) * int64(p1[2])
	return dSquare <= rSquare
}
