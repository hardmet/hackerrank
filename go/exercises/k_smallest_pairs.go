package exercises

import (
	"container/heap"

	"github.com/hardmet/hackerrank/go/priority_queue"
)

type PairWithIndex struct {
	values []int
}

func makePair(a, b, i int) PairWithIndex {
	return PairWithIndex{[]int{a, b, i, a + b}}
}

func (w PairWithIndex) ToInt() int {
	return w.values[3]
}

func kSmallestPairs(nums1 []int, nums2 []int, k int) [][]int {
	var result [][]int
	if len(nums1) == 0 || len(nums2) == 0 {
		return result
	}

	q := priority_queue.PriorityQueueInt64{}
	for i := 0; i < len(nums1) && i < k; i++ { // only need first k number in nums1 to start
		heap.Push(&q, makePair(nums1[i], nums2[0], 0))
	}
	for i := 1; i <= k && q.Len() > 0; i++ { // get the first k sums
		p := heap.Pop(&q).(PairWithIndex)
		result = append(result, p.values[:2])
		if p.values[2] < len(nums2)-1 { // get to next value in nums2
			next := p.values[2] + 1
			heap.Push(&q, makePair(p.values[0], nums2[next], next))
		}
	}
	return result
}
