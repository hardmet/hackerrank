package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func minimizeArrayValue(nums []int) int {
	var sum, res int64
	for i := 0; i < len(nums); i++ {
		sum += int64(nums[i])
		longI := int64(i)
		res = math_utils.Max64(res, (sum+longI)/(longI+1))
	}
	return int(res)
}
