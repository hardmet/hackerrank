package exercises

import (
	"container/heap"
	"math"

	"github.com/hardmet/hackerrank/go/priority_queue"
)

type Worker struct {
	cost   int
	isLeft bool
}

func (w Worker) ToInt() int {
	return w.cost
}

func (w Worker) SecondaryToInt() int {
	if w.isLeft {
		return math.MinInt
	}
	return math.MaxInt
}

func totalCost(costs []int, k int, candidates int) int64 {
	i, j := 0, len(costs)-1
	q := priority_queue.PriorityQueueInt64{}

	for ; i < candidates && i <= j; i++ {
		heap.Push(&q, Worker{costs[i], true})
	}
	for ; len(costs)-1-j < candidates && i <= j; j-- {
		heap.Push(&q, Worker{costs[j], false})
	}
	var totalCost int64
	for ; k > 0; k-- {
		w := heap.Pop(&q).(Worker)
		totalCost += int64(w.ToInt())
		if i <= j {
			if w.isLeft {
				heap.Push(&q, Worker{costs[i], true})
				i++
			} else {
				heap.Push(&q, Worker{costs[j], false})
				j--
			}
		}
	}
	return totalCost
}
