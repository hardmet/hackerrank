package exercises

func backspaceCompare(s string, t string) bool {
	for i, j := len(s)-1, len(t)-1; i >= 0; i, j = i-1, j-1 {
	loop1:
		for skip := 0; i >= 0; {
			switch {
			case s[i] == '#':
				i--
				skip++
			case skip > 0:
				i--
				skip--
			case true:
				break loop1
			}
		}
	loop2:
		for skip := 0; j >= 0; {
			switch {
			case t[j] == '#':
				j--
				skip++
			case skip > 0:
				j--
				skip--
			case true:
				break loop2
			}
		}
		if i < 0 && j < 0 {
			return true
		}
		if i < 0 || j < 0 || s[i] != t[j] {
			return false
		}
	}
	return true
}

// bs1 := removeSymbol([]byte(s), '#')
// bs2 := removeSymbol([]byte(t), '#')
// if len(bs1) != len(bs2) {
// 	return false
// }
// for i := 0; i < len(bs1); i++ {
// 	if bs1[i] != bs2[i] {
// 		return false
// 	}
// }
// return true

func removeSymbol(bs []byte, symbol byte) []byte {
	res := bs
	for i := 0; i < len(bs); i++ {
		if bs[i] == symbol {
			if len(res) > 0 {
				res = res[:len(res)-1]
			}
		} else {
			res = append(res, bs[i])
		}
	}
	return res
}
