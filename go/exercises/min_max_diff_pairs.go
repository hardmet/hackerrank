package exercises

import "sort"

func minimizeMax(nums []int, p int) int {
	sort.Ints(nums)
	l, r := 0, nums[len(nums)-1]-nums[0]
	for l < r {
		mid := (l + r) / 2
		if countValidPairs(&nums, mid) >= p {
			r = mid
		} else {
			l = mid + 1
		}
	}
	return l
}

func countValidPairs(nums *[]int, threshold int) int {
	count := 0
	for i := 0; i < len(*nums)-1; {
		if (*nums)[i+1]-(*nums)[i] <= threshold {
			count++
			i++
		}
		i++
	}
	return count
}
