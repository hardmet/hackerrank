package exercises

func sumNumbers(root *TreeNode) int {
	return dfsSumRoot(root.Left, root.Val, 0) +
		dfsSumRoot(root.Right, root.Val, 0)
}

func dfsSumRoot(root *TreeNode, n, acc int) int {
	if root == nil {
		return n + acc
	}
	n = n*10 + root.Val
	return dfsSumRoot(root.Left, n, acc) + dfsSumRoot(root.Right, n, acc)
}
