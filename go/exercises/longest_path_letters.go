package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func longestPath(parent []int, s string) int {
	children := make([][]int, len(parent))
	for i := 0; i < len(parent); i++ {
		children[i] = []int{}
	}
	for i := 1; i < len(parent); i++ {
		children[parent[i]] = append(children[parent[i]], i)
	}
	_, r := dfsLongestPathLetters(0, children, s, 1)
	return r
}

func dfsLongestPathLetters(root int, children [][]int, s string, result int) (int, int) {
	if len(children[root]) == 0 {
		return 1, result
	}
	longest, secondLongest := 0, 0
	for _, child := range children[root] {
		var longestFromChild int
		longestFromChild, result = dfsLongestPathLetters(child, children, s, result)
		if s[root] == s[child] {
			continue
		}
		if longestFromChild > longest {
			secondLongest = longest
			longest = longestFromChild
		} else if longestFromChild > secondLongest {
			secondLongest = longestFromChild
		}
	}
	result = math_utils.Max(result, longest+secondLongest+1)
	return longest + 1, result
}
