package exercises

import "math"

func maxScore1(s string) int {
	ones, zeros := 0, 0
	maxScore := math.MinInt
	for i := 0; i < len(s)-1; i++ {
		if s[i] == '1' {
			ones++
		} else {
			zeros++
		}
		if maxScore < zeros-ones {
			maxScore = zeros - ones
		}
	}
	if s[len(s)-1] == '1' {
		ones++
	}
	return maxScore + ones
}
