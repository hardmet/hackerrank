package exercises

func numWays(words []string, target string) int {
	n, m := len(words[0]), len(target)
	dp := make([]int, m+1)
	dp[0] = 1
	count := make([][]int, n)
	for i := 0; i < n; i++ {
		count[i] = make([]int, 26)
	}
	for i := 0; i < n; i++ {
		for _, word := range words {
			count[i][word[i]-'a']++
		}
	}
	for i := 0; i < n; i++ {
		for j := m - 1; j >= 0; j-- {
			dp[j+1] += dp[j] * count[i][target[j]-'a']
			dp[j+1] = dp[j+1] % 1_000_000_007
		}
	}

	return dp[m]
}
