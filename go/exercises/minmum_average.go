package exercises

import (
	"math"

	"github.com/hardmet/hackerrank/go/math_utils"
)

func minimumAverageDifference(nums []int) int {
	n := len(nums)
	minAvgDiff := math.MaxInt
	currPrefixSum := 0
	ans := -1
	totalSum := 0
	for _, num := range nums {
		totalSum += num
	}
	for i := 0; i < n; i++ {
		currPrefixSum += nums[i]
		leftPartAverage := currPrefixSum / (i + 1)
		rightPartAverage := totalSum - currPrefixSum
		if i != n-1 {
			rightPartAverage = rightPartAverage / (n - i - 1)
		}
		currDifference := math_utils.AbsDiff(leftPartAverage, rightPartAverage)
		if currDifference < minAvgDiff {
			minAvgDiff = currDifference
			ans = i
		}
	}
	return ans
}
