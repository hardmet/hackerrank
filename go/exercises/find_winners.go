package exercises

import "sort"

func findWinners(matches [][]int) [][]int {
	loses := map[int]int{}
	for _, match := range matches {
		loses[match[1]] = loses[match[1]] + 1
		_, contains := loses[match[0]]
		if !contains {
			loses[match[0]] = 0
		}
	}
	ans := [][]int{{}, {}}
	for player, lose := range loses {
		if lose == 1 {
			ans[1] = append(ans[1], player)
		}
		if lose == 0 {
			ans[0] = append(ans[0], player)
		}
	}
	sort.Slice(ans[0], func(i, j int) bool {
		return ans[0][i] < ans[0][j]
	})
	sort.Slice(ans[1], func(i, j int) bool {
		return ans[1][i] < ans[1][j]
	})
	return ans
}
