package exercises

import (
	"github.com/hardmet/hackerrank/go/trie"
	"regexp"
)

type WordDictionary struct {
	trie *trie.Trie
}

func ConstructorWordDictionary() WordDictionary {
	tr := trie.ConstructorTrie()
	return WordDictionary{trie: &tr}
}

func (d *WordDictionary) AddWord(word string) {
	d.trie.Insert(word)
}

func (d *WordDictionary) Search(word string) bool {
	value := d.trie.GetVal()
	return regexp.MustCompile(word).Find([]byte(value)) != nil
}
