package exercises

type BrowserHistory struct {
	stack []string
	i     int
}

func ConstructorBrowserHistory(homepage string) BrowserHistory {
	return BrowserHistory{[]string{homepage}, 0}
}

func (h *BrowserHistory) Visit(url string) {
	if h.i == len(h.stack)-1 {
		h.stack = append(h.stack, url)
		h.i++
	} else {
		h.stack = append(h.stack[:h.i+1], url)
		h.i++
	}
}

func (h *BrowserHistory) Back(steps int) string {
	if h.i-steps < 0 {
		h.i = 0
	} else {
		h.i -= steps
	}
	return h.stack[h.i]
}

func (h *BrowserHistory) Forward(steps int) string {
	if h.i-steps < 0 {
		h.i = len(h.stack) - 1
	} else {
		h.i += steps
	}
	return h.stack[h.i]
}
