package exercises

import "sort"

func answerQueries(nums []int, queries []int) []int {
	sort.Slice(nums, func(i, j int) bool {
		if nums[i] < nums[j] {
			return true
		}
		return false
	})
	for i := 1; i < len(nums); i++ {
		nums[i] = nums[i-1] + nums[i]
	}
	answer := make([]int, len(queries))
	for i, q := range queries {
		answer[i] = sort.Search(len(nums), func(x int) bool {
			return nums[x] <= q
		})
	}
	return answer
}
