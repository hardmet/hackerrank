package exercises

func isValid(s string) bool {
	if len(s) == 0 {
		return true
	}
	openBraces := []rune{'(', '[', '{'}
	closeBraces := []rune{')', ']', '}'}
	var stack []rune
	for _, si := range []rune(s) {
		if contains(si, openBraces) {
			stack = append(stack, si)
		} else {
			for i := range openBraces {
				if si == closeBraces[i] && !shouldHasOpened(openBraces[i], &stack) {
					return false
				}
			}
		}
	}
	return len(stack) == 0
}

func contains(s rune, openBraces []rune) bool {
	for _, openBrace := range openBraces {
		if s == openBrace {
			return true
		}
	}
	return false
}

func shouldHasOpened(expected rune, stack *[]rune) bool {
	if len(*stack) == 0 {
		return false
	}
	if (*stack)[len(*stack)-1] == expected {
		*stack = (*stack)[0 : len(*stack)-1]
		return true
	} else {
		return false
	}
}
