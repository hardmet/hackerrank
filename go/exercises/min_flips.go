package exercises

func minFlips(a int, b int, c int) int {
	equal := (a | b) ^ c
	ans := 0
	var mask int
	for i := 0; i < 31; i++ {
		mask = 1 << i
		if equal&mask > 0 {
			if a&mask == b&mask && c&mask == 0 {
				ans += 2
			} else {
				ans += 1
			}
		}
	}
	return ans
}
