package exercises

import "math"

func minSpeedOnTime(dist []int, hour float64) int {
	l, r := 1, int(1e7)
	minSpeed := -1
	for l <= r {
		mid := (l + r) / 2
		if isPossible(dist, mid, hour) {
			minSpeed = mid
			r = mid - 1
		} else {
			l = mid + 1
		}
	}
	return minSpeed
}

func isPossible(dist []int, speed int, hour float64) bool {
	var acc float64
	speed64 := float64(speed)
	for i, d := range dist {
		time := float64(d) / speed64
		if i != len(dist)-1 {
			acc = acc + math.Ceil(time)
		} else {
			acc += time
		}
		if acc > hour {
			return false
		}
	}
	return acc <= hour
}
