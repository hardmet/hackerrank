package exercises

import (
	"math"

	"github.com/hardmet/hackerrank/go/math_utils"
)

func minimumDeleteSum(s1, s2 string) int {
	dp := make([][]int, 1002)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, 1002)
		for j := 0; j < len(dp[i]); j++ {
			dp[i][j] = -1
		}
	}
	var dpRec func(i, j int) int
	dpRec = func(i, j int) int {
		if dp[i][j] != -1 {
			return dp[i][j]
		}
		if i == len(s1) {
			res := 0
			for k := j; k < len(s2); k++ {
				res += int(s2[k])
			}
			return res
		}
		if j == len(s2) {
			res := 0
			for k := i; k < len(s1); k++ {
				res += int(s1[k])
			}
			return res
		}
		var sol2 int = math.MaxInt
		sol1 := math_utils.Min(int(s1[i])+dpRec(i+1, j), int(s2[j])+dpRec(i, j+1))
		if s1[i] == s2[j] {
			sol2 = dpRec(i+1, j+1)
		}
		dp[i][j] = math_utils.Min(sol1, sol2)
		return dp[i][j]
	}
	return dpRec(0, 0)
}
