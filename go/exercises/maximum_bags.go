package exercises

import "sort"

func maximumBags(capacity []int, rocks []int, additionalRocks int) int {
	remaining := make([]int, len(capacity))
	for i := range capacity {
		remaining[i] = capacity[i] - rocks[i]
	}
	sort.Sort(sort.IntSlice(remaining))
	fullBags := 0
	for i := range capacity {
		if additionalRocks >= remaining[i] {
			additionalRocks -= remaining[i]
			fullBags++
		} else {
			break
		}
	}
	return fullBags
}
