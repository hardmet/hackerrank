package exercises

func partition(s string) [][]string {
	return dfsPartition(0, [][]string{}, []string{}, s)
}

func dfsPartition(start int, result [][]string, current []string, s string) [][]string {
	if start >= len(s) {
		result = append(result, append([]string{}, current...))
	}
	for end := start; end < len(s); end++ {
		if isPalindrome(s, start, end) {
			current = append(current, s[start:end+1])
			result = dfsPartition(end+1, result, current, s)
			current = current[:len(current)-1]
		}
	}
	return result
}

func isPalindrome(s string, low, high int) bool {
	for low < high {
		if s[low] != s[high] {
			return false
		}
		low++
		high--
	}
	return true
}
