package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"math"
)

func maxProfit(prices []int) int {
	sell, prevSell := 0, 0
	prevBuy, buy := math.MinInt, math.MinInt
	for _, price := range prices {
		prevBuy = buy
		buy = maxXY(prevSell-price, prevBuy)
		prevSell = sell
		sell = maxXY(prevBuy+price, prevSell)
	}
	return sell
}

/*
1. Define States

To represent the decision at index i:

buy[i]: Max profit till index i. The series of transaction is ending with a buy.
sell[i]: Max profit till index i. The series of transaction is ending with a sell.
To clarify:

Till index i, the buy / sell action must happen and must be the last action. It may not happen at index i. It may happen at i - 1, i - 2, ... 0.
In the end n - 1, return sell[n - 1]. Apparently we cannot finally end up with a buy. In that case, we would rather take a rest at n - 1.
For special case no transaction at all, classify it as sell[i], so that in the end, we can still return sell[n - 1]. Thanks @alex153 @kennethliaoke @anshu2.
2. Define Recursion

buy[i]: To make a decision whether to buy at i, we either take a rest, by just using the old decision at i - 1, or sell at/before i - 2, then buy at i, We cannot sell at i - 1, then buy at i, because of cooldown.
sell[i]: To make a decision whether to sell at i, we either take a rest, by just using the old decision at i - 1, or buy at/before i - 1, then sell at i.
So we get the following formula:

buy[i] = Math.max(buy[i - 1], sell[i - 2] - prices[i]);
sell[i] = Math.max(sell[i - 1], buy[i - 1] + prices[i]);
*/
func maxProfit2(prices []int) int {
	if len(prices) <= 1 {
		return 0
	}
	buy := make([]int, len(prices), len(prices))
	sell := make([]int, len(prices), len(prices))
	cooldown := make([]int, len(prices), len(prices))
	buy[0] = -prices[0]
	for i := 1; i < len(prices); i++ {
		cooldown[i] = sell[i-1]

		sell[i] = math_utils.Max(sell[i-1], buy[i-1]+prices[i])
		buy[i] = math_utils.Max(buy[i-1], cooldown[i-1]-prices[i])
	}
	return math_utils.Max(sell[len(prices)-1], cooldown[len(prices)-1])
}

func maxXY(x, y int) int {
	if x > y {
		return x
	}
	return y
}
