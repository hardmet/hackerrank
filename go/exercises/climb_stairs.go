package exercises

func climbStairs(n int) int {
	return fibonachi(n)
}

func fibonachi(n int) int {
	if n == 0 {
		return 0
	}
	if n == 1 {
		return 1
	}
	if n == 2 {
		return 2
	}
	fn1 := 2
	fn2 := 1
	res := 0
	for i := 2; i < n; i++ {
		res = fn1 + fn2
		fn2 = fn1
		fn1 = res
	}
	return res
}
