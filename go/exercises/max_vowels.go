package exercises

func maxVowels(s string, k int) int {
	var isVowel = func(c byte) bool {
		switch c {
		case 'a', 'e', 'i', 'o', 'u':
			return true
		default:
			return false
		}
	}
	n := len(s)
	var count = 0
	for i := 0; i < k; i++ {
		if isVowel(s[i]) {
			count++
		}
	}
	ans := count
	for i := k; i < n; i++ {
		if isVowel(s[i]) {
			count++
		}
		if isVowel(s[i-k]) {
			count--
		}
		if count > ans {
			ans = count
		}
	}
	return ans
}
