package exercises

import "github.com/hardmet/hackerrank/go/int_union_find"

func validPath(n int, edges [][]int, source, destination int) bool {
	uf := int_union_find.MapUFConstructor()
	for _, edge := range edges {
		uf.Union(edge[0], edge[1])
	}
	return uf.Find(source) == uf.Find(destination)
}
