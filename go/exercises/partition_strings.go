package exercises

func partitionString(s string) int {
	i, ans, flag := 0, 1, 0
	var ch uint8
	for i < len(s) {
		ch = s[i] - 'a'
		if (flag & (1 << ch)) != 0 {
			flag = 0
			ans++
		}
		flag = flag | (1 << ch)
		i++
	}
	return ans
}
