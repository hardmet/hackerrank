package exercises

func countVowelPermutation(n int) int {
	const MOD = 1e9 + 7
	dp := make([][]int, n+1)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, 5)
	}
	for i := 0; i < 5; i++ {
		dp[1][i] = 1
	}
	for i := 1; i < n; i++ {
		dp[i+1][0] = (dp[i][1] + dp[i][2] + dp[i][4]) % MOD
		dp[i+1][1] = (dp[i][0] + dp[i][2]) % MOD
		dp[i+1][2] = (dp[i][1] + dp[i][3]) % MOD
		dp[i+1][3] = (dp[i][2]) % MOD
		dp[i+1][4] = (dp[i][2] + dp[i][3]) % MOD
	}
	var count int = 0
	for i := 0; i < 5; i++ {
		count = (count + dp[n][i]) % MOD
	}
	return count
}
