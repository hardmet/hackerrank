package exercises

import "fmt"

func smallestNumber(pattern string) string {
	result := []byte{}
	stack := make([]int, len(pattern)+1)
	index := 0
	for i := 0; i <= len(pattern); i++ {
		stack[index] = i + 1
		index++
		if i == len(pattern) || pattern[i] == 'I' {
			for index > 0 {
				index--
				result = fmt.Append(result, stack[index])
			}
		}
	}
	return string(result)
}
