package exercises

import "github.com/hardmet/hackerrank/go/crossed_node"

func nodeFabric(grid [][]int, rowStart, colStart, rowEnd, colEnd int) *crossed_node.Node {
	return crossed_node.Construct(grid)
}
