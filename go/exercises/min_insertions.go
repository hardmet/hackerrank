package exercises

func minInsertions(s string) int {
	n := len(s)
	dp := make([]int, n)
	var prev, temp int
	for i := n - 2; i >= 0; i-- {
		prev = 0
		for j := i + 1; j < n; j++ {
			temp = dp[j]
			if s[i] == s[j] {
				dp[j] = prev
			} else {
				if dp[j-1] < dp[j] {
					dp[j] = dp[j-1] + 1
				} else {
					dp[j]++
				}
			}
			prev = temp
		}
	}
	return dp[n-1]
}
