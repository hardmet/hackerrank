package exercises

import "sort"

func numBusesToDestination(routes [][]int, source int, target int) int {
	adjList := [][]int{}
	if source == target {
		return 0
	}
	for i := 0; i < len(routes); i++ {
		sort.Ints(routes[i])
		adjList = append(adjList, []int{})
	}
	createGraph(routes, &adjList)
	q := []int{}
	addStartingNodes(&q, routes, source)
	visitedMap := map[int]struct{}{}
	busCount := 1
	for len(q) > 0 {
		for sz := len(q); sz > 0; sz-- {
			node := q[0]
			q = q[1:]
			if isStopExist(routes[node], target) {
				return busCount
			}
			for _, adj := range adjList[node] {
				if _, visited := visitedMap[adj]; !visited {
					visitedMap[adj] = struct{}{}
					q = append(q, adj)
				}
			}
		}
		busCount++
	}
	return -1
}

func createGraph(routes [][]int, adjList *[][]int) {
	for i := 0; i < len(routes); i++ {
		for j := i + 1; j < len(routes); j++ {
			if haveCommonNode(routes[i], routes[j]) {
				(*adjList)[i] = append((*adjList)[i], j)
				(*adjList)[j] = append((*adjList)[j], i)
			}
		}
	}
}

func haveCommonNode(route1, route2 []int) bool {
	for i, j := 0, 0; i < len(route1) && j < len(route2); {
		if route1[i] == route2[j] {
			return true
		}
		if route1[i] < route2[j] {
			i++
		} else {
			j++
		}
	}
	return false
}

func addStartingNodes(q *[]int, routes [][]int, source int) {
	for i := 0; i < len(routes); i++ {
		if isStopExist(routes[i], source) {
			*q = append(*q, i)
		}
	}
}

func isStopExist(route []int, stop int) bool {
	for j := 0; j < len(route); j++ {
		if route[j] == stop {
			return true
		}
	}
	return false
}
