package exercises

import (
	"fmt"
	"strconv"
)

func isValidSudoku(board [][]byte) bool {
	limits := [][3]int{{0, 1, 2}, {3, 4, 5}, {6, 7, 8}}

	var cells []byte
	for _, r := range limits {
		for _, c := range limits {
			cells = []byte{}
			for _, i := range r {
				for _, j := range c {
					cells = append(cells, board[i][j])
				}
				if !isValidSquare(cells) {
					return false
				}
			}
		}
	}
	for i := 0; i < 9; i++ {
		if !isValidRow(board, i) {
			return false
		}
		if !isValidColumn(board, i) {
			return false
		}
	}
	return true
}

func isValidRow(board [][]byte, i int) bool {
	visited := make([]int, 10)
	for _, cell := range board[i] {
		if '0' <= cell && cell <= '9' {
			if visited[toInt(cell)] > 0 {
				return false
			}
			visited[toInt(cell)]++
		}
	}
	return true
}

func isValidColumn(board [][]byte, i int) bool {
	visited := make([]int, 10)
	for _, column := range board {
		if '0' <= column[i] && column[i] <= '9' {
			cell := toInt(column[i])
			if visited[cell] > 0 {
				return false
			}
			visited[cell]++
		}
	}
	return true
}

func isValidSquare(cells []byte) bool {
	visited := make([]int, 10)
	for _, c := range cells {
		if '0' <= c && c <= '9' {
			cell := toInt(c)
			if visited[cell] > 0 {
				return false
			}
			visited[cell]++
		}
	}
	return true
}

func toInt(b byte) int {
	val, _ := strconv.Atoi(string(b))
	return val
}

func sudokuTests() {
	test1 := [][]byte{
		{'5', '3', '.', '.', '7', '.', '.', '.', '.'},
		{'6', '.', '.', '1', '9', '5', '.', '.', '.'},
		{'.', '9', '8', '.', '.', '.', '.', '6', '.'},
		{'8', '.', '.', '.', '6', '.', '.', '.', '3'},
		{'4', '.', '.', '8', '.', '3', '.', '.', '1'},
		{'7', '.', '.', '.', '2', '.', '.', '.', '6'},
		{'.', '6', '.', '.', '.', '.', '2', '8', '.'},
		{'.', '.', '.', '4', '1', '9', '.', '.', '5'},
		{'.', '.', '.', '.', '8', '.', '.', '7', '9'},
	}

	test2 := [][]byte{
		{'.', '.', '.', '.', '5', '.', '.', '1', '.'},
		{'.', '4', '.', '3', '.', '.', '.', '.', '.'},
		{'.', '.', '.', '.', '.', '3', '.', '.', '1'},
		{'8', '.', '.', '.', '.', '.', '.', '2', '.'},
		{'.', '.', '2', '.', '7', '.', '.', '.', '.'},
		{'.', '1', '5', '.', '.', '.', '.', '.', '.'},
		{'.', '.', '.', '.', '.', '2', '.', '.', '.'},
		{'.', '2', '.', '9', '.', '.', '.', '.', '.'},
		{'.', '.', '4', '.', '.', '.', '.', '.', '.'},
	}
	isValidSudoku(test1)
	fmt.Println(isValidSudoku(test2))
}
