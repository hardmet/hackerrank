package exercises

import "strings"

func repeatedSubstringPattern(s string) bool {
	t := s + s
	return strings.Contains(t, s)
}
