package exercises

func longestPalindrome(s string) string {
	n := len(s)
	dp := make([][]bool, n)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]bool, n)
	}
	l, r := 0, 0
	for i := 0; i < n; i++ {
		dp[i][i] = true
	}
	for i := 0; i < n-1; i++ {
		if s[i] == s[i+1] {
			dp[i][i+1] = true
			l = i
			r = i + 1
		}
	}
	for diff := 2; diff < n; diff++ {
		for i := 0; i < n-diff; i++ {
			j := i + diff
			if s[i] == s[j] && dp[i+1][j-1] {
				dp[i][j] = true
				l = i
				r = j
			}
		}
	}
	return s[l : r+1]
}

func anotherLongestPalindrome(s string) string {
	var l, r int
	var max int
	var mr string

	var findPalindrome func() = func() {
		for l >= 0 && r < len(s) && s[l] == s[r] {
			l--
			r++
		}
		if r-l-1 > max {
			max = r - l - 1
			mr = s[l+1 : r]
		}
	}

	for i := 0; i < len(s); i++ {
		l, r = i-1, i+1
		findPalindrome()
		l, r = i, i+1
		findPalindrome()
	}
	return mr
}
