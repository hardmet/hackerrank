package exercises

import (
	"fmt"
	"github.com/hardmet/hackerrank/go/math_utils"
	"sort"
)

func makeArrayIncreasing(arr1 []int, arr2 []int) int {
	sort.Ints(arr2)
	answer := countCostDFS(0, -1, arr1, arr2, map[string]int{})

	if answer < 2_001 {
		return answer
	}
	return -1
}

func bisectRight(arr []int, value int) int {
	left, right := 0, len(arr)
	var mid int
	for left < right {
		mid = (left + right) / 2
		if arr[mid] <= value {
			left = mid + 1
		} else {
			right = mid
		}
	}
	return left
}

func countCostDFS(i, prev int, arr1, arr2 []int, dp map[string]int) int {
	if i == len(arr1) {
		return 0
	}
	if value, contains := dp[fmt.Sprintf("%d;%d", i, prev)]; contains {
		return value
	}

	cost := 2_001

	// If arr1[i] is already greater than prev, we can leave it be.
	if arr1[i] > prev {
		cost = countCostDFS(i+1, arr1[i], arr1, arr2, dp)
	}

	// Find a replacement with the smallest value in arr2.
	idx := bisectRight(arr2, prev)

	// Replace arr1[i], with a cost of 1 operation.
	if idx < len(arr2) {
		cost = math_utils.Min(cost, 1+countCostDFS(i+1, arr2[idx], arr1, arr2, dp))
	}
	dp[fmt.Sprintf("%d;%d", i, prev)] = cost
	return cost
}
