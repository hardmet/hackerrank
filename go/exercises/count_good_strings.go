package exercises

func countGoodStrings(low, high, zero, one int) int {
	var dp = make([]int, high+1)
	var res = 0
	var mod = 1_000_000_007
	dp[0] = 1
	for i := 1; i <= high; i++ {
		if i >= zero {
			dp[i] = (dp[i] + dp[i-zero]) % mod
		}
		if i >= one {
			dp[i] = (dp[i] + dp[i-one]) % mod
		}
		if i >= low {
			res = (res + dp[i]) % mod
		}
	}
	return res
}
