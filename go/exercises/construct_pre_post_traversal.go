package exercises

func constructFromPrePost(preorder []int, postorder []int) *TreeNode {
	preIndex := 0
	postIndex := 0

	var construct func() *TreeNode
	construct = func() *TreeNode {
		root := &TreeNode{Val: preorder[preIndex]}
		preIndex++
		if root.Val != postorder[postIndex] {
			root.Left = construct()
		}

		if root.Val != postorder[postIndex] {
			root.Right = construct()
		}
		postIndex++
		return root
	}
	return construct()
}
