package exercises

import (
	"github.com/hardmet/hackerrank/go/int_union_find"
	"sort"
)

func maxNumEdgesToRemove(n int, edges [][]int) int {
	sort.Slice(edges, func(i, j int) bool {
		return edges[i][0] < edges[j][0]
	})
	ufAlice := int_union_find.ArrayUFConstructor(n + 1)
	ufBob := int_union_find.ArrayUFConstructor(n + 1)
	removed := 0
	aliceEdges := 0
	bobEdges := 0
	for _, edge := range edges {
		if edge[0] == 3 {
			if ufAlice.Union(edge[1], edge[2]) {
				ufBob.Union(edge[1], edge[2])
				aliceEdges++
				bobEdges++
			} else {
				removed++
			}
		} else if edge[0] == 2 {
			if ufBob.Union(edge[1], edge[2]) {
				bobEdges++
			} else {
				removed++
			}
		} else {
			if ufAlice.Union(edge[1], edge[2]) {
				aliceEdges++
			} else {
				removed++
			}
		}
	}
	if bobEdges == n-1 && aliceEdges == n-1 {
		return removed
	} else {
		return -1
	}
}
