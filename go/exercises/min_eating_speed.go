package exercises

import "math"

func minEatingSpeed(piles []int, h int) int {
	l, r := 1, 0

	for _, pile := range piles {
		if pile > r {
			r = pile
		}
	}
	var mid int
	for l <= r {
		mid = l + (r-l)/2
		hoursSpent := 0
		for _, bananasInPile := range piles {
			hoursSpent += int(math.Ceil(float64(bananasInPile) / float64(mid)))
		}
		if hoursSpent <= h {
			r = mid - 1
		} else {
			l = mid + 1
		}
	}
	return l
}
