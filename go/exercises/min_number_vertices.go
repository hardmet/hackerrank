package exercises

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	var res []int
	seen := make([]int, n)
	for _, edge := range edges {
		seen[edge[1]] = 1
	}
	for i := 0; i < n; i++ {
		if seen[i] == 0 {
			res = append(res, i)
		}
	}
	return res
}
