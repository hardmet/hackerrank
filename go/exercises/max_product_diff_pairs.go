package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func maxProductDifference(nums []int) int {
	maxFst, maxSnd := math_utils.Max(nums[0], nums[1]), math_utils.Min(nums[0], nums[1])
	minFst, minSnd := math_utils.Min(nums[len(nums)-2], nums[len(nums)-1]), math_utils.Max(nums[len(nums)-2], nums[len(nums)-1])
	for i := 2; i < len(nums); i++ {
		if nums[i] > maxFst {
			maxSnd, maxFst = maxFst, nums[i]
		} else if nums[i] > maxSnd {
			maxSnd = nums[i]
		}
		if nums[len(nums)-1-i] < minFst {
			minSnd, minFst = minFst, nums[len(nums)-1-i]
		} else if nums[len(nums)-1-i] < minSnd {
			minSnd = nums[len(nums)-1-i]
		}
	}
	return (maxFst * maxSnd) - (minFst * minSnd)
}
