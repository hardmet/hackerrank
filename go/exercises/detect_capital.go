package exercises

import "unicode"

func detectCapitalUse(word string) bool {
	if len(word) == 1 {
		return true
	}
	if unicode.IsUpper(rune(word[0])) {
		if unicode.IsUpper(rune(word[1])) {
			for _, ch := range word {
				if !unicode.IsUpper(ch) {
					return false
				}
			}
		} else {
			for i := 1; i < len(word); i++ {
				if unicode.IsUpper(rune(word[i])) {
					return false
				}
			}
		}
	} else {
		for _, ch := range word {
			if unicode.IsUpper(ch) {
				return false
			}
		}
	}
	return true
}
