package exercises

func numberOfArrays(s string, k int) int {
	dp := make([]int, len(s))
	for i := 0; i < len(dp); i++ {
		dp[i] = -1
	}
	var dfsNumberArrays func(s string, k int64, i int, dp []int) int
	dfsNumberArrays = func(s string, k int64, i int, dp []int) int {
		if i == len(s) {
			return 1
		}
		if s[i] == '0' {
			return 0
		}
		if dp[i] != -1 {
			return dp[i]
		}
		ans := 0
		var num int64
		for j := i; j < len(s); j++ {
			num = num*10 + int64(s[j]-'0')
			if num > k {
				break
			}
			ans = (ans + dfsNumberArrays(s, k, j+1, dp)) % 1_000_000_007
		}
		dp[i] = ans
		return ans
	}
	return dfsNumberArrays(s, int64(k), 0, dp)
}
