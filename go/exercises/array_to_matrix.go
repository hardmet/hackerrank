package exercises

func findMatrix(nums []int) [][]int {
	freq := make([]int, len(nums)+1)
	matrix := [][]int{}
	for _, num := range nums {
		if freq[num] >= len(matrix) {
			matrix = append(matrix, []int{})
		}
		matrix[freq[num]] = append(matrix[freq[num]], num)
		freq[num]++
	}
	return matrix
}
