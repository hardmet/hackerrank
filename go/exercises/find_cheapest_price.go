package exercises

import (
	"container/heap"
	"math"
)

type Flight struct {
	dist, node, steps int
}

type PriorityQueue []*Flight

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].dist < pq[j].dist
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *PriorityQueue) Push(x interface{}) {
	*pq = append(*pq, x.(*Flight))
}

func (pq *PriorityQueue) Pop() interface{} {
	p := (*pq)[len(*pq)-1]
	(*pq)[len(*pq)-1] = nil // avoid memory leak
	*pq = (*pq)[:len(*pq)-1]
	return p
}

func findCheapestPrice(n int, flights [][]int, src, dst, k int) int {
	adj := make(map[int][][]int)
	for _, flight := range flights {
		if _, contains := adj[flight[0]]; contains {
			adj[flight[0]] = append(adj[flight[0]], []int{flight[1], flight[2]})
		} else {
			adj[flight[0]] = [][]int{{flight[1], flight[2]}}
		}
	}
	stops := make([]int, n)
	for i := range stops {
		stops[i] = math.MaxInt
	}
	pq := make(PriorityQueue, 0)

	heap.Push(&pq, &Flight{0, src, 0})
	for pq.Len() > 0 {
		temp := heap.Pop(&pq).(*Flight)
		if temp.steps > stops[temp.node] || temp.steps > k+1 {
			continue
		}
		stops[temp.node] = temp.steps
		if temp.node == dst {
			return temp.dist
		}
		if list, contains := adj[temp.node]; contains {
			for _, a := range list {
				heap.Push(&pq, &Flight{temp.dist + a[1], a[0], temp.steps + 1})
			}
		}
	}
	return -1
}
