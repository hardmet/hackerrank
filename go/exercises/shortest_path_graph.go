package exercises

import "github.com/hardmet/hackerrank/go/tuples"

type MaskedNode struct {
	node, mask, cost int
}

func shortestPathLength(graph [][]int) int {
	all := (1 << len(graph)) - 1
	visited := map[tuples.Pair]struct{}{}
	q := []MaskedNode{}
	for i := 0; i < len(graph); i++ {
		maskValue := 1 << i
		current := MaskedNode{i, maskValue, 1}
		q = append(q, current)
		visited[tuples.Pair{i, maskValue}] = struct{}{}
	}
	for len(q) > 0 {
		curr := q[0]
		q = q[1:]
		if curr.mask == all {
			return curr.cost - 1
		}
		for _, adj := range graph[curr.node] {
			bothVisitedMask := curr.mask | (1 << adj)
			current := MaskedNode{adj, bothVisitedMask, curr.cost + 1}
			if _, contains := visited[tuples.Pair{adj, bothVisitedMask}]; !contains {
				visited[tuples.Pair{adj, bothVisitedMask}] = struct{}{}
				q = append(q, current)
			}
		}
	}
	return -1
}
