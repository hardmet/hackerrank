package exercises

import "sort"

func numSubseq(a []int, k int) int {
	sort.Ints(a)
	res := 0
	twoPowers := make([]int, len(a))
	twoPowers[0] = 1
	for i := 1; i < len(twoPowers); i++ {
		twoPowers[i] = twoPowers[i-1] * 2 % 1_000_000_007
	}
	l, r := 0, len(a)-1
	for l <= r {
		if a[l]+a[r] > k {
			r--
		} else {
			res = (res + twoPowers[r-l]) % 1_000_000_007
			l++
		}
	}
	return res
}
