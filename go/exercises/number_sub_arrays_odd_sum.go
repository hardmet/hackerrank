package exercises

func numOfSubarrays(arr []int) int {
	MOD := 1_000_000_007
	var count, prefixSum, oddCount, evenCount int
	evenCount = 1
	for _, num := range arr {
		prefixSum += num
		if prefixSum%2 == 0 {
			count += oddCount
			evenCount++
		} else {
			count += evenCount
			oddCount++
		}

		count = count % MOD
	}
	return count
}
