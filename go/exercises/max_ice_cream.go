package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"math"
)

func maxIceCream(costs []int, coins int) int {
	max := math.MinInt
	for _, cost := range costs {
		if cost > max {
			max = cost
		}
	}
	buckets := make([]int, max+1)
	for _, cost := range costs {
		buckets[cost]++
	}
	res := 0
	for price, count := range buckets {
		if coins < price {
			break
		}
		if count > 0 {
			res += math_utils.Min(count, coins/price)
			coins -= math_utils.Min(coins, price*count)
		}
	}
	return res
}
