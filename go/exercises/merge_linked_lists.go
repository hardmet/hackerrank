package exercises

import (
	"container/heap"
	"github.com/hardmet/hackerrank/go/int_heap"
	"github.com/hardmet/hackerrank/go/tuples"
)

func mergeKLists(lists []*ListNode) *ListNode {
	minQueue := int_heap.MinIntPairHeap{}
	for i := range lists {
		pushNode(&minQueue, &lists, i)
	}
	if minQueue.Len() == 0 {
		return nil
	}
	cur := heap.Pop(&minQueue).(tuples.Pair)
	pushNode(&minQueue, &lists, cur.Snd)
	var merged *ListNode = &ListNode{cur.Fst, nil}
	head := merged
	for minQueue.Len() > 0 {
		cur = heap.Pop(&minQueue).(tuples.Pair)
		merged.Next = &ListNode{cur.Fst, nil}
		merged = merged.Next
		pushNode(&minQueue, &lists, cur.Snd)
	}
	return head
}

func pushNode(minQueue *int_heap.MinIntPairHeap, nodes *[]*ListNode, i int) {
	if (*nodes)[i] != nil {
		heap.Push(minQueue, tuples.Pair{Fst: (*nodes)[i].Val, Snd: i})
		(*nodes)[i] = (*nodes)[i].Next
	}
}
