package exercises

func Search(l, r, x int, p func(int) int) int {
	if l == r {
		return l
	}
	if l > r {
		return -1
	}
	mid := (r + (r-l)/2)
	res := p(mid)
	if res == 0 {
		return mid
	}
	if res < 0 {
		return Search(l, mid-1, x, p)
	}
	if res > 0 {
		return Search(mid+1, r, x, p)
	}
	return -1
}
