package exercises

func maxProfit1(prices []int) int {
	var curMin, ans int = prices[0], 0
	for i := range prices {
		if curMin >= prices[i] {
			curMin = prices[i]
		} else {
			if prices[i]-curMin > ans {
				ans = prices[i] - curMin
			}
		}
	}
	return ans
}
