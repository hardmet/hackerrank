package exercises

func swapPairs(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	a := head
	c := true

	for b := head.Next; b != nil; b = b.Next {
		if c {
			a.Val, b.Val = b.Val, a.Val
		}
		a = a.Next
		c = !c
	}
	return head
}
