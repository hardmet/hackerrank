package exercises

func findNumberOfLIS(nums []int) int {
	dp := make([]int, len(nums))
	count := make([]int, len(nums))
	for i := 0; i < len(dp); i++ {
		dp[i] = 1
		count[i] = 1
	}
	maxLen := 1

	for i := 1; i < len(nums); i++ {
		for j := 0; j < i; j++ {
			if nums[i] > nums[j] {
				if dp[j]+1 > dp[i] {
					dp[i] = dp[j] + 1
					count[i] = count[j]
				} else if dp[j]+1 == dp[i] {
					count[i] += count[j]
				}
			}
		}
		if dp[i] > maxLen {
			maxLen = dp[i]
		}
	}
	res := 0
	for i := 0; i < len(nums); i++ {
		if dp[i] == maxLen {
			res += count[i]
		}
	}
	return res
}
