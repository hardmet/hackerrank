package exercises

func maxSlidingWindow(nums []int, k int) []int {
	q := make([]int, 0, k)
	result := make([]int, 0, len(nums)-k+1)
	for i, n := range nums {
		for len(q) > 0 && nums[q[len(q)-1]] <= n {
			q = q[:len(q)-1]
		}
		q = append(q, i)

		if i < k-1 {
			continue
		}

		result = append(result, nums[q[0]])
		if q[0] == i-k+1 {
			q = q[1:]
		}
	}
	return result
}
