package exercises

import "fmt"

func summaryRanges(nums []int) []string {
	if len(nums) == 0 {
		return []string{}
	}
	start := nums[0]
	var res []string
	for i := 1; i < len(nums); i++ {
		if nums[i] != nums[i-1]+1 {
			addRange(start, nums[i-1], &res)
			start = nums[i]
		}
	}
	addRange(start, nums[len(nums)-1], &res)
	return res
}

func addRange(start, end int, result *[]string) {
	if start == end {
		*result = append(*result, fmt.Sprintf("%d", start))
	} else {
		*result = append(*result, fmt.Sprintf("%d->%d", start, end))
	}
}
