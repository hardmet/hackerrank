package exercises

func minOperationsFunc(s string) int {
	start := 0
	for i := 0; i < len(s); i++ {
		if i%2 == 0 {
			if s[i] == '1' {
				start++
			}
		} else {
			if s[i] == '0' {
				start++
			}
		}
	}
	if start < len(s)-start {
		return start
	}
	return len(s) - start
}
