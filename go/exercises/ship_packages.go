package exercises

func shipWithinDays(weights []int, days int) int {
	var totalLoad, maxLoad int
	for i := 0; i < len(weights); i++ {
		totalLoad += weights[i]
		if weights[i] > maxLoad {
			maxLoad = weights[i]
		}
	}
	l := maxLoad
	r := totalLoad
	ans := r
	for l <= r {
		mid := l + (r-l)/2
		if feasible(weights, mid, days) {
			r = mid - 1
			ans = mid
		} else {
			l = mid + 1
		}
	}
	return ans
}

func feasible(weights []int, capacity, days int) bool {
	daysNeeded := 1
	currentLoad := 0
	for i := 0; i < len(weights); i++ {
		if currentLoad+weights[i] <= capacity {
			currentLoad += weights[i]
		} else {
			daysNeeded++
			currentLoad = weights[i]
		}
	}
	return daysNeeded <= days
}
