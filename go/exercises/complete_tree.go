package exercises

func isCompleteTree(root *TreeNode) bool {
	q := []*TreeNode{}
	q = append(q, root)
	seenNull := false
	for ; len(q) > 0; q = q[1:] {
		node := q[0]
		if node == nil {
			seenNull = true
		} else {
			if seenNull {
				return false
			}
			q = append(q, node.Left)
			q = append(q, node.Right)
		}
	}
	return true
}
