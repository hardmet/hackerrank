package exercises

func pivotArray(nums []int, pivot int) []int {
	lessOrEqual := []int{}
	bigger := []int{}
	countEqual := 0
	for _, num := range nums {
		if num < pivot {
			lessOrEqual = append(lessOrEqual, num)
		} else if num == pivot {
			countEqual++
		} else {
			bigger = append(bigger, num)
		}
	}
	for i := 0; i < countEqual; i++ {
		lessOrEqual = append(lessOrEqual, pivot)
	}
	lessOrEqual = append(lessOrEqual, bigger...)
	return lessOrEqual
}
