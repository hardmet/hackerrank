package exercises

func findAllConcatenatedWordsInADict(words []string) []string {
	var dictionary = make(map[string]bool, len(words))
	for _, word := range words {
		dictionary[word] = true
	}
	var answer []string
	for _, word := range words {
		if concatenateDFS(word, 0, make([]bool, len(word)), dictionary) {
			answer = append(answer, word)
		}
	}
	return answer
}

func concatenateDFS(word string, length int, visited []bool, dictionary map[string]bool) bool {
	if length == len(word) {
		return true
	}
	if visited[length] {
		return false
	}
	visited[length] = true
	var k int
	if length == 0 {
		k = 1
	}
	for i := len(word) - k; i > length; i-- {
		if dictionary[word[length:i]] && concatenateDFS(word, i, visited, dictionary) {
			return true
		}
	}
	return false
}
