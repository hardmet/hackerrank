package exercises

import "sort"

type SummaryRanges struct {
	values     map[int]bool
	sortedKeys []int
}

func ConstructorSR() SummaryRanges {
	return SummaryRanges{map[int]bool{}, []int{}}
}

func (r *SummaryRanges) AddNum(value int) {
	if !r.values[value] {
		r.values[value] = true
		index := sort.SearchInts(r.sortedKeys, value)
		if len(r.sortedKeys) == index { // nil or empty slice or after last element
			r.sortedKeys = append(r.sortedKeys, value)
		} else {
			r.sortedKeys = append(r.sortedKeys[:index+1], r.sortedKeys[index:]...)
			r.sortedKeys[index] = value
		}
	}
}

func (r *SummaryRanges) GetIntervals() [][]int {
	var answer [][]int
	var start, end = -1, -1
	for _, val := range r.sortedKeys {
		if start == -1 {
			start = val
			end = val
		} else if val-end == 1 {
			end = val
		} else {
			answer = append(answer, []int{start, end})
			start = val
			end = val
		}
	}
	if start != -1 {
		answer = append(answer, []int{start, end})
	}
	return answer
}
