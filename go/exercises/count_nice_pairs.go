package exercises

func countNicePairs(nums []int) int {
	numsWithReversed := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		numsWithReversed[i] = nums[i] - reverseNum(nums[i])
	}
	cache := map[int]int{}
	ans := 0
	const MOD = 1e9 + 7
	for _, num := range numsWithReversed {
		ans = (ans + cache[num]) % MOD
		cache[num] = cache[num] + 1
	}
	return ans
}

func reverseNum(num int) int {
	result := 0
	for num > 0 {
		result = result*10 + num%10
		num /= 10
	}
	return result
}
