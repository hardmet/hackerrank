package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func minCostClimbingStairs(cost []int) int {
	prevPrev, prev := cost[0], cost[1]
	for i := 2; i < len(cost); i++ {
		prevPrev, prev = prev, math_utils.Min(prevPrev+cost[i], prev+cost[i])
	}
	return math_utils.Min(prev, prevPrev)
}
