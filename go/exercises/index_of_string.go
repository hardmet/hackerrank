package exercises

func strStr(haystack string, needle string) int {
	hsk, ndl := []byte(haystack), []byte(needle)

	var isSame func(a, b []byte) bool
	isSame = func(a, b []byte) bool {
		switch {
		case len(b) == 0:
			return true
		case len(a) == 0:
			return false
		case a[0] == b[0]:
			return isSame(a[1:], b[1:])
		default:
			return false
		}
	}

	for i := range haystack {
		if haystack[i] == needle[0] {
			if isSame(hsk[i:], ndl) {
				return i
			}
		}
	}
	return -1
}
