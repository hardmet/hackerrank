package exercises

import (
	"github.com/hardmet/hackerrank/go/list"
	"github.com/hardmet/hackerrank/go/math_utils"
)

func largestPathValue(colors string, edges [][]int) int {
	n, k := len(colors), 26
	indegrees := make([]int, n)
	graph := [][]int{}
	for i := 0; i < n; i++ {
		graph = append(graph, []int{})
	}
	for _, edge := range edges {
		graph[edge[0]] = append(graph[edge[0]], edge[1])
		indegrees[edge[1]]++
	}
	zeroIndegrees := map[int]struct{}{}
	for i := 0; i < n; i++ {
		if indegrees[i] == 0 {
			zeroIndegrees[i] = struct{}{}
		}
	}
	counts := make([][]int, n)
	for i := 0; i < n; i++ {
		counts[i] = make([]int, k)
	}
	for i := 0; i < n; i++ {
		counts[i][colors[i]-'a']++
	}
	var maxCount, visited int
	zeroIndegreesSliece := []int{}
	for key := range zeroIndegrees {
		zeroIndegreesSliece = append(zeroIndegreesSliece, key)
	}
	for len(zeroIndegreesSliece) > 0 {
		u := zeroIndegreesSliece[0]
		zeroIndegreesSliece = zeroIndegreesSliece[1:]
		delete(zeroIndegrees, u)
		visited++
		for _, vertex := range graph[u] {
			for i := 0; i < k; i++ {
				var isI int
				if int(colors[vertex]-'a') == i {
					isI = 1
				}
				counts[vertex][i] = math_utils.Max(counts[vertex][i], counts[u][i]+isI)
			}
			indegrees[vertex]--
			if indegrees[vertex] == 0 {
				if _, contains := zeroIndegrees[vertex]; !contains {
					zeroIndegreesSliece = append(zeroIndegreesSliece, vertex)
					zeroIndegrees[vertex] = struct{}{}
				}
			}
		}
		maxCount = math_utils.Max(maxCount, list.MaxList(counts[u]))
	}
	if visited == n {
		return maxCount
	}
	return -1
}
