package exercises

func getAverages(nums []int, k int) []int {
	res, doubleK := make([]int, len(nums)), 2*k
	windowLen := doubleK + 1
	var windowSum = 0
	for i := 0; i < len(nums); i++ {
		if doubleK > i || i >= len(nums)-k {
			res[i] = -1
		}
		windowSum += nums[i]
		if i >= doubleK {
			res[i-k] = windowSum / windowLen
			windowSum -= nums[i-doubleK]
		}
	}
	return res
}
