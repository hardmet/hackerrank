package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func minimumTimeParCourses3(n int, relations [][]int, time []int) int {
	graph := map[int][]int{}
	memo := map[int]int{}
	for i := 0; i < n; i++ {
		graph[i] = []int{}
	}
	for _, edge := range relations {
		graph[edge[0]-1] = append(graph[edge[0]-1], edge[1]-1)
	}
	ans := 0
	for node := 0; node < n; node++ {
		ans = math_utils.Max(ans, dfsParCourses3(node, time, memo, graph))
	}
	return ans
}

func dfsParCourses3(node int, time []int, memo map[int]int, graph map[int][]int) int {
	if value, contains := memo[node]; contains {
		return value
	}
	if len(graph[node]) == 0 {
		return time[node]
	}
	ans := 0
	for _, neighbor := range graph[node] {
		ans = math_utils.Max(ans, dfsParCourses3(neighbor, time, memo, graph))
	}
	memo[node] = time[node] + ans
	return memo[node]
}
