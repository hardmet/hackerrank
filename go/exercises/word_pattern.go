package exercises

import "strings"

func wordPattern(pattern string, s string) bool {
	wordMap := map[byte]string{}
	words := strings.Split(s, " ")
	values := map[string]bool{}
	if len(words) != len(pattern) {
		return false
	}
	for i := range pattern {
		if value, contains := wordMap[pattern[i]]; contains {
			if value != words[i] {
				return false
			}
		} else {
			wordMap[pattern[i]] = words[i]
			values[words[i]] = true
		}
	}
	return len(wordMap) == len(values)
}
