package exercises

func getRow(rowIndex int) []int {
	return generatePascal(rowIndex + 1)[rowIndex]
}
