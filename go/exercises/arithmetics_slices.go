package exercises

func numberOfArithmeticSlices(nums []int) int {
	count, n := 0, len(nums)
	dp := make([]map[int]int, n)
	for i := 0; i < n; i++ {
		dp[i] = map[int]int{}
	}
	for i := 0; i < n; i++ {
		for j := 0; j < i; j++ {
			diff := nums[i] - nums[j]

			dp[i][diff] = dp[i][diff] + 1
			if _, contains := dp[j][diff]; contains {
				dp[i][diff] = dp[i][diff] + dp[j][diff]
				count += dp[j][diff]
			}
		}
	}
	return count
}
