package exercises

func shuffle(nums []int, n int) []int {
	res := make([]int, 0)
	var j int = n
	for i := 0; i < n; i++ {
		res = append(res, nums[i])
		res = append(res, nums[j])
		j++
	}
	return res
}
