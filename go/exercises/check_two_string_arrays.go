package exercises

func arrayStringsAreEqual(word1 []string, word2 []string) bool {
	i, ii, j, jj := 0, 0, 0, 0
	for i < len(word1) && j < len(word2) {
		if word1[i][ii] != word2[j][jj] {
			return false
		}
		ii++
		jj++
		if ii == len(word1[i]) {
			i++
			ii = 0
		}
		if jj == len(word2[j]) {
			j++
			jj = 0
		}
	}
	return i == len(word1) && j == len(word2)
}
