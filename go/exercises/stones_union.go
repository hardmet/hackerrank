package exercises

import (
	"github.com/hardmet/hackerrank/go/int_union_find"
)

func RemoveStones(stones [][]int) int {
	uf := int_union_find.MapUFConstructor()
	for _, stone := range stones {
		uf.Union(stone[0], stone[1]+10000)
	}
	uniqRoots := map[int]int{}
	for k := range uf.GetParent() {
		uniqRoots[uf.Find(k)] = 1
	}
	return len(stones) - len(uniqRoots)
}
