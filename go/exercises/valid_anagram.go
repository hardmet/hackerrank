package exercises

func isAnagram(s string, t string) bool {
	symbols := make([]int, 26)
	for _, si := range s {
		symbols[si-'a']++
	}
	for _, ti := range t {
		symbols[ti-'a']--
	}
	for _, count := range symbols {
		if count != 0 {
			return false
		}
	}
	return true
}
