package exercises

func isUgly(n int) bool {
	if n == 0 {
		return false
	}
	for n != 1 {
		ld := n % 10

		if ld == 0 || ld == 5 {
			n = n / 5
			continue
		}

		if ld%2 == 0 {
			n = n / 2
			continue
		}
		sum := 0
		m := n
		for m != 0 {
			sum += m % 10
			m = m / 10
		}
		if sum%3 == 0 {
			n = n / 3
			continue
		}
		return false
	}
	if n == 1 || n == -1 {
		return true
	}
	return false
}
