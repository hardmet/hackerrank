package exercises

func arraySign(nums []int) int {
	isNegative := false
	for _, num := range nums {
		if num == 0 {
			return 0
		}
		if num < 0 {
			isNegative = !isNegative
		}
	}
	if isNegative {
		return -1
	}
	return 1
}
