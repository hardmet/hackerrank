package exercises

func totalFruit(fruits []int) int {
	basket := make(map[int]int, 3)
	var left, maxPicked int
	for right, rightFruit := range fruits {
		basket[rightFruit]++
		for len(basket) > 2 {
			basket[fruits[left]]--
			if basket[fruits[left]] == 0 {
				delete(basket, fruits[left])
			}
			left++
		}
		if right-left+1 > maxPicked {
			maxPicked = right - left + 1
		}
	}
	return maxPicked
}
