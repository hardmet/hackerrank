package exercises

func zip(a, b string) string {
	i, j := 0, 0
	var res []byte
	for i < len(a) && j < len(b) {
		if i == j {
			res = append(res, a[i])
			i++
		} else {
			res = append(res, b[j])
			j++
		}
	}
	if i == len(a) {
		res = append(res, b[j:]...)
	} else {
		res = append(res, a[i:]...)
	}
	return string(res)
}

func ZipTest() {
	println(zip("abcd", "pq"))
	println(zip("abcd", ""))
	println(zip("", "pqrs"))
	println(zip("abc", "pqr"))
	println(zip("ab", "pqrs"))
}
