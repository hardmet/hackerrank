package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"math"
)

func minFallingPathSum(matrix [][]int) int {
	if len(matrix) == 1 {
		res := math.MaxInt
		for j := range matrix[0] {
			res = math_utils.Min(res, matrix[0][j])
		}
		return res
	}
	for i := len(matrix) - 2; i > 0; i-- {
		for j := range matrix[i] {
			minVal := matrix[i+1][j]
			if j-1 >= 0 {
				minVal = math_utils.Min(minVal, matrix[i+1][j-1])
			}
			if j+1 < len(matrix[i]) {
				minVal = math_utils.Min(minVal, matrix[i+1][j+1])
			}
			matrix[i][j] = matrix[i][j] + minVal
		}
	}
	res := math.MaxInt
	for j := range matrix[0] {
		minVal := matrix[1][j]
		if j-1 >= 0 {
			minVal = math_utils.Min(minVal, matrix[1][j-1])
		}
		if j+1 < len(matrix[0]) {
			minVal = math_utils.Min(minVal, matrix[1][j+1])
		}
		res = math_utils.Min(res, matrix[0][j]+minVal)
	}
	return res
}
