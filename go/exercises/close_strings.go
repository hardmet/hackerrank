package exercises

import "sort"

func closeStrings(word1 string, word2 string) bool {
	if len(word1) != len(word2) {
		return false
	}
	counts1 := count(word1)
	counts2 := count(word2)
	for i := 0; i < 26; i++ {
		if (counts1[i] == 0 && counts2[i] != 0) || (counts1[i] != 0 && counts2[i] == 0) {
			return false
		}
	}
	sort.Slice(counts1, func(i, j int) bool {
		return counts1[i] < counts1[j]
	})
	sort.Slice(counts2, func(i, j int) bool {
		return counts2[i] < counts2[j]
	})
	for i := 0; i < 26; i++ {
		if counts1[i] != counts2[i] {
			return false
		}
	}
	return true
}

func count(word string) []int {
	counts := make([]int, 26)
	for _, w := range []byte(word) {
		counts[w-'a']++
	}
	return counts
}
