package exercises

import "sort"

func maxCoins(piles []int) int {
	sort.Ints(piles)
	maxCoins := 0
	for i := len(piles) / 3; i < len(piles); i += 2 {
		maxCoins += piles[i]
	}
	return maxCoins
}
