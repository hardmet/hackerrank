package exercises

func lengthOfLongestSubstring(s string) int {
	maxLength := 0
	currentLength := 0
	chars := make(map[rune]int, 0)
	start := 0
	for i, v := range s {
		if lastIndex, contains := chars[v]; contains && lastIndex >= start {
			if currentLength > maxLength {
				maxLength = currentLength
			}
			start = lastIndex + 1
			currentLength = i - lastIndex
		} else {
			currentLength++
		}
		chars[v] = i
	}
	if currentLength > maxLength {
		maxLength = currentLength
	}
	return maxLength
}
