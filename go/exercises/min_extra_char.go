package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func minExtraChar(s string, dictionary []string) int {
	dictionarySet := map[string]struct{}{}
	for _, word := range dictionary {
		dictionarySet[word] = struct{}{}
	}
	dp := make([]int, len(s)+1)
	for start := len(s) - 1; start >= 0; start-- {
		dp[start] = dp[start+1] + 1
		for end := start; end < len(s); end++ {
			curr := s[start : end+1]
			if _, contains := dictionarySet[curr]; contains {
				dp[start] = math_utils.Min(dp[start], dp[end+1])
			}
		}
	}
	return dp[0]
}
