package exercises

func fullJustify(words []string, maxWidth int) []string {
	res := []string{}
	cur := [][]byte{}
	letterCount := 0
	for _, word := range words {
		if len(word)+len(cur)+letterCount > maxWidth {
			for i := 0; i < maxWidth-letterCount; i++ {
				var position int = 0
				if len(cur)-1 > 1 {
					position = i % (len(cur) - 1)
				}
				cur[position] = append(cur[position], ' ')
			}
			sb := []byte{}
			for _, s := range cur {
				sb = append(sb, s...)
			}
			res = append(res, string(sb))
			cur = cur[:0]
			letterCount = 0
		}
		cur = append(cur, []byte(word))
		letterCount += len(word)
	}
	lastLine := []byte{}
	for i := 0; i < len(cur); i++ {
		lastLine = append(lastLine, cur[i]...)
		if i != len(cur)-1 {
			lastLine = append(lastLine, ' ')
		}
	}
	for len(lastLine) < maxWidth {
		lastLine = append(lastLine, ' ')
	}
	res = append(res, string(lastLine))
	return res
}
