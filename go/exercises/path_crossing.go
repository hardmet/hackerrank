package exercises

import "github.com/hardmet/hackerrank/go/tuples"

func isPathCrossing(path string) bool {
	moves := map[rune]tuples.Pair{
		'N': tuples.Pair{0, 1},
		'S': tuples.Pair{0, -1},
		'W': tuples.Pair{-1, 0},
		'E': tuples.Pair{1, 0},
	}
	visited := map[tuples.Pair]struct{}{}
	visited[tuples.Pair{0, 0}] = struct{}{}
	x, y := 0, 0
	for _, c := range path {
		cur := moves[c]
		x += cur.Fst
		y += cur.Snd
		pair := tuples.Pair{x, y}
		if _, contains := visited[pair]; contains {
			return true
		}
		visited[pair] = struct{}{}
	}
	return false
}
