package exercises

func imageSmoother(img [][]int) [][]int {
	x := []int{0, 0, 1, -1, 1, -1, -1, 1}
	y := []int{1, -1, 0, 0, 1, -1, 1, -1}
	smothImg := make([][]int, len(img))
	for i := 0; i < len(img); i++ {
		smothImg[i] = make([]int, len(img[i]))
	}
	for i := 0; i < len(img); i++ {
		for j := 0; j < len(img[i]); j++ {
			sum := img[i][j]
			count := 1
			for d := 0; d < len(x); d++ {
				if x[d]+j >= 0 && x[d]+j < len(img[i]) && y[d]+i >= 0 && y[d]+i < len(img) {
					sum += img[y[d]+i][x[d]+j]
					count++
				}
			}
			smothImg[i][j] = sum / count
		}
	}
	return smothImg
}
