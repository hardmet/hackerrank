package exercises

import (
	"container/list"
	"strconv"
)

func addBinary(a, b string) string {
	resList := list.New()
	indexA := len(a) - 1
	indexB := len(b) - 1
	carry, digitSum := 0, 0
	for indexA >= 0 || indexB >= 0 || carry > 0 {
		digitSum = carry
		if indexA >= 0 {
			digitSum += int(a[indexA] - '0')
			indexA--
		}
		if indexB >= 0 {
			digitSum += int(b[indexB] - '0')
			indexB--
		}
		if digitSum > 1 {
			carry = 1
		} else {
			carry = 0
		}
		resList.PushFront(digitSum % 2)
	}
	resultStr := ""
	for n := resList.Front(); n != nil; n = n.Next() {
		resultStr += strconv.Itoa(n.Value.(int))
	}
	return string(resultStr)
}
