package exercises

import (
	"github.com/hardmet/hackerrank/go/sort"
	"reflect"
)

func successfulPairs(spells []int, potions []int, success int64) []int {
	sort.QuickSort(potions)
	psLen := len(potions)
	result := make([]int, len(spells))
	for i, spell := range spells {
		result[i] = psLen - searchPotion(potions, psLen, int64(spell), success)
	}
	return result
}

func searchPotion(potions []int, psLen int, spell, bound int64) int {
	var l, r = 0, psLen - 1
	var mid, res = 0, psLen
	for l <= r {
		mid = (l + r) / 2
		var pair = int64(potions[mid]) * spell
		if pair >= bound {
			res = mid
			r = mid - 1
		} else if pair < bound {
			l = mid + 1
		}
	}
	return res
}

func SpellPotionsTests() {
	println(reflect.DeepEqual(successfulPairs([]int{5, 1, 3}, []int{1, 2, 3, 4, 5}, int64(7)), []int{4, 0, 3}))
	println(reflect.DeepEqual(successfulPairs([]int{5, 1, 3}, []int{1, 2, 3, 4, 5}, int64(26)), []int{0, 0, 0}))
	println(reflect.DeepEqual(successfulPairs([]int{3, 1, 2}, []int{8, 5, 8}, int64(16)), []int{2, 0, 2}))
}
