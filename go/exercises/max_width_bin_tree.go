package exercises

type NodeWithLevel struct {
	node  *TreeNode
	level int
}

func widthOfBinaryTree(root *TreeNode) int {
	if root == nil {
		return 0
	}
	var q []NodeWithLevel
	q = append(q, NodeWithLevel{root, 0})
	maxWidth := 0
	for len(q) > 0 {
		levelLen := len(q)
		width := (q[len(q)-1].level - q[0].level) + 1
		if width > maxWidth {
			maxWidth = width
		}
		for i := 0; i < levelLen; i++ {
			qi := q[i]
			if qi.node.Left != nil {
				q = append(q, NodeWithLevel{qi.node.Left, 2*qi.level + 1})
			}
			if qi.node.Right != nil {
				q = append(q, NodeWithLevel{qi.node.Right, 2*qi.level + 2})
			}
		}
		q = q[levelLen:]
	}
	return maxWidth
}
