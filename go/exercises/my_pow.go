package exercises

func myPow(x float64, n int) float64 {
	var pow func(xi float64, ni int) float64
	pow = func(xi float64, ni int) float64 {
		if xi == 0 {
			return 0
		}
		if ni == 0 {
			return 1
		}
		res := pow(xi, ni/2)
		res = res * res
		if ni%2 == 1 {
			return res * xi
		}
		return res
	}
	if n < 0 {
		return 1 / pow(x, -n)
	}
	return pow(x, n)
}
