package exercises

func calcEquation(equations [][]string, values []float64, queries [][]string) []float64 {
	graph := make(map[string]map[string]float64)
	for i := 0; i < len(equations); i++ {
		first, second := equations[i][0], equations[i][1]
		makeMapIfNotExists(graph, first)
		makeMapIfNotExists(graph, second)
		graph[first][second] = values[i]
		graph[second][first] = 1.0 / values[i]
	}

	ans := make([]float64, len(queries))
	for i, query := range queries {
		ans[i] = calcQuery(graph, query)
	}

	return ans
}

func makeMapIfNotExists(graph map[string]map[string]float64, key string) {
	if _, contains := graph[key]; !contains {
		graph[key] = make(map[string]float64)
	}
}

func calcQuery(graph map[string]map[string]float64, query []string) float64 {
	first, second := query[0], query[1]
	_, containsFst := graph[first]
	_, containsSnd := graph[second]
	if !containsFst || !containsSnd {
		return -1.0
	}
	if first == second {
		return 1.0
	}
	visited := make(map[string]struct{})
	return calcDFS(visited, graph, first, second)
}

func calcDFS(visited map[string]struct{}, graph map[string]map[string]float64, current string, target string) float64 {
	if current == target {
		return 1.0
	}

	if _, ok := visited[current]; ok {
		return -1.0
	}

	visited[current] = struct{}{}
	neighbors := graph[current]
	for neighbor, val := range neighbors {
		ret := calcDFS(visited, graph, neighbor, target)
		if ret == -1.0 {
			continue
		}

		return ret * val
	}

	return -1.0
}
