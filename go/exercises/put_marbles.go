package exercises

import "sort"

func putMarbles(weights []int, k int) int64 {
	pairWeights := make([]int, len(weights)-1)
	for i := 0; i < len(weights)-1; i++ {
		pairWeights[i] = weights[i] + weights[i+1]
	}
	sort.Ints(pairWeights)
	var sumWeight int64
	for i := 0; i < k-1; i++ {
		sumWeight += int64(pairWeights[len(weights)-2-i] - pairWeights[i])
	}

	return sumWeight
}
