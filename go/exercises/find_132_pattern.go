package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func find132pattern(nums []int) bool {
	if len(nums) < 3 {
		return false
	}
	mins := make([]int, len(nums))
	minStack := make([]int, len(nums))
	mins[0] = nums[0]
	for i := 1; i < len(nums); i++ {
		minStack[i] = math_utils.Min(minStack[i-1], nums[i])
	}
	for j, k := len(nums)-1, len(nums); j >= 0; j-- {
		if nums[j] > minStack[j] {
			for k < len(nums) && nums[k] <= minStack[j] {
				k++
			}
			if k < len(nums) && nums[k] < nums[j] {
				return true
			}
			k--
			nums[k] = nums[j]
		}
	}
	return false
}
