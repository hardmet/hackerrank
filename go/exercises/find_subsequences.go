package exercises

func findSubsequences(nums []int) [][]int {
	var seq []int
	var result [][]int
	var backtrack func(index int)
	backtrack = func(index int) {
		if len(seq) > 1 {
			result = append(result, append([]int{}, seq...))
		}
		used := make(map[int]bool, len(nums))
		for i := index; i < len(nums); i++ {
			if (len(seq) > 0 && nums[i] < seq[len(seq)-1]) || used[nums[i]] {
				continue
			}
			seq = append(seq, nums[i])
			used[nums[i]] = true
			backtrack(i + 1)
			seq = seq[:len(seq)-1]
		}
	}
	backtrack(0)
	return result
}
