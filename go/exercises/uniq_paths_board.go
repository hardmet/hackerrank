package exercises

func uniquePaths(m int, n int) int {
	cache := make([][]int, m)
	for i := 0; i < len(cache); i++ {
		cache[i] = make([]int, n)
		for j := 0; j < len(cache[i]); j++ {
			cache[i][j] = -1
		}
	}
	return uniqPathsRec(0, 0, m, n, cache)
}

func uniqPathsRec(x, y, m, n int, cache [][]int) int {
	if x == m-1 && y == n-1 {
		return 1
	}
	if cache[x][y] != -1 {
		return cache[x][y]
	}
	right, down := 0, 0
	if x < m-1 {
		right = uniqPathsRec(x+1, y, m, n, cache)
	}

	if y < n-1 {
		down = uniqPathsRec(x, y+1, m, n, cache)
	}
	cache[x][y] = right + down
	return cache[x][y]
}
