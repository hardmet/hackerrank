package exercises

type MyQueue struct {
	q1 []int
	q2 []int
}

func ConstructorQ() MyQueue {
	return MyQueue{
		q1: []int{}, q2: []int{},
	}
}

func (this *MyQueue) Push(x int) {
	this.q1 = append(this.q1, x)
}

func (this *MyQueue) Pop() int {
	getLastQ2 := func() int {
		res := this.q2[len(this.q2)-1]
		this.q2 = this.q2[:len(this.q2)-1]
		return res
	}
	if len(this.q2) > 0 {
		return getLastQ2()
	}
	for len(this.q1) > 0 {
		this.q2 = append(this.q2, this.q1[len(this.q1)-1])
		this.q1 = this.q1[:len(this.q1)-1]
	}
	return getLastQ2()
}

func (this *MyQueue) Peek() int {
	if len(this.q2) > 0 {
		return this.q2[len(this.q2)-1]
	}
	return this.q1[0]
}

func (this *MyQueue) Empty() bool {
	return len(this.q1) == 0 && len(this.q2) == 0
}
