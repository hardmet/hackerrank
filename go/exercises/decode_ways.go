package exercises

import "strconv"

func numDecodings(s string) int {
	if len(s) == 0 {
		return 0
	}
	dp := make([]int, len(s)+1)
	dp[0] = 1
	if s[0] == '0' {
		dp[1] = 0
	} else {
		dp[1] = 1
	}
	var fst, snd int
	for i := 2; i <= len(s); i++ {
		fst, _ = strconv.Atoi(string(s[i-1 : i]))
		snd, _ = strconv.Atoi(string(s[i-2 : i]))
		if fst >= 1 && fst <= 9 {
			dp[i] += dp[i-1]
		}
		if snd >= 10 && snd <= 26 {
			dp[i] += dp[i-2]
		}
	}
	return dp[len(s)]
}
