package exercises

func inorderTraversal(root *TreeNode) []int {
	if root != nil {
		return append(
			append(inorderTraversal(root.Left), root.Val),
			inorderTraversal(root.Right)...,
		)
	}
	return []int{}
}
