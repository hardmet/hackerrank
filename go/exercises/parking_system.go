package exercises

type ParkingSystem struct {
	types [3]int
}

func ConstructorPS(big int, medium int, small int) ParkingSystem {
	return ParkingSystem{[3]int{big, medium, small}}
}

func (ps *ParkingSystem) AddCar(carType int) bool {
	ps.types[carType-1]--
	return ps.types[carType-1] >= 0
}
