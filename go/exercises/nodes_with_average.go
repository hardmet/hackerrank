package exercises

func averageOfSubtree(root *TreeNode) int {
	return findSumAndCountDFS(root)[2]
}

func findSumAndCountDFS(node *TreeNode) []int {
	if node != nil {
		left := findSumAndCountDFS(node.Left)
		right := findSumAndCountDFS(node.Right)
		valuesSum := left[0] + right[0] + node.Val
		valuesCount := left[1] + right[1] + 1
		if valuesSum/valuesCount == node.Val {
			return []int{valuesSum, valuesCount, left[2] + right[2] + 1}
		} else {
			return []int{valuesSum, valuesCount, left[2] + right[2]}
		}
	}
	return []int{0, 0, 0}
}
