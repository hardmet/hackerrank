package exercises

import "github.com/hardmet/hackerrank/go/sort"

func numRescueBoats(people []int, limit int) int {
	sort.QuickSort(people)
	var l, r, res = 0, len(people) - 1, 0
	for r >= l {
		if people[r]+people[l] <= limit {
			l++
		}
		r--
		res++
	}
	return res
}

func RescueBoatsTests() {
	println(numRescueBoats([]int{2}, 3) == 1)
	println(numRescueBoats([]int{2, 1}, 3) == 1)
	println(numRescueBoats([]int{1, 2, 1}, 3) == 2)
	println(numRescueBoats([]int{1, 2, 1, 1}, 3) == 2)
	println(numRescueBoats([]int{1, 2, 1, 3}, 4) == 2)
	println(numRescueBoats([]int{3, 2, 2, 1}, 3) == 3)
	println(numRescueBoats([]int{3, 5, 3, 4}, 5) == 4)
	println(numRescueBoats([]int{3, 5, 4, 3, 4}, 8) == 3)
}
