package exercises

func guess(num int) int {
	// todo
	return 0
}

func guessNumber(n int) int {
	return GuessSearch(1, n, guess)
}

func GuessSearch(l, r int, p func(int) int) int {
	if l == r {
		return l
	}
	if l > r {
		return -1
	}
	mid := (l + (r-l)/2)
	res := p(mid)
	if res == 0 {
		return mid
	}
	if res < 0 {
		return GuessSearch(l, mid-1, p)
	}
	if res > 0 {
		return GuessSearch(mid+1, r, p)
	}
	return -1
}
