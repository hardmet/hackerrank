package exercises

func singleNonDuplicate(nums []int) int {
	left := 0
	right := len(nums)
	var mid int
	for left < right {
		mid = (left + right) / 2
		if mid+1 < len(nums) && mid%2 == 0 && nums[mid] == nums[mid+1] {
			left = mid + 1
		} else if mid-1 >= 0 && mid%2 != 0 && nums[mid] == nums[mid-1] {
			left = mid + 1
		} else {
			right = mid
		}
	}
	return nums[left]
}
