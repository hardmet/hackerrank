package exercises

import (
	"fmt"
	"sort"

	"github.com/hardmet/hackerrank/go/int_union_find"
)

func numberOfGoodPaths(vals []int, edges [][]int) int {
	adj := make(map[int][]int, len(edges))
	for i := 0; i < len(edges); i++ {
		adj[i] = []int{}
	}
	for _, edge := range edges {
		adj[edge[0]] = append(adj[edge[0]], edge[1])
		adj[edge[1]] = append(adj[edge[1]], edge[0])
	}

	values := []int{}
	valuesToNodes := make(map[int][]int, len(vals))
	for i := 0; i < len(vals); i++ {
		valuesToNodes[vals[i]] = []int{}
	}
	for i := 0; i < len(vals); i++ {
		if nodes, _ := valuesToNodes[vals[i]]; len(nodes) == 0 {
			values = append(values, vals[i])
		}
		valuesToNodes[vals[i]] = append(valuesToNodes[vals[i]], i)
	}
	sort.Ints(values)
	uf := int_union_find.ArrayUFConstructor(len(vals))
	result := 0
	for _, valueKey := range values {
		for _, node := range valuesToNodes[valueKey] {
			if neighbors, contains := adj[node]; contains {
				for _, neighbor := range neighbors {
					if vals[node] >= vals[neighbor] {
						uf.Union(node, neighbor)
					}
				}
			}
		}
		group := make(map[int]int, len(valuesToNodes[valueKey]))
		for _, node := range valuesToNodes[valueKey] {
			groupNumber := uf.Find(node)
			group[groupNumber]++
		}
		for _, count := range group {
			result += count * (count + 1) / 2
		}
	}
	return result
}

func NumberOfGoodPathsTests() {
	fmt.Println(numberOfGoodPaths([]int{1, 3, 2, 1, 3}, [][]int{{0, 1}, {0, 2}, {2, 3}, {2, 4}}))
	fmt.Println(numberOfGoodPaths([]int{1, 1, 2, 2, 3}, [][]int{{0, 1}, {1, 2}, {2, 3}, {2, 4}}))
	fmt.Println(numberOfGoodPaths([]int{1}, [][]int{}))
}
