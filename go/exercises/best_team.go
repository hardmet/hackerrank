package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"math"
	"sort"
)

func bestTeamScore(scores []int, ages []int) int {
	agesScores := make([][2]int, len(ages))
	for i := 0; i < len(ages); i++ {
		agesScores[i][0] = scores[i]
		agesScores[i][1] = ages[i]
	}
	sort.Slice(agesScores, func(i, j int) bool {
		if agesScores[i][0] == agesScores[j][0] {
			return agesScores[i][1] < agesScores[j][1]
		}
		return agesScores[i][0] < agesScores[j][0]
	})
	var highestAge int
	for _, age := range ages {
		highestAge = math_utils.Max(highestAge, age)
	}
	bit := make([]int, highestAge+1)
	answer := math.MinInt
	for _, ageScore := range agesScores {
		best := ageScore[0] + queryBit(bit, ageScore[1])
		answer = math_utils.Max(answer, best)
		updateBit(bit, ageScore[1], best)
	}
	return answer
}

func queryBit(bit []int, age int) int {
	maxScore := math.MinInt
	for i := age; i > 0; i -= i & (-i) {
		maxScore = math_utils.Max(maxScore, bit[i])
	}
	return maxScore
}

func updateBit(bit []int, age, currentBest int) {
	for i := age; i < len(bit); i += i & (-i) {
		bit[i] = math_utils.Max(bit[i], currentBest)
	}
}
