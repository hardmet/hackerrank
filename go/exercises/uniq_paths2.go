package exercises

func uniquePathsWithObstacles(grid [][]int) int {
	if grid == nil || len(grid) == 0 || len(grid[0]) == 0 || grid[0][0] == 1 {
		return 0
	}
	prev := make([]int, len(grid[0]))
	current := make([]int, len(grid[0]))
	prev[0] = 1
	for i := 0; i < len(grid); i++ {
		if grid[i][0] == 1 {
			current[0] = 0
		} else {
			current[0] = prev[0]
		}
		for j := 1; j < len(grid[0]); j++ {
			if grid[i][j] == 1 {
				current[j] = 0
			} else {
				current[j] = current[j-1] + prev[j]
			}
		}
		copy(prev, current)
	}
	return prev[len(grid[0])-1]
}
