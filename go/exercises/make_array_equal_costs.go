package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func minCostArray(nums, costs []int) int64 {
	var left, right int64 = 1, 1_000_000
	ns, cs := make([]int64, len(nums)), make([]int64, len(costs))
	for i, num := range nums {
		ns[i] = int64(num)
		cs[i] = int64(costs[i])
		if ns[i] < left {
			left = ns[i]
		}
		if ns[i] > right {
			right = ns[i]
		}
	}
	var ans = findCost(ns, cs, 1)
	var mid, y1, y2 int64
	for left < right {
		mid = (left + right) / 2
		y1, y2 = findCost(ns, cs, mid), findCost(ns, cs, mid+1)
		ans = math_utils.Min64(y1, y2)
		if y1 < y2 {
			right = mid
		} else {
			left = mid + 1
		}
	}
	return ans
}

func findCost(nums, costs []int64, x int64) int64 {
	var res int64
	for i := 0; i < len(nums); i++ {
		res += math_utils.Abs64(nums[i]-x) * costs[i]
	}
	return res
}
