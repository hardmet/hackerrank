package exercises

import "math"

func convert(s string, numRows int) string {
	if numRows == 1 {
		return s
	}
	sections := int(math.Ceil(float64(len(s)) / (2*float64(numRows) - 2.0)))
	numCols := sections * (numRows - 1)
	var matrix = make([][]byte, numRows)
	for i := 0; i < numRows; i++ {
		matrix[i] = make([]byte, numCols)
		for j := 0; j < numCols; j++ {
			matrix[i][j] = ' '
		}
	}
	currRow, currCol, stringIndex := 0, 0, 0
	for stringIndex < len(s) {
		for currRow < numRows && stringIndex < len(s) {
			matrix[currRow][currCol] = s[stringIndex]
			currRow++
			stringIndex++
		}
		currRow -= 2
		currCol++
		for currRow > 0 && currCol < numCols && stringIndex < len(s) {
			matrix[currRow][currCol] = s[stringIndex]
			currRow--
			currCol++
			stringIndex++
		}
	}
	var answer []byte
	for _, row := range matrix {
		for j := range row {
			if row[j] != ' ' {
				answer = append(answer, row[j])
			}
		}
	}
	return string(answer)
}
