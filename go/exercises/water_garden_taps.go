package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func minTaps(n int, ranges []int) int {
	maxReach := make([]int, n+1)
	for i := 0; i < len(ranges); i++ {
		start := math_utils.Max(0, i-ranges[i])
		end := math_utils.Min(n, i+ranges[i])
		maxReach[start] = math_utils.Max(maxReach[start], end)
	}

	taps, currEnd, nextEnd := 0, 0, 0
	for i := 0; i <= n; i++ {
		if i > nextEnd {
			return -1
		}

		if i > currEnd {
			taps++
			currEnd = nextEnd
		}
		nextEnd = math_utils.Max(nextEnd, maxReach[i])
	}
	return taps
}
