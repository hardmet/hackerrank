package exercises

func garbageCollection(garbage []string, travel []int) int {
	for i := 1; i < len(travel); i++ {
		travel[i] = travel[i-1] + travel[i]
	}
	garbageLastPost := map[rune]int{}
	count := 0
	for i := 0; i < len(garbage); i++ {
		for _, c := range garbage[i] {
			garbageLastPost[c] = i
		}
		count += len(garbage[i])
	}
	garbageTypes := "MPG"
	for _, garbageType := range garbageTypes {
		if value := garbageLastPost[garbageType]; value != 0 {
			count += travel[value-1]
		}
	}
	return count
}
