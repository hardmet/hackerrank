package exercises

func countOrders(n int) int {
	if n == 1 {
		return 1
	}
	prev := 1
	for i := 2; i <= n; i++ {
		prev = prev * (2*i - 1) * i % 1_000_000_007
	}
	return prev
}
