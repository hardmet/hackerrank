package exercises

func numOfArrays(n, m, k int) int {
	dp := makeArr(m+1, k+1)
	prefix := makeArr(m+1, k+1)
	prevDP := makeArr(m+1, k+1)
	prevPrefix := makeArr(m+1, k+1)
	for num := 1; num < len(dp); num++ {
		dp[num][1] = 1
	}
	var mod int64 = 1e9 + 7
	m64 := int64(m)
	for i := 1; i <= n; i++ {
		if i > 1 {
			dp = makeArr(m+1, k+1)
		}
		prefix = makeArr(m+1, k+1)
		var maxNum int64
		for maxNum = 1; maxNum <= m64; maxNum++ {
			for cost := 1; cost <= k; cost++ {
				var ans int64 = (maxNum * prevDP[maxNum][cost]) % mod
				ans = (ans + prevPrefix[maxNum-1][cost-1]) % mod
				dp[maxNum][cost] += ans
				dp[maxNum][cost] %= mod
				prefix[maxNum][cost] = prefix[maxNum-1][cost] + dp[maxNum][cost]
				prefix[maxNum][cost] %= mod
			}
		}
		prevDP = dp
		prevPrefix = prefix
	}
	return int(prefix[m][k])
}

func makeArr(m, k int) [][]int64 {
	arr := make([][]int64, m)
	for i := 0; i < len(arr); i++ {
		arr[i] = make([]int64, k)
	}
	return arr
}
