package exercises

import (
	"container/heap"

	"github.com/hardmet/hackerrank/go/int_heap"
)

type SeatManager struct {
	maker          int
	availableSeats *int_heap.MinIntHeap
}

func ConstructorSeatManager(n int) SeatManager {
	q := int_heap.MinIntHeap{}
	return SeatManager{maker: 1, availableSeats: &q}
}

func (this *SeatManager) Reserve() int {
	if this.availableSeats.Len() > 0 {
		seatNumber := heap.Pop(this.availableSeats)
		return seatNumber.(int)
	}
	this.maker++
	return this.maker - 1
}

func (this *SeatManager) Unreserve(seatNumber int) {
	heap.Push(this.availableSeats, seatNumber)
}
