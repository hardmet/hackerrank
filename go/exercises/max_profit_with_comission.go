package exercises

func maxProfitFee(prices []int, fee int) int {
	buy := prices[0] + fee
	profit := 0
	for i := 1; i < len(prices); i++ {
		if prices[i]+fee < buy {
			buy = prices[i] + fee
		} else if prices[i] > buy {
			profit += prices[i] - buy
			buy = prices[i]
		}
	}
	return profit
}
