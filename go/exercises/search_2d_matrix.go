package exercises

func searchMatrix(matrix [][]int, target int) bool {
	var l, r, mid int = 0, len(matrix) - 1, 0
	for l < r {
		mid = (l + r) / 2
		if matrix[mid][0] <= target && matrix[mid][len(matrix[mid])-1] >= target {
			break
		}
		if matrix[mid][0] > target {
			r = mid - 1
		} else {
			l = mid
		}
	}
	row := mid
	l, r = 0, len(matrix[mid])-1
	for l < r {
		mid = (l + r) / 2
		if matrix[row][mid] == target {
			return true
		} else if matrix[row][mid] < target {
			l = mid
		} else {
			r = mid - 1
		}
	}
	return false
}
