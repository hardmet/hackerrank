package exercises

func buyChoco(prices []int, money int) int {
	var fstMin, sndMin int
	if prices[0] > prices[1] {
		fstMin = prices[1]
		sndMin = prices[0]
	} else {
		fstMin = prices[0]
		sndMin = prices[1]
	}
	for i := 2; i < len(prices); i++ {
		if prices[i] < fstMin {
			sndMin = fstMin
			fstMin = prices[i]
		} else if prices[i] < sndMin {
			sndMin = prices[i]
		}
	}
	if money-fstMin-sndMin > 0 {
		return money - fstMin - sndMin
	}
	return money
}
