package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func minFlipsMonoIncr(s string) int {
	onces, dp := 0, 0
	for i := 0; i < len(s); i++ {
		if s[i] == '0' {
			dp = math_utils.Min(dp+1, onces)
		} else {
			onces++
		}
	}
	return dp
}
