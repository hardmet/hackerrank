package exercises

func zeroFilledSubarray(nums []int) int64 {
	var total, current int64

	for _, n := range nums {
		if n == 0 {
			current++
			total += current
		} else {
			current = 0
		}
	}
	return total
}
