package exercises

func sortArrayByParity(nums []int) []int {
	l := 0
	for i, ni := range nums {
		if ni%2 == 0 {
			nums[l], nums[i] = nums[i], nums[l]
			l++
		}
	}
	return nums
}
