package exercises

import "math"

func ways(pizza []string, k int) int {
	m, n := len(pizza), len(pizza[0])
	dp := make([][][]int, k)
	for i := 0; i < k; i++ {
		dp[i] = make([][]int, m)
		for j := 0; j < m; j++ {
			dp[i][j] = make([]int, n)
			for h := 0; h < n; h++ {
				dp[i][j][h] = math.MinInt
			}
		}
	}
	preSum := make([][]int, m+1)
	for i := 0; i < m+1; i++ {
		preSum[i] = make([]int, n+1)
	}
	for r := m - 1; r >= 0; r-- {
		for c := n - 1; c >= 0; c-- {
			var isApple int
			if pizza[r][c] == 'A' {
				isApple = 1
			} else {
				isApple = 0
			}
			preSum[r][c] = preSum[r][c+1] + preSum[r+1][c] - preSum[r+1][c+1] + isApple
		}
	}
	return dfsWays(m, n, k-1, 0, 0, dp, preSum)
}

func dfsWays(m, n, k, r, c int, dp [][][]int, preSum [][]int) int {
	if preSum[r][c] == 0 {
		return 0
	}
	if k == 0 {
		return 1
	}
	if dp[k][r][c] != math.MinInt {
		return dp[k][r][c]
	}
	ans := 0
	for nr := r + 1; nr < m; nr++ {
		if preSum[r][c]-preSum[nr][c] > 0 {
			ans = (ans + dfsWays(m, n, k-1, nr, c, dp, preSum)) % 1_000_000_007
		}
	}
	for nc := c + 1; nc < n; nc++ {
		if preSum[r][c]-preSum[r][nc] > 0 {
			ans = (ans + dfsWays(m, n, k-1, r, nc, dp, preSum)) % 1_000_000_007
		}
	}
	dp[k][r][c] = ans
	return dp[k][r][c]
}
