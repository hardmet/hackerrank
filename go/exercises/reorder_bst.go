package exercises

func numOfWays(nums []int) int {
	arr := make([]int, len(nums))
	copy(arr, nums)
	return int(getCombs(arr, getTriangle(len(nums)+1))) - 1
}

func getCombs(nums []int, combs [][]int64) int64 {
	if len(nums) <= 2 {
		return 1
	}
	root := nums[0]
	var left, right []int
	for _, num := range nums {
		if num < root {
			left = append(left, num)
		} else if num > root {
			right = append(right, num)
		}
	}
	var mod int64 = 1_000_000_007
	return (combs[len(left)+len(right)][len(left)] * (getCombs(left, combs) % mod) % mod) * getCombs(right, combs) % mod
}

// Yang Hui (Pascale) triangle
func getTriangle(n int) [][]int64 {
	triangle := make([][]int64, n)
	for i := 0; i < len(triangle); i++ {
		triangle[i] = make([]int64, n)
	}
	for i := 0; i < n; i++ {
		triangle[i][i] = 1
		triangle[i][0] = triangle[i][i]
	}
	for i := 2; i < n; i++ {
		for j := 1; j < i; j++ {
			triangle[i][j] = (triangle[i-1][j] + triangle[i-1][j-1]) % 1_000_000_007
		}
	}
	return triangle
}
