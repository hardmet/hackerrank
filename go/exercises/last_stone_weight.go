package exercises

import (
	"container/heap"

	"github.com/hardmet/hackerrank/go/int_heap"
)

func lastStoneWeight(stones []int) int {
	pq := int_heap.MaxIntHeap{IntHeap: stones}
	heap.Init(&pq)
	for pq.Len() > 1 {
		s1, s2 := heap.Pop(&pq).(int), heap.Pop(&pq).(int)
		if s1 > s2 {
			heap.Push(&pq, s1-s2)
		}
	}
	if pq.Len() == 0 {
		return 0
	}
	return heap.Pop(&pq).(int)
}

func LastStoneWeightTest() {
	println(lastStoneWeight([]int{2, 2}))
}
