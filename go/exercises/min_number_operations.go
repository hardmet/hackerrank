package exercises

import "sort"

func minNumOperations(nums []int) int {
	n := len(nums)
	ans := n
	unique := map[int]struct{}{}
	for _, num := range nums {
		unique[num] = struct{}{}
	}
	newNums := make([]int, len(unique))
	index := 0
	for num, _ := range unique {
		newNums[index] = num
		index++
	}
	sort.Ints(newNums)
	for i, j := 0, 0; i < len(newNums); i++ {
		for j < len(newNums) && newNums[j] < newNums[i]+n {
			j++
		}
		if n-j+i < ans {
			ans = n - j + i
		}
	}
	return ans
}
