package exercises

import (
	"container/heap"

	"github.com/hardmet/hackerrank/go/int_heap"
)

type MedianFinder struct {
	minHeap *int_heap.MinIntHeap
	maxHeap *int_heap.MinIntHeap
}

func ConstructorMedianFinder() MedianFinder {
	return MedianFinder{maxHeap: &int_heap.MinIntHeap{}, minHeap: &int_heap.MinIntHeap{}}
}

func (f *MedianFinder) AddNum(num int) {
	if len(*f.maxHeap) == 0 || num <= -(*f.maxHeap)[0] {
		heap.Push(f.maxHeap, -num)
	} else {
		heap.Push(f.minHeap, num)
	}

	if f.minHeap.Len() > f.maxHeap.Len() {
		heap.Push(f.maxHeap, -heap.Pop(f.minHeap).(int))
	} else if f.maxHeap.Len() > f.minHeap.Len()+1 {
		heap.Push(f.minHeap, -heap.Pop(f.maxHeap).(int))
	}
}

func (f *MedianFinder) FindMedian() float64 {
	if (f.minHeap.Len()+f.maxHeap.Len())%2 == 0 {
		return float64(-(*f.maxHeap)[0]+(*f.minHeap)[0]) / 2
	}
	return -float64((*f.maxHeap)[0])
}
