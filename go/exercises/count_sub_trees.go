package exercises

func countSubTrees(n int, edges [][]int, labels string) []int {
	g := make([][]int, n)
	for i := range g {
		g[i] = []int{}
	}
	for _, edge := range edges {
		g[edge[0]] = append(g[edge[0]], edge[1])
		g[edge[1]] = append(g[edge[1]], edge[0])
	}
	ans := make([]int, len(labels))
	var counting func(root, up int) [26]int
	counting = func(root, up int) [26]int {
		counts := [26]int{}
		for _, node := range g[root] {
			if node != up {
				countsSubTrees := counting(node, root)
				for i, count := range countsSubTrees {
					counts[i] += count
				}
			}
		}
		counts[labels[root]-'a']++
		ans[root] = counts[labels[root]-'a']
		return counts
	}
	counting(0, -1)
	return ans
}
