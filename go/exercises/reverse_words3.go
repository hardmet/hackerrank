package exercises

func reverseWords(s string) string {
	bytes := []byte(s)
	var start, end, lastSpaceIndex int = 0, 0, -1

	for i := 0; i <= len(bytes); i++ {
		if i == len(bytes) || bytes[i] == ' ' {
			start, end = lastSpaceIndex+1, i-1
			ReverseLR(&bytes, start, end)
			lastSpaceIndex = i
		}
	}
	return string(bytes)
}
