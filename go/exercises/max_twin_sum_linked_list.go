package exercises

func pairSum(head *ListNode) int {
	slow, fast := head, head
	maxSum := 0
	// getting middle of list
	for fast != nil && fast.Next != nil {
		fast = fast.Next.Next
		slow = slow.Next
	}

	// reverse second half of linked list
	var nextNode, prevNode *ListNode
	for slow != nil {
		nextNode = slow.Next
		slow.Next = prevNode
		prevNode = slow
		slow = nextNode
	}

	for prevNode != nil {
		twinSum := head.Val + prevNode.Val
		if twinSum > maxSum {
			maxSum = twinSum
		}
		prevNode = prevNode.Next
		head = head.Next
	}
	return maxSum
}
