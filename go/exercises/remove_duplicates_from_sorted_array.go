package exercises

func removeDuplicatesArray(nums []int) int {
	n := 0
	for i := 1; i < len(nums); i++ {
		if nums[i-1] == nums[i] {
			continue
		}
		n++
		nums = append(nums[:i], nums[i+1:]...)
	}
	return n
}
