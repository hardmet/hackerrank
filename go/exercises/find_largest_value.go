package exercises

func largestValues(root *TreeNode) []int {
	values := []int{}
	bfsLargestInLevel(root, 0, &values)
	return values
}

func bfsLargestInLevel(node *TreeNode, level int, values *[]int) {
	if node != nil {
		switch {
		case len(*values) < level+1:
			*values = append(*values, node.Val)
		case (*values)[level] < node.Val:
			(*values)[level] = node.Val
		}
		bfsLargestInLevel(node.Left, level+1, values)
		bfsLargestInLevel(node.Right, level+1, values)
	}
}
