package exercises

import (
	"sort"

	"github.com/hardmet/hackerrank/go/math_utils"
)

func maxFrequency(nums []int, k int) int {
	k64 := int64(k)
	nums64 := make([]int64, len(nums))
	for i := 0; i < len(nums64); i++ {
		nums64[i] = int64(nums[i])
	}
	sort.Slice(nums64, func(i, j int) bool {
		return nums64[i] < nums64[j]
	})
	prefix := make([]int64, len(nums))
	prefix[0] = nums64[0]
	for i := 1; i < len(nums64); i++ {
		prefix[i] = nums64[i] + prefix[i-1]
	}
	maxFreq := 0
	for i := 0; i < len(nums); i++ {
		maxFreq = math_utils.Max(maxFreq, check(i, k64, nums64, prefix))
	}
	return maxFreq
}

func check(i int, k int64, nums, prefix []int64) int {
	var target int64 = int64(nums[i])
	l, r, best, mid := 0, i, i, 0
	for l <= r {
		mid = (l + r) / 2
		var count int64 = int64(i - mid + 1)
		finalSum := count * target
		originalSum := prefix[i] - prefix[mid] + int64(nums[mid])
		operationsRequired := finalSum - originalSum
		if operationsRequired > k {
			l = mid + 1
		} else {
			best = mid
			r = mid - 1
		}
	}
	return i - best + 1
}
