package exercises

func jump(nums []int) int {
	var answer, curEnd, curFar int
	for i := 0; i < len(nums)-1; i++ {
		if curFar < i+nums[i] {
			curFar = i + nums[i]
		}
		if i == curEnd {
			answer++
			curEnd = curFar
		}
	}
	return answer
}
