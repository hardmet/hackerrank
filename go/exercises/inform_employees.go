package exercises

func numOfMinutes(n int, headID int, manager []int, informTime []int) int {
	var dfs func(int) int
	dfs = func(i int) int {
		if manager[i] == -1 {
			return informTime[i]
		}
		informTime[i] += dfs(manager[i])
		manager[i] = -1
		return informTime[i]
	}

	maxTime := 0
	for i := range manager {
		dfsi := dfs(i)
		if dfsi > maxTime {
			maxTime = dfsi
		}
	}
	return maxTime
}
