package exercises

func buildTree(inorder, postorder []int) *TreeNode {
	return build(inorder, 0, len(inorder)-1, postorder, 0, len(postorder)-1)
}

func build(in []int, istart, iend int, post []int, pstart, pend int) *TreeNode {
	if istart > iend || pstart > pend {
		return nil
	}
	rootVal := post[pend]
	root := TreeNode{Val: rootVal}
	rootIndex := 0
	for i := istart; i <= iend; i++ {
		if in[i] == rootVal {
			rootIndex = i
			break
		}
	}
	leftSize, rightSize := rootIndex-istart, iend-rootIndex
	root.Left = build(in, istart, rootIndex-1, post, pstart, pstart+leftSize-1)
	root.Right = build(in, rootIndex+1, iend, post, pend-rightSize, pend-1)
	return &root
}
