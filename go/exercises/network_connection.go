package exercises

import (
	"github.com/hardmet/hackerrank/go/int_union_find"
)

func makeConnected(n int, connections [][]int) int {
	edges := len(connections)
	if edges < n-1 {
		return -1
	}
	uf := int_union_find.ArrayUFConstructor(n)
	components := n
	for _, connection := range connections {
		parX, parY := uf.Find(connection[0]), uf.Find(connection[1])
		uf.Union(connection[0], connection[1])
		if parX != parY {
			components -= 1
		}
	}
	return components - 1
}
