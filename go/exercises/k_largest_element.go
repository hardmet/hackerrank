package exercises

import (
	"math/rand"
)

func findKthLargest(nums []int, k int) int {
	return partitioned(nums, k)
}

func partitioned(nums []int, k int) int {
	pivotIndex := rand.Intn(len(nums))
	if pivotIndex == len(nums) {
		pivotIndex--
	}
	pivot := nums[pivotIndex]
	var left, mid, right []int
	for _, num := range nums {
		if num > pivot {
			left = append(left, num)
		} else if num < pivot {
			right = append(right, num)
		} else {
			mid = append(mid, num)
		}
	}
	if k <= len(left) {
		return partitioned(left, k)
	}
	if len(left)+len(mid) < k {
		return partitioned(right, k-len(left)-len(mid))
	}
	return pivot
}
