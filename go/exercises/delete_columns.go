package exercises

func minDeletionSize(grid []string) int {
	var counter int
	for column := range grid[0] {
		for j := 1; j < len(grid); j++ {
			if grid[j][column] < grid[j-1][column] {
				counter++
				break
			}
		}
	}
	return counter
}
