package exercises

import "strconv"

func evalRPN(tokens []string) int {
	var stack []int
	for _, t := range tokens {
		if t == "+" {
			l := stack[len(stack)-2]
			r := stack[len(stack)-1]
			stack = append(stack[:len(stack)-2], l+r)
		} else if t == "-" {
			l := stack[len(stack)-2]
			r := stack[len(stack)-1]
			stack = append(stack[:len(stack)-2], l-r)
		} else if t == "/" {
			l := stack[len(stack)-2]
			r := stack[len(stack)-1]
			stack = append(stack[:len(stack)-2], l/r)
		} else if t == "*" {
			l := stack[len(stack)-2]
			r := stack[len(stack)-1]
			stack = append(stack[:len(stack)-2], l*r)
		} else {
			n, _ := strconv.Atoi(t)
			stack = append(stack, n)
		}
	}
	return stack[0]
}
