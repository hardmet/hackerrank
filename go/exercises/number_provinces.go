package exercises

import "github.com/hardmet/hackerrank/go/int_union_find"

func findCircleNum(isConnected [][]int) int {
	visited := make([]int, len(isConnected))
	var dfs func(visited []int, k int)
	dfs = func(visited []int, k int) {
		for j := 0; j < len(isConnected); j++ {
			if isConnected[k][j] == 1 && visited[j] == 0 {
				visited[j] = 1
				dfs(visited, j)
			}
		}
	}
	count := 0
	for i := 0; i < len(isConnected); i++ {
		if visited[i] == 0 {
			dfs(visited, i)
			count++
		}
	}
	return count
}

func findCircleNum2(isConnected [][]int) int {
	uf := int_union_find.ArrayUFConstructor(len(isConnected))
	set := map[int]struct{}{}
	for i := 0; i < len(isConnected); i++ {
		for j := 0; j < len(isConnected); j++ {
			if isConnected[i][j] == 1 {
				uf.Union(i, j)
			}
		}
	}
	for i := 0; i < len(isConnected); i++ {
		set[uf.Find(i)] = struct{}{}
	}
	return len(set)
}
