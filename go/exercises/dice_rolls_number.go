package exercises

func numRollsToTarget(dices int, rolls int, target int) int {
	dp := make([][]int, dices+1)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, target+1)
	}
	dp[0][0] = 1
	var add int = 0
	for i := 1; i <= dices; i++ {
		for j := 1; j <= target; j++ {
			for k := 1; k <= rolls; k++ {
				if k <= j {
					add = dp[i-1][j-k]
				} else {
					add = 0
				}
				dp[i][j] = (dp[i][j] + add) % 1_000_000_007
			}
		}
	}
	return dp[dices][target]
}
