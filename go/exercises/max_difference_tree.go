package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func maxAncestorDiff(root *TreeNode) int {
	return bfs(root, root.Val, root.Val)
}

func bfs(root *TreeNode, minVal, maxVal int) int {
	if root == nil {
		return maxVal - minVal
	}
	minVal = math_utils.Min(minVal, root.Val)
	maxVal = math_utils.Max(maxVal, root.Val)
	r := bfs(root.Right, minVal, maxVal)
	l := bfs(root.Left, minVal, maxVal)
	return math_utils.Max(r, l)
}
