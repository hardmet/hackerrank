package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func wordBreak(s string, dict []string) bool {
	dp := make([]bool, len(s)+1)
	dp[0] = true
	maxLen := 0
	for _, word := range dict {
		if len(word) > maxLen {
			maxLen = len(word)
		}
	}
	for i := 1; i < len(s)+1; i++ {
		for j := i - 1; j >= math_utils.Max(i-maxLen-1, 0); j-- {
			if dp[j] && containsWord(s[j:i], dict) {
				dp[i] = true
				break
			}
		}
	}
	return dp[len(s)]
}

func containsWord(s string, ss []string) bool {
	for _, word := range ss {
		if s == word {
			return true
		}
	}
	return false
}
