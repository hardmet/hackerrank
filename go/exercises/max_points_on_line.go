package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"math"
)

func maxPoints(points [][]int) int {
	result := 0
	for _, a := range points {
		cache := make(map[float64]int)
		for _, b := range points {
			if a[0] == b[0] && a[1] == b[1] {
				continue
			}
			var k float64
			if b[0]-a[0] == 0 {
				k = math.MaxFloat64
			} else {
				k = float64(b[1]-a[1]) / float64(b[0]-a[0])
			}
			cache[k]++
			result = math_utils.Max(result, cache[k])
		}
	}
	return result + 1
}
