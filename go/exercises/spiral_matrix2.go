package exercises

func generateMatrix(n int) [][]int {
	if n == 1 {
		return [][]int{{1}}
	}
	top := 0
	bottom := n - 1
	left := 0
	right := n - 1

	matrix := make([][]int, n)
	for i := 0; i < n; i++ {
		matrix[i] = make([]int, n)
	}
	gens := []struct {
		begin     *int
		end       *int
		increment *int
		generator func(*int, *int, *int, *int, [][]int)
	}{
		{&left, &right, &top, genRight},
		{&top, &bottom, &right, genBottom},
		{&right, &left, &bottom, genLeft},
		{&bottom, &top, &left, genUp},
	}
	c := 1
	for i := 0; i < n*n; i++ {
		next := i % 4
		gens[next].generator(gens[next].begin, gens[next].end, gens[next].increment, &c, matrix)
	}
	return matrix
}

func genRight(l, r, t, n *int, matrix [][]int) {
	for i := *l; i <= *r; i++ {
		matrix[*t][i] = *n
		*n++
	}
	*t++
}

func genBottom(t, b, r, n *int, matrix [][]int) {
	for i := *t; i <= *b; i++ {
		matrix[i][*r] = *n
		*n++
	}
	*r--
}

func genLeft(r, l, b, n *int, matrix [][]int) {
	for i := *r; i >= *l; i-- {
		matrix[*b][i] = *n
		*n++
	}
	*b--
}

func genUp(b, t, l, n *int, matrix [][]int) {
	for i := *b; i >= *t; i-- {
		matrix[i][*l] = *n
		*n++
	}
	*l++
}
