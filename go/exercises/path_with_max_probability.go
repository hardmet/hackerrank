package exercises

type edge struct {
	node int
	prob float64
}

func maxProbability(n int, edges [][]int, succProb []float64, start int, end int) float64 {
	adj := getAdjList(n, edges, succProb)
	scores := make([]float64, n)
	scores[start] = 1.0

	for queue := []int{start}; len(queue) > 0; queue = queue[1:] {
		for _, edge := range adj[queue[0]] {
			score := scores[queue[0]] * edge.prob
			adjNode := edge.node
			if scores[adjNode] < score {
				scores[adjNode] = score
				queue = append(queue, adjNode)
			}
		}
	}

	return scores[end]
}

func getAdjList(n int, edges [][]int, succProb []float64) [][]edge {
	adj := make([][]edge, n)
	for i, e := range edges {
		u, v := e[0], e[1]
		adj[u] = append(adj[u], edge{node: v, prob: succProb[i]})
		adj[v] = append(adj[v], edge{node: u, prob: succProb[i]})
	}
	return adj
}
