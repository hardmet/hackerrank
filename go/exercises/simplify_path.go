package exercises

import "strings"

func simplifyPath(path string) string {
	commands := strings.Split(path, "/")
	var stack []string
	for _, c := range commands {
		if c == ".." && len(stack) > 0 {
			stack = stack[:len(stack)-1]
		} else if c != "" && c != "." && c != ".." {
			stack = append(stack, c)
		}
	}
	r := strings.Builder{}
	for _, si := range stack {
		r.WriteRune('/')
		r.WriteString(si)
	}
	if r.Len() == 0 {
		r.WriteRune('/')
	}
	return r.String()
}
