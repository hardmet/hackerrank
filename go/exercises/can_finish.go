package exercises

func canFinish(numCourses int, prerequisites [][]int) bool {
	degree := make([]int, numCourses)
	cache := make([][]int, numCourses)
	bfs := []int{}
	for i := 0; i < numCourses; i++ {
		cache[i] = []int{}
	}
	for _, courses := range prerequisites {
		cache[courses[1]] = append(cache[courses[1]], courses[0])
		degree[courses[0]]++
	}
	for i := 0; i < numCourses; i++ {
		if degree[i] == 0 {
			bfs = append(bfs, i)
		}
	}
	for _, course := range bfs {
		for _, j := range cache[course] {
			degree[j]--
			if degree[j] == 0 {
				bfs = append(bfs, j)
			}
		}
	}
	return len(bfs) == numCourses
}
