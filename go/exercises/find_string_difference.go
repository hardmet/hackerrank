package exercises

func findTheDifference(s string, t string) byte {
	counts := [26]byte{}
	for _, c := range t {
		counts[c-'a']++
	}
	for _, c := range s {
		counts[c-'a']--
	}
	for i := 0; i < len(counts); i++ {
		if counts[i] > 0 {
			return byte('a' + i)
		}
	}
	return 0
}
