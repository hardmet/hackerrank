package exercises

import (
	"github.com/hardmet/hackerrank/go/int_union_find"
)

func countPairs(n int, edges [][]int) int64 {
	uf := int_union_find.ArrayUFConstructor(n)
	for _, edge := range edges {
		uf.Union(edge[0], edge[1])
	}
	components := make([]int64, n)
	for i := 0; i < n; i++ {
		par := uf.Find(i)
		components[par] += 1
	}
	var pairs int64
	var remaining int64 = int64(n)
	for i := 0; i < n; i++ {
		if components[i] == 0 {
			continue
		}
		remaining -= components[i]
		pairs += (components[i] * remaining)
	}
	return pairs
}
