package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func longestCycle(edges []int) int {
	longestCycle, timeStep := -1, 1
	visitsCount := make([]int, len(edges))

	for i := range edges {
		if visitsCount[i] > 0 {
			continue
		}
		startTime := timeStep
		u := i
		for u != -1 && visitsCount[u] == 0 {
			visitsCount[u] = timeStep
			timeStep++
			u = edges[u]
		}
		if u != -1 && visitsCount[u] >= startTime {
			longestCycle = math_utils.Max(longestCycle, timeStep-visitsCount[u])
		}
	}
	return longestCycle
}
