package exercises

import (
	"math"

	"github.com/hardmet/hackerrank/go/math_utils"
)

func minCostConnectPoints(points [][]int) int {
	dp := make([]int, len(points))
	visited := make([]bool, len(points))
	for i := 0; i < len(points); i++ {
		dp[i] = math.MaxInt32
	}
	dist, curr := 0, 0
	var (
		best   int
		p1, p2 []int
	)

	for i := 1; i < len(points); i++ {
		best = curr
		dp[curr] = math.MaxInt32
		visited[curr] = true
		for j := 0; j < len(points); j++ {
			if visited[j] {
				continue
			}
			p1 = points[curr]
			p2 = points[j]
			dp[j] = math_utils.Min(dp[j], math_utils.Abs(p1[0]-p2[0])+math_utils.Abs(p1[1]-p2[1]))
			if dp[j] < dp[best] {
				best = j
			}
		}
		dist += dp[best]
		curr = best
	}
	return dist
}
