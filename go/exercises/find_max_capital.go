package exercises

import (
	"container/heap"
	"sort"

	"github.com/hardmet/hackerrank/go/int_heap"
)

type Project struct {
	capital, profit int
}

func findMaximizedCapital(k, w int, profits, capitals []int) int {
	var n = len(profits)
	projects := make([]Project, n)
	for i := 0; i < n; i++ {
		projects[i] = Project{capital: capitals[i], profit: profits[i]}
	}
	sort.Slice(projects, func(x, y int) bool {
		return projects[x].capital < projects[y].capital
	})

	pq := &int_heap.MaxIntHeap{}
	ptr := 0
	for i := 0; i < k; i++ {
		for ptr < n && projects[ptr].capital <= w {
			heap.Push(pq, projects[ptr].profit)
			ptr++
		}
		if pq.Len() == 0 {
			break
		}
		w += heap.Pop(pq).(int)
	}
	return w
}
