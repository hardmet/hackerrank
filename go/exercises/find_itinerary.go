package exercises

import "sort"

func findItinerary(tickets [][]string) []string {
	targets := make(map[string][]string)

	for _, ticket := range tickets {
		targets[ticket[0]] = append(targets[ticket[0]], ticket[1])
	}

	for target := range targets {
		sort.Sort(sort.Reverse(sort.StringSlice(targets[target])))
	}

	stack := []string{"JFK"}
	var itinerary []string

	for len(stack) > 0 {
		for len(targets[stack[len(stack)-1]]) > 0 {
			last := len(targets[stack[len(stack)-1]]) - 1
			stack = append(stack, targets[stack[len(stack)-1]][last])
			targets[stack[len(stack)-2]] = targets[stack[len(stack)-2]][:last]
		}
		itinerary = append(itinerary, stack[len(stack)-1])
		stack = stack[:len(stack)-1]
	}

	for i := 0; i < len(itinerary)/2; i++ {
		itinerary[i], itinerary[len(itinerary)-1-i] = itinerary[len(itinerary)-1-i], itinerary[i]
	}

	return itinerary
}
