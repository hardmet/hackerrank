package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"sort"
)

func jobScheduling(startTime []int, endTime []int, profit []int) int {
	n := len(startTime)
	sep := make([][3]int, n)
	for i := range startTime {
		sep[i][0] = startTime[i]
		sep[i][1] = endTime[i]
		sep[i][2] = profit[i]
	}
	sort.Slice(sep, func(i, j int) bool {
		if sep[i][1] == sep[j][1] {
			return sep[i][2] > sep[j][2]
		} else {
			return sep[i][1] < sep[j][1]
		}
	})
	dp := make([]int, n+1)
	for i := 0; i < len(dp); i++ {
		dp[i] = 0
	}
	dp[1] = sep[0][2]
	for i := 2; i <= n; i++ {
		l, r, j := 0, i-2, 0
		for l <= r {
			mid := l + (r-l)/2
			if sep[mid][1] <= sep[i-1][0] {
				j = mid + 1
				l = mid + 1
			} else {
				r = mid - 1
			}
		}
		dp[i] = math_utils.Max(dp[i-1], sep[i-1][2]+dp[j])
	}
	return dp[n]
}
