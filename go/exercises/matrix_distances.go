package exercises

func updateMatrix(mat [][]int) [][]int {
	if mat == nil || len(mat) == 0 || len(mat[0]) == 0 {
		return [][]int{}
	}
	m, n := len(mat), len(mat[0])
	q := make([][]int, 0)
	MaxValue := m * n
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if mat[i][j] == 0 {
				q = append(q, []int{i, j})
			} else {
				mat[i][j] = MaxValue
			}
		}
	}
	directions := [][]int{{1, 0}, {-1, 0}, {0, 1}, {0, -1}}
	for len(q) > 0 {
		cell := q[0]
		q = q[1:]
		for _, dir := range directions {
			r, c := cell[0]+dir[0], cell[1]+dir[1]
			if r >= 0 && r < m && c >= 0 && c < n && mat[r][c] > mat[cell[0]][cell[1]]+1 {
				q = append(q, []int{r, c})
				mat[r][c] = mat[cell[0]][cell[1]] + 1
			}
		}
	}
	return mat
}
