package exercises

func countHomogenous(s string) int {
	count := 0
	streak := 0
	const MOD = 1e9 + 7
	for i := 0; i < len(s); i++ {
		if i == 0 || s[i] == s[i-1] {
			streak++
		} else {
			streak = 1
		}
		count = (count + streak) % MOD
	}
	return count
}
