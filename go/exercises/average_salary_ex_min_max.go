package exercises

import "math"

func average(salaries []int) float64 {
	min, max, total := math.MaxInt, math.MinInt, 0
	for _, salary := range salaries {
		if salary < min {
			min = salary
		}
		if salary > max {
			max = salary
		}
		total += salary
	}
	println(total)
	return float64(total-min-max) / float64(len(salaries)-2)
}
