package exercises

func nearestExit(maze [][]byte, entrance []int) int {
	m, n := len(maze), len(maze[0])
	directions := [4][2]int{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}
	queue := [][2]int{{entrance[0], entrance[1]}}
	path := 0
	maze[entrance[0]][entrance[1]] = '+'
	for len(queue) > 0 {
		qn := len(queue)
		path++
		for k := 0; k < qn; k++ {
			io, jo := queue[0][0], queue[0][1]
			queue = queue[1:]
			for _, ij := range directions {
				i, j := io+ij[0], jo+ij[1]
				if 0 > i || i >= m || 0 > j || j >= n || maze[i][j] != '.' {
					continue
				}
				if i == 0 || j == 0 || i == m-1 || j == n-1 {
					return path
				}
				queue = append(queue, [2]int{i, j})
				maze[i][j] = '+'
			}
		}
	}
	return -1
}
