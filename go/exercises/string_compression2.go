package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func getLengthOfOptimalCompression(s string, k int) int {
	dp := [110][110]int{}
	for i := 0; i <= len(s); i++ {
		for j := 0; j <= len(s); j++ {
			dp[i][j] = 9999
		}
	}
	dp[0][0] = 0
	var count, deletions int
	for i := 1; i <= len(s); i++ {
		for j := 0; j <= k; j++ {
			count, deletions = 0, 0
			for l := i; l >= 1; l-- {
				if s[l-1] == s[i-1] {
					count++
				} else {
					deletions++
				}
				if j-deletions >= 0 {
					value := 0
					if count >= 100 {
						value = 3
					} else if count >= 10 {
						value = 2
					} else if count >= 2 {
						value = 1
					}
					dp[i][j] = math_utils.Min(dp[i][j], dp[l-1][j-deletions]+1+value)
				}
			}
			if j > 0 {
				dp[i][j] = math_utils.Min(dp[i][j], dp[i-1][j-1])
			}
		}
	}
	return dp[len(s)][k]
}
