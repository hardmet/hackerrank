package exercises

func reorganizeString(s string) string {
	var symCount [26]int
	for _, si := range s {
		symCount[si-'a']++
	}
	maxCount, letter := 0, 0
	for i := 0; i < len(symCount); i++ {
		if symCount[i] > maxCount {
			maxCount = symCount[i]
			letter = i
		}
	}
	if maxCount > (len(s)+1)/2 {
		return ""
	}
	res := make([]byte, len(s))
	resi := 0
	for ; symCount[letter] > 0; resi += 2 {
		res[resi] = byte(letter + 'a')
		symCount[letter]--
	}
	for i, count := range symCount {
		for count > 0 {
			if resi >= len(res) {
				resi = 1
			}
			res[resi] = byte(i + 'a')
			resi += 2
			count--
		}
	}
	return string(res)
}
