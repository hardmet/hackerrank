package exercises

import "sort"

func numFactoredBinaryTrees(arr []int) int {
	const mod = 1e9 + 7
	sort.Ints(arr)
	s := make(map[int]bool)
	for _, x := range arr {
		s[x] = true
	}
	dp := map[int]int64{}
	for _, n := range arr {
		dp[n] = 1
	}

	for _, i := range arr {
		for _, j := range arr {
			if j*j > i {
				break
			}
			if _, contains := s[i/j]; contains && i%j == 0 {
				temp := (dp[j] * dp[i/j]) % mod
				if i/j == j {
					dp[i] = (dp[i] + temp) % mod
				} else {
					dp[i] = (dp[i] + temp*2) % mod
				}
			}
		}
	}
	res := int64(0)
	for _, v := range dp {
		res = (res + v) % mod
	}
	return int(res)
}
