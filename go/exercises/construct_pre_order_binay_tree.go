package exercises

import "strconv"

func tree2str(root *TreeNode) string {
	if root != nil {
		var l, r, preOrder string
		l = tree2str(root.Left)
		r = tree2str(root.Right)
		preOrder = strconv.Itoa(root.Val)
		if len(l) == 0 && len(r) == 0 {
			return preOrder
		}
		preOrder += "(" + l + ")"
		if len(r) > 0 {
			preOrder += "(" + r + ")"
		}
		return preOrder
	}
	return ""
}
