package exercises

func isSubs(s1, s2 string, i, j int, t [][]int) int {
	if i == 0 || j == 0 {
		return 0
	}
	if t[i][j] != -1 {
		return t[i][j]
	}
	if s1[i-1] == s2[j-1] {
		t[i][j] = 1 + isSubs(s1, s2, i-1, j-1, t)
		return t[i][j]
	} else {
		t[i][j] = isSubs(s1, s2, i, j-1, t)
		return t[i][j]
	}
}

func isSubsequence(s1, s2 string) bool {
	if len(s1) > len(s2) {
		return false
	}
	t := make([][]int, len(s1)+1)
	for i := 0; i < len(t); i++ {
		t[i] = make([]int, len(s2)+1)
		for j := 0; j < len(t[i]); j++ {
			t[i][j] = -1
		}
	}
	return isSubs(s1, s2, len(s1), len(s2), t) == len(s1)
}
