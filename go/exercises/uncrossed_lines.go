package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func maxUncrossedLines(a, b []int) int {
	return lca[int](a, b)
}

func lca[T comparable](a, b []T) int {
	dp := make([][]int, len(a)+1)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, len(b)+1)
	}

	for i := 1; i < len(a)+1; i++ {
		for j := 1; j < len(b)+1; j++ {
			if a[i-1] == b[j-1] {
				dp[i][j] = dp[i-1][j-1] + 1
			} else {
				dp[i][j] = math_utils.Max(dp[i][j-1], dp[i-1][j])
			}
		}
	}
	return dp[len(a)][len(b)]
}
