package exercises

func numberOfWays(corridor string) int {
	const MOD = 1_000_000_007
	count := make([][3]int, len(corridor)+1)
	count[len(corridor)][0] = 0
	count[len(corridor)][1] = 0
	count[len(corridor)][2] = 1
	for index := len(corridor) - 1; index >= 0; index-- {
		if corridor[index] == 'S' {
			count[index][0] = count[index+1][1]
			count[index][1] = count[index+1][2]
			count[index][2] = count[index+1][1]
		} else {
			count[index][0] = count[index+1][0]
			count[index][1] = count[index+1][1]
			count[index][2] = (count[index+1][0] + count[index+1][2]) % MOD
		}
	}
	return count[0][0]
}
