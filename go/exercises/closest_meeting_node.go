package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"math"
)

func closestMeetingNode(edges []int, node1, node2 int) int {
	dist1 := make([]int, len(edges))
	dist2 := make([]int, len(edges))
	for i := range dist1 {
		dist1[i], dist2[i] = math.MaxInt, math.MaxInt
	}
	bfsClosest(node1, edges, dist1)
	bfsClosest(node1, edges, dist2)
	minDistNode, minDistTillNow := -1, math.MaxInt
	for i := 0; i < len(edges); i++ {
		if minDistTillNow > math_utils.Max(dist1[i], dist2[i]) {
			minDistNode = i
			minDistTillNow = math_utils.Max(dist1[i], dist2[i])
		}
	}
	return minDistNode
}

func bfsClosest(start int, edges []int, dist []int) {
	var q = []int{start}
	visited := make([]bool, len(edges))
	dist[start] = 0
	var node, neighbor int
	for len(q) > 0 {
		node = q[0]
		q = q[1:]
		if visited[node] {
			continue
		}
		visited[node] = true
		neighbor = edges[node]
		if neighbor != -1 && !visited[neighbor] {
			dist[neighbor] = 1 + dist[node]
			q = append(q, neighbor)
		}
	}
}
