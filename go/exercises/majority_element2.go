package exercises

func majorityElement(nums []int) []int {
	candidate1, candidate2, c1, c2 := 0, 0, 0, 0
	for _, num := range nums {
		if candidate1 == num {
			c1++
		} else if candidate2 == num {
			c2++
		} else if c1 == 0 {
			candidate1 = num
			c1 = 1
		} else if c2 == 0 {
			candidate2 = num
			c2 = 1
		} else {
			c1--
			c2--
		}
	}

	c1, c2 = 0, 0
	for _, num := range nums {
		if num == candidate1 {
			c1++
		} else if num == candidate2 {
			c2++
		}
	}
	ans := []int{}
	if c1 > len(nums)/3 {
		ans = append(ans, candidate1)
	}
	if c2 > len(nums)/3 {
		ans = append(ans, candidate2)
	}
	return ans
}
