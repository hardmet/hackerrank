package exercises

import "sort"

func minDeletions(s string) int {
	frequencies := make([]int, 26)
	for _, c := range s {
		frequencies[c-'a']++
	}
	sort.Ints(frequencies)
	deletions := 0
	for i := 24; i >= 0 && frequencies[i] != 0; i-- {
		if frequencies[i] >= frequencies[i+1] {
			prev := frequencies[i]
			frequencies[i] = 0
			if frequencies[i+1]-1 > 0 {
				frequencies[i] = frequencies[i+1] - 1
			}
			deletions += prev - frequencies[i]
		}
	}
	return deletions
}
