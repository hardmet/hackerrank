package exercises

type FindElements struct {
	TreeNode
	values map[int]bool
}

func ConstructorFindElements(root *TreeNode) FindElements {
	if root == nil {
		return FindElements{}
	}
	root.Val = 0
	values := map[int]bool{0: true}

	var initTree func(root *TreeNode) TreeNode
	initTree = func(root *TreeNode) TreeNode {
		if root.Left != nil {
			root.Left.Val = 2*root.Val + 1
			values[root.Left.Val] = true
			initTree(root.Left)
		}
		if root.Right != nil {
			root.Right.Val = 2*root.Val + 2
			values[root.Right.Val] = true
			initTree(root.Right)
		}
		return *root
	}
	return FindElements{initTree(root), values}
}

func (fe *FindElements) Find(target int) bool {
	return fe.values[target]
}

/**
 * Your FindElements object will be instantiated and called as such:
 * obj := Constructor(root);
 * param_1 := obj.Find(target);
 */
