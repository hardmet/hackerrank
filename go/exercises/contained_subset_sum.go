package exercises

import "math"

func constrainedSubsetSum(nums []int, k int) int {
	q := []int{}
	dp := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		if len(q) > 0 && i-q[0] > k {
			q = q[1:]
		}
		if len(q) > 0 {
			dp[i] = dp[q[0]] + nums[i]
		} else {
			dp[i] = nums[i]
		}
		for len(q) > 0 && dp[q[len(q)-1]] < dp[i] {
			q = q[:len(q)-1]
		}
		if dp[i] > 0 {
			q = append(q, i)
		}
	}
	maxSum := math.MinInt
	for _, num := range dp {
		if num > maxSum {
			maxSum = num
		}
	}
	return maxSum
}
