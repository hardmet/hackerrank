package exercises

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	n := len(nums1) + len(nums2)
	if n%2 == 1 {
		return float64(medianSearch(nums1, nums2, n/2, 0, len(nums1)-1, 0, len(nums2)-1))
	} else {
		return float64(
			medianSearch(nums1, nums2, n/2, 0, len(nums1)-1, 0, len(nums2)-1)+
				medianSearch(nums1, nums2, n/2-1, 0, len(nums1)-1, 0, len(nums2)-1)) / 2
	}
}

func medianSearch(a, b []int, k, aStart, aEnd, bStart, bEnd int) int {
	if aEnd < aStart {
		return b[k-aStart]
	}
	if bEnd < bStart {
		return a[k-bStart]
	}
	i := (aStart + aEnd) / 2
	j := (bStart + bEnd) / 2
	if i+j < k {
		if a[i] > b[j] {
			return medianSearch(a, b, k, aStart, aEnd, j+1, bEnd)
		} else {
			return medianSearch(a, b, k, i+1, aEnd, bStart, bEnd)
		}
	} else {
		if a[i] > b[j] {
			return medianSearch(a, b, k, aStart, i-1, bStart, bEnd)
		} else {
			return medianSearch(a, b, k, aStart, aEnd, bStart, j-1)
		}
	}
}
