package exercises

func partitionLinked(head *ListNode, x int) *ListNode {
	var lHead, rHead, l, r *ListNode
	for node := head; node != nil; node = node.Next {
		if node.Val >= x {
			if rHead == nil {
				rHead = &ListNode{Val: node.Val}
				r = rHead
			} else {
				r.Next = &ListNode{Val: node.Val}
				r = r.Next
			}
		} else {
			if lHead == nil {
				lHead = &ListNode{Val: node.Val}
				l = lHead
			} else {
				l.Next = &ListNode{Val: node.Val}
				l = l.Next
			}
		}
	}
	if l == nil {
		return rHead
	}
	l.Next = rHead
	return lHead
}
