package exercises

import (
	"fmt"
	"strconv"
)

func calculate(s string) int {
	num, sign := 0, 1
	stack := []int{0}

	for _, c := range s {
		if '0' <= c && c <= '9' {
			n, _ := strconv.Atoi(string(c))
			num = num*10 + n
		} else if c == ' ' {
			continue
		} else if c == '+' {
			stack[len(stack)-1] = stack[len(stack)-1] + num*sign
			sign = 1
			num = 0
		} else if c == '-' {
			stack[len(stack)-1] = stack[len(stack)-1] + num*sign
			sign = -1
			num = 0
		} else if c == '(' {
			stack = append(stack, sign, 0)
			sign = 1
			num = 0
		} else if c == ')' {
			stackLastNum := stack[len(stack)-1]
			lastSign := stack[len(stack)-2]
			stack = stack[:len(stack)-2]
			stack[len(stack)-1] = stack[len(stack)-1] + (stackLastNum+num*sign)*lastSign
			sign = 1
			num = 0
		}
	}
	fmt.Println(stack)
	return stack[len(stack)-1] + num*sign
}
