package exercises

func getWinner(arr []int, k int) int {
	winner := arr[0]
	winStreak := 0
	for i := 1; i < len(arr); i++ {
		if arr[i] < winner {
			winStreak++
		} else {
			if winStreak >= k {
				return winner
			}
			winner = arr[i]
			winStreak = 1
		}
	}
	return winner
}
