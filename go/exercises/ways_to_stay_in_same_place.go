package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func numOfWaysToStayInSamePlace(steps, arrLen int) int {
	arrLen = math_utils.Min(arrLen, steps)
	cache := make([][]int, arrLen)
	for i := 0; i < len(cache); i++ {
		cache[i] = make([]int, steps+1)
		for j := 0; j < len(cache[i]); j++ {
			cache[i][j] = -1
		}
	}
	const MOD = 1e9 + 7
	var dp func(curr, remain int) int
	dp = func(curr, remain int) int {
		if remain == 0 {
			if curr == 0 {
				return 1
			}
			return 0
		}
		if cache[curr][remain] != -1 {
			return cache[curr][remain]
		}
		ans := dp(curr, remain-1)
		if curr > 0 {
			ans = (ans + dp(curr-1, remain-1)) % MOD
		}
		if curr < arrLen-1 {
			ans = (ans + dp(curr+1, remain-1)) % MOD
		}
		cache[curr][remain] = ans
		return ans
	}
	return dp(0, steps)
}
