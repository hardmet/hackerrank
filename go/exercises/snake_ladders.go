package exercises

import (
	"container/heap"
	"github.com/hardmet/hackerrank/go/math_utils"
)

func snakesAndLadders(board [][]int) int {
	cells, columns := make([][]int, len(board)*len(board)+1), make([]int, len(board))
	for i := range columns {
		columns[i] = i
	}
	label := 1
	for row := len(board) - 1; row >= 0; row-- {
		for _, column := range columns {
			cells[label] = []int{row, column}
			label++
		}
		ReverseInt(&columns)
	}
	dist := make([]int, len(cells))
	for i := range dist {
		dist[i] = -1
	}
	dist[1] = 0
	pq := make(PriorityQueue, 0)
	heap.Push(&pq, &Vertex{
		distance: 0,
		label:    1,
	})
	for pq.Len() > 0 {
		vertex := heap.Pop(&pq).(*Vertex)
		if vertex.distance != dist[vertex.label] {
			continue
		}
		for next := vertex.label + 1; next <= math_utils.Min(vertex.label+6, len(board)*len(board)); next++ {
			row, column := cells[next][0], cells[next][1]
			var destination = next
			if board[row][column] != -1 {
				destination = board[row][column]
			}
			if dist[destination] == -1 || dist[vertex.label]+1 < dist[destination] {
				dist[destination] = dist[vertex.label] + 1
				heap.Push(&pq, &Vertex{
					distance: dist[destination],
					label:    destination,
				})
			}
		}
	}
	return dist[len(board)*len(board)]
}

func ReverseInt(bs *[]int) {
	ReverseLRInt(bs, 0, len(*bs)-1)
}

func ReverseLRInt(bs *[]int, l, r int) {
	for i := (r-l+1)/2 - 1; i >= 0; i-- {
		t := (*bs)[l+i]
		(*bs)[l+i] = (*bs)[r-i]
		(*bs)[r-i] = t
	}
}

type Vertex struct {
	distance, label int
}

type PriorityQueueVertex []*Vertex

func (pq PriorityQueueVertex) Len() int { return len(pq) }

func (pq PriorityQueueVertex) Less(i, j int) bool {
	// We want Pop to give us the highest, not lowest, priority so, we use greater than here.
	return pq[i].distance < pq[j].distance
}

func (pq PriorityQueueVertex) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *PriorityQueueVertex) Push(x interface{}) {
	*pq = append(*pq, x.(*Vertex))
}

func (pq *PriorityQueueVertex) Pop() interface{} {
	p := (*pq)[len(*pq)-1]
	(*pq)[len(*pq)-1] = nil // avoid memory leak
	*pq = (*pq)[:len(*pq)-1]
	return p
}
