package exercises

func buildArray(target []int, _ int) []string {
	ans := []string{}
	i := 0
	for _, num := range target {
		for ; i < num-1; i++ {
			ans = append(ans, "Push", "Pop")
		}
		ans = append(ans, "Push")
		i++
	}
	return ans
}
