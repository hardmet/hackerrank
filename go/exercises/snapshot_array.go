package exercises

type ValueSnapshot struct {
	value, version int
}

type ValueSnapshots []ValueSnapshot

func (vs *ValueSnapshots) nonEmpty() bool {
	return len(*vs) > 0
}

func (vs *ValueSnapshots) isLastHasSameVersionAs(v int) bool {
	return (*vs)[len(*vs)-1].version == v
}

type SnapshotArray struct {
	changesHistory []ValueSnapshots
	version        int
}

func SAConstructor(length int) SnapshotArray {
	return SnapshotArray{
		changesHistory: make([]ValueSnapshots, length),
		version:        0,
	}
}

func (sa *SnapshotArray) Set(index int, val int) {
	// set to last version if last has same version
	if sa.changesHistory[index].nonEmpty() && sa.changesHistory[index].isLastHasSameVersionAs(sa.version) {
		sa.changesHistory[index][len(sa.changesHistory[index])-1].value = val
	} else {
		sa.changesHistory[index] = append(sa.changesHistory[index], ValueSnapshot{val, sa.version})
	}
}

func (sa *SnapshotArray) Snap() int {
	sa.version++
	return sa.version - 1
}

func (sa *SnapshotArray) Get(index int, version int) int {
	return searchValue(sa.changesHistory[index], version)
}

func searchValue(valueSnapshots []ValueSnapshot, version int) int {
	left, right := 0, len(valueSnapshots)-1

	for left <= right {
		mid := left + (right-left)/2
		if valueSnapshots[mid].version == version {
			return valueSnapshots[mid].value
		}

		if valueSnapshots[mid].version < version {
			left = mid + 1
		} else {
			right = mid - 1
		}
	}

	if left-1 <= len(valueSnapshots) && left-1 >= 0 {
		return valueSnapshots[left-1].value
	}
	return 0 // this is empty valueSnapshots
}
