package exercises

func candy(ratings []int) int {
	if len(ratings) == 0 {
		return 0
	}
	up, down, peak, candies := 0, 0, 0, 1
	for i := 0; i < len(ratings)-1; i++ {
		prev, curr := ratings[i], ratings[i+1]
		if prev < curr {
			up++
			down = 0
			peak = up
			candies += 1 + up
		} else if prev == curr {
			up, down, peak = 0, 0, 0
			candies += 1
		} else {
			up = 0
			down++
			candies += 1 + down
			if peak >= down {
				candies--
			}
		}
	}
	return candies
}
