package exercises

import "github.com/hardmet/hackerrank/go/tuples"

func soupServings(n int) float64 {
	if n > 4800 {
		return 1
	}
	cache := map[tuples.Pair]float64{}
	var dp func(a, b int) float64
	dp = func(a, b int) float64 {
		if a <= 0 && b <= 0 {
			return 0.5
		}
		if a <= 0 {
			return 1.0
		}
		if b <= 0 {
			return 0.0
		}
		k := tuples.Pair{Fst: a, Snd: b}
		if value, contains := cache[k]; contains {
			return value
		}
		cache[k] = 0.25 * (dp(a-4, b) + dp(a-3, b-1) + dp(a-2, b-2) + dp(a-1, b-3))
		return cache[k]
	}
	n = (n + 24) / 25
	return dp(n, n)
}
