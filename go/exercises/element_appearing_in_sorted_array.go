package exercises

func findSpecialInteger(arr []int) int {
	if len(arr) < 4 {
		return arr[0]
	}
	count := 1
	quaterCount := len(arr) / 4

	for i := 1; i < len(arr); i++ {
		if arr[i] == arr[i-1] {
			count++
			if count > quaterCount {
				return arr[i]
			}
		} else {
			count = 1
		}
	}
	return -1
}
