package exercises

import "fmt"

func halvesAreAlike(s string) bool {
	vowels := map[byte]int{'a': 0, 'e': 0, 'i': 0, 'o': 0, 'u': 0, 'A': 0, 'E': 0, 'I': 0, 'O': 0, 'U': 0}
	mid := len(s)/2 - 1
	p1 := mid + 1
	p2 := p1 + (len(s) % 2)
	bs := []byte(s)
	return countVowels(bs, vowels, 0, p1) == countVowels(bs, vowels, p2, len(s))
}

func countVowels(bs []byte, vowels map[byte]int, l, r int) int {
	count := 0
	for i := l; i < r; i++ {
		_, contains := vowels[bs[i]]
		if contains {
			count++
		}
	}
	return count
}

func HalvesAlikeTest() {
	fmt.Println(halvesAreAlike("book"))
	fmt.Println(halvesAreAlike("ttobb"))
	fmt.Println(halvesAreAlike("atobu"))
	fmt.Println(!halvesAreAlike("textbook"))
	fmt.Println(halvesAreAlike("tttbb"))
	fmt.Println(halvesAreAlike("ttbb"))
	fmt.Println(halvesAreAlike("ato"))
	fmt.Println(halvesAreAlike("ao"))
	fmt.Println(!halvesAreAlike("so"))
	fmt.Println(halvesAreAlike("ss"))
	fmt.Println(halvesAreAlike("a"))
}
