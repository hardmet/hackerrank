package exercises

func findAnagrams(s, p string) []int {
	var answer []int
	if len(p) > len(s) {
		return answer
	}

	frequencyS := [26]int{}
	frequencyP := [26]int{}
	n, m := len(s), len(p)
	for i := 0; i < m; i++ {
		frequencyS[s[i]-'a']++
		frequencyP[p[i]-'a']++
	}
	for i := 0; i <= n-m; i++ {
		if isSame(frequencyS, frequencyP) {
			answer = append(answer, i)
		}
		frequencyS[s[i]-'a']--
		if i+m < n {
			frequencyS[s[i+m]-'a']++
		}
	}
	return answer
}

func isSame(frequencyS, frequencyP [26]int) bool {
	for i := 0; i < 26; i++ {
		if frequencyS[i] != frequencyP[i] {
			return false
		}
	}
	return true
}
