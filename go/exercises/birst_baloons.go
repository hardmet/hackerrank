package exercises

import (
	"math"
	"sort"

	"github.com/hardmet/hackerrank/go/math_utils"
)

func minCost(n int, cuts []int) int {
	var c = append([]int{}, cuts...)
	c = append(c, 0, n)
	sort.Ints(c)
	dp := make([][]int, len(c))
	for i := 0; i < len(c); i++ {
		dp[i] = make([]int, len(c))
	}
	for i := len(c) - 1; i >= 0; i-- {
		for j := i + 1; j < len(c); j++ {
			for k := i + 1; k < j; k++ {
				ij := math.MaxInt
				if dp[i][j] != 0 {
					ij = dp[i][j]
				}
				dp[i][j] = math_utils.Min(ij, c[j]-c[i]+dp[i][k]+dp[k][j])
			}
		}
	}
	return dp[0][len(c)-1]
}
