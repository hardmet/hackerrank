package exercises

func searchInsert(nums []int, target int) int {
	l, r := 0, len(nums)-1
	ans := 0
	for l <= r {
		mid := l + (r-l)/2
		if nums[mid] == target {
			ans = mid
			break
		} else if nums[mid] < target {
			ans = mid + 1
			l = mid + 1
		} else {
			r = mid - 1
		}
	}
	return ans
}
