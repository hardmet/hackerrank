package exercises

func getLastMoment(n int, left []int, right []int) int {
	position := 0
	for _, num := range left {
		if num > position {
			position = num
		}
	}
	for _, num := range right {
		if n-num > position {
			position = n - num
		}
	}
	return position
}
