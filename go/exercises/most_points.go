package exercises

func mostPoints(questions [][]int) int64 {
	dp := [200001]int64{}
	for i := len(questions) - 1; i >= 0; i-- {
		dp[i] = max64(int64(questions[i][0])+dp[questions[i][1]+i+1], dp[i+1])
	}
	return dp[0]
}

func max64(a, b int64) int64 {
	if a > b {
		return a
	}
	return b
}
