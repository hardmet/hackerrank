package exercises

import (
	"math"

	"github.com/hardmet/hackerrank/go/math_utils"
)

func mostProfitablePath(edges [][]int, bob int, amount []int) int {
	tree := make([][]int, len(amount))
	for i := 0; i < len(tree); i++ {
		tree[i] = []int{}
	}
	distanceFromBob := make([]int, len(amount))
	for _, edge := range edges {
		tree[edge[0]] = append(tree[edge[0]], edge[1])
		tree[edge[1]] = append(tree[edge[1]], edge[0])
	}
	var findPaths func(sourceNode, parentNode, time, bob int) int
	findPaths = func(sourceNode int, parentNode int, time int, bob int) int {
		maxIncome := 0
		maxChild := math.MinInt
		if sourceNode == bob {
			distanceFromBob[sourceNode] = 0
		} else {
			distanceFromBob[sourceNode] = len(amount)
		}

		for _, adjacentNode := range tree[sourceNode] {
			if adjacentNode != parentNode {
				maxChild = math_utils.Max(maxChild, findPaths(adjacentNode, sourceNode, time+1, bob))
				distanceFromBob[sourceNode] = math_utils.Min(distanceFromBob[sourceNode], distanceFromBob[adjacentNode]+1)
			}
		}

		if distanceFromBob[sourceNode] > time {
			maxIncome += amount[sourceNode]
			// Alice and Bob reach the node at the same time
		} else if distanceFromBob[sourceNode] == time {
			maxIncome += amount[sourceNode] / 2
		}
		if maxChild == math.MinInt {
			return maxIncome
		} else {
			return maxIncome + maxChild
		}
	}
	return findPaths(0, 0, 0, bob)
}
