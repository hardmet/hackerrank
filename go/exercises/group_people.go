package exercises

func groupThePeople(groupSizes []int) [][]int {
	groupMap := map[int][]int{}
	result := [][]int{}
	for i, size := range groupSizes {
		if _, contains := groupMap[size]; contains {
			groupMap[size] = append(groupMap[size], i)
		} else {
			groupMap[size] = []int{size}
		}
		if len(groupMap[size]) == size {
			dst := make([]int, size)
			copy(dst, groupMap[size])
			result = append(result, dst)
			groupMap[size] = []int{}
		}
	}
	return result
}
