package exercises

import "sort"

func minPairSum(nums []int) int {
	sort.Ints(nums)
	maxSum := 0
	sum := 0
	for i := 0; i < len(nums)/2; i++ {
		sum = nums[i] + nums[len(nums)-i-1]
		if sum > maxSum {
			maxSum = sum
		}
	}
	return maxSum
}
