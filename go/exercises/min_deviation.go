package exercises

import (
	"container/heap"
	"github.com/hardmet/hackerrank/go/int_heap"
	"github.com/hardmet/hackerrank/go/math_utils"
	"math"
)

func minimumDeviation(nums []int) int {
	pq := int_heap.MaxIntHeap{}
	mn := math.MaxInt32
	for _, v := range nums {
		if v%2 == 1 {
			v *= 2
		}
		mn = math_utils.Min(mn, v)
		heap.Push(&pq, v)
	}

	res := math.MaxInt32
	for pq.Len() > 0 {
		cur := heap.Pop(&pq).(int)
		if cur >= mn {
			res = math_utils.Min(res, cur-mn)
		}

		if cur%2 == 0 {
			cur /= 2
			mn = math_utils.Min(mn, cur)
			heap.Push(&pq, cur)
		} else {
			break
		}
	}
	return res
}
