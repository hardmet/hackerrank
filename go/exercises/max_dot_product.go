package exercises

import (
	"math"

	"github.com/hardmet/hackerrank/go/math_utils"
)

func maxDotProduct(nums1, nums2 []int) int {
	firstMax, firstMin := findMinMax(nums1)
	secondMax, secondMin := findMinMax(nums2)
	if firstMax < 0 && secondMin > 0 {
		return firstMax * secondMin
	}

	if firstMin > 0 && secondMax < 0 {
		return firstMin * secondMax
	}
	m := len(nums2) + 1
	var dp []int
	prevDP := make([]int, m)
	for i := len(nums1) - 1; i >= 0; i-- {
		dp = make([]int, m)
		for j := len(nums2) - 1; j >= 0; j-- {
			use := nums1[i]*nums2[j] + prevDP[j+1]
			dp[j] = math_utils.Max(use, math_utils.Max(dp[j+1], prevDP[j]))
		}
		prevDP = dp
	}
	return dp[0]
}

func findMinMax(arr []int) (int, int) {
	curMax, curMin := math.MinInt, math.MaxInt
	for _, num := range arr {
		if num > curMax {
			curMax = num
		}
		if num < curMin {
			curMin = num
		}
	}
	return curMax, curMin
}
