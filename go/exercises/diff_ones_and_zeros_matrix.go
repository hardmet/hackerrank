package exercises

func onesMinusZeros(grid [][]int) [][]int {
	rowOnes := make([]int, len(grid))
	colOnes := make([]int, len(grid[0]))
	rowZeros := make([]int, len(grid))
	colZeros := make([]int, len(grid[0]))
	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[i]); j++ {
			switch grid[i][j] {
			case 1:
				rowOnes[i]++
				colOnes[j]++
			case 0:
				rowZeros[i]++
				colZeros[j]++
			}
		}
	}
	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[i]); j++ {
			grid[i][j] = rowOnes[i] + colOnes[j] - rowZeros[i] - colZeros[j]
		}
	}
	return grid
}
