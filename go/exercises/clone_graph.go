package exercises

import "github.com/hardmet/hackerrank/go/graph"

func cloneGraph(node *graph.Node) *graph.Node {
	return graph.DeepCopy(node)
}
