package exercises

func new21Game(n int, k int, maxPts int) float64 {
	if k == 0 || n >= k+maxPts {
		return 1
	}
	var dp = make([]float64, n+1)
	var ptsSum, res float64 = 1, 0
	dp[0] = 1
	for i := 1; i <= n; i++ {
		dp[i] = ptsSum / float64(maxPts)
		if i < k {
			ptsSum += dp[i]
		} else {
			res += dp[i]
		}
		if i-maxPts >= 0 {
			ptsSum -= dp[i-maxPts]
		}
	}
	return res
}
