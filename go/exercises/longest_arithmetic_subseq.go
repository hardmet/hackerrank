package exercises

func longestArithSeqLength(nums []int) int {
	dp := make([][1001]int16, len(nums))
	var max int16
	for ri, right := range nums {
		for li, left := range nums[:ri] {
			diff := right - left + 500
			count := dp[li][diff] + 1
			dp[ri][diff] = count
			if count > max {
				max = count
			}
		}
	}
	return int(max) + 1
}
