package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"math"
	"sort"
)

type Point struct {
	x, y int
}

func outerTreesC(trees [][]int) [][]int {
	var points []Point
	for _, tree := range trees {
		points = append(points, Point{x: tree[0], y: tree[1]})
	}
	max, min := points[0], points[0]
	for i := 1; i < len(points); i++ {
		max = maxX(max, points[i])
		min = minX(min, points[i])
	}
	part1 := quickHull(points, min, max)
	part2 := quickHull(points, max, min)
	distinctHull := map[Point]int{}
	addDistinctPoints(part1, &distinctHull)
	addDistinctPoints(part2, &distinctHull)
	intervals := []Point{}
	for point := range distinctHull {
		intervals = append(intervals, point)
	}
	intervals = append(intervals, intervals[0])
	answer := [][]int{}
	for i := 1; i < len(intervals); i++ {
		for _, point := range points {
			if _, ok := distinctHull[point]; !ok {
				if isBetween(intervals[i-1], point, intervals[i]) {
					distinctHull[point] = 1
				}
			}
		}
	}
	for point := range distinctHull {
		answer = append(answer, []int{point.x, point.y})
	}
	return answer
}

func addDistinctPoints(ps []Point, distinctHull *map[Point]int) {
	for _, p := range ps {
		if _, ok := (*distinctHull)[p]; !ok {
			(*distinctHull)[p] = 1
		}
	}
}

func quickHull(points []Point, lineA, lineB Point) []Point {
	filteredPoints := map[Point]int{}
	for _, p := range points {
		d := distance(lineA, lineB, p)
		if d <= 0 {
			filteredPoints[p] = d
		}
	}
	if len(filteredPoints) == 0 {
		return []Point{}
	}
	var maxPoint Point
	maxD := math.MinInt
	for point, d := range filteredPoints {
		if math_utils.Abs(d) > maxD {
			maxD = math_utils.Abs(d)
			maxPoint = point
		}
	}
	outsideTriangle := func(p Point) bool {
		return !isInsideTriangle(lineA, lineB, maxPoint, p)
	}
	outMaxAndLine := []Point{}
	for point := range filteredPoints {
		if outsideTriangle(point) {
			outMaxAndLine = append(outMaxAndLine, point)
		}
	}
	res := []Point{lineA}
	res = append(res, quickHull(outMaxAndLine, lineA, maxPoint)...)
	res = append(res, maxPoint)
	res = append(res, quickHull(outMaxAndLine, maxPoint, lineB)...)
	res = append(res, lineB)
	return res
}

func distance(start, end, point Point) int {
	return (point.y-start.y)*(end.x-start.x) -
		(end.y-start.y)*(point.x-start.x)
}

func distanceP(a, b Point) float64 {
	return math.Sqrt(float64(a.x-b.x)*float64(a.x-b.x) + float64(a.y-b.y)*float64(a.y-b.y))
}

func isBetween(a, c, b Point) bool {
	return distanceP(a, c)+distanceP(c, b) == distanceP(a, b)
}

func isInsideTriangle(a, b, c, p Point) bool {
	return square(a, b, c) == square(a, b, p)+square(a, c, p)+square(b, c, p)
}

func square(a, b, c Point) float64 {
	return float64(a.x*(b.y-c.y)+b.x*(c.y-a.y)+c.x*(a.y-b.y)) / 2.0
}

func maxX(a, b Point) Point {
	if a.x > b.x {
		return a
	} else {
		return b
	}
}

func minX(a, b Point) Point {
	if a.x < b.x {
		return a
	} else {
		return b
	}
}

func outerTrees(trees [][]int) [][]int {
	var points []Point
	for _, tree := range trees {
		points = append(points, Point{x: tree[0], y: tree[1]})
	}
	sort.Slice(points, func(i, j int) bool { return points[i].x < points[j].x })
	leftToRight := constructHalfHull(points)
	////	### reverse points, so we are moving from right to left
	for i, j := 0, len(points)-1; i < j; i, j = i+1, j-1 {
		points[i], points[j] = points[j], points[i]
	}
	rightToLeft := constructHalfHull(points)
	//
	//	//### it is posible that the top and bottom parts have same points (e.g., all points form a line)
	//	//### we remove the duplicated points using a set
	distinctHull := map[Point]int{}
	addDistinctPoints(leftToRight, &distinctHull)
	addDistinctPoints(rightToLeft, &distinctHull)
	result := make([][]int, len(distinctHull))
	for point := range distinctHull {
		result = append(result, []int{point.x, point.y})
	}
	return result
}

func crossProduct(p1, p2, p3 Point) int {
	a := p3.x - p2.x
	b := p3.y - p2.y
	c := p2.x - p1.x
	d := p2.y - p1.y
	return a*d - b*c
}

func constructHalfHull(points []Point) []Point {
	var stack []Point
	for _, p := range points {
		//### if the chain formed by the current point
		//### and the last two points in the stack is not counterclockwise, pop it
		for len(stack) >= 2 && crossProduct(stack[len(stack)-2], stack[len(stack)-1], p) > 0 {
			stack = stack[:len(stack)-1]
		}
		stack = append(stack, p)
	}
	return stack
}
