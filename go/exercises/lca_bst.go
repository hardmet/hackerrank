package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func lowestCommonAncestorBST(root, p, q *TreeNode) *TreeNode {
	minimum := math_utils.Min(p.Val, q.Val)
	maximum := math_utils.Max(p.Val, q.Val)

	for minimum > root.Val || root.Val > maximum {
		if maximum < root.Val {
			root = root.Left
		} else if minimum > root.Val {
			root = root.Right
		}
	}
	return root
}
