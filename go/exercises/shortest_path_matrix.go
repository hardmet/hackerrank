package exercises

func shortestPathBinaryMatrix(grid [][]int) int {
	dir := [8][2]int{{0, 1}, {0, -1}, {1, 0}, {-1, 0}, {1, -1}, {-1, 1}, {-1, -1}, {1, 1}}
	m, n := len(grid), len(grid[0])
	if grid[0][0] == 1 || grid[m-1][n-1] == 1 {
		return -1
	}
	visited := make([][]bool, m)
	for i := 0; i < len(visited); i++ {
		visited[i] = make([]bool, n)
	}
	visited[0][0] = true
	var queue [][2]int
	queue = append(queue, [2]int{0, 0})
	ans := 0
	for len(queue) > 0 {
		size := len(queue)
		for i := 0; i < size; i++ {
			head := queue[0]
			queue = queue[1:]
			if head[0] == m-1 && head[1] == n-1 {
				return ans + 1
			}
			for k := 0; k < 8; k++ {
				dx := dir[k][0] + head[0]
				dy := dir[k][1] + head[1]
				if 0 <= dx && dx < m && 0 <= dy && dy < n && !visited[dx][dy] && grid[dx][dy] == 0 {
					queue = append(queue, [2]int{dx, dy})
					visited[dx][dy] = true
				}
			}
		}
		ans++
	}
	return -1
}
