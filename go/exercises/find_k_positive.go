package exercises

func findKthPositive(arr []int, k int) int {
	var i int = 0
	n := 1
	for k != 0 {
		if i < len(arr) {
			if arr[i] == n {
				i++
			} else {
				k--
			}
		} else {
			k--
		}
		n++
	}
	return n
}
