package exercises

func smallestSufficientTeam(reqSkills []string, people [][]string) []int {
	m := map[string]int{}
	i := 0
	for _, skill := range reqSkills {
		m[skill] = i
		i++
	}
	reqSkillsBitMask := (1 << i) - 1
	skills := makePeopleSkillsMask(people, m)
	localTeam := []int{}
	return findTeam(reqSkillsBitMask, skills, 0, 0, &localTeam, []int{})
}

func makePeopleSkillsMask(people [][]string, skillMaskMap map[string]int) []int {
	skills := make([]int, len(people))
	for i := 0; i < len(people); i++ {
		for _, skill := range people[i] {
			skills[i] |= (1 << skillMaskMap[skill])
		}
	}
	return skills
}

func findTeam(reqSkill int, skills []int, teamSkills, person int, localTeam *[]int, mainTeam []int) []int {
	if len(mainTeam) > 0 && len(*localTeam) >= len(mainTeam)-1 || person == len(skills) {
		return mainTeam
	}
	*localTeam = append(*localTeam, person)
	if (teamSkills | skills[person]) == reqSkill {
		mainTeam = make([]int, len(*localTeam))
		copy(mainTeam, *localTeam)
		*localTeam = (*localTeam)[:len(*localTeam)-2]
		return mainTeam
	} else if (teamSkills | skills[person]) > teamSkills {
		mainTeam = findTeam(reqSkill, skills, teamSkills|skills[person], person+1, localTeam, mainTeam)
	}
	*localTeam = (*localTeam)[:len(*localTeam)-2]
	return findTeam(reqSkill, skills, teamSkills, person+1, localTeam, mainTeam)
}
