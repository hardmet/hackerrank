package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func maxScore(nums []int) int {
	var dfs func(ns []int, dp [][]int, i, mask int) int
	dfs = func(ns []int, dp [][]int, i, mask int) int {
		if i > len(ns)/2 {
			return 0
		}
		if dp[i][mask] == 0 {
			for j := 0; j < len(ns); j++ {
				for k := j + 1; k < len(ns); k++ {
					newMask := 1<<j + 1<<k
					if mask&newMask == 0 {
						dp[i][mask] = math_utils.Max(dp[i][mask], i*gcd(ns[j], ns[k])+dfs(ns, dp, i+1, mask+newMask))
					}
				}
			}
		}
		return dp[i][mask]
	}
	dp := make([][]int, len(nums)/2+1)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, 1<<len(nums))
	}
	return dfs(nums, dp, 1, 0)
}
