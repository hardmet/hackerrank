package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func maxSubarraySumCircular(nums []int) int {
	curMax, curMin, sum, maxSum, minSum := 0, 0, 0, nums[0], nums[0]
	for _, num := range nums {
		curMax = math_utils.Max(curMax, 0) + num
		maxSum = math_utils.Max(maxSum, curMax)
		curMin = math_utils.Min(curMin, 0) + num
		minSum = math_utils.Min(minSum, curMin)
		sum += num
	}
	if sum == minSum {
		return maxSum
	}
	return math_utils.Max(maxSum, sum-minSum)
}
