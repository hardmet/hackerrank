package exercises

func hammingWeight(num uint32) int {
	bitCount := 0
	for num != 0 {
		num &= (num - 1)
		bitCount++
	}
	return bitCount
}
