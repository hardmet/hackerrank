package exercises

type UndergroundSystem struct {
	travels map[string][2]int
	ins     map[int]string
	times   map[int]int
}

func ConstructorUS() UndergroundSystem {
	return UndergroundSystem{}
}

func (s *UndergroundSystem) CheckIn(id int, stationName string, t int) {
	s.ins[id] = stationName
	s.times[id] = t
}

func (s *UndergroundSystem) CheckOut(id int, stationName string, t int) {
	key := makeKey(s.ins[id], stationName)
	if timesAndCount, contains := s.travels[key]; contains {
		s.travels[key] = [2]int{t - s.times[id] + timesAndCount[0], timesAndCount[1] + 1}
	} else {
		s.travels[key] = [2]int{t - s.times[id], 1}
	}
	delete(s.ins, id)
	delete(s.times, id)
}

func (s *UndergroundSystem) GetAverageTime(startStation string, endStation string) float64 {
	timeAndCount := s.travels[makeKey(startStation, endStation)]
	return float64(timeAndCount[0]) / float64(timeAndCount[1])
}

func makeKey(a, b string) string {
	return a + ":" + b
}
