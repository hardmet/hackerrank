package exercises

import "fmt"

func smallestEquivalentString(s1 string, s2 string, baseStr string) string {
	alphabet := [26]int{}
	for i := 0; i < 26; i++ {
		alphabet[i] = i
	}
	for i := range s1 {
		unionABC(int(s1[i]-'a'), int(s2[i]-'a'), &alphabet)
	}
	res := make([]byte, len(baseStr))
	for i, s := range []byte(baseStr) {
		res[i] = byte('a' + findABC(int(s-'a'), &alphabet))
	}
	return string(res)
}

func findABC(x int, abc *[26]int) int {
	if abc[x] == x {
		return x
	}
	abc[x] = findABC(abc[x], abc)
	return abc[x]
}

func unionABC(x, y int, abc *[26]int) {
	x = findABC(x, abc)
	y = findABC(y, abc)
	if x < y {
		abc[y] = x
	} else if x > y {
		abc[x] = y
	}
}

func SmallestSTRTests() {
	fmt.Println(smallestEquivalentString("leetcode", "programs", "sourcecode"))
	fmt.Println(smallestEquivalentString("hello", "world", "hold"))
	fmt.Println(smallestEquivalentString("parker", "morris", "parser"))
}
