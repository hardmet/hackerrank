package exercises

func dailyTemperatures(temps []int) []int {
	if len(temps) <= 1 {
		return []int{0}
	}
	ans := make([]int, len(temps))
	for i := len(temps) - 1; i >= 0; i-- {
		if i == len(temps)-1 {
			ans[i] = 0
		} else if temps[i] < temps[i+1] {
			ans[i] = 1
		} else if temps[i] == temps[i+1] {
			if ans[i+1] == 0 {
				ans[i] = 0
			} else {
				ans[i] = ans[i+1] + 1
			}
		} else {
			j := i + 1
			for j < len(temps) && ans[j] != 0 && temps[j] <= temps[i] {
				j = j + ans[j]
			}
			if j == len(temps) || temps[j] <= temps[i] {
				ans[i] = 0
			} else {
				ans[i] = j - i
			}
		}
	}
	return ans
}
