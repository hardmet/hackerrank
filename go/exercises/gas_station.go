package exercises

func canCompleteCircuit(gas []int, cost []int) int {
	totalGas, totalCost := 0, 0
	for i := range gas {
		totalGas += gas[i]
		totalCost += cost[i]
	}
	if totalGas < totalCost {
		return -1
	}
	remainsGas, start := 0, 0
	for i := range gas {
		remainsGas = remainsGas + gas[i] - cost[i]
		if remainsGas < 0 {
			start = i + 1
			remainsGas = 0
		}
	}
	return start
}
