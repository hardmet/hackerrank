package exercises

func makeEqual(words []string) bool {
	letters := [26]int{}
	for _, word := range words {
		for _, letter := range word {
			letters[letter-'a']++
		}
	}
	for _, letterCount := range letters {
		if letterCount%len(words) != 0 {
			return false
		}
	}
	return true
}
