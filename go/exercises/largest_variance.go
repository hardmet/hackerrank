package exercises

func largestVariance(s string) int {
	frequencies := [26]int{}
	for _, si := range s {
		frequencies[si-'a']++
	}
	ans := 0
	var c1, c2 int
	for i := 0; i < 26; i++ {
		for j := 0; j < 26; j++ {
			if j != i && frequencies[i] != 0 && frequencies[j] != 0 {
				for k := 1; k <= 2; k++ {
					c1, c2 = 0, 0
					for _, si := range s {
						if int(si-'a') == i {
							c1++
						}
						if int(si-'a') == j {
							c2++
						}
						if c2 > c1 {
							c1 = 0
							c2 = 0
						}
						if c1 > 0 && c2 > 0 && c1-c2 > ans {
							ans = c1 - c2
						}
					}
					runes := []rune(s)
					for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
						runes[i], runes[j] = runes[j], runes[i]
					}
					s = string(runes)
				}
			}

		}
	}
	return ans
}
