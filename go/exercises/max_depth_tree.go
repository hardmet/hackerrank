package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func maxDepth(root *TreeNode) int {
	return dfsMaxDepth(root, 0)
}

func dfsMaxDepth(root *TreeNode, current int) int {
	if root != nil {
		l, r := dfsMaxDepth(root.Left, current), dfsMaxDepth(root.Right, current)
		return math_utils.Max(r, l) + 1
	}
	return current
}
