package exercises

func countPalindromicSubsequence(s string) int {
	fstOccurences := [26]int{}
	lastOccurences := [26]int{}
	for i, si := range s {
		if fstOccurences[si-'a'] == 0 {
			fstOccurences[si-'a'] = i + 1
		}
		lastOccurences[si-'a'] = i
	}
	uniqMidSymbols := map[byte]struct{}{}
	count := 0
	for i := 0; i < len(fstOccurences); i++ {
		for j := fstOccurences[i]; j < lastOccurences[i]; j++ {
			uniqMidSymbols[s[j]] = struct{}{}
		}
		count += len(uniqMidSymbols)
		uniqMidSymbols = map[byte]struct{}{}
	}
	return count
}
