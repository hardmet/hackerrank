package exercises

import "math"

func poorPigs(buckets int, minutesToDie int, minutesToTest int) int {
	var pigs float64 = 0
	x := float64(minutesToTest/minutesToDie + 1)
	bx := float64(buckets)
	for math.Pow(x, pigs) < bx {
		pigs++
	}
	return int(pigs)
}
