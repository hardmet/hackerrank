package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func minCostColorRope(colors string, cost []int) int {
	result := 0
	for i := 1; i < len(colors); i++ {
		if colors[i] == colors[i-1] {
			result += math_utils.Min(cost[i], cost[i-1])
			cost[i] = math_utils.Max(cost[i], cost[i-1])
		}
	}
	return result
}
