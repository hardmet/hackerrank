package exercises

func uniquePathsIII(grid [][]int) int {
	zero, a, b := 0, 0, 0
	for i := range grid {
		for j := range grid[i] {
			if grid[i][j] == 0 {
				zero++
			} else if grid[i][j] == 1 {
				a = i
				b = j
			}
		}
	}
	return path(grid, a, b, zero)
}

func path(grid [][]int, x, y, zero int) int {
	if x < 0 || y < 0 || x >= len(grid) || y >= len(grid[0]) || grid[x][y] == -1 {
		return 0
	}
	if grid[x][y] == 2 {
		if zero == -1 {
			return 1
		} else {
			return 0
		}
	}
	grid[x][y] = -1
	zero--
	total := path(grid, x+1, y, zero) + path(grid, x, y+1, zero) +
		path(grid, x-1, y, zero) + path(grid, x, y-1, zero)
	grid[x][y] = 0
	zero++
	return total
}
