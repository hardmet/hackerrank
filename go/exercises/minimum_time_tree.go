package exercises

func minTime(n int, edges [][]int, hasApple []bool) int {
	adj := make(map[int][]int, len(edges)*2)
	for i := 0; i < 2*len(edges); i++ {
		adj[i] = []int{}
	}
	for _, edge := range edges {
		adj[edge[0]] = append(adj[edge[0]], edge[1])
		adj[edge[1]] = append(adj[edge[1]], edge[0])
	}
	return dfsMinTime(0, -1, &adj, hasApple)
}

func dfsMinTime(node, parent int, adj *map[int][]int, hasApple []bool) int {
	vert, contains := (*adj)[node]
	if !contains {
		return 0
	}
	total, childTime := 0, 0
	for _, child := range vert {
		if child == parent {
			continue
		}
		childTime = dfsMinTime(child, node, adj, hasApple)
		if childTime > 0 || hasApple[child] {
			total += childTime + 2
		}
	}
	return total
}
