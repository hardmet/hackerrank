package exercises

import (
	"math"

	"github.com/hardmet/hackerrank/go/math_utils"
	"github.com/hardmet/hackerrank/go/tuples"
)

func minScore(n int, roads [][]int) int {
	cache := make([][]tuples.Pair, n+1)
	for _, edge := range roads {
		cache[edge[0]] = append(cache[edge[0]], tuples.Pair{Fst: edge[1], Snd: edge[2]})
		cache[edge[1]] = append(cache[edge[1]], tuples.Pair{Fst: edge[0], Snd: edge[2]})
	}
	visited := make([]int, n+1)
	var ans int
	ans = math.MaxInt
	return dfsMinScore(1, ans, cache, visited)
}

func dfsMinScore(node int, ans int, cache [][]tuples.Pair, visited []int) int {
	visited[node] = 1
	for _, neighbors := range cache[node] {
		ans = math_utils.Min(ans, neighbors.Snd)
		if visited[neighbors.Fst] == 0 {
			visited[neighbors.Fst] = 1
			ans = math_utils.Min(ans, dfsMinScore(neighbors.Fst, ans, cache, visited))
		}
	}
	return ans
}
