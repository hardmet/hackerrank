package exercises

func asteroidCollision(asteroids []int) []int {
	var explode func(result []int, a int, res []int) []int
	explode = func(result []int, a int, current []int) []int {
		if len(result) == 0 {
			return append(current, a)
		} else if result[len(result)-1] <= a {
			result = append(result, current...)
			result = append(result, a)
			return result
		} else if result[len(result)-1] < 0 {
			return explode(result[:len(result)-1], a, append([]int{result[len(result)-1]}, current...))
		} else if result[len(result)-1]+a < 0 {
			return explode(result[:len(result)-1], a, current)
		} else if result[len(result)-1]+a == 0 {
			return append(current, result[:len(result)-1]...)
		} else {
			return append(current, result...)
		}
	}
	res := []int{}
	for i := 0; i < len(asteroids); i++ {
		if asteroids[i] > 0 {
			res = append(res, asteroids[i])
		} else {
			res = explode(res, asteroids[i], []int{})
		}
	}
	return res
}
