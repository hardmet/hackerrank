package exercises

func validPartition(nums []int) bool {
	if len(nums) == 1 {
		return false
	}
	dp := []bool{true, false, len(nums) > 1 && nums[0] == nums[1]}
	for i := 2; i < len(nums); i++ {
		current := false
		if nums[i] == nums[i-1] && dp[1] {
			current = true
		} else if nums[i] == nums[i-1] && nums[i] == nums[i-2] && dp[0] {
			current = true
		} else if nums[i]-nums[i-1] == 1 && nums[i-1]-nums[i-2] == 1 && dp[0] {
			current = true
		}
		dp[0] = dp[1]
		dp[1] = dp[2]
		dp[2] = current
	}
	return dp[2]
}
