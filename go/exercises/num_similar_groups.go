package exercises

func numSimilarGroups(strings []string) int {
	groups := 0
	n := len(strings)
	visited := make([]bool, n)
	for i := 0; i < n; i++ {
		if !visited[i] {
			groups++
			similarGroupsDFS(i, strings, &visited)
		}
	}
	return groups
}

func similarGroupsDFS(i int, strings []string, visited *[]bool) {
	(*visited)[i] = true
	for j := 0; j < len(strings); j++ {
		if !(*visited)[j] {
			if isSimilar(strings[i], strings[j]) {
				similarGroupsDFS(j, strings, visited)
			}
		}
	}
}

func isSimilar(a, b string) bool {
	count := 0
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			count++
		}
	}
	return count == 2 || count == 0
}
