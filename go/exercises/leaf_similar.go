package exercises

func leafSimilar(root1 *TreeNode, root2 *TreeNode) bool {
	l1 := getLeafs(root1)
	l2 := getLeafs(root2)
	if len(l1) != len(l2) {
		return false
	}
	for i := range l1 {
		if l1[i] != l2[i] {
			return false
		}
	}
	return true
}

func dfs(node *TreeNode, leafs *[]int) {
	if node.Left == nil && node.Right == nil {
		*leafs = append(*leafs, node.Val)
		return
	}
	if node.Left != nil {
		dfs(node.Left, leafs)
	}
	if node.Right != nil {
		dfs(node.Right, leafs)
	}
}

func getLeafs(root *TreeNode) []int {
	leafs := []int{}
	dfs(root, &leafs)
	return leafs
}
