package exercises

func longestSubsequence(arr []int, difference int) int {
	dp := map[int]int{}
	longest := 0
	for _, a := range arr {
		dp[a] = dp[a-difference] + 1
		if dp[a] > longest {
			longest = dp[a]
		}
	}
	return longest
}
