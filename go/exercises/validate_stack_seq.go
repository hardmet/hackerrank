package exercises

func validateStackSequences(pushed, popped []int) bool {
	var st []int
	j := 0
	for _, value := range pushed {
		st = append(st, value)
		for len(st) > 0 && st[len(st)-1] == popped[j] {
			st = st[:len(st)-1]
			j++
		}
	}
	return len(st) == 0
}
