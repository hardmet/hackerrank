package exercises

import "sort"

func eliminateMaximum(dist []int, speed []int) int {
	arrival := make([]float64, len(dist))
	for i := 0; i < len(dist); i++ {
		arrival[i] = float64(dist[i]) / float64(speed[i])
	}
	sort.Float64s(arrival)
	maximum := 0
	for i := 0; i < len(arrival); i++ {
		if arrival[i] <= float64(i) {
			break
		}
		maximum++
	}
	return maximum
}
