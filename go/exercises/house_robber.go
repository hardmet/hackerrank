package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func rob(nums []int) int {
	result, prev := 0, 0
	for _, val := range nums {
		tmp := math_utils.Max(prev+val, result)
		prev = result
		result = tmp
	}
	return result
}
