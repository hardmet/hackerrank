package exercises

func bestClosingTime(customers string) int {
	minPenalty, curPenalty, earliestHour := 0, 0, 0
	for i := 0; i < len(customers); i++ {
		if customers[i] == 'Y' {
			curPenalty--
		} else {
			curPenalty++
		}
		if curPenalty < minPenalty {
			earliestHour = i + 1
			minPenalty = curPenalty
		}
	}
	return earliestHour
}
