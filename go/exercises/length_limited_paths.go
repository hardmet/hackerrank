package exercises

import (
	"github.com/hardmet/hackerrank/go/int_union_find"
	"sort"
)

func distanceLimitedPathsExist(n int, edgeList [][]int, queries [][]int) []bool {
	for i := 0; i < len(queries); i++ {
		queries[i] = append(queries[i], i)
	}
	sort.Slice(queries, func(i, j int) bool {
		return queries[i][2] < queries[j][2]
	})
	sort.Slice(edgeList, func(i, j int) bool {
		return edgeList[i][2] < edgeList[j][2]
	})
	i := 0
	res := make([]bool, len(queries))
	uf := int_union_find.ArrayUFConstructor(n)
	for _, q := range queries {
		for i < len(edgeList) && edgeList[i][2] < q[2] {
			uf.Union(edgeList[i][0], edgeList[i][1])
			i++
		}

		if uf.Find(q[0]) == uf.Find(q[1]) {
			res[q[3]] = true
		}
	}
	return res
}
