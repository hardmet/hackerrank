package exercises

func change(amount int, coins []int) int {
	memo := make([][]int, len(coins))
	for i := 0; i < len(memo); i++ {
		memo[i] = make([]int, amount+1)
		for j := 0; j < len(memo[i]); j++ {
			memo[i][j] = -1
		}
	}

	var numberOfWays func(i, amount int) int
	numberOfWays = func(i, amount int) int {
		if amount == 0 {
			return 1
		}
		if i == len(coins) {
			return 0
		}
		if memo[i][amount] != -1 {
			return memo[i][amount]
		}
		if coins[i] > amount {
			memo[i][amount] = numberOfWays(i+1, amount)
			return memo[i][amount]
		}
		memo[i][amount] = numberOfWays(i, amount-coins[i]) + numberOfWays(i+1, amount)
		return memo[i][amount]
	}

	return numberOfWays(0, amount)
}
