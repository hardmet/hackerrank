package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

var d = []int{0, 1, 0, -1, 0}

func minimumEffortPath(heights [][]int) int {
	left, right := 0, 1_000_000
	result := 0
	for left <= right {
		mid := (left + right) / 2
		visited := make([][]bool, len(heights))
		for i := 0; i < len(visited); i++ {
			visited[i] = make([]bool, len(heights[i]))
		}
		if minEffortDFS(0, 0, visited, mid, heights) {
			right = mid - 1
			result = mid
		} else {
			left = mid + 1
		}
	}
	return result
}

func minEffortDFS(y, x int, visited [][]bool, threshold int, heights [][]int) bool {
	if y == len(heights)-1 && x == len(heights[0])-1 {
		return true
	}
	visited[y][x] = true
	for i := 0; i < 4; i++ {
		dy := y + d[i]
		dx := x + d[i+1]
		if dy < 0 || dy == len(heights) || dx < 0 || dx == len(heights[0]) || visited[dy][dx] {
			continue
		}
		if math_utils.Abs(heights[dy][dx]-heights[y][x]) <= threshold && minEffortDFS(dy, dx, visited, threshold, heights) {
			return true
		}
	}
	return false
}
