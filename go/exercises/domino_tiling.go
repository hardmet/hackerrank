package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func numTilings(n int) int {
	dp := make([]int, math_utils.Max(n+1, 4))
	dp[0] = 1
	dp[1] = 1
	dp[2] = 2
	dp[3] = 5
	for i := 4; i < len(dp); i++ {
		dp[i] = dp[i-1]*2%1_000_000_007 + dp[i-3]%1_000_000_007
	}
	return dp[n] % 1_000_000_007
}
