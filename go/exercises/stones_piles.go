package exercises

import (
	"container/heap"

	"github.com/hardmet/hackerrank/go/int_heap"
)

func minStoneSum(piles []int, k int) int {
	ih := int_heap.MaxIntHeap{IntHeap: piles}
	heap.Init(&ih)
	sum := 0

	for ; k > 0; k-- {
		ih.IntHeap[0] -= ih.IntHeap[0] / 2
		heap.Fix(&ih, 0)
	}

	for _, v := range piles {
		sum += v
	}

	return sum
}
