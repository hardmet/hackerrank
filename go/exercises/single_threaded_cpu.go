package exercises

import (
	"container/heap"
	"github.com/hardmet/hackerrank/go/math_utils"
	"sort"
)

type Item struct {
	startTime      int
	processingTime int
	index          int
}

type PriorityQueueItem []*Item

func (pq PriorityQueueItem) Len() int { return len(pq) }

func (pq PriorityQueueItem) Less(i, j int) bool {
	// We want Pop to give us the highest, not lowest, priority so we use greater than here.
	if pq[i].processingTime == pq[j].processingTime {
		return pq[i].index < pq[j].index
	} else {
		return pq[i].processingTime < pq[j].processingTime
	}
}

func (pq PriorityQueueItem) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *PriorityQueueItem) Push(x interface{}) {
	*pq = append(*pq, x.(*Item))
}

func (pq *PriorityQueueItem) Pop() interface{} {
	p := (*pq)[len(*pq)-1]
	(*pq)[len(*pq)-1] = nil // avoid memory leak
	*pq = (*pq)[:len(*pq)-1]
	return p
}

func getOrder(tasks [][]int) []int {
	res := make([]int, 0)
	pq := make(PriorityQueue, 0)
	cpuStack := make([]*Item, 0)

	for i, t := range tasks {
		cpuStack = append(cpuStack, &Item{t[0], t[1], i})
	}
	sort.Slice(cpuStack, func(i, j int) bool {
		return cpuStack[i].startTime < cpuStack[j].startTime
	})

	curTime := cpuStack[0].startTime
	nextTime := cpuStack[0].startTime
	for i := 0; i < len(cpuStack); {
		for ; i < len(cpuStack) && cpuStack[i].startTime <= curTime; i++ {
			heap.Push(&pq, cpuStack[i])
		}
		for len(pq) > 0 && i < len(cpuStack) && nextTime < cpuStack[i].startTime {
			cur := heap.Pop(&pq).(*Item)
			res = append(res, cur.index)
			nextTime += cur.processingTime
		}
		if i < len(cpuStack) {
			nextTime = math_utils.Max(nextTime, cpuStack[i].startTime)
		}
		curTime = nextTime
	}
	for len(pq) > 0 {
		res = append(res, heap.Pop(&pq).(*Item).index)
	}
	return res
}
