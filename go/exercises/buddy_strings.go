package exercises

func buddyStrings(s string, goal string) bool {
	if len(s) != len(goal) {
		return false
	}
	diff := -1
	letterCount := [26]int{}
	for i := 0; i < len(s); i++ {
		letterCount[s[i]-'a']++
		if s[i] != goal[i] {
			if diff == -1 {
				diff = i
			} else if diff == -2 {
				return false
			} else if s[diff] == goal[i] && goal[diff] == s[i] {
				diff = -2
			} else {
				return false
			}
		}
	}
	if diff == -1 {
		for _, count := range letterCount {
			if count > 1 {
				return true
			}
		}
	}
	return diff == -2
}
