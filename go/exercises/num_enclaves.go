package exercises

func numEnclaves(grid [][]int) int {
	result := 0
	var q [][2]int
	for i := 0; i < len(grid); i++ {
		//dfs
		q = append(q, [2]int{i, 0})
		q = append(q, [2]int{i, len(grid[0]) - 1})
	}
	for j := 0; j < len(grid[0]); j++ {
		//dfs
		if j != 0 {
			q = append(q, [2]int{0, j})
		}
		if j != len(grid[0]) {
			q = append(q, [2]int{len(grid) - 1, j})
		}
	}
	for len(q) > 0 {
		i := q[0][0]
		j := q[0][1]
		q = q[1:]
		if 0 <= i && i <= len(grid)-1 && 0 <= j && j <= len(grid[i])-1 && grid[i][j] == 1 {
			grid[i][j] = 0
			q = append(q, [2]int{i + 1, j})
			q = append(q, [2]int{i - 1, j})
			q = append(q, [2]int{i, j + 1})
			q = append(q, [2]int{i, j - 1})
		}
	}
	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[i]); j++ {
			if grid[i][j] == 1 {
				result++
			}
		}
	}
	return result
}
