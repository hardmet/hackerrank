package exercises

func isBipartite(edges [][]int) bool {
	n := len(edges)
	nodesColors := make([]int, n) // 0 - hasn't colorized, 1 - color A, 2 - color B
	for i := 0; i < n; i++ {
		if !isColorized(i, nodesColors) && !tryColorizeNodeAndAdjacentDFS(edges, nodesColors, i, 1) {
			return false
		}
	}
	return true
}

func isColorized(i int, nodesColors []int) bool {
	return nodesColors[i] != 0
}

func tryColorizeNodeAndAdjacentDFS(edges [][]int, nodesColors []int, node, color int) bool {
	if isColorized(node, nodesColors) {
		return nodesColors[node] == color
	}
	nodesColors[node] = color
	for _, adjacentNode := range edges[node] {
		if !tryColorizeNodeAndAdjacentDFS(edges, nodesColors, adjacentNode, -color) {
			return false
		}
	}
	return true
}
