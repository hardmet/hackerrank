package exercises

func longestZigZag(root *TreeNode) int {
	if root == nil {
		return 0
	}
	var maximum int
	maximum = longestZigZagR(root.Left, false, 0, maximum)
	return longestZigZagR(root.Right, true, 0, maximum)
}

func longestZigZagR(root *TreeNode, isRight bool, current, maximum int) int {
	if root == nil {
		if current > maximum {
			return current
		} else {
			return maximum
		}
	}
	if isRight {
		maximum = longestZigZagR(root.Left, false, current+1, maximum)
		return longestZigZagR(root.Right, true, 0, maximum)
	} else {
		maximum = longestZigZagR(root.Right, true, current+1, maximum)
		return longestZigZagR(root.Left, false, 0, maximum)
	}
}
