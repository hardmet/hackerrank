package exercises

func minimumRounds(tasks []int) int {
	frequencies := make(map[int]int, len(tasks))
	for i := range tasks {
		frequencies[tasks[i]]++
	}
	result := 0
	for _, count := range frequencies {
		if count == 1 {
			return -1
		}
		if count%3 == 0 {
			result += count / 3
		} else {
			result += count/3 + 1
		}
	}
	return result
}
