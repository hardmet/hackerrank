package exercises

func minOperationsToEmpty(nums []int) int {
	counter := map[int]int{}
	for _, num := range nums {
		counter[num]++
	}
	operations := 0
	for _, count := range counter {
		if count == 1 {
			return -1
		}
		operations += count / 3
		if count%3 != 0 {
			operations++
		}
	}
	return operations
}
