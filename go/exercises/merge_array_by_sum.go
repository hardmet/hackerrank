package exercises

func mergeArrays(nums1 [][]int, nums2 [][]int) [][]int {
	var huge, small [][]int
	if len(nums1) > len(nums2) {
		huge = nums1
		small = nums2
	} else {
		huge = nums2
		small = nums1
	}
	res := make([][]int, 0)
	var i, j int = 0, 0
	for i < len(small) && j < len(huge) {
		if small[i][0] == huge[j][0] {
			res = append(res, []int{small[i][0], small[i][1] + huge[j][1]})
			i++
			j++
		} else if small[i][0] < huge[j][0] {
			res = append(res, []int{small[i][0], small[i][1]})
			i++
		} else {
			res = append(res, []int{huge[j][0], huge[j][1]})
			j++
		}
	}
	for ; i < len(small); i++ {
		res = append(res, []int{small[i][0], small[i][1]})
	}
	for ; j < len(huge); j++ {
		res = append(res, []int{huge[j][0], huge[j][1]})
	}
	return res
}
