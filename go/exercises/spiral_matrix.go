package exercises

func spiralOrder(matrix [][]int) []int {
	var res []int
	if len(matrix) == 0 || len(matrix[0]) == 0 {
		return res
	}
	top := 0
	bottom := len(matrix) - 1
	left := 0
	right := len(matrix[0]) - 1

	fillers := []struct {
		begin     *int
		end       *int
		increment *int
		filler    func(*int, *int, *int, [][]int, *[]int)
	}{
		{&left, &right, &top, fillRight},
		{&top, &bottom, &right, fillBottom},
		{&right, &left, &bottom, fillLeft},
		{&bottom, &top, &left, fillUp},
	}
	for i := 0; !isEnd(&left, &right, &top, &bottom); i++ {
		next := i % 4
		fillers[next].filler(fillers[next].begin, fillers[next].end, fillers[next].increment, matrix, &res)
	}
	return res
}

func fillRight(l, r, t *int, matrix [][]int, res *[]int) {
	for i := *l; i <= *r; i++ {
		*res = append(*res, matrix[*t][i])
	}
	*t++
}

func fillBottom(t, b, r *int, matrix [][]int, res *[]int) {
	for i := *t; i <= *b; i++ {
		*res = append(*res, matrix[i][*r])
	}
	*r--
}

func fillLeft(r, l, b *int, matrix [][]int, res *[]int) {
	for i := *r; i >= *l; i-- {
		*res = append(*res, matrix[*b][i])
	}
	*b--
}

func fillUp(b, t, l *int, matrix [][]int, res *[]int) {
	for i := *b; i >= *t; i-- {
		*res = append(*res, matrix[i][*l])
	}
	*l++
}

func isEnd(l, r, t, b *int) bool {
	return *l > *r || *t > *b
}
