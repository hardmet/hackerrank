package exercises

func romanToInt(str string) int {
	romeDigits := map[byte]int{
		'I': 1,
		'V': 5,
		'X': 10,
		'L': 50,
		'C': 100,
		'D': 500,
		'M': 1000,
	}
	var sum, current, prev int
	prev = romeDigits[str[0]]
	for i := 1; i < len(str); i++ {
		current = romeDigits[str[i]]
		if current > prev {
			sum -= prev
		} else {
			sum += prev
		}
		prev = current
	}
	return sum + prev
}
