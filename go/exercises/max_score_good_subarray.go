package exercises

func maximumScore(nums []int, k int) int {
	n, l, r, maxScore, curMin := len(nums), k, k, nums[k], nums[k]
	for l > 0 || r < n-1 {
		tmpL, tmpR := 0, 0
		if l > 0 {
			tmpL = nums[l-1]
		}
		if r < n-1 {
			tmpR = nums[r+1]
		}
		if tmpL < tmpR {
			r++
			if nums[r] < curMin {
				curMin = nums[r]
			}
		} else {
			l--
			if nums[l] < curMin {
				curMin = nums[l]
			}
		}
		value := curMin * (r - l + 1)
		if value > maxScore {
			maxScore = value
		}
	}
	return maxScore
}
