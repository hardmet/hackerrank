package exercises

func eventualSafeNodes(graph [][]int) []int {
	adj := [][]int{}
	for i := 0; i < len(graph); i++ {
		list := []int{}
		for j := 0; j < len(graph[i]); j++ {
			list = append(list, graph[i][j])
		}
		adj = append(adj, list)
	}
	visited := make([]bool, len(graph))
	recst := make([]bool, len(graph))
	for i := 0; i < len(graph); i++ {
		if !visited[i] {
			dfsSafeNodes(adj, i, visited, recst)
		}
	}
	ans := []int{}
	for i, recsti := range recst {
		if !recsti {
			ans = append(ans, i)
		}
	}
	return ans
}

func dfsSafeNodes(adj [][]int, src int, visited []bool, recst []bool) bool {
	visited[src] = true
	recst[src] = true
	for _, x := range adj[src] {
		if (!visited[x] && dfsSafeNodes(adj, x, visited, recst)) || recst[x] {
			return true
		}
	}
	recst[src] = false
	return false
}
