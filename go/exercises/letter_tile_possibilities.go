package exercises

func numTilePossibilities(tiles string) int {

	var backtrack func(freqMap map[rune]int) int
	backtrack = func(freqMap map[rune]int) int {
		count := 0
		for ch, frequency := range freqMap {
			if frequency > 0 {
				freqMap[ch]--
				count += 1 + backtrack(freqMap)
				freqMap[ch]++
			}
		}
		return count
	}

	freqMap := map[rune]int{}
	for _, char := range tiles {
		freqMap[char]++
	}
	return backtrack(freqMap)
}
