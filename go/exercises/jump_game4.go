package exercises

func minJumps(arr []int) int {
	if len(arr) == 1 {
		return 0
	}
	indices := make(map[int][]int)
	for i, v := range arr {
		indices[v] = append(indices[v], i)
	}
	queue := []int{0}
	visited := map[int]interface{}{0: true}
	var steps int
	for len(queue) > 0 {
		size := len(queue)
		for j := 0; j < size; j++ {
			i := queue[0]
			queue = queue[1:]
			if i == len(arr)-1 {
				return steps
			}
			if _, isVisited := visited[i-1]; !isVisited && i-1 >= 0 {
				queue = append(queue, i-1)
				visited[i-1] = true
			}
			if _, isVisited := visited[i+1]; !isVisited && i+1 < len(arr) {
				queue = append(queue, i+1)
				visited[i+1] = true
			}
			for _, sameValue := range indices[arr[i]] {
				if _, contains := visited[sameValue]; !contains {
					queue = append(queue, sameValue)
					visited[sameValue] = true
				}
			}
			indices[arr[i]] = []int{}
		}
		steps++
	}
	return -1
}
