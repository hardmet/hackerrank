package exercises

func diagonalSum(mat [][]int) int {
	s := 0
	for i := 0; i < len(mat); i++ {
		s += mat[i][i] + mat[i][len(mat)-i-1]
	}
	if len(mat)%2 == 1 {
		return s - mat[len(mat)/2][len(mat)/2]
	} else {
		return s
	}
}
