package exercises

func addToArrayForm(num []int, k int) []int {
	current := k
	for i := len(num) - 1; i >= 0; i-- {
		current += num[i]
		num[i] = current % 10
		current /= 10
	}
	for current > 0 {
		num = append([]int{current % 10}, num...)
		current /= 10
	}
	return num
}
