package exercises

func minDepth(root *TreeNode) int {
	if root == nil {
		return 0
	}
	current, nextLevel := []*TreeNode{root}, []*TreeNode{}
	depth := 1
	for {
		for _, node := range current {
			if node.Left == nil && node.Right == nil {
				return depth
			}
			if node.Left != nil {
				nextLevel = append(nextLevel, node.Left)
			}
			if node.Right != nil {
				nextLevel = append(nextLevel, node.Right)
			}
		}
		current = nextLevel
		nextLevel = []*TreeNode{}
		depth++
	}
}
