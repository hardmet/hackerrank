package exercises

import (
	"github.com/hardmet/hackerrank/go/int_union_find"
)

func possibleBipartition(n int, dislikes [][]int) bool {
	adjacency := make(map[int][]int, n+1)
	for i := 1; i <= len(adjacency); i++ {
		adjacency[i] = []int{}
	}
	for _, dislike := range dislikes {
		adjacency[dislike[0]] = append(adjacency[dislike[0]], dislike[1])
		adjacency[dislike[1]] = append(adjacency[dislike[1]], dislike[0])
	}
	uf := int_union_find.ArrayUFConstructor(n + 1)
	for node := 1; node <= n; node++ {
		for j := range adjacency[node] {
			if uf.Find(node) == uf.Find(adjacency[node][j]) {
				return false
			}
			uf.Union(adjacency[node][0], adjacency[node][j])
		}
	}
	return true
}
