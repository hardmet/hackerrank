package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func mincostTickets(days, costs []int) int {
	last7 := [][]int{}
	last30 := [][]int{}
	cost := 0
	for _, d := range days {
		for len(last7) > 0 && last7[0][0]+7 <= d {
			last7 = last7[1:]
		}
		for len(last30) > 0 && last30[0][0]+30 <= d {
			last30 = last30[1:]
		}
		last7 = append(last7, []int{d, cost + costs[1]})
		last30 = append(last30, []int{d, cost + costs[2]})
		cost = math_utils.Min(cost+costs[0], math_utils.Min(last7[0][1], last30[0][1]))
	}
	return cost
}
