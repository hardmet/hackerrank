package exercises

func canCross(stones []int) bool {
	mark := map[int]int{}
	dp := [2001][2001]bool{}
	for i := 0; i < len(stones); i++ {
		mark[stones[i]] = i
	}
	dp[0][0] = true
	for i := 0; i < len(stones); i++ {
		for prevJump := 0; prevJump <= len(stones); prevJump++ {
			if dp[i][prevJump] {
				if value, contains := mark[stones[i]+prevJump]; contains {
					dp[value][prevJump] = true
				}
				if value, contains := mark[stones[i]+prevJump+1]; contains {
					dp[value][prevJump+1] = true
				}
				if value, contains := mark[stones[i]+prevJump-1]; contains {
					dp[value][prevJump-1] = true
				}
			}
		}
	}
	for i := 0; i <= len(stones); i++ {
		if dp[len(stones)-1][i] {
			return true
		}
	}
	return false
}
