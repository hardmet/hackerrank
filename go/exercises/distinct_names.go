package exercises

func distinctNames(ideas []string) int {
	initialGroup := [26]map[string]bool{}
	for i := 0; i < 26; i++ {
		initialGroup[i] = map[string]bool{}
	}
	for _, idea := range ideas {
		initialGroup[idea[0]-'a'][idea[1:]] = true
	}
	var answer int
	for i := 0; i < 25; i++ {
		for j := i + 1; j < 26; j++ {
			var numOfMutual int
			for idea := range initialGroup[i] {
				if _, contains := initialGroup[j][idea]; contains {
					numOfMutual++
				}
			}
			answer += 2 * (len(initialGroup[i]) - numOfMutual) * (len(initialGroup[j]) - numOfMutual)
		}
	}
	return answer
}
