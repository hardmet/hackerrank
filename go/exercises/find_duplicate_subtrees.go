package exercises

import "fmt"

func findDuplicateSubtrees(root *TreeNode) []*TreeNode {
	res := []*TreeNode{}
	findDuplicates(&res, root, &map[string]int{})
	return res
}

func findDuplicates(res *[]*TreeNode, root *TreeNode, cache *map[string]int) string {
	if root == nil {
		return ""
	}
	left := findDuplicates(res, root.Left, cache)
	right := findDuplicates(res, root.Right, cache)
	stringFormed := fmt.Sprintf("%d$%s$%s", root.Val, left, right)
	if (*cache)[stringFormed] == 1 {
		*res = append(*res, root)
	}
	(*cache)[stringFormed]++
	return stringFormed
}
