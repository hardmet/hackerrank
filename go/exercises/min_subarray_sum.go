package exercises

import (
	"math"

	"github.com/hardmet/hackerrank/go/math_utils"
)

func minSubArrayLen(target int, nums []int) int {
	if nums == nil || len(nums) == 0 {
		return 0
	}
	i, sum, min := 0, 0, math.MaxInt

	for j := 0; j < len(nums); j++ {
		sum += nums[j]
		for sum >= target {
			min = math_utils.Min(min, j+1-i)
			sum -= nums[i]
			i++
		}
	}
	if min == math.MaxInt {
		return 0
	}
	return min
}
