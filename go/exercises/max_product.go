package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func maxProduct(root *TreeNode) int {
	sum := 0
	maxProd := 0
	dfsMaxProduct(root, &sum)
	checkMax(root, &sum, &maxProd)
	return maxProd % (1_000_000_007)
}

func dfsMaxProduct(root *TreeNode, sum *int) {
	if root != nil {
		*sum = *(sum) + root.Val
		dfsMaxProduct(root.Left, sum)
		dfsMaxProduct(root.Right, sum)
	}
}

func checkMax(root *TreeNode, sum, maxPr *int) int {
	if root == nil {
		return 0
	}
	l := checkMax(root.Left, sum, maxPr)
	r := checkMax(root.Right, sum, maxPr)
	*maxPr = math_utils.Max(*(maxPr), (l+r+root.Val)*(*(sum)-l-r-root.Val))
	return l + r + root.Val
}
