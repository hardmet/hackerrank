package exercises

func maxConsecutiveAnswers(answerKey string, k int) int {
	var count [26]int
	res, max := 0, 0
	for i, answerSymbol := range answerKey {
		count[answerSymbol-'A']++
		if count[answerSymbol-'A'] > max {
			max = count[answerSymbol-'A']
		}
		if res-max < k {
			res++
		} else {
			count[answerKey[i-res]-'A']--
		}

	}
	return res
}
