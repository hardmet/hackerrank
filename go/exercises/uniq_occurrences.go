package exercises

func uniqueOccurrences(arr []int) bool {
	occurrences := map[int]int{}
	for _, n := range arr {
		_, contains := occurrences[n]
		if !contains {
			occurrences[n] = 0
		}
	}
	for _, n := range arr {
		occurrences[n]++
	}
	counts := map[int]int{}
	for _, count := range occurrences {
		_, contains := counts[count]
		if contains {
			return false
		}
		counts[count] = 1
	}
	return true
}
