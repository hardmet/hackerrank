package exercises

import "math"

func minimumTime(time []int, totalTrips int) int64 {
	var low int64 = math.MaxInt64
	var high int64
	var minimum int64 = math.MaxInt64
	for _, t := range time {
		if int64(t) < low {
			low = int64(t)
		}
	}
	minimum = low
	high = int64(totalTrips) * minimum
	var mid int64
	for low < high {
		mid = low + (high-low)/2
		if blackbox(mid, totalTrips, time) {
			high = mid
		} else {
			low = mid + 1
		}
	}
	return low
}

func blackbox(isValidTime int64, total int, time []int) bool {
	var trips int64 = 0
	for _, t := range time {
		trips += isValidTime / int64(t)
	}
	return trips >= int64(total)
}
