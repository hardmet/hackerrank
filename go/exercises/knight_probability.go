package exercises

func knightProbability(n int, k int, row int, column int) float64 {
	dx := []int{2, 2, -2, -2, 1, 1, -1, -1}
	dy := []int{1, -1, 1, -1, -2, 2, 2, -2}
	dp := make([][][]float64, n)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([][]float64, n)
		for j := 0; j < len(dp[i]); j++ {
			dp[i][j] = make([]float64, k+1)
		}
	}
	var bfs func(r, c, step int) float64
	bfs = func(r, c, step int) float64 {
		if isValidCell(r, c, n) {
			if step == 0 {
				return 1
			}
			if dp[r][c][step] != 0 {
				return dp[r][c][step]
			}
			var rate float64
			for i := 0; i < len(dx); i++ {
				rate += 0.125 * bfs(dx[i]+r, dy[i]+c, step-1)
			}
			dp[r][c][step] = rate
			return rate
		}
		return 0
	}
	return bfs(row, column, k)
}

func isValidCell(x, y, n int) bool {
	return 0 <= x && x < n && 0 <= y && y < n
}
