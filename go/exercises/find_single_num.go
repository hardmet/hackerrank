package exercises

func singleNumber(nums []int) int {
	res := make([]int, 32)
	sign := 0
	for _, n := range nums {
		if n < 0 {
			n *= -1
			sign++
		}
		for i := 0; n > 0; i, n = i+1, n/2 {
			res[i] += n % 2
		}
	}
	ans := 0
	for i := len(res) - 1; i >= 0; i-- {
		ans *= 2
		ans += res[i] % 3
	}
	if sign%3 == 1 {
		ans *= -1
	}
	return ans
}
