package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"math"
)

func maxDistance(grid [][]int) int {
	m, n := len(grid), len(grid[0])
	mn := m + n
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if grid[i][j] == 1 {
				continue
			}
			var top, left int = mn, mn
			if i-1 >= 0 {
				top = grid[i-1][j]
			}
			if j-1 >= 0 {
				left = grid[i][j-1]
			}
			grid[i][j] = math_utils.Min(top, left) + 1
		}
	}
	for i := m - 1; i >= 0; i-- {
		for j := n - 1; j >= 0; j-- {
			if grid[i][j] == 1 {
				continue
			}
			bottom, right := mn, mn
			if i+1 < m {
				bottom = grid[i+1][j]
			}
			if j+1 < n {
				right = grid[i][j+1]
			}
			grid[i][j] = math_utils.Min(grid[i][j], math_utils.Min(bottom, right)+1)
		}
	}
	count := math.MinInt
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			count = math_utils.Max(count, grid[i][j])
		}
	}
	if count-1 == mn+1 || count-1 == 0 {
		return -1
	} else {
		return count - 1
	}
}
