package exercises

func countRoutes(locations []int, start int, finish int, fuel int) int {
	const MOD = 1000000007
	n := len(locations)

	dp := make([][]int, n)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, fuel+1)
	}
	for f := 0; f <= fuel; f++ {
		dp[finish][f] = 1
	}

	var absDist int
	for f := 0; f <= fuel; f++ {
		for city := 0; city < n; city++ {
			for nextCity := 0; nextCity < n; nextCity++ {
				absDist = locations[nextCity] - locations[city]
				if absDist < 0 {
					absDist = -1 * absDist
				}
				if nextCity != city && f >= absDist {
					dp[city][f] = (dp[city][f] + dp[nextCity][f-absDist]) % MOD
				}
			}
		}
	}

	return dp[start][fuel]
}
