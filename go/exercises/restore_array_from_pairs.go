package exercises

import "math"

func restoreArray(adjacentPairs [][]int) []int {
	graph := map[int][]int{}
	for _, edge := range adjacentPairs {
		if _, contains := graph[edge[0]]; contains {
			graph[edge[0]] = append(graph[edge[0]], edge[1])
		} else {
			graph[edge[0]] = []int{edge[1]}
		}
		if _, contains := graph[edge[1]]; contains {
			graph[edge[1]] = append(graph[edge[1]], edge[0])
		} else {
			graph[edge[1]] = []int{edge[0]}
		}
	}
	root := 0
	for node, neighbors := range graph {
		if len(neighbors) == 1 {
			root = node
			break
		}
	}
	nums := make([]int, len(graph))
	nums[0] = root
	current := root
	prev := math.MaxInt
	for i := 1; i < len(graph); {
		for _, neighbor := range graph[current] {
			if neighbor != prev {
				nums[i] = neighbor
				i++
				prev = current
				current = neighbor
				break
			}
		}
	}
	return nums
}
