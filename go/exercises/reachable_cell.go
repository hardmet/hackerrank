package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func isReachableAtTime(sx int, sy int, fx int, fy int, t int) bool {
	if sx == fx && sy == fy {
		return t == 0 || t > 1
	}
	dx := math_utils.Abs(sx - fx)
	dy := math_utils.Abs(sy - fy)
	if dx > dy {
		return dy+dx-dy <= t
	} else {
		return dx+dy-dx <= t
	}
}
