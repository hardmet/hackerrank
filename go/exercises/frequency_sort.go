package exercises

import "sort"

func frequencySort(s string) string {
	counts := countDict(s)
	bs := []byte(s)
	sort.SliceStable(bs, func(i, j int) bool {
		return counts[bs[i]] > counts[bs[j]] || (counts[bs[i]] == counts[bs[j]] && bs[i] < bs[j])
	})
	return string(bs)
}

func countDict(word string) [128]int {
	counts := [128]int{}
	for _, w := range []byte(word) {
		counts[w]++
	}
	return counts
}
