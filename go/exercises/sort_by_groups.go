package exercises

func sortItems(n int, m int, group []int, beforeItems [][]int) []int {
	groups, inDegrees := make([][]int, n+m), make([]int, n+m)
	for i, groupId := range group {
		if groupId > -1 {
			groupId += n
			groups[groupId] = append(groups[groupId], i)
			inDegrees[i]++
		}
	}
	for i, ancestors := range beforeItems {
		groupI := group[i]
		if groupI == -1 {
			groupI = i
		} else {
			groupI += n
		}
		for _, ancestor := range ancestors {
			groupAncestor := group[ancestor]
			if groupAncestor == -1 {
				groupAncestor = ancestor
			} else {
				groupAncestor += n
			}
			if groupI == groupAncestor {
				groups[ancestor] = append(groups[ancestor], i)
				inDegrees[i]++
			} else {
				groups[groupAncestor] = append(groups[groupAncestor], groupI)
				inDegrees[groupI]++
			}
		}
	}
	res := []int{}
	for i, d := range inDegrees {
		if d == 0 {
			sortItemsDFS(i, n, &res, &inDegrees, &groups)
		}
	}
	if len(res) != n {
		return nil
	}
	return res
}

func sortItemsDFS(i, n int, res, inDegrees *[]int, groups *[][]int) {
	if i < n {
		*res = append(*res, i)
	}
	(*inDegrees)[i] = -1
	for _, ch := range (*groups)[i] {
		if (*inDegrees)[ch]--; (*inDegrees)[ch] == 0 {
			sortItemsDFS(ch, n, res, inDegrees, groups)
		}
	}
}
