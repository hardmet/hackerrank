package exercises

func insert(intervals [][]int, newInterval []int) [][]int {
	var res [][]int
	var startIntersection, endIntersection, startNewInt, endNewInt int
	startNewInt = newInterval[0]
	endNewInt = newInterval[1]
	startIntersection = startNewInt
	endIntersection = endNewInt
	for _, interval := range intervals {
		start, end := interval[0], interval[1]
		switch {
		case end < startNewInt:
			res = append(res, interval)
		case start > endNewInt:
			if startIntersection != -1 {
				res = append(res, []int{startIntersection, endIntersection})
				startIntersection = -1
			}
			res = append(res, interval)
		case start < startNewInt:
			startIntersection = start
			if end > endNewInt {
				endIntersection = end
			}
		case end > endNewInt:
			endIntersection = end
		}
	}
	if startIntersection != -1 {
		res = append(res, []int{startIntersection, endIntersection})
	}
	return res
}
