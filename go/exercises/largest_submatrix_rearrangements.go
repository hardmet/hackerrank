package exercises

import "github.com/hardmet/hackerrank/go/tuples"

func largestSubmatrix(matrix [][]int) int {
	m, n := len(matrix), len(matrix[0])
	prevHeights := []tuples.Pair{}
	largest := 0
	for row := 0; row < m; row++ {
		heights := []tuples.Pair{}
		seen := make([]bool, n)
		for _, pair := range prevHeights {
			height := pair.Fst
			col := pair.Snd
			if matrix[row][col] == 1 {
				heights = append(heights, tuples.Pair{Fst: height + 1, Snd: col})
				seen[col] = true
			}
		}
		for col := 0; col < n; col++ {
			if !seen[col] && matrix[row][col] == 1 {
				heights = append(heights, tuples.Pair{Fst: 1, Snd: col})
			}
		}
		for i := 0; i < len(heights); i++ {
			if heights[i].Fst*(i+1) > largest {
				largest = heights[i].Fst * (i + 1)
			}
		}
		prevHeights = heights
	}
	return largest
}
