package exercises

func knightDialer(n int) int {
	if n == 1 {
		return 10
	}
	a, b, c, d := 4, 2, 2, 1
	var prevA, prevB, prevC, prevD int
	const MOD = 1e9 + 7
	for i := 0; i < n-1; i++ {
		prevA = a
		prevB = b
		prevC = c
		prevD = d
		a = ((2*prevB)%MOD + (2*prevC)%MOD) % MOD
		b = prevA
		c = (prevA + (2*prevD)%MOD) % MOD
		d = prevC
	}
	count := (a + b) % MOD
	count = (count + c) % MOD
	return (count + d) % MOD
}
