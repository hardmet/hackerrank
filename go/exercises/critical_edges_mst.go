package exercises

import (
	"math"
	"sort"
)

type UnionFind []int

func (uf *UnionFind) find(x int) int {
	if x != (*uf)[x] {
		(*uf)[x] = uf.find((*uf)[x])
	}
	return (*uf)[x]
}

func (uf *UnionFind) union(x, y int) {
	px := uf.find(x)
	py := uf.find(y)
	if px != py {
		(*uf)[py] = px
	}
}

func ConstructorUF(n int) UnionFind {
	parents := make([]int, n)
	for i := 0; i < n; i++ {
		parents[i] = i
	}
	return parents
}

func findCriticalAndPseudoCriticalEdges(n int, edges [][]int) [][]int {
	for i := 0; i < len(edges); i++ {
		edges[i] = append(edges[i], i)
	}
	sort.Slice(edges, func(i, j int) bool {
		if edges[i][2] == edges[j][2] {
			if edges[i][1] == edges[j][1] {
				return edges[i][0] < edges[j][0]
			} else {
				return edges[i][1] < edges[j][1]
			}
		}
		return edges[i][2] < edges[j][2]
	})

	mstwt := findMST(n, edges, -1, -1)

	ce := []int{}
	pce := []int{}
	for i := 0; i < len(edges); i++ {
		if mstwt < findMST(n, edges, i, -1) {
			ce = append(ce, edges[i][3])
		} else if mstwt == findMST(n, edges, -1, i) {
			pce = append(pce, edges[i][3])
		}
	}
	return [][]int{ce, pce}
}

func findMST(n int, edges [][]int, block, e int) int {
	uf := ConstructorUF(n)
	weigh := 0
	if e != -1 {
		weigh += edges[e][2]
		uf.union(edges[e][0], edges[e][1])
	}

	for i := 0; i < len(edges); i++ {
		if i != block && uf.find(edges[i][0]) != uf.find(edges[i][1]) {
			uf.union(edges[i][0], edges[i][1])
			weigh += edges[i][2]
		}
	}
	for i := 0; i < n; i++ {
		if uf.find(i) != uf.find(0) {
			return math.MaxInt
		}
	}
	return weigh
}
