package exercises

func profitableSchemes(n, minProfit int, group, profit []int) int {
	dp := make([][]int, minProfit+1)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, n+1)
	}
	dp[0][0] = 1
	for k := 0; k < len(group); k++ {
		g, p := group[k], profit[k]
		for i := minProfit; i >= 0; i-- {
			for j := n - g; j >= 0; j-- {
				np := i + p
				if minProfit < np {
					np = minProfit
				}
				dp[np][j+g] += dp[i][j] & 1_000_000_007
				dp[np][j+g] %= 1_000_000_007
			}
		}
	}
	sum := 0
	for i := 0; i <= n; i++ {
		sum += dp[minProfit][i]
		sum %= 1_000_000_007
	}
	return sum
}
