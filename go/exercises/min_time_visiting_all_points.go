package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func minTimeToVisitAllPoints(points [][]int) int {
	time := 0
	for i := 0; i < len(points)-1; i++ {
		curX, curY := points[i][0], points[i][1]
		targetX, targetY := points[i+1][0], points[i+1][1]
		time += math_utils.Max(math_utils.Abs(targetX-curX), math_utils.Abs(targetY-curY))
	}
	return time
}
