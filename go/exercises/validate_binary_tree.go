package exercises

func validateBinaryTreeNodes(n int, leftChild []int, rightChild []int) bool {
	root := findBTRoot(n, leftChild, rightChild)
	if root == -1 {
		return false
	}
	visited := map[int]struct{}{}
	stack := []int{}
	visited[root] = struct{}{}
	stack = append(stack, root)
	for len(stack) > 0 {
		node := stack[len(stack)-1]
		stack = stack[:len(stack)-1]
		lChild, rChild := leftChild[node], rightChild[node]
		checkedLeft := checkNode(lChild, &visited, &stack)
		if !checkedLeft {
			return false
		}
		checkedRight := checkNode(rChild, &visited, &stack)
		if !checkedRight {
			return false
		}
	}
	return len(visited) == n
}

func checkNode(node int, visited *map[int]struct{}, stack *[]int) bool {
	if node != -1 {
		if _, contains := (*visited)[node]; contains {
			return false
		}
		*stack = append(*stack, node)
		(*visited)[node] = struct{}{}
	}
	return true
}

func findBTRoot(n int, left, right []int) int {
	children := map[int]struct{}{}
	for _, node := range left {
		children[node] = struct{}{}
	}
	for _, node := range right {
		children[node] = struct{}{}
	}
	for i := 0; i < n; i++ {
		if _, contains := children[i]; !contains {
			return i
		}
	}
	return -1
}
