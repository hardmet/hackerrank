package exercises

func findDiagonalOrder(nums [][]int) []int {
	rowsQueue := []int{0}
	columnsQueue := []int{0}
	var r, c int
	res := []int{}
	for len(rowsQueue) > 0 {

		r = rowsQueue[0]
		c = columnsQueue[0]
		rowsQueue = rowsQueue[1:]
		columnsQueue = columnsQueue[1:]

		res = append(res, nums[r][c])
		if c == 0 && r+1 < len(nums) {
			rowsQueue = append(rowsQueue, r+1)
			columnsQueue = append(columnsQueue, c)
		}
		if c+1 < len(nums[r]) {
			rowsQueue = append(rowsQueue, r)
			columnsQueue = append(columnsQueue, c+1)
		}
	}

	return res
}
