package exercises

func canJump(nums []int) bool {
	if nums[0] == 0 {
		return len(nums) == 1
	}
	maxJump := 0
	for i := 0; maxJump >= i && maxJump < len(nums)-1; i++ {
		if i+nums[i] > maxJump {
			maxJump = i + nums[i]
		}
	}
	return maxJump >= len(nums)-1
}
