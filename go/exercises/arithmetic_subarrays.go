package exercises

import "sort"

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	isArithmetics := []bool{}
	for i := 0; i < len(l); i++ {
		arr := make([]int, r[i]-l[i]+1)
		for j := 0; j < len(arr); j++ {
			arr[j] = nums[l[i]+j]
		}
		isArithmetics = append(isArithmetics, isArithmetic(arr))
	}
	return isArithmetics
}

func isArithmetic(arr []int) bool {
	sort.Ints(arr)
	diff := arr[1] - arr[0]
	for i := 2; i < len(arr); i++ {
		if arr[i]-arr[i-1] != diff {
			return false
		}
	}
	return true
}
