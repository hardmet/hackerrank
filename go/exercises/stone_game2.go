package exercises

import "github.com/hardmet/hackerrank/go/math_utils"

func stoneGameII(piles []int) int {
	presum := append([]int{}, piles...)
	for i := len(presum) - 2; i >= 0; i-- {
		presum[i] += presum[i+1]
	}
	memo := make([][]int, len(piles))
	for i := 0; i < len(memo); i++ {
		memo[i] = make([]int, len(piles))
	}
	return stoneGameIIDFS(presum, 1, 0, memo)
}

func stoneGameIIDFS(presum []int, m, p int, memo [][]int) int {
	if p+2*m >= len(presum) {
		return presum[p]
	}

	if memo[p][m] > 0 {
		return memo[p][m]
	}
	res, take := 0, 0
	for i := 1; i <= 2*m; i++ {
		take = presum[p] - presum[p+i]
		tmp := take + presum[p+i] - stoneGameIIDFS(presum, math_utils.Max(i, m), p+i, memo)
		if tmp > res {
			res = tmp
		}
	}
	memo[p][m] = res
	return res
}
