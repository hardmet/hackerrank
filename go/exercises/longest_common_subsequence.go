package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
)

func longestCommonSubsequence(t1, t2 string) int {
	dp := make([][]int, len(t2)+1)
	for i := range dp {
		dp[i] = make([]int, len(t1)+1)
	}
	for i := 1; i < len(t2)+1; i++ {
		for j := 1; j < len(t1)+1; j++ {
			if t2[i-1] == t1[j-1] {
				dp[i][j] = dp[i-1][j-1] + 1
			} else {
				dp[i][j] = math_utils.Max(dp[i-1][j], dp[i][j-1])
			}
		}
	}
	return dp[len(t2)][len(t1)]
}
