package exercises

import "sort"

type MaxBit []int

func MaxBitConstructor(n int) *MaxBit {
	var nums MaxBit = make([]int, n+1)
	return &nums
}

func (mb *MaxBit) get(id int) int {
	ans := 0
	for ; id > 0; id -= id & (-id) {
		if (*mb)[id] > ans {
			ans = (*mb)[id]
		}
	}
	return ans
}

func (mb *MaxBit) update(id, val int) {
	for ; id < len(*mb); id += id & (-id) {
		if val > (*mb)[id] {
			(*mb)[id] = val
		}
	}
}

func longestObstacleCourseAtEachPosition(nums []int) []int {
	var sorted []int
	contains := map[int]struct{}{}
	for _, num := range nums {
		if _, ok := contains[num]; !ok {
			contains[num] = struct{}{}
			sorted = append(sorted, num)
		}
	}
	sort.Ints(sorted)
	for i := range nums {
		nums[i] = sort.Search(len(sorted), func(j int) bool {
			return sorted[j] >= nums[i]
		}) + 1
	}

	nUnique := len(sorted)
	bit := MaxBitConstructor(nUnique)
	for i, num := range nums {
		nums[i] = bit.get(num) + 1
		bit.update(num, nums[i])
	}
	return nums
}
