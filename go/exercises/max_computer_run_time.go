package exercises

func maxRunTime(n int, batteries []int) int64 {
	var sum, mid, n64 int64
	n64 = int64(n)
	for _, b := range batteries {
		sum += int64(b)
	}
	var l, r int64 = 1, sum / n64
	for l < r {
		mid = (l + r + 1) / 2
		if isPossibleSet(batteries, mid, n64) {
			l = mid
		} else {
			r = mid - 1
		}
	}
	return l
}

func isPossibleSet(bs []int, mid, n int64) bool {
	var total int64 = 0
	for _, b := range bs {
		if int64(b) < mid {
			total += int64(b)
		} else {
			total += mid
		}
	}
	return total >= n*mid
}
