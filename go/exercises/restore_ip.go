package exercises

import (
	"github.com/hardmet/hackerrank/go/math_utils"
	"strings"
)

func restoreIpAddresses(s string) []string {
	isValid := func(start, length int) bool {
		return length == 1 || (s[start] != '0' && (length < 3 || s[start:start+length] <= "255"))
	}
	var ans []string
	for len1 := math_utils.Max(1, len(s)-9); len1 <= 3 && len1 <= len(s)-3; len1++ {
		if !isValid(0, len1) {
			continue
		}
		for len2 := math_utils.Max(1, len(s)-len1-6); len2 <= 3 && len2 <= len(s)-len1-2; len2++ {
			if !isValid(len1, len2) {
				continue
			}
			for len3 := math_utils.Max(1, len(s)-len1-len2-3); len3 <= 3 && len3 <= len(s)-len1-len2-1; len3++ {
				if isValid(len1+len2, len3) &&
					isValid(len1+len2+len3, len(s)-len1-len2-len3) {
					ans = append(ans,
						strings.Join(
							[]string{
								s[0:len1],
								s[len1 : len1+len2],
								s[len1+len2 : len1+len2+len3],
								s[len1+len2+len3:],
							}, "."),
					)
				}
			}
		}
	}
	return ans
}
