package exercises

func ReverseWords(s string) string {
	bs := []byte(s)
	trim(&bs)
	Reverse(&bs)
	space := byte(' ')
	l, r := -1, -1
	for i, b := range bs {
		if b == space && l != -1 {
			r = i - 1
			ReverseLR(&bs, l, r)
			l, r = -1, -1
		}
		if b != space && l == -1 {
			l = i
		}
	}
	if l != -1 {
		ReverseLR(&bs, l, len(bs)-1)
	}
	return string(bs)
}

func trim(bs *[]byte) {
	space := byte(' ')
	firstSpace := -1
	for i := 0; i < len(*bs); i++ {
		b := (*bs)[i]
		if b == space && firstSpace == -1 {
			firstSpace = i
			continue
		}
		if firstSpace != -1 && b != space {
			*bs = append((*bs)[:firstSpace+1], (*bs)[i:]...)
			i = i - (i - firstSpace) + 1
			firstSpace = -1
		}
	}
	if firstSpace != -1 {
		*bs = (*bs)[:firstSpace]
	}
	if (*bs)[0] == space {
		*bs = (*bs)[1:]
	}
}

func Reverse(bs *[]byte) {
	ReverseLR(bs, 0, len(*bs)-1)
}

func ReverseLR(bs *[]byte, l, r int) {
	for i := (r-l+1)/2 - 1; i >= 0; i-- {
		t := (*bs)[l+i]
		(*bs)[l+i] = (*bs)[r-i]
		(*bs)[r-i] = t
	}
}
