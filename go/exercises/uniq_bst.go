package exercises

func generateTrees(n int) []*TreeNode {
	var genTrees func(start, end int) []*TreeNode
	genTrees = func(start, end int) []*TreeNode {
		trees := []*TreeNode{}
		if start > end {
			return append(trees, nil)
		}
		for i := start; i <= end; i++ {
			lTrees := genTrees(start, i-1)
			rTrees := genTrees(i+1, end)
			for _, l := range lTrees {
				for _, r := range rTrees {
					current := TreeNode{Val: i, Left: l, Right: r}
					trees = append(trees, &current)
				}
			}
		}
		return trees
	}
	if n > 0 {
		return genTrees(1, n)
	}
	return []*TreeNode{}
}
