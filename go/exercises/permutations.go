package exercises

func permute(nums []int) [][]int {
	res := [][]int{}
	if len(nums) == 1 {
		return append(res, []int{nums[0]})
	}
	for i, ni := range nums {
		remainNums := make([]int, len(nums)-1)
		c := 0
		for j, nj := range nums {
			if j != i {
				remainNums[c] = nj
				c++
			}
		}
		perms := permute(remainNums)
		for i := 0; i < len(perms); i++ {
			perms[i] = append(perms[i], ni)
		}
		res = append(res, perms...)
	}
	return res
}
