package exercises

import "sort"

func maxSatisfaction(satisfaction []int) int {
	sort.Ints(satisfaction)
	n, presum, res := len(satisfaction), 0, 0
	for i := n - 1; i >= 0; i-- {
		presum += satisfaction[i]
		if presum < 0 {
			break
		}
		res += presum
	}
	return res
}
