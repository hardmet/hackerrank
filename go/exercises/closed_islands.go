package exercises

func closedIsland(grid [][]int) int {
	rows, cols := len(grid), len(grid[0])
	count := 0
	for i := 0; i < rows; i++ {
		for j := 0; j < cols; j++ {
			if grid[i][j] == 0 && islandsDFS(grid, i, j) {
				count++
			}
		}
	}
	return count
}

func islandsDFS(grid [][]int, i, j int) bool {
	rows, cols := len(grid), len(grid[0])
	if i < 0 || j < 0 || i >= rows || j >= cols {
		return false
	}
	if grid[i][j] == 1 {
		return true
	}
	grid[i][j] = 1
	left := islandsDFS(grid, i, j-1)
	right := islandsDFS(grid, i, j+1)
	up := islandsDFS(grid, i-1, j)
	down := islandsDFS(grid, i+1, j)
	return left && right && up && down
}
