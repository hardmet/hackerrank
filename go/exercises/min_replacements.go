package exercises

func minimumReplacement(nums []int) int64 {
	var replacements int64
	for i := len(nums) - 2; i >= 0; i-- {
		if nums[i] > nums[i+1] {
			elements := int64(nums[i]+nums[i+1]-1) / int64(nums[i+1])
			replacements += elements - 1
			nums[i] = nums[i] / int(elements)
		}
	}
	return replacements
}
