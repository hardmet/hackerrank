package exercises

func allPathsSourceTarget(graph [][]int) [][]int {
	res := make([][]int, 0)
	var dfs func(path []int, i int)
	dfs = func(path []int, i int) {
		path = append(path, i)
		if i == len(graph)-1 {
			res = append(res, append([]int{}, path...))
		}
		for _, j := range graph[i] {
			dfs(path, j)
		}
	}
	dfs([]int{}, 0)
	return res
}
