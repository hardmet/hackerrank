package exercises

func sumOfDistancesInTree(n int, edges [][]int) []int {
	res := make([]int, n)
	count := make([]int, n)
	var tree []map[int]bool
	for i := 0; i < n; i++ {
		tree = append(tree, map[int]bool{})
	}
	for _, e := range edges {
		tree[e[0]][e[1]] = true
		tree[e[1]][e[0]] = true
	}
	countDFS(tree, 0, -1, &res, &count)
	sumDFS(tree, 0, -1, &res, &count)
	return res
}

func countDFS(tree []map[int]bool, root, pre int, res, count *[]int) {
	for node := range tree[root] {
		if node == pre {
			continue
		}
		countDFS(tree, node, root, res, count)
		(*count)[root] += (*count)[node]
		(*res)[root] += (*res)[node] + (*count)[node]
	}
	(*count)[root]++
}

func sumDFS(tree []map[int]bool, root, pre int, res, count *[]int) {
	for node := range tree[root] {
		if node == pre {
			continue
		}
		(*res)[node] = (*res)[root] - (*count)[node] + len(*count) - (*count)[node]
		sumDFS(tree, node, root, res, count)
	}
}
