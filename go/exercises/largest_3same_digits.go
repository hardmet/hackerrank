package exercises

import "fmt"

func largestGoodInteger(num string) string {
	var prev byte = num[0]
	count := 1
	largest := ""
	for i := 1; i < len(num); i++ {
		if num[i] == prev {
			if count == 2 {
				good := fmt.Sprintf("%c%c%c", prev, prev, prev)
				if good > largest {
					largest = good
				}
				count = 1
			} else {
				count++
			}
		} else {
			count = 1
			prev = num[i]
		}
	}
	return largest
}
