package custommap

const defaultSize = 1000

type ListNodeWithKey struct {
	Key  int
	Val  int
	Next *ListNodeWithKey
}

type MyHashMap struct {
	size   int
	chains []*ListNodeWithKey
}

func Constructor() MyHashMap {
	return MyHashMap{
		size:   defaultSize,
		chains: make([]*ListNodeWithKey, defaultSize),
	}
}

func (m *MyHashMap) Put(key int, value int) {
	index := m.hashCode(key)
	if m.chains[index] == nil {
		m.chains[index] = &ListNodeWithKey{
			Key:  key,
			Val:  value,
			Next: nil,
		}
	} else {
		head := m.chains[index]
		found := false
		for ; head != nil; head = head.Next {
			if head.Key == key {
				head.Val = value
				found = true
				break
			}
		}
		if !found {
			newNode := &ListNodeWithKey{
				Key:  key,
				Val:  value,
				Next: m.chains[index],
			}
			m.chains[index] = newNode
		}
	}
}

func (m *MyHashMap) Get(key int) int {
	head := m.chains[m.hashCode(key)]
	for ; head != nil; head = head.Next {
		if head.Key == key {
			return head.Val
		}
	}
	return -1
}

func (m *MyHashMap) Remove(key int) {
	index := m.hashCode(key)
	head := m.chains[index]
	if head != nil {
		if head.Key == key {
			m.chains[index] = m.chains[index].Next
		} else {
			prev := head
			head = head.Next
			for ; head != nil; head = head.Next {
				if head.Key == key {
					prev.Next = head.Next
					break
				}
				prev = head
			}
		}
	}
}

func (m *MyHashMap) hashCode(key int) int {
	return key % m.size
}
